from datacirco.modules_data.data import DataCirco, GetDataCirco
from datacirco.export_to_json.population import export_population
from datacirco.export_to_json.environnement import export_environnement
from datacirco.export_to_json.elections import export_elections
from datacirco.export_to_json.entreprises import export_entreprises
from datacirco.export_to_json.emploi import export_emploi
from datacirco.export_to_json.logement import export_logement
from datacirco.export_to_json.sante import export_sante
from datacirco.export_to_json.meteo import export_meteo
from datacirco.export_to_json.education import export_education
from datacirco.export_to_json.revenu import export_revenu
from datacirco.connexion_db import db, engine
from datacirco import utilitaires
from datacirco.map import tile_provider_forte
import staticmaps
import re
import json
import sys
from time import time

circo = sys.argv[1]
dept = circo.split("-")
dept = re.sub("^0", "", dept[0])
debug = False
if len(sys.argv) > 2:
    if sys.argv[2] == "debug":
        debug = True

if debug:
    utilitaires.trace("Circo: %s" % circo)
    start = time()


def extract_to_json(filename, data, cls=False):
    with open(filename, "w", encoding="utf8") as outfile:
        if cls:
            json.dump(data, outfile, indent=4, ensure_ascii=False, cls=cls)
        else:
            json.dump(data, outfile, indent=4, ensure_ascii=False)


communes = utilitaires.get_circo_communes(circo)
# db.execute(
#     db.mogrify(
#         """
#     select
#         insee_com,
#         nom,
#         round((st_area(st_intersection(co.wkb_geometry, ci.wkb_geometry)::geography) / st_area(co.wkb_geometry::geography))::numeric,2) as pct_surf
#     from ign_commune co
#     join zone_circo ci on (st_intersects(co.wkb_geometry, ci.wkb_geometry) and st_area(st_intersection(co.wkb_geometry, ci.wkb_geometry)::geography) / st_area(co.wkb_geometry::geography) > 0.1 )
#     where ref = %s
#     UNION
#     select
#         insee_arm,
#         regexp_replace(nom,'[0-9].*$',''),
#         round((st_area(st_intersection(co.wkb_geometry, ci.wkb_geometry)::geography) / st_area(co.wkb_geometry::geography))::numeric,2) as pct_surf
#     from ign_arrondissement_municipal co
#     join zone_circo ci on (st_intersects(co.wkb_geometry, ci.wkb_geometry) and st_area(st_intersection(co.wkb_geometry, ci.wkb_geometry)::geography) / st_area(co.wkb_geometry::geography) > 0.1 )
#     where ref = %s
#     order by 3 desc,2
# """,
#         (circo, circo),
#     )
# )
# old_communes = db.fetchall()

# assert len(communes) == len(old_communes)
# for i, com in enumerate(communes):
#     print(f"{com=} == {old_communes[i]=}")
#     assert com == old_communes[i][0]
in_communes = ",".join("'%s'" % com for com in communes)


db.execute(
    db.mogrify(
        "SELECT ST_AsGeojson(wkb_geometry)::json FROM zone_circo WHERE ref= %s",
        (circo,),
    )
)
geo = db.fetchone()
assert geo is not None

### common data
# circo nom
db.execute(
    db.mogrify(
        "select nom from ign_departement where insee_dep = %s",
        (re.sub("^0", "", circo[:3]),),
    )
)
departement = db.fetchone()
assert departement is not None
# TODO: Respecter la nomenclature de l'Académie française https://www.academie-francaise.fr/abreviations-des-adjectifs-numeraux
# TODO: Ou mieux, reprendre le nom officiel INSEE des circonscriptions ?
circo_nom = f"{departement[0]} - {int(circo[-2:])}ème circonscription"
circo_nom = circo_nom.replace("- 1ème", "- Première")

# superficie circo
db.execute(
    db.mogrify(
        "select st_area(wkb_geometry::geography)/1000000 from zone_circo where ref = %s",
        (circo,),
    )
)
superficie_circo = db.fetchone()
superficie_circo = superficie_circo[0]

# Superficie nat, sauf Guyane
db.execute(
    db.mogrify(
        "select sum(st_area(wkb_geometry::geography)::float)/1000000 from ign_region where insee_reg != '03';"
    )
)
superficie_nat = db.fetchone()
superficie_nat = superficie_nat[0]
carte = staticmaps.Context()
carte.set_tile_provider(tile_provider_forte)
utilitaires.draw_poly(carte, geo)
try:
    image = carte.render_cairo(900, 600)
    image.write_to_png("carte_circo.png")
except RuntimeError as e:
    raise e


# Extract data to json files for all modules

data = DataCirco(dept, circo)
data = GetDataCirco(db, data).data

common_data = {
    "circo": circo_nom,
    "superficie_circo": superficie_circo,
    "superficie_nat": superficie_nat,
    "limites_circo": {
        "type": "Feature",
        "properties": {
            "name": circo_nom,
            "amenity": "Circonscription",
            "popupContent": "Frontière de la circonscription",
        },
        "geometry": data.geo[0],
    },
}
extract_to_json("common_data.json", common_data)

if debug:
    utilitaires.trace(f"Temps de traitement common_data.json: {time() - start:.2f}s")
    start = time()

# Population
population = export_population(db, data, dept)
extract_to_json("population.json", population, utilitaires.CustomJsonEncoder)
if debug:
    utilitaires.trace(f"Temps de traitement population.json: {time() - start:.2f}s")
    start = time()

# Elections
elections = export_elections(db, data)
extract_to_json("elections.json", elections, utilitaires.CustomJsonEncoder)
if debug:
    utilitaires.trace(f"Temps de traitement elections.json: {time() - start:.2f}s")
    start = time()

# Entreprises
entreprises = export_entreprises(db, data, geo)
extract_to_json("entreprises.json", entreprises, utilitaires.CustomJsonEncoder)
if debug:
    utilitaires.trace(f"Temps de traitement entreprises.json: {time() - start:.2f}s")
    start = time()

# Emploi
if dept < "97":
    emploi = export_emploi(db, data, in_communes, superficie_circo, geo)
    extract_to_json("emploi.json", emploi, utilitaires.CustomJsonEncoder)
    if debug:
        utilitaires.trace(f"Temps de traitement emploi.json: {time() - start:.2f}s")
        start = time()

# Logement
if dept != "976":
    logement = export_logement(db, data, in_communes, superficie_circo, geo)
    extract_to_json("logement.json", logement, utilitaires.CustomJsonEncoder)
    if debug:
        utilitaires.trace(f"Temps de traitement logement.json: {time() - start:.2f}s")
        start = time()

# Sante
sante = export_sante(db, data, in_communes, geo)
extract_to_json("sante.json", sante, utilitaires.CustomJsonEncoder)


# Meteo & Envrionnement
environnement = export_environnement(db=db, data=data, in_communes=in_communes)
extract_to_json("environnement.json", environnement, utilitaires.CustomJsonEncoder)

meteo = export_meteo(db, data, engine)
if meteo is not None:
    with open("meteo.json", "w", encoding="utf8") as outfile:
        data_json = json.dumps(
            meteo, indent=4, ensure_ascii=False, default=str, allow_nan=True
        )
        data_json = data_json.replace("NaN", "null")
        outfile.write(data_json)
    if debug:
        utilitaires.trace(f"Temps de traitement meteo.json: {time() - start:.2f}s")
        start = time()
if debug:
    utilitaires.trace("Education")

# Education
education = export_education(db, data, geo)
extract_to_json("education.json", education, utilitaires.CustomJsonEncoder)
if debug:
    utilitaires.trace(f"Temps de traitement education.json: {time() - start:.2f}s")
    start = time()

# Revenus
revenus = export_revenu(db, data)
extract_to_json("revenu.json", revenus, utilitaires.CustomJsonEncoder)

# Mobilites
# TODO: Pour l"instant ces données ne sont pas exploitées
# from datacirco.export_to_json.mobilites import export_mobilites
# mobilites = export_mobilites(db, data)
# extract_to_json("mobilites.json", mobilites, utilitaires.CustomJsonEncoder)

# C'est le même fichier pour toutes les circonscriptions, on peut donc éviter de le générer 577 fois...
if circo == "001-01":
    modules = [
        "elus",
        "emplois",
        "environnement",
        "entreprises",
        "equipements",
        "geographie",
        "logements",
        "population",
        "education",
        "revenus",
        "sante",
        "meteo",
        "mobilites",
    ]
    sources = {}
    for module in modules:
        sources[module] = utilitaires.get_sources(module)
    extract_to_json("sources.json", sources, utilitaires.CustomJsonEncoder)

    sql_circos_list = """
        SELECT LEFT(ref, 3) as code_departement, nom as nom_departement, ref as code_circo, CAST(RIGHT(ref, 2) as integer) as num_circo, ST_X(ST_PointOnSurface(zone_circo.wkb_geometry)) as longitude,
            ST_Y(ST_Centroid(zone_circo.wkb_geometry)) as latitude
        FROM zone_circo, ign_departement 
        WHERE insee_dep = (SELECT CASE 
            WHEN LEFT(ref, 1) = '0' THEN SUBSTRING(ref, 2, 2)
            ELSE LEFT(ref, 3)
        END)
        ORDER BY code_circo;
        """
    db.execute(sql_circos_list)
    columns = [col[0] for col in utilitaires.db.description]
    circos_list = [dict(zip(columns, row)) for row in db.fetchall()]
    for circo in circos_list:
        if circo["num_circo"] == 1:
            circo_lib = f"{circo['nom_departement']} - Première"
        else:
            circo_lib = f"{circo['nom_departement']} - {circo['num_circo']}ème"
        circo["libelle_circo"] = circo_lib
    circos_centre = db.fetchall()
    extract_to_json("circos_list.json", circos_list)
if debug:
    utilitaires.trace(
        f"Temps de traitement sources.json et circos_list.json : {time() - start:.2f}s"
    )
    start = time()
