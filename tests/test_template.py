import os

from datacirco.latex_template import use_latex_template

root_path = os.path.dirname(os.path.realpath(__file__))
print(root_path)
circo_nom = "NomCirco"
titre = "TitreDoc"
genre_et_nom = "GenreEtNom"
print(
    use_latex_template(
        "first_pages.tex", circo_nom=circo_nom, titre=titre, genre_et_nom=genre_et_nom
    )
)

print(
    use_latex_template("last_pages.tex", url="URL leximpact", email="EMAIL leximpact")
)
