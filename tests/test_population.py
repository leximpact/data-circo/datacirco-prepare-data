import psycopg2
from decouple import config

from datacirco.population_pages import population_pages

db_host = config("DATABASE_HOST", default="postgres_datacirco")
db_name = config("DATABASE_NAME", default="infocirco")
db_pass = config("DATABASE_PASS", default="datacirco")
db_port = config("DATABASE_PORT", default="5432")
db_user = config("DATABASE_USER", default="datacirco")


# connexion à la base postgres
try:
    pg = psycopg2.connect(
        dbname=db_name,
        user=db_user,
        password=db_pass,
        host=db_host,
        port=db_port,
    )
except psycopg2.OperationalError:
    # Whe try the outside Docker config
    pg = psycopg2.connect(
        dbname="infocirco",
        user="datacirco",
        password="datacirco",
        host="localhost",
        port="5491",
    )
pg.set_client_encoding("UTF8")
db = pg.cursor()

# dept = "01"
# circo = "001-05"
circo = "972-01"
dept = "972"
content_population, pop2015 = population_pages(db, dept, circo, debug=True)
print(content_population)
