import psycopg2
import psycopg2.extras
from decouple import config

from datacirco.entreprise_pages import entreprise_pages
from datacirco import utilitaires

# circo = "972-01"
# dept = "972"
circo = "001-01"
dept = "001"

db_host = config("DATABASE_HOST", default="postgres_datacirco")
db_name = config("DATABASE_NAME", default="infocirco")
db_pass = config("DATABASE_PASS", default="datacirco")
db_port = config("DATABASE_PORT", default="5432")
db_user = config("DATABASE_USER", default="datacirco")

# connexion à la base postgres
try:
    pg = psycopg2.connect(
        dbname=db_name,
        user=db_user,
        password=db_pass,
        host=db_host,
        port=db_port,
    )
except psycopg2.OperationalError:
    # Whe try the outside Docker config
    pg = psycopg2.connect(
        dbname="infocirco",
        user="datacirco",
        password="datacirco",
        host="localhost",
        port="5491",
    )
pg.set_client_encoding("UTF8")
db = pg.cursor()
utilitaires.db = db
db_dict = pg.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
# liste des communes couvertes par la circonscription
db.execute(
    db_dict.mogrify(
        """
    select
        insee_com,
        nom,
        round((st_area(st_intersection(co.wkb_geometry, ci.wkb_geometry)::geography) / st_area(co.wkb_geometry::geography))::numeric,2) as pct_surf
    from ign_commune co
    join zone_circo ci on (st_intersects(co.wkb_geometry, ci.wkb_geometry) and st_area(st_intersection(co.wkb_geometry, ci.wkb_geometry)::geography) / st_area(co.wkb_geometry::geography) > 0.1 )
    where ref = %s
    UNION
    select
        insee_arm,
        regexp_replace(nom,'[0-9].*$',''),
        round((st_area(st_intersection(co.wkb_geometry, ci.wkb_geometry)::geography) / st_area(co.wkb_geometry::geography))::numeric,2) as pct_surf
    from ign_arrondissement_municipal co
    join zone_circo ci on (st_intersects(co.wkb_geometry, ci.wkb_geometry) and st_area(st_intersection(co.wkb_geometry, ci.wkb_geometry)::geography) / st_area(co.wkb_geometry::geography) > 0.1 )
    where ref = %s
    order by 3 desc,2
""",
        (circo, circo),
    )
)
communes = db.fetchall()
utilitaires.db_exec(
    db.mogrify(
        "SELECT ST_AsGeojson(wkb_geometry)::json FROM zone_circo WHERE ref= %s",
        (circo,),
    )
)
geo = db.fetchone()

in_communes = ""
for com in communes:
    in_communes = in_communes + ("'%s'," % com[0])
in_communes = in_communes[:-1]  # suppression virgule finale

# superficie de la circonscription
utilitaires.db_exec(
    db.mogrify(
        "select st_area(wkb_geometry::geography)/1000000 from zone_circo where ref = %s",
        (circo,),
    )
)
superficie_circo = db.fetchone()
superficie_circo = superficie_circo[0]


# dept = "01"
# circo = "001-05"

content = entreprise_pages(
    db,
    dept,
    circo,
    geo,
    in_communes,
    superficie_circo,
    mode_kit_depute=False,
    debug=True,
)
print(content)
