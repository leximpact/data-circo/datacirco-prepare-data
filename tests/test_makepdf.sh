#! /bin/bash

source .venv/bin/activate
echo "Starting script at `date`"

parallel -j 75% ./makepdf.sh ::: 075-01 091-07 052-01 02A-01 971-01 972-01 973-01 974-01

echo "Done script at `date`"