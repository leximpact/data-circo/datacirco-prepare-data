#! /usr/bin/python3

import time
from datacirco.send_mail import send_mail
import logging

# sender_email = "login-impot@leximpact.dev"  # Ca fonctionne !
sender_email = "leximpact@an.fr"  # Ne fonctionne pas encore.

recipients = [["091", "07", "benoit.courty@assemblee-nationale.fr", "M."]]

for recipient in recipients:
    recipient_email = recipient[2]
    circo = recipient[0] + "-" + recipient[1]
    if recipient[3] == "M.":
        civ = "Monsieur le député,"
    else:
        civ = "Madame la députée,"

    # Create the plain-text and HTML version of your message
    text = """\
%s

Le service LexImpact de la direction du Contrôle et de l'évaluation de l'Assemblée nationale, a le plaisir de vous envoyer la première version de DataCirco !

Lien vers le PDF de votre circonscription: https://leximpact.an.fr/datacirco/circo/%s.pdf

Ce document traite diverses thématiques telles que la population, les entreprises, le logement et la santé, qui seront enrichies au fur et à mesure. Certaines parties sont indiquées comme ”projet” afin de donner de la visibilité sur les évolutions futures prévues.

Vous avez une suggestion ? des interrogations ? un avis sur les évolutions prévues ? N'hésitez pas à nous écrire par retour de mail.

Pour consulter les PDF des autres circonscriptions, rendez-vous sur https://leximpact.an.fr/datacirco/


Cordialement,

--
L'équipe LexImpact
https://leximpact.an.fr/
""" % (
        civ,
        circo,
    )

    html = """\
<html>
<body>
    %s<p>

    Le service LexImpact de la direction du Contrôle et de l'évaluation de l'Assemblée nationale, a le plaisir de vous envoyer la première version de DataCirco !<p>

    <a href="https://leximpact.an.fr/datacirco/circo/%s.pdf?s=m">
        <img style="border:1px solid black;" src="https://leximpact.an.fr/datacirco/circo/%s.pdf.png?s=m"><br>
    </a>
    <a href="https://leximpact.an.fr/datacirco/circo/%s.pdf?s=m">https://leximpact.an.fr/datacirco/circo/%s.pdf</a>
    <p>

    Ce document traite diverses thématiques telles que la population, les entreprises, le logement et la santé, qui seront enrichies au fur et à mesure. Certaines parties sont indiquées comme ”projet” afin de donner de la visibilité sur les évolutions futures prévues.<p>

    Vous avez une suggestion ? des interrogations ? un avis sur les évolutions prévues ? N'hésitez pas à nous écrire par retour de mail.<p>

    Pour consulter les PDF des autres circonscriptions, rendez-vous sur <a href="https://leximpact.an.fr/datacirco/">https://leximpact.an.fr/datacirco</a><p>

    Cordialement,<p>

    <hr>
    L'équipe LexImpact<br>
    <a href="https://leximpact.an.fr/">https://leximpact.an.fr/</a><br>
    <hr>
    </p>
</body>
</html>
""" % (
        civ,
        circo,
        circo,
        circo,
        circo,
    )

    send_mail(
        recipient_email=recipient_email,
        subject="Première version de DataCirco disponible en PDF",
        content_html=html,
        content_text=text,
        sender_address=sender_email,
        sender_name="LexImpact",
    )

    logging.warning(f"Mail envoyé pour la circo {circo} à {recipient_email}")
    time.sleep(1)
