echo "Starting script at `date`"
# Fail on first error
set -e
for c in $(cat circo_test.txt)
do
    ./get_json.sh $c
done
echo "Done script `date` in $SECONDS s at `date`"