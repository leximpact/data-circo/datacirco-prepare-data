/* Circo avec le nom le plus long */
SELECT dep, circo, prenom, nom, LENGTH(CONCAT(prenom,nom)) FROM an_mailing GROUP BY dep, circo, prenom, nom ORDER BY 5 DESC;
dep | circo |      prenom      |          nom          | length
-----+-------+------------------+-----------------------+--------
 091 | 02    | Nathalie         | Da Conceicao Carvalho |     29
 076 | 09    | Marie-Agnès      | Poussier-Winsback     |     28
 035 | 02    | Laurence         | Maillart-Méhaignerie  |     28
 013 | 16    | Emmanuel         | Taché de la Pagerie   |     27



/*
Dans la table insee_population_carroyee_2010 il n'y a pas les communes de Réunion (974) et Martinique (972) !
Et donc pas non plus dans la table circo_population_2010 qui en est issue.
Pourtant on a importé des données dans insee_population_carroyee_2010_car et insee_population_carroyee_2010_rect pour cela.
*/

/* 2 314 836 en local et sur le serveur */
select count(*) from insee_population_carroyee_2015;



/* Résultat vide pour 974 dans insee_population_carroyee_2010 ! */
select * from insee_population_carroyee_2010 WHERE depcom  IN ('91326', '97412');

/* Résultat vide pour 974 dans insee_population_carroyee_2010_car !
Pourtant
*/
select n.depcom, c.idinspire from insee_population_carroyee_2015 n inner join insee_population_carroyee_2010_car c on c.idinspire=n.idinspire
WHERE n.depcom IN ('91326', '97412');

/* Résultat vide pour 974 dans insee_population_carroyee_2010_car ! */
select n.depcom, c.idinspire, c.nbcar, r.idk, r.men
from insee_population_carroyee_2015 n left join insee_population_carroyee_2010_car c on c.idinspire=n.idinspire
left join insee_population_carroyee_2010_rect r on r.idk = c.idk and r.nbcar = c.nbcar
WHERE n.depcom = '97412';

select n.depcom from insee_population_carroyee_2015 n left join insee_population_carroyee_2010_car c on c.idinspire=n.idinspire
WHERE n.depcom = '97412';

/*
2 303 290 lignes en left join, inner join ou natural join
*/
select count(*)
from insee_population_carroyee_2010_car c
inner join insee_population_carroyee_2010_rect r on r.idk = c.idk and r.nbcar = c.nbcar;

/*DROP TABLE if exists insee_population_carroyee_2010 CASCADE;*/


select wkb_geometry,
    idinspire,
    (select depcom from insee_population_carroyee_2015 n where c.idinspire=n.idinspire) as depcom,
    ind_c as ind,
    men*ind_c/ind_r as men,
    men_surf*ind_c/ind_r as men_surf,
    men_coll*ind_c/ind_r as men_coll,
    men_5ind*ind_c/ind_r as men_5ind,
    men_1ind*ind_c/ind_r as men_1ind,
    men_prop*ind_c/ind_r as men_prop,
    men_basr*ind_c/ind_r as men_pauv,
    ind_age1*ind_c/ind_r as ind_0_3,
    ind_age2*ind_c/ind_r as ind_4_5,
    ind_age3*ind_c/ind_r as ind_6_10,
    ind_age4*ind_c/ind_r as ind_11_14,
    ind_age5*ind_c/ind_r as ind_15_17,
    (ind_r-ind_age1-ind_age2-ind_age3-ind_age4-ind_age5-ind_age6)*ind_c/ind_r as ind_18_24,
    ind_age6*ind_c/ind_r as ind_25p,
    ind_age7*ind_c/ind_r as ind_65p,
    ind_age8*ind_c/ind_r as ind_75p,
    ind_srf * ind_c/ind_r as ind_snv
from insee_population_carroyee_2010_car c
inner join insee_population_carroyee_2010_rect r on r.idk = c.idk and r.nbcar = c.nbcar;

