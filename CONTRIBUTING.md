# Contribuer à DataCirco

## Installation local

```sh
git clone git@git.leximpact.dev:leximpact/datacirco.git
cd datacirco
cp .env-example .env
```

Il est conseillé d'utiliser Docker pour exécuter DataCirco en local car il nécessite beaucoup de composants particulier :

- Des outils de manipulations de CSV et de données géographique.
- Postgresql >=14 avec PostGIS pour l'indexation des données géographiques.

Voir [INSTALL.md](INSTALL.md) pour l'installation sans Docker.

Avec docker il suffit de faire :

```sh
cd datacirco
docker-compose up
```

Pour gagner du temps il est conseillé de récupérer la base de production en local, cela évite de devoir tout télécharger et importer.

## Windows WSL2

Pour la gestion des clefs dans WSL2 : <https://esc.sh/blog/ssh-agent-windows10-wsl2/>
Pour l'installation de Docker dans WSL2, voir <https://nickjanetakis.com/blog/install-docker-in-wsl-2-without-docker-desktop>

## Installation sur Mac OS X, sans Docker

### Création d'un environnement virtuel

Créer l'environnement et l'activer:

```sh
python3 -m venv --system-site-package .venv
source .venv/bin/activate
```

Installer les librairies nécessaires:

```sh
poetry install
```

### Installation de ImageMagick

```sh
brew install imagemagick
```

Pour ne pas avoir l'erreur :

```sh
convert: no decode delegate for this image format `PDF' @ error/constitute.c/ReadImage/746.
```

Installer `ghostscript`:

```sh
brew install ghostscript
```

### Installation de Cairo

Pour la génération des cartes, il faut installer :

```sh
brew install cairo
brew install py3cairo OR pip3 install pycairo
```

## Backup base

```sh
ssh ysabell
pct enter 134
su - datacirco
pg_dump -Fc --clean --create -d infocirco >  /home/datacirco/datacirco-prepare-data/backup/datacirco_full-`date +%F`.dump
```

_-Fc_ permet de compresser la sortie.

__Attention__ : taille de la base compressée 7 Go !

La récupérer en local :

`scp ysabell:/data/private-data/output/datacirco/backup/datacirco_full-`date +%F`.dump /tmp`

## Création d'un environnement Docker pour recevoir la base

```sh
docker compose up
```

Dans un autre terminal :

```sh
docker exec -it postgres_datacirco bash
psql -U $POSTGRES_USER -d $POSTGRES_DB -c 'CREATE EXTENSION IF NOT EXISTS postgis;'
psql -U $POSTGRES_USER -d $POSTGRES_DB -c 'CREATE EXTENSION IF NOT EXISTS tablefunc; CREATE EXTENSION IF NOT EXISTS "unaccent";'
```

## Création d'une base vide

```sh
psql -U datacirco -d postgres
postgres=# DROP DATABASE infocirco WITH (FORCE);
postgres=# quit
createdb -U datacirco infocirco
```

## Import base

Si vous utilisez Docker, s'y connecter :

```sh
# Plus nécessaire de copier car le /tmp local est monté dans le conteneur dans /tmp/backup
# docker cp /tmp/datacirco_full-*.dump postgres_datacirco:/tmp
docker exec -it postgres_datacirco bash
```

Effacer puis importer les données dans la base :

```sh
pg_restore -U datacirco -d infocirco --jobs=8 --clean --create /tmp/backup/datacirco_full-`date +%F`.dump
```

Si ça ne fonctionne pas, sur code-server, c'est la commande suivante depuis root :
```sh
root@code-server:/# sudo --user=postgres pg_restore  -d infocirco --jobs=8 --clean --create /home/datacirco/datacirco-prepare-data/backup/datacirco_full-`date +%F`.dump
```

Si vous n'avez pas effacé la base, l'import fonctionne mais des erreurs peuvent apparaître, ce n'est pas gênant.

Il faut plusieurs minutes, c'est normal car la base est grosse : > 30 Go.

## Utiliser l'image Docker pour exporter les json

Cela évite d'installer les dépendances sur votre machine.

```sh
docker exec -it datacirco_datacirco_1 bash
poetry shell
./get_json.sh 006-08 debug
```

### Copier des fichiers dans l'image

```sh
docker exec datacirco_datacirco_1 mkdir -p /opt/datacirco/data/meteo
docker cp /mnt/data-out/datacirco/data/meteo/FR_all_safran_meteo.csv datacirco_datacirco_1:/opt/datacirco/data/meteo
```

### Travailler avec la base

Pour faciliter les commandes, mettre en mémoire les paramètres de connexion:

```sh
export PGHOST=postgres_datacirco
export PGDATABASE=infocirco
export PGUSER=datacirco
export PGPASSWORD=datacirco
```

Accéder à la console psql: `psql`

Lancer une réindexation complète de la base: `REINDEX DATABASE infocirco;`

#### Trouver les requêtes les plus longues

Si vous avez un doute sur la performance de la base, ou le fait que la commande soit toujours en cours, vous pouvez lister les requêtes qui tournent depuis plus de 5 minutes:

```sql
SELECT
  pid,
  now() - pg_stat_activity.query_start AS duration,
  query,
  state
FROM pg_stat_activity
WHERE (now() - pg_stat_activity.query_start) > interval '5 minutes';
```

## Récupération des tables de correspondances CNAMTS

Les données cnamts_codes_profession cnamts_codes_nature_exercice cnamts_codes_mode_exercice n'existent pas directement en CSV, elles ont été récupérées manuellemnt dans le PDF <https://static.data.gouv.fr/resources/annuaire-sante-de-la-cnam/20180404-182737/Annuaire_sante_-_Description_contenu_PS_InfosPratiques.pdf>

```sh
\copy (SELECT * FROM cnamts_codes_profession) to cnamts_codes_profession.csv csv header;
\copy (SELECT * FROM cnamts_codes_nature_exercice) to cnamts_codes_nature_exercice.csv csv header;
\copy (SELECT * FROM cnamts_codes_mode_exercice) to cnamts_codes_mode_exercice.csv csv header;
```

## Mise à jour de l'image Docker

Après avoir modifié le fichier `Dockerfile` il faut reconstruire l'image et la pousser sur le registre Docker.

```sh
docker login # Voir dans KeyPass
docker build . -t leximpact/datacirco:1.1.1 -f docker/Dockerfile
docker push leximpact/datacirco:1.1.1
```

N'oubliez pas ensuite de mettre à jour le fichier `docker-compose.yml` pour utiliser la nouvelle version.

## Manipulation géographique

GDAL : Librairie d'abstraction pour données raster.
OGR : Librairie pour données vectorielles.

Les données sont chargées brutes dans la base.

ST_Union : regroupe les morceaux de circo en un seul

EPSG:4326 ce sont des données sphérique.

Métropole : projection 2154
Hors Métropole c'est une autre projection.

Les projections sont nécessaires pour les calculs de surface et distance.

Les spécificités sont dans le code Python.

La Guyane est très grande avec des communes

On garde les communes dans la circo que si la circo possède au moins 10% de la commune.

`wbk_geometry::geography` => passe d'une géométrie en dégré à une projection local en m². PostGis trouve tout seul la projection local appropriée.

`st_` préfix les requêtes spatiales.

Pour les données géolocalisées c'est l'intersection avec les limites de la circo.

Pour les données carroyées on prend l'intersection des carreaux et la limite des circonscriptions. => on prend le pourcentage de chaque carreau qui est compris dans la circo. C'est pour cela qu'il y a "pct" partout.

Ce `pct` est précalculé dans `population.sh`, c'est une requête qui met plusieurs heures.

`CLUSTER` => réordonne les data sur le disque en fonction de l'index.

### Zones de projection des services d'urgence

Utilise la liste de commune pour trouver les services d'urgences, le temps d'intervention est donné par le DREES.

### Localisation des entreprises

ST_X, ST_Y : calcul le centre pondéré de la commune.
Il faut faire attention à l'échelle pour que les cercles soient bien proportionés.

### Fond de carte

Utilise `staticmap` pour le fond de carte.

Le `JEOJSON` donne les éléments à ajouter à la carte.

`draw_poly` dessine les données et la limite de circo.

### Carte des circos sur le site

Fait à la main sur <https://umap.openstreetmap.fr/fr/> en chargeant le fichier des circos législatives et la règle d'obtention des vignettes.

## Note sur les temps de traitement

### 14/02/2025

Sur le serveur Ysabell, dans le conteneur (qui a 24 Go de RAM et 6 coeurs), il faut entre 30 minutes et 1 heure par circonscription pour générer les données.

Sur mon poste personnel (32Go de RAM et 32 coeurs) c'est 950 secondes (15 min) pour 075-05.

Après `REINDEX DATABASE infocirco;` la requête ne prends pas moins de temps.

La requête qui prend du temps est :
```sql
SELECT classe_ges, coalesce(a.annee_2013, 0) as annee_2013, coalesce(a.annee_2014, 0) as annee_2014, coalesce(a.annee_2015, 0) as annee_2015,
            coalesce(a.annee_2016, 0) as annee_2016, coalesce(a.annee_2017, 0) as annee_2017, coalesce(a.annee_2018, 0) as annee_2018,
            coalesce(a.annee_2019, 0) as annee_2019, coalesce(a.annee_2020, 0) as annee_2020, coalesce(a.annee_2021, 0) as annee_2021
                     FROM (
                        select * from crosstab(
                            $$SELECT d.classe_estimation_ges, LEFT(d.date_etablissement_dpe,4) as annee_etablissement_DPE,
                            coalesce(COUNT(DISTINCT d.numero_dpe), 0) as nb_ges
                            FROM ademe_dpe_logements as d
                            LEFT JOIN zone_circo c
                            ON st_intersects(c.wkb_geometry, d.geom)
                            WHERE (c.ref = '075-05' OR (d.geom IS NULL AND (d.code_insee_commune_actualise in ('75103','75110'))))
                            AND LEFT(d.date_etablissement_dpe,4) between '2013' and '2021'
                            AND d.classe_estimation_ges IN ('A', 'B', 'C', 'D', 'E', 'F', 'G')
                            GROUP BY 1,2 ORDER BY 1,2$$,
                            $$SELECT generate_series(2013,2021)$$
                            ) as T (
                                classe_ges text,
                                "annee_2013" int,
                                "annee_2014" int,
                                "annee_2015" int,
                                "annee_2016" int,
                                "annee_2017" int,
                                "annee_2018" int,
                                "annee_2019" int,
                                "annee_2020" int,
                                "annee_2021" int
                            )) as a;
```

Pour optimiser, il faut précalculer les données et les stocker dans une table. Voir le commit e3581a10910f484764dfce36657a8625c6328d52

En faisant une seule requête de 15 minutes pour préparer les données lors de l'import initial, nous sommes passé de 760 secondes (12 minutes) pour la requête la plus longue à 0.128 secondes.
