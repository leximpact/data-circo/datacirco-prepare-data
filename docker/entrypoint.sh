#!/bin/bash
cd /opt/datacirco
set +e # Don't stop on error
sleep 1
echo "$DATABASE_USER@$DATABASE_HOST:$DATABASE_PORT"
until PGPASSWORD=$DATABASE_PASS psql -h "$DATABASE_HOST" -U "$DATABASE_USER" -p "$DATABASE_PORT" -d "$DATABASE_NAME" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up !"

bash makepdf.sh 001-05
tail -f /dev/null