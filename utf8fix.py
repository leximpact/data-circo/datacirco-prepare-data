#! /bin/env/python3

# Corrige les fichiers CSV contenant un mélange d'UTF8 et d'ISO + conversion séparateur décimal en point

import re
import sys

with open(sys.argv[1], "rb") as f:
    sep = sys.argv[2]
    for line in f.readlines():
        try:
            t = line[:-1].decode()
        except Exception:
            t = ""
            for col in line.split(bytes(sep, encoding="utf-8")):
                try:
                    col = col.decode()
                except Exception:
                    col = col.decode("iso-8859-1")
                t += col + sep
            t = t[:-2]

        t = re.sub(r";(\d+),(\d+)", r";\1.\2", t)
        # print(t)
