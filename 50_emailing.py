#! /usr/bin/python3

import smtplib
import sys
import time
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import psycopg2

if len(sys.argv) < 4:
    print("Usage: emailing.py smtphost user password")
    exit(1)

port = 465  # For SSL
port = 587

sender_email = "leximpact@assemblee-nationale.fr"

with smtplib.SMTP(sys.argv[1], port) as server:
    server.login(sys.argv[2], sys.argv[3])

    pg = psycopg2.connect("dbname=infocirco")
    db = pg.cursor()
    db.execute("SELECT * FROM an_mailing WHERE dep like '0%' ORDER BY 1,2")
    recipients = db.fetchall()

    for recipient in recipients:
        recipient_email = recipient[2]
        circo = recipient[0] + "-" + recipient[1]
        if recipient[3] == "M.":
            civ = "Monsieur le député,"
        else:
            civ = "Madame la députée,"

        message = MIMEMultipart("alternative")
        message["Subject"] = "Première version de DataCirco disponible en PDF"
        message["From"] = "DataCirco-LexImpact <" + sender_email + ">"
        message["To"] = recipient_email

        # Create the plain-text and HTML version of your message
        text = """\
    %s

    Le service LexImpact de la direction du Contrôle et de l'évaluation de l'Assemblée nationale, a le plaisir de vous envoyer la première version de DataCirco !

    Lien vers le PDF de votre circonscription: https://leximpact.an.fr/datacirco/circo/%s.pdf

    Ce document traite diverses thématiques telles que la population, les entreprises, le logement et la santé, qui seront enrichies au fur et à mesure. Certaines parties sont indiquées comme ”projet” afin de donner de la visibilité sur les évolutions futures prévues.

    Vous avez une suggestion ? des interrogations ? un avis sur les évolutions prévues ? N'hésitez pas à nous écrire par retour de mail.

    Pour consulter les PDF des autres circonscriptions, rendez-vous sur https://leximpact.an.fr/datacirco/


    Cordialement,

    --
    Christian pour LexImpact
    https://leximpact.an.fr/
    """ % (
            civ,
            circo,
        )

        html = """\
    <html>
    <body>
        %s<p>

        Le service LexImpact de la direction du Contrôle et de l'évaluation de l'Assemblée nationale, a le plaisir de vous envoyer la première version de DataCirco !<p>

        <a href="https://leximpact.an.fr/datacirco/circo/%s.pdf?s=m">
            <img style="border:1px solid black;" src="https://leximpact.an.fr/datacirco/circo/%s.pdf.png?s=m"><br>
        </a>
        https://leximpact.an.fr/datacirco/circo/%s.pdf<p>


        Ce document traite diverses thématiques telles que la population, les entreprises, le logement et la santé, qui seront enrichies au fur et à mesure. Certaines parties sont indiquées comme ”projet” afin de donner de la visibilité sur les évolutions futures prévues.<p>

        Vous avez une suggestion ? des interrogations ? un avis sur les évolutions prévues ? N'hésitez pas à nous écrire par retour de mail.<p>

        Pour consulter les PDF des autres circonscriptions, rendez-vous sur <a href="https://leximpact.an.fr/datacirco/">https://leximpact.an.fr/datacirco</a><p>

        Cordialement,<p>

        <hr>
        Christian pour LexImpact<br>
        <a href="https://leximpact.an.fr/">https://leximpact.an.fr/</a><br>
        <hr>
        </p>
    </body>
    </html>
    """ % (
            civ,
            circo,
            circo,
            circo,
        )

        # Turn these into plain/html MIMEText objects
        part1 = MIMEText(text, "plain")
        part2 = MIMEText(html, "html")

        # Add HTML/plain-text parts to MIMEMultipart message
        # The email client will try to render the last part first
        message.attach(part1)
        message.attach(part2)

        server.sendmail(sender_email, recipient_email, message.as_string())
        print(circo, recipient_email)
        time.sleep(1)
