from datacirco import utilitaires
import glob
from tqdm import tqdm
from datacirco.connexion_db import db, run

utilitaires.start("meteo_import.py")

print("METEO : Historique ERA5")

tables = ["meteo_era5_monde", "meteo_era5_europe", "meteo_era5_france"]
files = [
    "ERA5/ERA5_moyenne_mensuelle_monde.csv",
    "ERA5/ERA5_moyenne_mensuelle_europe.csv",
    "ERA5/ERA5_moyenne_mensuelle_france.csv",
]

for table, file in tqdm(zip(tables, files)):
    run(
        f"""
        DROP TABLE IF EXISTS {table} CASCADE;
        DROP INDEX IF EXISTS {table}_geom CASCADE;
        CREATE TABLE {table} (
            date date not null,
            annee int not null,
            mois int not null,
            precipitation float not null,
            temperature float not null
        );
    """
    )

    with open(file, "r") as f:
        db.copy_expert(f"COPY {table} FROM STDIN WITH (FORMAT CSV, HEADER TRUE)", f)

table = "meteo_era5_circo"
run(
    f"""
        DROP TABLE IF EXISTS {table} CASCADE;
        DROP INDEX IF EXISTS {table}_geom CASCADE;
        CREATE TABLE {table} (
            longitude float,
            latitude float,
            date date not null,
            annee int not null,
            mois int not null,
            precipitation float not null,
            temperature float not null
        );
    """
)

for file in tqdm(glob.glob("ERA5/FR_circo_ERA5_*.csv")):
    with open(file, "r") as f:
        db.copy_expert(f"COPY {table} FROM STDIN WITH (FORMAT CSV, HEADER TRUE)", f)

run(
    f"""
    ALTER TABLE {table} ADD geom geometry;
    UPDATE {table} SET geom=ST_SetSRID(ST_Makepoint(longitude::numeric, latitude::numeric),4326);
    CREATE INDEX {table}_geom ON {table} USING GIST (geom);
    CLUSTER {table} USING {table}_geom;
"""
)

utilitaires.end("meteo_import.py")
