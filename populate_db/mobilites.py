from datacirco import utilitaires
from tqdm import tqdm
from sqlalchemy import types
import pandas as pd
from datacirco.connexion_db import engine, run
import geopandas as gpd
import glob

utilitaires.start("mobilites.py")

print("Données des bornes de recharge de véhicules électriques et hybrides")
# IRVE = Infrastructures de recharge pour véhicules électriques
table = "irve_2024"
# on importe la table au format csv depuis l'adresse du site.
df_velec = pd.read_csv(
    "https://www.data.gouv.fr/fr/datasets/r/eb76d20a-8501-400e-b336-d85724de5435"
)
run(f"""DROP TABLE IF EXISTS {table};""")
df_velec[
    [
        "nom_amenageur",
        "code_insee_commune",
        "nbre_pdc",
        "puissance_nominale",
        "date_mise_en_service",
        "consolidated_longitude",
        "consolidated_latitude",
    ]
].to_sql(name=table, con=engine, index=False)

# Creation d'une variable géometrie par transformation des longitudes et latitude de la table initiale
run(
    """
    ALTER TABLE irve_2024 ADD COLUMN geom geometry(Point, 4326);
    UPDATE irve_2024
    SET geom = ST_SetSRID(ST_MakePoint(
                CAST(consolidated_longitude AS FLOAT)::numeric,
                CAST(consolidated_latitude AS FLOAT)::numeric), 4326);
    CREATE INDEX irve_2024_geom ON irve_2024 USING GIST (geom);
    CLUSTER irve_2024 USING irve_2024_geom;
    """
)

print("Données de nombres du stock de voitures immatriculées")
table = "sdes_vehicules_2024"
run(f"""DROP TABLE IF EXISTS {table};""")
header = [
    "libelle_commune",
    "code_commune",
    "categorie",
    "energie",
    "statut",
    "2010",
    "2011",
    "2012",
    "2013",
    "2014",
    "2015",
    "2016",
    "2017",
    "2018",
    "2019",
    "2020",
    "2021",
    "2022",
    "2023",
    "type_achat",
    "vignette_critair",
]
# Import de chaque fichier csv dans la table
for file_path in tqdm(
    glob.iglob("notebooks/immatriculations_2023_tableaux_communaux/*.csv")
):

    with open(file_path, "r") as csv_file:
        print("Reading", file_path)
        df = pd.read_csv(
            csv_file,
            sep=",",
            dtype={
                "libelle_commune": "str",
                "code_commune": "str",
                "vignette_critair": "str",
            },
            names=header,
            skiprows=1,
        )
        df.assign(
            vignette_critair=lambda x: (
                None if "vignette_critair" not in df.columns else x["vignette_critair"]
            ),
            code_commune=lambda x: x["code_commune"][0].zfill(5),
            inplace=True,
        )
        df.columns = header
        try:
            df.to_sql(
                table,
                con=engine,
                if_exists="append",
                index=False,
                dtype=dict(
                    zip(
                        df.columns.tolist(),
                        (types.VARCHAR(length=100),)
                        + (types.VARCHAR(length=100),) * 4
                        + (types.Integer(),) * 14
                        + (types.VARCHAR(length=10),),
                    )
                ),
            )
        except Exception as e:
            print(e)

print("Tables des véhicules chargées")

print("Données de stations de carburant de voitures")
# file_stations = requests.get("http://donnees.roulez-eco.fr/opendata/annee")
# with ZipFile(io.BytesIO(file_stations.content), 'r') as z:
#     # Read the XML file as bytes
#     with z.open('PrixCarburants_annuel_2024.xml') as xml_file:
#         # Convert bytes to a file-like object
#         xml_bytes = xml_file.read()
#         xml_file_like = io.BytesIO(xml_bytes)
#         # Read the XML file using pandas
#         df_stations = pd.read_xml(xml_file_like, parser='etree', dtype= {'cp' : str})
table = "carburants_2024"
run(f"""DROP TABLE IF EXISTS {table};""")
df_stations = gpd.read_file(
    "https://www.data.gouv.fr/fr/datasets/r/c465b7f9-f2d7-4e32-a575-d9d69494d112"
)
df_stations["longitude"] = df_stations["geometry"].x
df_stations["latitude"] = df_stations["geometry"].y
df_stations[
    ["id", "latitude", "longitude", "cp", "ville", "code_departement", "code_region"]
].to_sql(name=table, con=engine)

# Creation d'une variable géometrie par transformation des longitudes et latitude de la table initiale
run(
    """
    ALTER TABLE carburants_2024 ADD COLUMN geom geometry(Point, 4326);
    UPDATE carburants_2024
    SET geom = st_SetSRID(ST_MakePoint(
                CAST(longitude AS FLOAT)::numeric,
                CAST(latitude AS FLOAT)::numeric), 4326);
    CREATE INDEX carburants_2024_geom ON carburants_2024 USING GIST (geom);
    CLUSTER carburants_2024 USING carburants_2024_geom;
    """
)


utilitaires.end("mobilites.py")
