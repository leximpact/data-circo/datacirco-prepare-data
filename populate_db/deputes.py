from datacirco import utilitaires
from datacirco.connexion_db import database_command_line, run
import glob

utilitaires.start("deputes.py")

run(
    """CREATE TABLE IF NOT EXISTS an (id text, j jsonb);
     CREATE INDEX IF NOT EXISTS an_id ON an (id);
     TRUNCATE an;"""
)

# json_files = glob.glob("data/elus/acteur/*.json")
# json_files += glob.glob("data/elus/organe/*.json")
json_files = glob.glob("acteur/*.json")
json_files += glob.glob("organe/*.json")

for j in json_files:
    # On met le contenu des json dans la deuxième colonne "j", en ajoutant un | devant le json pour laisser vide la première colonne.
    utilitaires.execute_command(
        f"echo \"|$(jq . {j} -c)\" | {database_command_line} -c \"copy an from stdin with (format csv, delimiter '|', quote e'\x01')\""
    )


run(
    """update an set id = j->'acteur'->'uid'->>'#text';
    CREATE OR REPLACE VIEW an_mailing AS
    select
        ident.id as id,
        replace(right('0'||(mandat->'lieu'->>'numDepartement'),3),'099','999') as dep,
        right('0'||(mandat->'lieu'->>'numCirco'),2) as circo,
        adr.adresse->>'valElec' as email,
        ident->>'civ' as civ,
        ident->>'prenom' as prenom,
        ident->>'nom' as nom
    from (
        select * from (
            select id,
                jsonb_array_elements(j->'acteur'->'mandats'->'mandat')->'election' as mandat
            from an
        ) m
        where mandat is not null
    ) as circo
    join (
        select id,
            j->'acteur'->'etatCivil'->'ident' as ident from an
        ) ident
    on (ident.id=circo.id)
    join (
        select id,
            jsonb_array_elements(
                case when j->'acteur'->'adresses'->>'adresse' like '{%'
                    then format('[%s]', j->'acteur'->'adresses'->>'adresse')::jsonb
                    else j->'acteur'->'adresses'->'adresse'
                end
             ) as adresse
        from an
    ) adr
    on (adr.id=circo.id)
    where adr.adresse->>'valElec' ~ '@assemblee-nationale.fr'
    AND adr.adresse->>'@xsi:type' = 'AdresseMail_Type'
    and mandat->'lieu'->>'numCirco' is not null
    group by 1,2,3,4,5,6,7
    order by 1,2;
    """
)

# PO59048 : Commission des finances
run(
    """CREATE OR REPLACE VIEW commission_finances AS
    SELECT ident.id,
        replace(right('0'::text || ((circo.mandat -> 'lieu'::text) ->> 'numDepartement'::text), 3), '099'::text, '999'::text) AS dep,
        right('0'::text || ((circo.mandat -> 'lieu'::text) ->> 'numCirco'::text), 2) AS circo,
        LOWER(adr.adresse ->> 'valElec'::text) AS email,
        ident.ident ->> 'civ'::text AS civ,
        ident.ident ->> 'prenom'::text AS prenom,
        ident.ident ->> 'nom'::text AS nom,
        REPLACE(commision.commision::text,'\"', '') as organe
        /* commision.organes */
       FROM ( SELECT m.id,
                m.mandat
               FROM ( SELECT an.id,
                        jsonb_array_elements(((an.j -> 'acteur'::text) -> 'mandats'::text) -> 'mandat'::text) -> 'election'::text AS mandat
                       FROM an) m
              WHERE m.mandat IS NOT NULL) circo
         JOIN ( SELECT an.id,
                ((an.j -> 'acteur'::text) -> 'etatCivil'::text) -> 'ident'::text AS ident
               FROM an) ident ON ident.id = circo.id
         JOIN ( SELECT an.id,
                jsonb_array_elements(
                    CASE
                        WHEN (((an.j -> 'acteur'::text) -> 'adresses'::text) ->> 'adresse'::text) ~~ '{%'::text THEN format('[%s]'::text, ((an.j -> 'acteur'::text) -> 'adresses'::text) ->> 'adresse'::text)::jsonb
                        ELSE ((an.j -> 'acteur'::text) -> 'adresses'::text) -> 'adresse'::text
                    END) AS adresse
               FROM an) adr ON adr.id = circo.id
        JOIN ( SELECT an.id,
                jsonb_array_elements(((an.j -> 'acteur'::text) -> 'mandats'::text) -> 'mandat'::text) -> 'organes'::text -> 'organeRef'::text as commision,
              jsonb_array_elements(((an.j -> 'acteur'::text) -> 'mandats'::text) -> 'mandat'::text) AS organes
               FROM an) commision ON commision.id = adr.id
      WHERE (adr.adresse ->> 'valElec'::text) ~ '@assemblee-nationale.fr'::text AND (adr.adresse ->> '@xsi:type'::text) = 'AdresseMail_Type'::text
       AND ((circo.mandat -> 'lieu'::text) ->> 'numCirco'::text) IS NOT NULL 
       AND organes ->> 'dateFin'::text IS NULL /* Uniquement les mandats encore actifs */
       AND commision.commision::text ='\"PO59048\"'
      GROUP BY 1,2,3, 4, 5, ident.ident , commision.commision /* , commision.organes */ 
      ORDER BY nom, prenom
    """
)


# PO420120 : Commission des affaires sociales
run(
    """CREATE OR REPLACE VIEW commission_affaires_sociales AS
    SELECT ident.id,
        replace(right('0'::text || ((circo.mandat -> 'lieu'::text) ->> 'numDepartement'::text), 3), '099'::text, '999'::text) AS dep,
        right('0'::text || ((circo.mandat -> 'lieu'::text) ->> 'numCirco'::text), 2) AS circo,
        LOWER(adr.adresse ->> 'valElec'::text) AS email,
        ident.ident ->> 'civ'::text AS civ,
        ident.ident ->> 'prenom'::text AS prenom,
        ident.ident ->> 'nom'::text AS nom,
        REPLACE(commision.commision::text,'\"', '') as organe
       FROM ( SELECT m.id,
                m.mandat
               FROM ( SELECT an.id,
                        jsonb_array_elements(((an.j -> 'acteur'::text) -> 'mandats'::text) -> 'mandat'::text) -> 'election'::text AS mandat
                       FROM an) m
              WHERE m.mandat IS NOT NULL) circo
         JOIN ( SELECT an.id,
                ((an.j -> 'acteur'::text) -> 'etatCivil'::text) -> 'ident'::text AS ident
               FROM an) ident ON ident.id = circo.id
         JOIN ( SELECT an.id,
                jsonb_array_elements(
                    CASE
                        WHEN (((an.j -> 'acteur'::text) -> 'adresses'::text) ->> 'adresse'::text) ~~ '{%'::text THEN format('[%s]'::text, ((an.j -> 'acteur'::text) -> 'adresses'::text) ->> 'adresse'::text)::jsonb
                        ELSE ((an.j -> 'acteur'::text) -> 'adresses'::text) -> 'adresse'::text
                    END) AS adresse
               FROM an) adr ON adr.id = circo.id
        JOIN ( SELECT an.id,
                jsonb_array_elements(((an.j -> 'acteur'::text) -> 'mandats'::text) -> 'mandat'::text) -> 'organes'::text -> 'organeRef'::text as commision,
              jsonb_array_elements(((an.j -> 'acteur'::text) -> 'mandats'::text) -> 'mandat'::text) AS organes
               FROM an) commision ON commision.id = adr.id
      WHERE (adr.adresse ->> 'valElec'::text) ~ '@assemblee-nationale.fr'::text AND (adr.adresse ->> '@xsi:type'::text) = 'AdresseMail_Type'::text
       AND ((circo.mandat -> 'lieu'::text) ->> 'numCirco'::text) IS NOT NULL 
       AND organes ->> 'dateFin'::text IS NULL /* Uniquement les mandats encore actifs */
       AND commision.commision::text ='\"PO420120\"'
      GROUP BY 1,2,3, 4, 5, ident.ident , commision.commision
      ORDER BY nom, prenom"""
)

utilitaires.end("deputes.py")
