SELECT onisep.uai, n_siret as siret, onisep.type_d_etablissement as type_etablissement, nom as appellation_officielle, onisep.sigle, LOWER(onisep.secteur_public_prive_libe) as secteur_public_prive_libe, tutelle, universite_de_rattachement_libelle,
	universite_de_rattachement_id_et_url_onisep, etablissements_lies_libelles, etablissements_lies_url_et_id_onisep,
	bote_postale as boite_postale, onisep.adresse as adresse_uai, cp as code_postal, onisep.commune as libelle_commune, commune__cog_ as code_commune, cedex, telephone, arrondissement, onisep.departement, onisep.academie,
	onisep.region, region__cog_ as code_region, journees_portes_ouvertes, label_generation_2024, url_et_id_onisep,
	typologie_d_universites_et_assimiles, site_internet, date_creation, 
    effectifs_d_etudiants_inscrits_2022_23::numeric as effectifs, longitude, latitude, circo
    FROM educ_onisep_structures_denseignement_superieur onisep LEFT JOIN educ_principaux_etablissements_enseignement_superieur ens ON onisep.uai = ens.uai
    WHERE circo IS NOT NULL
	/* AND tutelle != 'Ministère chargé de l''Éducation nationale et de la Jeunesse' */
    ORDER BY circo, onisep.commune;  

SELECT * FROM educ_etablissements_superieur as onisep INNER JOIN public.educ_etablissements_premier_et_second_degre as en ON en.uai=onisep.uai

SELECT * FROM educ_onisep_structures_denseignement_superieur as onisep WHERE tutelle = 'Ministère chargé de l''Éducation nationale et de la Jeunesse';

SELECT tutelle, count(*)
FROM educ_onisep_structures_denseignement_superieur
GROUP BY tutelle
ORDER BY 2 DEsC;

SELECT *
FROM educ_onisep_structures_denseignement_superieur
WHERE tutelle = 'Premier ministre'


/* Lycées qui ne sont pas dans la base ONISEP */
SELECT * from public.educ_etablissements_premier_et_second_degre en
WHERE uai IS NOT null and en.uai NOT IN (SELECT uai FROM educ_onisep_structures_denseignement_superieur WHERE uai IS NOT null)
AND denomination_principale LIKE 'LYCEE%'

SELECT * from public.educ_etablissements_premier_et_second_degre en
WHERE  denomination_principale LIKE '%AGRICOLE%'
/* 463 etab */


SELECT * FROM educ_etablissements_details

SELECT effectifs from educ_etablissements_superieur WHERE LOWER(secteur_public_prive_libe) != 'public';
/* Ils sont tous null */