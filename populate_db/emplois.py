from datacirco import utilitaires
from datacirco.connexion_db import db, run

utilitaires.start("emplois.py")

fields, _ = utilitaires.get_fields(
    "etablissements-et-effectifs-salaries-au-niveau-commune-x-ape.csv", delimiter=";"
)

table = "urssaf_salaries_prive"
run(
    f"""
    DROP TABLE IF EXISTS {table};
    CREATE TABLE {table} ({fields});
"""
)

with open(
    "etablissements-et-effectifs-salaries-au-niveau-commune-x-ape.csv",
    "r",
    encoding="utf-8",
) as csv_file:
    db.copy_expert(
        f"COPY {table} FROM STDIN WITH (FORMAT csv, HEADER true, DELIMITER ';')",
        csv_file,
    )

run(f"CREATE INDEX IF NOT EXISTS {table}_depcom ON {table} (code_commune)")

utilitaires.end("emplois.py")
