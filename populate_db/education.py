from datacirco import utilitaires
from datacirco.connexion_db import db, run
import pandas as pd
from tqdm import tqdm
import json
from downloads.education import datasets_education

utilitaires.start("education.py")
"""
Attention, il y a des problèmes avec l'appariement des adresses.
Par exemple : Il y a une école "3 rue Jules Ferry - 61430 Athis-Val de Rouvre"
mais l'Open Data de l'Education nationale la localise à Athis-Mons où cette rue existe aussi.
Et dans la base adresse nationale, cette rue n'existe pas à Athis-Val de Rouvre...
Dans Open Street Map, cette rue s'appelle "rue de Condé",
sur Google il y a bien une "rue Jules Ferry", qui est plus une allée/parking qu'une rue... 

Il y a 203 établissements qui ont un appariement "Mauvais" (sur 64 000).
Pour le contourner, on va vérifier que la commune est bien dans la circonscription : cela évitera de récupérer une école qui n'est pas dans la circonscription.
Pour le cas des écoles qui ne sont pas géolocalisées dans la bonne circonscription, on va les récupérer grâce à la liste des communes.
"""


for data in tqdm(datasets_education):
    local_filename = data["filename"]
    if not local_filename.endswith(".csv"):
        continue
    utilitaires.fix_number_infile(local_filename)
    # Retire les % présents dans certains fichiers pour les taux
    utilitaires.replace_in_file(local_filename, "%", "")
    # Convertit les virgules en points pour les nombres décimaux
    # TODO: c'est pas très propre, car ça change dans les adresses aussi
    utilitaires.replace_in_file(local_filename, ",", ".")
    utilitaires.replace_in_file(local_filename, "(1);", ";")
    utilitaires.replace_in_file(local_filename, ";ND", ";")
    utilitaires.replace_in_file(local_filename, ";nd", ";")
    utilitaires.replace_in_file(local_filename, ";NS", ";")
    # Oui, il faut le faire deux fois pour que ça marche ! Mystère...
    utilitaires.replace_in_file(local_filename, ";.;", ";;")
    utilitaires.replace_in_file(local_filename, ";.;", ";;")
    table_name = data["table_name"]
    try:
        run(f"DROP TABLE IF EXISTS {table_name} CASCADE")
        fields, columns = utilitaires.get_fields(local_filename, ";")
        for c in columns:
            if (
                "nombre" in c
                or "ips" in c
                or "effectifs" in c
                or "session" in c
                or "annee" in c
                or "presents" in c
                or "taux" in c
                or "valeur" in c
            ):
                fields = fields.replace(f'"{c}" text', f'"{c}" float')

        # fields = fields.replace('"ips" text', '"ips" float')
        # fields = fields.replace('"ips_ensemble_gt_pro" text', '"ips_ensemble_gt_pro" float')
        # fields = fields.replace('"effectifs_ensemble_gt_pro" text', '"effectifs_ensemble_gt_pro" float')

        fields = fields.replace("numero_de_l_ecole", "uai")
        fields = fields.replace("numero_du_college", "uai")
        fields = fields.replace("numero_du_lycee", "uai")
        fields = fields.replace("numero_d_etablissement", "uai")
        fields = fields.replace("uai___identifiant", "uai")
        if "onisep" in table_name:
            fields = fields.replace("code_uai", "uai")
            fields = fields.replace("statut", "secteur_public_prive_libe")
            fields = fields.replace("longitude__x_", "longitude")
            fields = fields.replace("latitude__y_", "latitude")
        fields = fields.replace("ep_2022_2023", "etab_prioritaire")

        run(f"CREATE TABLE {table_name} ({fields});")
        with open(local_filename, "r") as csv_file:
            db.copy_expert(
                f"COPY {table_name} FROM STDIN WITH (FORMAT csv, HEADER true, DELIMITER ';')",
                csv_file,
            )
        run(f"CREATE INDEX IF NOT EXISTS {table_name}_id ON {table_name} (uai);")
    except Exception as e:
        print(f"Error with {local_filename} ({fields}) : {e}")
        raise e


# Insert GeoJSON features into the table
def load_etablissements(table_name="educ_etablissements_premier_et_second_degre"):
    """Importe les 64 000 établissements dans la base de données."""

    run(f"DROP TABLE IF EXISTS {table_name} CASCADE")
    run(
        f"""CREATE TABLE IF NOT EXISTS {table_name} (
        uai VARCHAR(8),
        appellation_officielle TEXT,
        denomination_principale TEXT,
        patronyme_uai TEXT,
        secteur_public_prive_libe TEXT,
        adresse_uai TEXT,
        lieu_dit_uai TEXT,
        boite_postale_uai TEXT,
        code_postal_uai VARCHAR(10),
        localite_acheminement_uai TEXT,
        libelle_commune TEXT,
        latitude DOUBLE PRECISION,
        longitude DOUBLE PRECISION,
        appariement TEXT,
        localisation TEXT,
        nature_uai INTEGER,
        nature_uai_libe TEXT,
        etat_etablissement INTEGER,
        etat_etablissement_libe TEXT,
        code_departement VARCHAR(10),
        code_region VARCHAR(10),
        code_academie VARCHAR(10),
        code_commune VARCHAR(10),
        libelle_departement TEXT,
        libelle_region TEXT,
        libelle_academie TEXT,
        secteur_prive_code_type_contrat INTEGER,
        secteur_prive_libelle_type_contrat TEXT,
        code_ministere VARCHAR(10),
        libelle_ministere TEXT,
        date_ouverture DATE
    );

        CREATE INDEX IF NOT EXISTS {table_name}_id ON {table_name} (uai);
    """
    )

    local_filename = (
        "adresse-et-geolocalisation-etablissements-premier-et-second-degre.geojson"
    )
    # Load GeoJSON data
    with open(local_filename, "r") as f:
        geojson_data = json.load(f)

    query_str = ""
    i = 0
    for feature in tqdm(geojson_data["features"]):
        json.dumps(feature["geometry"])
        properties = feature["properties"]

        # Prepare data
        non_string_fields = [
            "latitude",
            "longitude",
            "nature_uai",
            "etat_etablissement",
            "secteur_prive_code_type_contrat",
        ]

        data = []
        for col in [
            "numero_uai",
            "appellation_officielle",
            "denomination_principale",
            "patronyme_uai",
            "secteur_public_prive_libe",
            "adresse_uai",
            "lieu_dit_uai",
            "boite_postale_uai",
            "code_postal_uai",
            "localite_acheminement_uai",
            "libelle_commune",
            "latitude",
            "longitude",
            "appariement",
            "localisation",
            "nature_uai",
            "nature_uai_libe",
            "etat_etablissement",
            "etat_etablissement_libe",
            "code_departement",
            "code_region",
            "code_academie",
            "code_commune",
            "libelle_departement",
            "libelle_region",
            "libelle_academie",
            "secteur_prive_code_type_contrat",
            "secteur_prive_libelle_type_contrat",
            "code_ministere",
            "libelle_ministere",
            "date_ouverture",
        ]:
            if col in non_string_fields:
                value = properties.get(col, "NULL")
                if value:
                    data.append(value)
                else:
                    data.append("NULL")
            else:
                value = properties.get(col, "NULL")
                if value:
                    data.append("'" + value.replace("'", r"''") + "'")
                else:
                    data.append("NULL")

        data_string = ", ".join(str(info) for info in data)
        # print(data_string)

        # Prepare SQL query string with placeholders for each column
        query_str += f"""
            INSERT INTO {table_name} (
                uai, appellation_officielle, denomination_principale, patronyme_uai,
                secteur_public_prive_libe, adresse_uai, lieu_dit_uai, boite_postale_uai,
                code_postal_uai, localite_acheminement_uai, libelle_commune, latitude,
                longitude, appariement, localisation, nature_uai, nature_uai_libe,
                etat_etablissement, etat_etablissement_libe, code_departement, code_region,
                code_academie, code_commune, libelle_departement, libelle_region,
                libelle_academie, secteur_prive_code_type_contrat, secteur_prive_libelle_type_contrat,
                code_ministere, libelle_ministere, date_ouverture
            ) VALUES ({data_string});
        """
        i += 1
        if i > 1000:
            # Insert data
            run(query_str)
            query_str = ""
            i = 0
    # Insert remaining data
    run(query_str)
    query_str = ""


table_name = "educ_etablissements_premier_et_second_degre"
load_etablissements(table_name)

run(
    f"""
    ALTER TABLE {table_name} ADD geom geometry;
    UPDATE {table_name} SET geom=ST_SetSRID(ST_Makepoint(longitude::numeric, latitude::numeric),4326);
    CREATE INDEX {table_name}_geom ON {table_name} USING GIST (geom);
    CLUSTER {table_name} USING {table_name}_geom;
"""
)
"""On crée une copie des données des établissments en ajoutant la
circonscription.

Cela va nous permettre de corriger les problème d'appariement des
adresses.
"""


run(
    """
DROP TABLE IF EXISTS educ_etablissements CASCADE;
CREATE TABLE educ_etablissements AS
select c.ref as circo, et.uai, et.appellation_officielle, et.denomination_principale, et.patronyme_uai, LOWER(et.secteur_public_prive_libe) as secteur_public_prive_libe, et.adresse_uai,
	et.libelle_commune, et.latitude, et.longitude, et.nature_uai, et.nature_uai_libe, et.code_departement, et.code_region, et.code_academie,
	et.code_commune, et.secteur_prive_code_type_contrat, et.secteur_prive_libelle_type_contrat, et.code_ministere, et.libelle_ministere,
	et.date_ouverture, et.appariement
FROM zone_circo c
		join educ_etablissements_premier_et_second_degre et on (st_intersects(c.wkb_geometry, et.geom))
WHERE et.etat_etablissement = 1 /*'OUVERT'*/
ORDER BY circo, libelle_commune;
"""
)

utilitaires.trace("Corrige les appariements des établissements")
utilitaires.db_exec(
    utilitaires.db.mogrify(
        """
                select uai, code_commune, circo from educ_etablissements
WHERE appariement = 'Mauvaise'
                """
    )
)
bad_appariement = utilitaires.db.fetchall()
df = pd.DataFrame(bad_appariement)
df.columns = [col[0] for col in utilitaires.db.description]


def corrige_appariement(row):
    uai = row["uai"]
    code_commune = row["code_commune"]
    circo = row["circo"]
    # La commune est-elle dans la circonscription ?

    db.execute(
        db.mogrify(
            """
        select insee_com
        from circo_commune
        where circo = %s
        """,
            (circo,),
        )
    )
    communes = db.fetchall()
    communes = [c[0] for c in communes]
    if code_commune not in communes:
        # Il faut changer le code circo
        # Dans quelle comune est cette circonscription ?
        db.execute(
            db.mogrify(
                """
                select circo
                from circo_commune
                where insee_com = %s
            """,
                (code_commune,),
            )
        )
        new_circo = db.fetchall()
        new_circo = [c[0] for c in new_circo]
        replace_circo = new_circo[0] if len(new_circo) > 0 else None
        if len(new_circo) > 1:
            if uai == "9740295C":
                replace_circo = "974-02"
            else:
                print(
                    f"WARNING : Deux circos pour une même commune {uai=} : {new_circo}"
                )
                return

        if len(new_circo) == 0:
            if uai in ["9711288Y", "9711323L"]:
                # (normal pour 978 car DataCirco ne couvre pas Saint-Barthélemy et Saint-Martin)
                return
            print(f"WARNING : Pas de circo pour {uai=} {circo=} {code_commune=} !")
            return

        run(
            f"""
            UPDATE educ_etablissements
            SET circo = '{replace_circo}'
            WHERE uai = '{uai}'
            """
        )


for row in df.iterrows():
    corrige_appariement(row[1])

print("Création des vues")

run(
    """
CREATE OR REPLACE VIEW educ_college_effectifs_last_year AS
SELECT * FROM educ_college_effectifs WHERE rentree_scolaire = (SELECT max(rentree_scolaire) FROM educ_college_effectifs);
"""
)
run(
    """
CREATE OR REPLACE VIEW educ_dnb_par_etablissement_last_year AS
SELECT * FROM educ_dnb_par_etablissement WHERE session = (SELECT max(session) FROM educ_dnb_par_etablissement);
"""
)
run(
    """
CREATE OR REPLACE VIEW educ_ecoles_effectifs_nb_classes_last_year AS
SELECT * FROM educ_ecoles_effectifs_nb_classes WHERE rentree_scolaire = (SELECT max(rentree_scolaire) FROM educ_ecoles_effectifs_nb_classes);
"""
)
run(
    """
CREATE OR REPLACE VIEW educ_indicateurs_resultat_lycees_last_year AS
SELECT * FROM educ_indicateurs_resultat_lycees WHERE annee = (SELECT max(annee) FROM educ_indicateurs_resultat_lycees);
"""
)
run(
    """
CREATE OR REPLACE VIEW educ_indicateurs_resultat_lycees_pro_last_year AS
SELECT * FROM educ_indicateurs_resultat_lycees_pro WHERE annee = (SELECT max(annee) FROM educ_indicateurs_resultat_lycees_pro);
"""
)
run(
    """
CREATE OR REPLACE VIEW educ_indicateurs_valeur_ajoutee_colleges_last_year AS
SELECT * FROM educ_indicateurs_valeur_ajoutee_colleges WHERE session = (SELECT max(session) FROM educ_indicateurs_valeur_ajoutee_colleges);
"""
)
# IPS
run(
    """
CREATE OR REPLACE VIEW educ_ips_colleges_last_year AS
SELECT * FROM educ_ips_colleges WHERE rentree_scolaire = (SELECT max(rentree_scolaire) FROM educ_ips_colleges);
"""
)
run(
    """
CREATE OR REPLACE VIEW educ_ips_ecoles_last_year AS
SELECT * FROM educ_ips_ecoles WHERE rentree_scolaire = (SELECT max(rentree_scolaire) FROM educ_ips_ecoles);
"""
)
run(
    """
CREATE OR REPLACE VIEW educ_ips_erea_last_year AS
SELECT * FROM educ_ips_erea WHERE rentree_scolaire = (SELECT max(rentree_scolaire) FROM educ_ips_erea);
"""
)
run(
    """
CREATE OR REPLACE VIEW educ_ips_lycees_last_year AS
SELECT * FROM educ_ips_lycees WHERE rentree_scolaire = (SELECT max(rentree_scolaire) FROM educ_ips_lycees);
"""
)
run(
    """
CREATE OR REPLACE VIEW educ_uai_ips AS
SELECT uai, ips FROM educ_ips_colleges_last_year
UNION
SELECT uai, ips FROM educ_ips_ecoles_last_year
UNION
SELECT uai, ips FROM educ_ips_erea_last_year
UNION
SELECT uai, ips_ensemble_gt_pro as ips FROM educ_ips_lycees_last_year;
"""
)

# Effectifs
run(
    """
CREATE OR REPLACE VIEW educ_lycee_gt_effectifs_last_year AS
SELECT * FROM educ_lycee_gt_effectifs WHERE rentree_scolaire = (SELECT max(rentree_scolaire) FROM educ_lycee_gt_effectifs);
"""
)
run(
    """
CREATE OR REPLACE VIEW educ_lycee_pro_effectifs_last_year AS
SELECT * FROM educ_lycee_pro_effectifs WHERE rentree_scolaire = (SELECT max(rentree_scolaire) FROM educ_lycee_pro_effectifs);
"""
)
run(
    """
CREATE OR REPLACE VIEW educ_effectifs AS
SELECT uai, nombre_d_eleves_total as effectifs, NULL as nb_classes FROM educ_college_effectifs_last_year
UNION
SELECT uai, nombre_total_d_eleves as effectifs, educ_ecoles_effectifs_nb_classes_last_year.nombre_total_de_classes as nb_classes  FROM educ_ecoles_effectifs_nb_classes_last_year
UNION
SELECT uai, nombre_d_eleves as effectifs, NULL as nb_classes FROM educ_lycee_gt_effectifs_last_year
UNION
SELECT uai, nombre_d_eleves as effectifs, NULL as nb_classes FROM educ_lycee_pro_effectifs_last_year;
"""
)

# Vue détaillée
run(
    """
DROP VIEW IF EXISTS educ_etablissements_details;
CREATE OR REPLACE VIEW educ_etablissements_details AS
select ep.etab_prioritaire, educ_uai_ips.ips, educ_effectifs.effectifs,
	/* Taux de réussite aux examens */
	COALESCE(brevet.taux_de_reussite, bac.taux_de_reussite___toutes_series, bac_pro.taux_de_reussite___toutes_series) as taux_de_reussite,
	col_va.nb_mentions_global___g as brevet_mentions,
	/* Valeur ajoutée */
	COALESCE(col_va.va_du_taux_de_reussite___g, bac.valeur_ajoutee_du_taux_de_reussite___toutes_series, bac_pro.valeur_ajoutee_du_taux_de_reussite___toutes_series) as valeur_ajoutee,
	et.*
FROM  educ_etablissements et
left join educ_uai_ips on et.uai = educ_uai_ips.uai
left join educ_effectifs on et.uai = educ_effectifs.uai
left join educ_etab_prioritaires ep on et.uai = ep.uai
left join educ_dnb_par_etablissement_last_year brevet on et.uai = brevet.uai
left join educ_indicateurs_resultat_lycees_last_year bac on et.uai = bac.uai
left join educ_indicateurs_resultat_lycees_pro_last_year bac_pro on et.uai = bac_pro.uai
left join educ_indicateurs_valeur_ajoutee_colleges_last_year col_va on et.uai = col_va.uai
ORDER BY circo,libelle_commune; 
"""
)

###############################################################################""
# Etablissement enseignement superieur, géolocalisation
table_name = "educ_onisep_structures_denseignement_superieur"
run(
    f"""
    ALTER TABLE {table_name}
ADD COLUMN circo char(6);
"""
)

run(
    f"""
    ALTER TABLE {table_name} ADD geom geometry;
    UPDATE {table_name} SET geom=ST_SetSRID(ST_Makepoint(longitude::numeric, latitude::numeric),4326);
    CREATE INDEX {table_name}_geom ON {table_name} USING GIST (geom);
    CLUSTER {table_name} USING {table_name}_geom;
"""
)
run(
    f"""
UPDATE {table_name} SET circo = geoloc.ref
FROM (
    select c.wkb_geometry, c.ref
    FROM zone_circo c
            ) AS geoloc
WHERE ST_Intersects(geoloc.wkb_geometry, {table_name}.geom);
"""
)

run(
    """
DROP VIEW IF EXISTS educ_etablissements_superieur;
CREATE VIEW educ_etablissements_superieur AS
    SELECT onisep.uai, n_siret as siret, onisep.type_d_etablissement as type_etablissement, nom as appellation_officielle, onisep.sigle, LOWER(onisep.secteur_public_prive_libe) as secteur_public_prive_libe, tutelle, universite_de_rattachement_libelle_et_uai as universite_de_rattachement_libelle,
	universite_de_rattachement_id_et_url_onisep, etablissements_lies_libelles, etablissements_lies_url_et_id_onisep,
	bote_postale as boite_postale, onisep.adresse as adresse_uai, cp as code_postal, onisep.commune as libelle_commune, commune__cog_ as code_commune, cedex, telephone, arrondissement, onisep.departement, onisep.academie,
	onisep.region, region__cog_ as code_region, journees_portes_ouvertes, label_generation_2024, url_et_id_onisep,
	typologie_d_universites_et_assimiles, site_internet, date_creation, 
    effectifs_d_etudiants_inscrits_2022_23::numeric as effectifs, longitude, latitude, circo
    FROM educ_onisep_structures_denseignement_superieur onisep LEFT JOIN educ_principaux_etablissements_enseignement_superieur ens ON onisep.uai = ens.uai
    WHERE circo IS NOT NULL
	/* AND tutelle != 'Ministère chargé de l''Éducation nationale et de la Jeunesse' */
    ORDER BY circo, onisep.commune;
"""
)

utilitaires.end("education.py")
