from datacirco.utilitaires import get_fields
from datacirco import utilitaires
from tqdm import tqdm
import pandas as pd
from datacirco.connexion_db import db, run, engine
import glob
import os

utilitaires.start("logement.py")

print(" ------------- Logements (Insee)")
annees = ["2018", "2020"]
for annee in annees:
    print(f"\tBase logement {annee}")
    utilitaires.unzip_file(f"base-ic-logement-{annee}_csv.zip")
    fields, _ = utilitaires.get_fields(f"base-ic-logement-{annee}.CSV", ";")

    run(
        f"""
        DROP TABLE IF EXISTS insee_logement_{annee} CASCADE;
        CREATE TABLE insee_logement_{annee} ({fields});
    """
    )
    with open(f"base-ic-logement-{annee}.CSV", "r") as f:
        db.copy_expert(
            f"COPY insee_logement_{annee} FROM STDIN WITH (FORMAT CSV, HEADER TRUE, DELIMITER ';')",
            f,
        )

    run(
        f"CREATE INDEX IF NOT EXISTS insee_logement_{annee}_iris ON insee_logement_{annee} (iris);"
    )


print("Import de ademe_dpe_logements_pre_2021")
fields, _ = utilitaires.get_fields("dpe_01.csv", ",")
run(
    f"""
    DROP TABLE IF EXISTS ademe_dpe_logements CASCADE;
    CREATE TABLE IF NOT EXISTS ademe_dpe_logements ({fields}); 
    TRUNCATE ademe_dpe_logements;
"""
)

for d in tqdm([*range(1, 20), "2a", "2b", *range(21, 96)]):
    with open(f"dpe_{str(d).zfill(2)}.csv", "r") as f:
        db.copy_expert(
            "COPY ademe_dpe_logements FROM STDIN WITH (FORMAT CSV, HEADER TRUE)", f
        )
table_name = "ademe_dpe_logements"
print("Création des géométries : c'est long...")
run(
    f"""
    ALTER TABLE {table_name} ADD geom geometry;
    UPDATE {table_name} SET geom=ST_SetSRID(ST_Makepoint(longitude::numeric, latitude::numeric),4326);
    CREATE INDEX {table_name}_geom ON {table_name} USING GIST (geom);
    CLUSTER {table_name} USING {table_name}_geom;
    -- Pour les filtres principaux
    CREATE INDEX idx_dpe_code_insee ON ademe_dpe_logements(code_insee_commune_actualise);
    CREATE INDEX idx_dpe_date ON ademe_dpe_logements((LEFT(date_etablissement_dpe, 4)));
    CREATE INDEX idx_dpe_classe_ges ON ademe_dpe_logements(classe_estimation_ges);
"""
)

run(
    """
    CREATE INDEX IF NOT EXISTS ademe_dpe_logements_depcom ON ademe_dpe_logements (code_insee_commune_actualise);
    DROP MATERIALIZED VIEW IF EXISTS ademe_dpe_logements_stats;
    CREATE MATERIALIZED VIEW ademe_dpe_logements_stats AS
    SELECT
        classe_consommation_energie,
        left(date_etablissement_dpe,4) as annee,
        code_insee_commune_actualise,
        count(*) as nb
    FROM ademe_dpe_logements
    GROUP BY 1,2,3;
"""
)

print("-- correction code Insee incorrects")
run(
    """
    with u as (
        select d.code_postal, commune, code_insee_commune_actualise, cp.code_commune_insee
        from ademe_dpe_logements d
        join laposte_cp cp
            on (
                cp.code_postal=trim(d.code_postal)
                and libelle_d_acheminement=trim(regexp_replace(upper(unaccent(commune)),'[^A-Z]+',' ','g'))
            )
        where code_insee_commune_actualise !~ '^\d(\d|[AB])\d\d\d$' group by 1,2,3,4
    )
    update ademe_dpe_logements o
    set code_insee_commune_actualise=u.code_commune_insee
    from u
    where o.code_insee_commune_actualise=u.code_insee_commune_actualise
        and o.code_postal=u.code_postal
        and o.commune=u.commune;
"""
)

run(
    """
    with u as (
    select d.code_postal, commune, code_insee_commune_actualise,cp.code_commune_insee
    from ademe_dpe_logements d
    join laposte_cp cp on (cp.code_postal=trim(d.code_postal)
        and libelle_d_acheminement=trim(regexp_replace(upper(unaccent(commune)),'[^A-Z]+',' ','g')))
    left join ign_commune c on (c.insee_com = d.code_insee_commune_actualise)
    left join ign_arrondissement_municipal a on (insee_arm = d.code_insee_commune_actualise)
    where c.insee_com is null
        and a.insee_arm is null
    group by 1,2,3,4
    )
    update ademe_dpe_logements o
    set code_insee_commune_actualise=u.code_commune_insee
    from u
    where o.code_insee_commune_actualise=u.code_insee_commune_actualise
        and o.code_postal=u.code_postal
        and o.commune=u.commune;
"""
)


print(
    "Creation d'une MATERIALIZED VIEW pour les DPE 2013 à 2021, cela va être long... 14 minutes"
)
run(
    """
-- Création d'une table de liaison circonscription/communes
CREATE MATERIALIZED VIEW mv_circo_communes AS
SELECT ref, unnest(communes) AS code_insee 
FROM zone_circo;

-- Index pour les jointures sur les codes INSEE
CREATE INDEX idx_mv_circo_communes ON mv_circo_communes(ref, code_insee);

-- Vue matérialisée principale
CREATE MATERIALIZED VIEW mv_dpe_stats_2013_2021 AS
SELECT 
    COALESCE(c1.ref, c2.ref) AS circonscription,
    d.classe_estimation_ges,
    LEFT(d.date_etablissement_dpe,4) AS annee,
    COUNT(DISTINCT d.numero_dpe) AS nb_ges,
    BOOL_OR(d.geom IS NOT NULL) AS via_geom,
    BOOL_OR(d.geom IS NULL) AS via_insee
FROM ademe_dpe_logements d
LEFT JOIN zone_circo c1 
    ON d.geom IS NOT NULL 
    AND ST_Intersects(c1.wkb_geometry, d.geom)
LEFT JOIN mv_circo_communes c2 
    ON d.geom IS NULL 
    AND d.code_insee_commune_actualise = c2.code_insee
WHERE d.classe_estimation_ges IN ('A', 'B', 'C', 'D', 'E', 'F', 'G')
GROUP BY 1, 2, 3;

CREATE INDEX idx_mv_dpe_circo ON mv_dpe_stats_2013_2021(circonscription);
CREATE INDEX idx_mv_dpe_annee ON mv_dpe_stats_2013_2021(annee);
CREATE INDEX idx_mv_dpe_classe ON mv_dpe_stats_2013_2021(classe_estimation_ges);

"""
)

print("Base DPE des logements 2024")

table = "ademe_dpe_logements_2024"


# Donner le bon nom de table
fields, header = get_fields("dpe_logement_ademe_100.csv", ",")

# Creation de la table
run(
    f"""
    DROP TABLE IF EXISTS {table};
    CREATE TABLE {table} ({fields});
    TRUNCATE {table};
"""
)

# Import de chaque dataframe dans la table
for file_path in tqdm(glob.iglob("dpe_logement_ademe_*.csv")):
    fields, header = get_fields(file_path, ",")
    with open(file_path, "r") as csv_file:
        print("Reading", file_path)
        if os.stat(file_path).st_size < 31_488_530:
            df = pd.read_csv(csv_file)
            df.columns = header
            df.to_sql(table, con=engine, if_exists="append", index=False)
        else:
            for chunk in pd.read_csv(csv_file, chunksize=10000):
                chunk.columns = header
                chunk.to_sql(table, con=engine, if_exists="append", index=False)

print("Tables_chargées")

# On vérifie maintenant le nombre de lignes qu'il y a dans la table SQL:
try:
    db.execute(""" SELECT count(*) FROM ademe_dpe_logements_2024;""")
    print(
        "Il y a "
        + str(db.fetchone())
        + "observations dans la table ademe_dpe_logements_2024"
    )
except Exception as e:
    print(e)

# Creation d'une variable géometrie par transformation des longitudes et latitude de la table initiale
run(
    """
    ALTER TABLE ademe_dpe_logements_2024 ADD COLUMN geom geometry(Point, 4326);
    UPDATE ademe_dpe_logements_2024
    SET geom= ST_Transform(ST_SetSRID(ST_MakePoint(
                CAST(coordonnee_cartographique_x__ban_ AS FLOAT)::numeric,
                CAST(coordonnee_cartographique_y__ban_ AS FLOAT)::numeric), 2154), 4326) ;
    CREATE INDEX ademe_dpe_logements_2024_geom ON ademe_dpe_logements_2024 USING GIST (geom);
    CLUSTER ademe_dpe_logements_2024 USING ademe_dpe_logements_2024_geom;
    """
)

# On vérifie que la base SQL ait bien une colonne géométrie et que son SRID soit bien au format 4326
db.execute(""" SELECT st_SRID(geom) FROM ademe_dpe_logements_2024 LIMIT 1;""")
print("Le srid est " + str(db.fetchone()[0]))


print(
    "On crée une table qui regroupe les informations sur toute la France pour les DPE"
)
# Creation de la table
run("DROP TABLE IF EXISTS ademe_dpe_france_2024;")
run(
    """CREATE TABLE ademe_dpe_france_2024  AS  SELECT
            etiquette_dpe,
            count(DISTINCT ndpe)
        FROM
            ademe_dpe_logements_2024 d
        WHERE etiquette_dpe between 'A' and 'G'
        AND CAST(d.date_etablissement_dpe as VARCHAR) NOT LIKE '2024%'
        GROUP BY 1;
        """
)

run("DROP TABLE IF EXISTS ademe_ges_france_2024;")
run(
    """CREATE TABLE ademe_ges_france_2024  AS  SELECT
            etiquette_dpe,
            count(DISTINCT ndpe)
        FROM
            ademe_dpe_logements_2024 d
        WHERE etiquette_ges between 'A' and 'G'
        AND CAST(d.date_etablissement_dpe as VARCHAR) NOT LIKE '2024%'
        GROUP BY 1;
        """
)

print("Création d'une table pour agréger les DPE par circonscription")
run(
    """
    DROP TABLE IF EXISTS dpe_stats_by_circo;
    CREATE TABLE dpe_stats_by_circo (
        circonscription text,
        classe_consommation_energie text,
        annee_2013 int,
        annee_2014 int,
        annee_2015 int,
        annee_2016 int,
        annee_2017 int,
        annee_2018 int,
        annee_2019 int,
        annee_2020 int,
        annee_2021 int,
        PRIMARY KEY (circonscription, classe_consommation_energie)
    );
    """
)
print("Add communes array to zone_circo if it doesn't exist")
run(
    """
    DO $$ 
    BEGIN
        IF NOT EXISTS (SELECT 1 FROM information_schema.columns 
                    WHERE table_name = 'zone_circo' AND column_name = 'communes') THEN
            ALTER TABLE zone_circo ADD COLUMN communes text[];
        END IF;
    END $$;
    """
)
print("Update communes array in zone_circo")
run(
    """
UPDATE zone_circo z SET communes = (
    SELECT array_agg(insee_com)
    FROM (
        SELECT insee_com
        FROM ign_commune co
        WHERE st_intersects(co.wkb_geometry, z.wkb_geometry) 
        AND st_area(st_intersection(co.wkb_geometry, z.wkb_geometry)::geography) / st_area(co.wkb_geometry::geography) > 0.1
        UNION
        SELECT insee_arm
        FROM ign_arrondissement_municipal co
        WHERE st_intersects(co.wkb_geometry, z.wkb_geometry) 
        AND st_area(st_intersection(co.wkb_geometry, z.wkb_geometry)::geography) / st_area(co.wkb_geometry::geography) > 0.1
    ) sub
);
"""
)
print(
    "Populate the statistics table dpe_stats_by_circo - Took 45 minutes, be patient..."
)
run(
    """
    TRUNCATE TABLE dpe_stats_by_circo;
    INSERT INTO dpe_stats_by_circo
    WITH combined_data AS (
        -- Spatial intersection for all circonscriptions
        SELECT c.ref as circonscription,
            d.classe_consommation_energie, 
            LEFT(d.date_etablissement_dpe,4) as annee_etablissement_DPE,
            COUNT(DISTINCT d.numero_dpe) as nb_dpe
        FROM ademe_dpe_logements as d
        JOIN zone_circo c ON st_intersects(c.wkb_geometry, d.geom)
        WHERE LEFT(d.date_etablissement_dpe,4) between '2013' and '2021'
        AND d.classe_consommation_energie between 'A' and 'G'
        GROUP BY 1,2,3

        UNION ALL

        -- NULL geometry cases using the new communes array
        SELECT c.ref as circonscription,
            d.classe_consommation_energie,
            LEFT(d.date_etablissement_dpe,4) as annee_etablissement_DPE,
            COUNT(DISTINCT d.numero_dpe) as nb_dpe
        FROM ademe_dpe_logements d
        JOIN zone_circo c ON d.code_insee_commune_actualise = ANY(c.communes)
        WHERE d.geom IS NULL 
        AND LEFT(d.date_etablissement_dpe,4) between '2013' and '2021'
        AND d.classe_consommation_energie between 'A' and 'G'
        GROUP BY 1,2,3
    )
    SELECT circonscription, 
        classe_consommation_energie,
        COALESCE(SUM(CASE WHEN annee_etablissement_DPE = '2013' THEN nb_dpe END), 0) as annee_2013,
        COALESCE(SUM(CASE WHEN annee_etablissement_DPE = '2014' THEN nb_dpe END), 0) as annee_2014,
        COALESCE(SUM(CASE WHEN annee_etablissement_DPE = '2015' THEN nb_dpe END), 0) as annee_2015,
        COALESCE(SUM(CASE WHEN annee_etablissement_DPE = '2016' THEN nb_dpe END), 0) as annee_2016,
        COALESCE(SUM(CASE WHEN annee_etablissement_DPE = '2017' THEN nb_dpe END), 0) as annee_2017,
        COALESCE(SUM(CASE WHEN annee_etablissement_DPE = '2018' THEN nb_dpe END), 0) as annee_2018,
        COALESCE(SUM(CASE WHEN annee_etablissement_DPE = '2019' THEN nb_dpe END), 0) as annee_2019,
        COALESCE(SUM(CASE WHEN annee_etablissement_DPE = '2020' THEN nb_dpe END), 0) as annee_2020,
        COALESCE(SUM(CASE WHEN annee_etablissement_DPE = '2021' THEN nb_dpe END), 0) as annee_2021
    FROM combined_data
    GROUP BY 1,2;

    """
)
run(
    """
    CREATE INDEX IF NOT EXISTS idx_dpe_stats_circo ON dpe_stats_by_circo(circonscription);
    """
)

utilitaires.end("logement.py")
