from datacirco import utilitaires
from datacirco.connexion_db import run, engine, database_command_line
import pandas as pd
import os
import glob
import shutil

module = "environnement"
utilitaires.start(f"{module}.py")


def effacer_dir(chemin):
    base_dir = os.getcwd()
    # Utiliser glob pour rechercher des chemins contenant la chaîne spécifiée
    pattern = os.path.join(base_dir, "**", f"*{chemin}*")
    matching_paths = glob.glob(pattern, recursive=True)
    # Afficher les chemins trouvés
    for path in matching_paths:
        if path:
            if path[-3:] != "zip":
                shutil.rmtree(path)


print("=== Import SQL pour la section Occupation des sols et paysages ===")
table = "sdes_occupation_sols_2012"

# Dézippage des données métropolitaines
utilitaires.unzip_file("CLC12_FR_RGF_SHP.zip")

# On importe d'abord les données métropolitaines
utilitaires.execute_command(
    f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ CLC12_FR_RGF_SHP/CLC12_FR_RGF.shp -s_srs EPSG:2154 -t_srs EPSG:4326 -nln {table} -lco CREATE_TABLE=YES > sdes_occupation_metropole.sql"
)
# On gère la taille de la variable de nomenclature
utilitaires.execute_command(
    """sed -i 's/"code_12" VARCHAR(3);/"code_12" VARCHAR(4);/' sdes_occupation_metropole.sql"""
)
utilitaires.execute_command(
    f"{database_command_line} -b -f sdes_occupation_metropole.sql"
)

# On gère les tables de nomenclature
nomenclature_metro = pd.read_excel(
    "CLC12_FR_RGF_SHP/Metadonnees/CLC_nomenclature.xls",
    sheet_name="nomenclature_clc_niveau_3",
)
nomenclature_metro["couleur"] = nomenclature_metro.apply(
    lambda x: "#{:02X}{:02X}{:02X}".format(x["rouge"], x["vert"], x["bleu"]), axis=1
)
nomenclature_metro.drop(columns=["libelle_en", "rouge", "vert", "bleu"], inplace=True)
nomenclature_metro = nomenclature_metro.rename(
    columns={"code_clc_niveau_3": "code_clc"}
)

sdes_occupation_sols_2012_nomenclature = nomenclature_metro.copy()
sdes_occupation_sols_2012_nomenclature = sdes_occupation_sols_2012_nomenclature.rename(
    columns={"code_clc": "code_occupation", "libelle_fr": "nom_occupation"}
)
sdes_occupation_sols_2012_nomenclature.to_sql(
    name="sdes_occupation_sols_2012_nomenclature", con=engine, if_exists="replace"
)

nomenclature_drom = pd.read_excel(
    "CLC12_FR_RGF_SHP/Metadonnees/CLC_nomenclature.xls",
    sheet_name="nomenclature_clc_niveau_4_DOM",
)
nomenclature_drom["couleur"] = nomenclature_drom.apply(
    lambda x: "#{:02X}{:02X}{:02X}".format(x["rouge"], x["vert"], x["bleu"]), axis=1
)
nomenclature_drom.drop(columns=["libelle_en", "rouge", "vert", "bleu"], inplace=True)
nomenclature_drom = nomenclature_drom.rename(columns={"code_clc_niveau_4": "code_clc"})
nomenclature = pd.concat([nomenclature_drom, nomenclature_metro])
nomenclature.to_sql(name="nomenclature", con=engine, if_exists="replace")

effacer_dir("CLC12_FR_RGF_SHP")

# Dézippage des données de DROM-COM
utilitaires.unzip_file("CLC12_DOM_UTM_SHP.zip")
# On ajoute à la table les communes outre-mers
for dep in ["971", "972", "973", "974", "976"]:
    utilitaires.execute_command(
        f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ CLC12_DOM_UTM_SHP/CLC12_D{dep}/CLC12_D{dep}_UTM.shp -t_srs EPSG:4326 -nln {table} -lco CREATE_TABLE=NO| {database_command_line}"
    )
effacer_dir("CLC12_DOM_UTM_SHP")

# On ajoute à la table la colonne qui détermine le label de l'espace et sa couleur
run(
    f"""ALTER TABLE {table}
    ADD COLUMN libelle_fr TEXT;
    ALTER TABLE {table}
    ADD COLUMN couleur VARCHAR(7);
    UPDATE {table}
    SET (libelle_fr, couleur) = (
    SELECT nomenclature.libelle_fr, nomenclature.couleur
    FROM nomenclature
    WHERE {table}.code_12 = CAST(nomenclature.code_clc AS VARCHAR(4)));
    DROP TABLE nomenclature;"""
)
# On efface les tables dont on n'a plus besoin
os.remove(
    "/home/datacirco/datacirco-prepare-data/data/environnement/sdes_occupation_metropole.sql"
)
"""Il y a un problème de codification entre le niveau métropole et le niveau
DROM-COM. La codification DROM-COM est en 4 chiffres, plus précise, par exemple
pour la canne à sucre, alors que la codification métropole est en 3 chiffres.
Par mesure de simplicité, on ne garde que la codification métropole.

Pour cela il faut :
- Créer une nouvelle table qui supprimer les codes des DROM-COM mais garde les libellés
- Réduire la taille de la variable de codification à 3 chiffres
"""
run(
    """
DROP TABLE IF EXISTS sdes_occupation_sols_2012_metro_drom;
CREATE TABLE sdes_occupation_sols_2012_metro_drom AS
SELECT 
    ogc_fid,
    wkb_geometry,
    id,
    CASE 
        WHEN LENGTH(code_12) = 4 THEN LEFT(code_12, 3)
        ELSE code_12
    END as code_12,
    area_ha,
    libelle_fr,
    couleur
FROM sdes_occupation_sols_2012;
"""
)
run(
    """
ALTER TABLE sdes_occupation_sols_2012_metro_drom 
ALTER COLUMN code_12 TYPE bigint 
USING code_12::bigint;
DROP TABLE IF EXISTS sdes_occupation_sols_2012_metro_drom_aggregated;
CREATE TABLE sdes_occupation_sols_2012_metro_drom_aggregated AS
SELECT 
    t.code_12 as code_occupation, 
    clc.nom_occupation AS nom_occupation,
    (SELECT SUM(st_area(wkb_geometry::geography))/10000 
     FROM sdes_occupation_sols_2012_metro_drom) as surface_totale_france_ha,
    SUM(st_area(t.wkb_geometry::geography))/10000 as aire_oc_ha_france,
    (SUM(st_area(t.wkb_geometry::geography)) / 
     (SELECT SUM(st_area(wkb_geometry::geography)) 
      FROM sdes_occupation_sols_2012_metro_drom)) as part_superficie_france
FROM sdes_occupation_sols_2012_metro_drom as t
INNER JOIN sdes_occupation_sols_2012_nomenclature as clc ON clc.code_occupation = t.code_12
GROUP BY t.code_12, clc.nom_occupation
ORDER BY t.code_12;
"""
)


print("====== Import SQL des données de consommation des sols ===")
table = "cerema_artificalisation_2023"
df_arti = pd.read_csv("conso_sols_2009_2023_resultats_com.csv", sep=";")
df_arti.assign(
    idcom=lambda x: x["idcom"].astype(str).str.rjust(5, fillchar="0"),
    iddep=lambda x: x["iddep"].astype(str).str.rjust(2, fillchar="0"),
    inplace=True,
).to_sql(name=table, con=engine, index=False, if_exists="replace")

print("====== Import SQL pour la section Risques ===")

print("Import dans la base SQL des données sites et sols pollués")
table = "sites_pollues"
df_sites_pollues = pd.read_excel(
    "site_pollues.xlsx", na_values=["NaN", "nan", "NaN", ""], sheet_name="Instructions"
)
df_sites_pollues.columns = [
    utilitaires.clean_string(nom_col) for nom_col in df_sites_pollues.columns
]
df_sites_pollues.drop(
    columns=["code_icpe", "naf", "naf_rev1", "naf_rev2"], inplace=True
)
print(df_sites_pollues.shape)
# On y intègre les actions mises en place
df_sites_pollues_actions = pd.read_excel(
    "site_pollues.xlsx", na_values=["NaN", "nan", "NaN", ""], sheet_name="Actions"
)
df_sites_pollues_actions.columns = [
    utilitaires.clean_string(nom_col) for nom_col in df_sites_pollues_actions.columns
]
df_sites_pollues_actions.drop(
    columns=[
        "etude",
        "frequence_surveillance__nombre_par_an_",
        "nom_parametre_sandre",
        "nom_groupe_sandre",
        "mesure_de_gestion",
        "mesure_de_securite",
        "traitement_sur_site__hors_site",
        "traitement_in_situ",
        "traitement_rejets",
        "date_debut",
        "date_fin",
        "description",
    ],
    inplace=True,
)
df_sites_pollues_actions = df_sites_pollues_actions.groupby("code_metier").sum()
print(df_sites_pollues_actions.shape)
df_sql = df_sites_pollues.merge(
    df_sites_pollues_actions, left_on="code_metier", right_on="code_metier", how="left"
)
df_sql.to_sql(name=table, con=engine, index=False, if_exists="replace")
run(
    f"""
    ALTER TABLE {table} ADD geom geometry(Point, 4326);
    UPDATE {table} SET geom=ST_SetSRID(ST_Makepoint(coordonnee_x::numeric, coordonnee_y::numeric),4326);
    CREATE INDEX {table}_geom ON {table} USING GIST (geom);
    CLUSTER {table} USING {table}_geom;
"""
)
# Si on souhaite utiliser un fichier shapefile

# utilitaires.unzip_file("site_pollues.zip")
# utilitaires.execute_command(
#     f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ Export_shp_polygone_National_20240807.shp -t_srs EPSG:4326 -nln {table}| {database_command_line}"
# )
# utilitaires.execute_command(
#     f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ Export_shp_point_National_20240807.shp -t_srs EPSG:4326 -nln {table} -lco CREATE_TABLE=NO| {database_command_line}"
# )

print("Import dans la base SQL de la localisation retrait-gonflement des argiles")
table = "sdes_argiles_2021"
df_sites_pollues = pd.read_excel(
    "indicateur_retrait_gonflement_argiles.xlsx", sheet_name="RGA_2019_commune"
)
# TO DO : vérifier s'il est intérressant d'exporter également à d'autres échelles que celle de la commune.
df_sites_pollues.to_sql(table, con=engine, index=False, if_exists="replace")

# print("Import SQL de la base des risques radons")

utilitaires.end(f"{module}.py")
