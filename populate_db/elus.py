from datacirco import utilitaires
import glob
from datacirco.connexion_db import run, engine
import pandas as pd
from tqdm import tqdm

utilitaires.start("elus.py")

print("répertoire national des élu(e)s")
files = glob.glob("rne/*.csv")
for file in tqdm(files):
    fields = ""
    try:
        table = file.split("/")[-1].replace("-", "_").split(".")[0]
        run(f"DROP TABLE IF EXISTS {table} CASCADE")

        fields, header = utilitaires.get_fields(file, ";")
        run(f"CREATE TABLE {table} ({fields}); TRUNCATE {table}")
        df = pd.read_csv(file, sep=";")
        df.columns = header
        df.to_sql(table, con=engine, if_exists="append", index=False)
    except Exception as e:
        print(f"Erreur {file=} {fields=} : {e}")
        raise e

filenames = [file.split("/")[-1].replace("-", "_").split(".")[0] for file in files]
filenames.sort()
run("DROP VIEW IF EXISTS rne;")

run(
    """
    CREATE OR REPLACE VIEW rne AS (
        SELECT 'Conseil départemental' as elu, 4 as ordre, code_du_departement, code_sexe, date_de_naissance FROM elus_conseiller_departemental
        UNION
        SELECT 'Conseil régional' as elu, 6 as ordre,  insee_dep as code_du_departement, code_sexe, date_de_naissance
            FROM elus_regional inner join ign_departement ign on elus_regional.code_de_la_region = ign.insee_reg
        UNION
        SELECT 'Conseil municipal' as elu, 2 as ordre, insee_dep as code_du_departement, code_sexe, date_de_naissance
            FROM elus_municipal inner join ign_commune ign on elus_municipal.code_de_la_commune = ign.insee_com
        UNION
        SELECT 'Maires' as elu, 1 as ordre, insee_dep as code_du_departement, code_sexe, date_de_naissance
            FROM elus_maire  inner join ign_commune ign on elus_maire.code_de_la_commune = ign.insee_com
        UNION
        SELECT 'Assemblée nationale' as elu, 8 as ordre, code_du_departement, code_sexe, date_de_naissance FROM elus_depute WHERE code_du_departement IS NOT NULL
        UNION
        SELECT 'Assemblée nationale' as elu, 8 as ordre, code_de_la_collectivite__statut_particulier, code_sexe, date_de_naissance FROM elus_depute WHERE code_du_departement IS NULL
        UNION
        SELECT 'Membres des assemblées des collectivités à statut particulier' as elu, 5 as ordre, code_du_departement, code_sexe, date_de_naissance FROM elus_membre_assemblees
        UNION
        SELECT 'EPCI' as elu, 3 as ordre, code_du_departement, code_sexe, date_de_naissance FROM elus_epci WHERE code_du_departement IS NOT NULL
        UNION
        SELECT 'EPCI' as elu, 3 as ordre, code_de_la_collectivite__statut_particulier, code_sexe, date_de_naissance FROM elus_epci WHERE code_du_departement IS NULL
        UNION
        SELECT 'Sénat' as elu,7 as ordre,  code_du_departement, code_sexe, date_de_naissance FROM elus_senateur WHERE code_du_departement IS NOT NULL
        UNION
        SELECT 'Sénat' as elu, 7 as ordre, code_de_la_collectivite__statut_particulier, code_sexe, date_de_naissance FROM elus_senateur WHERE code_du_departement IS NULL
    );

    CREATE INDEX IF NOT EXISTS ign_dep_insee_reg ON ign_departement (insee_reg);
    CREATE INDEX IF NOT EXISTS ign_commune_insee_com ON ign_commune (insee_com);

    CREATE INDEX IF NOT EXISTS rne_cr_reg ON elus_regional (code_de_la_region);
    CREATE INDEX IF NOT EXISTS rne_cm_com ON elus_municipal (code_de_la_commune);
    CREATE INDEX IF NOT EXISTS rne_maires_com ON elus_maire (code_de_la_commune);

    CREATE INDEX IF NOT EXISTS rne_cm_dep ON elus_municipal (code_du_departement);
    CREATE INDEX IF NOT EXISTS rne_maires_dep ON elus_maire (code_du_departement);
    CREATE INDEX IF NOT EXISTS rne_epci_dep ON elus_epci (code_du_departement);
    """
)

utilitaires.end("elus.py")
