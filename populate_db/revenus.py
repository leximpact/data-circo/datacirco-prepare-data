from datacirco import utilitaires
from datacirco.connexion_db import engine, run
import pandas as pd

utilitaires.start("revenu.py")

print("Import SQL des tables sur les circonscriptions")
table = "insee_indic_stat_circo_2022"
df_data_insee = pd.read_excel(
    "indic-stat-circonscriptions-legislatives-2022.xlsx",
    header=7,
    sheet_name="indicateurs_circonscriptions",
    na_values=["nd", ""],
)
df_data_insee["circo"] = df_data_insee["circo"].apply(utilitaires.get_str_circo)
df_data_insee.columns = [utilitaires.clean_string(val) for val in df_data_insee.columns]
fields = "circo TEXT, nom_de_la_circonscription TEXT, "
chaine = fields + " NUMERIC, ".join(df_data_insee.columns[2:]) + " NUMERIC"
run(
    f"""
    DROP TABLE IF EXISTS {table};
    CREATE TABLE {table} ({chaine})
    """
)
df_data_insee.to_sql(table, con=engine, if_exists="append", index=False)
# Ajouter une ligne pour modifier le format des numéro de circonscriptions pour qu'il corresponde au numéro
# fields, header = utilitaires.get_fields("indic-stat-circonscriptions-legislatives-2022.xlsx", delimiter=',', skiprows=7)
# header = [utilitaires.clean_string(h) for h in df.columns]
# fields = '"' + '" text, "'.join(header) + '" text'
# run(
#     f"""
#     DROP TABLE IF EXISTS {table} CASCADE;
#     CREATE TABLE {table} ({fields}
#     );
#     TRUNCATE {table};
# """
# )
# df.to_sql(table, con=engine, if_exists="append", index=False)


print("Import SQL des tables de déciles nationaux")
df = pd.read_excel("reve-niv-vie-decile.xlsx", header=3)
table = "insee_decile_natio"
df.to_sql(table, con=engine, if_exists="replace", index=False)

print("Import SQL de la base départementale des revenus")
utilitaires.unzip_file("base-cc-filosofi-2019_XLSX_histo.zip")
df_revenu_dep = pd.read_excel(
    "base-cc-filosofi-2019.xlsx",
    sheet_name="DEP",
    skiprows=lambda x: x in [0, 1, 2, 3, 4, 104, 105, 106, 107],
)
table = "insee_revenu_dep_2019"
df_revenu_dep.columns = [utilitaires.clean_string(val) for val in df_revenu_dep.columns]

fields = "CODGEO TEXT, LIBGEO TEXT, "
chaine = fields + " NUMERIC, ".join(df_revenu_dep.columns[2:]) + " NUMERIC"
run(
    f"""
    DROP TABLE IF EXISTS {table};
    CREATE TABLE IF NOT EXISTS {table} ({chaine})
    """
)
df_revenu_dep.to_sql(table, con=engine, if_exists="append", index=False)

print("Données sur les revenus à l'échelle IRIS :")
utilitaires.unzip_file("BASE_TD_FILO_DISP_IRIS_2020_CSV.zip")
fields, header = utilitaires.get_fields(
    "BASE_TD_FILO_DISP_IRIS_2020.csv", delimiter=";"
)
run(
    f"""
    DROP TABLE IF EXISTS insee_revenu_pauvrete_2020 CASCADE;
    CREATE TABLE IF NOT EXISTS insee_revenu_pauvrete_2020 ({fields}); 
"""
)
df = pd.read_csv("BASE_TD_FILO_DISP_IRIS_2020.csv", sep=";")
df.columns = header
df.to_sql("insee_revenu_pauvrete_2020", con=engine, if_exists="append", index=False)

utilitaires.end("revenu.py")
