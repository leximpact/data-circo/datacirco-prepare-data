from datacirco import utilitaires
from datacirco.connexion_db import db, run

utilitaires.start("entreprises-01.py")

print("Import SIRENE (géocodée)...")
utilitaires.unzip_file("StockUniteLegale_utf8.zip")
fields, _ = utilitaires.get_fields("StockUniteLegale_utf8.csv")

run(
    f"""
    DROP TABLE IF EXISTS insee_siren CASCADE;
    CREATE TABLE insee_siren ({fields});
    TRUNCATE insee_siren;
"""
)

with open("StockUniteLegale_utf8.csv", "r") as f:
    db.copy_expert("COPY insee_siren FROM STDIN WITH (FORMAT CSV, HEADER TRUE)", f)

run("CREATE INDEX insee_siren_siren ON insee_siren (siren);")

print("Import SIRET (géocodée)...")
utilitaires.extract_gz("StockEtablissementActif_utf8_geo.csv.gz")
fields, _ = utilitaires.get_fields("StockEtablissementActif_utf8_geo.csv")

run(
    f"""
    DROP TABLE IF EXISTS insee_siret CASCADE;
    CREATE TABLE insee_siret ({fields});
"""
)

with open("StockEtablissementActif_utf8_geo.csv", "r") as f:
    db.copy_expert("COPY insee_siret FROM STDIN WITH (FORMAT CSV, HEADER TRUE)", f)

run(
    """
    ALTER TABLE insee_siret ADD geom geometry;
    UPDATE insee_siret SET geom=ST_SetSRID(ST_Makepoint(longitude::numeric, latitude::numeric),4326);
    CREATE INDEX insee_siret_geom ON insee_siret USING GIST (geom);
    CLUSTER insee_siret USING insee_siret_geom;
"""
)

run(
    """
    CREATE INDEX insee_siret_siren ON insee_siret (siren);
    CREATE INDEX insee_siret_codecommunetablissement ON insee_siret (codecommuneetablissement);
"""
)

print("Génération d'une table de correspondance CEDEX > DEPCOM")
run(
    """
    DROP TABLE IF EXISTS cedex CASCADE;
    CREATE TABLE cedex AS (
        SELECT codecedexetablissement AS cedex, libellecedexetablissement AS cedex_lib, codecommuneetablissement AS depcom
        FROM insee_siret
        GROUP BY 1, 2, 3
    );
"""
)
run("CREATE INDEX decex_decex ON cedex (cedex);")


utilitaires.end("entreprises-01.py")
