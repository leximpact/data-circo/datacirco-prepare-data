from datacirco import utilitaires
import glob
from datacirco.connexion_db import database_command_line, run, db

utilitaires.start("geographie.py")

print("limites des circonscriptions")
utilitaires.execute_command(
    f"unzip -p circonscriptions-legislatives.zip | PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ /vsistdin/ -nln zone_circo_tmp | {database_command_line}"
)

run(
    """
    DROP TABLE IF EXISTS zone_circo CASCADE;
    CREATE TABLE zone_circo AS SELECT ref, ST_Union(wkb_geometry) as wkb_geometry FROM zone_circo_tmp GROUP BY ref ORDER BY 2;
    CREATE INDEX ON zone_circo USING GIST (wkb_geometry);
    CREATE INDEX ON zone_circo (ref);
    ALTER TABLE zone_circo ADD geom geometry;
    UPDATE zone_circo set geom = st_transform(wkb_geometry,2154) where ref < '097';
    UPDATE zone_circo set geom = st_transform(wkb_geometry,5489) where ref LIKE '972%';
    UPDATE zone_circo set geom = st_transform(wkb_geometry,4627) where ref LIKE '974%';
    UPDATE zone_circo set geom = st_transform(wkb_geometry,4627) where ref LIKE '976%';
"""
)

print("Correspondance codes postaux / Insee")
# Nous n'utilisons pas les données géographiques
# utilitaires.execute_command(f"PG_USE_COPY=YES ogr2ogr  -f pgdump /vsistdout/ laposte_cp.csv | {database_command_line}")
# fields = "code_commune_insee;nom_de_la_commune;code_postal;libelle_d_acheminement;ligne_5"
table = "laposte_cp"
run(
    f"""
    DROP TABLE IF EXISTS {table};
    CREATE TABLE {table} (
        code_commune_insee text not null,
        nom_de_la_commune text not null,
        code_postal text not null,
        libelle_d_acheminement text not null,
        ligne_5 text
    );
"""
)

with open("laposte_cp.csv", "r", encoding="iso8859-1") as csv_file:
    db.copy_expert(
        f"COPY {table} FROM STDIN WITH (FORMAT csv, HEADER true, DELIMITER ';')",
        csv_file,
    )

run(f"CREATE INDEX IF NOT EXISTS idx_{table}_cp ON {table} (code_postal)")

print("Découpage des IRIS (GE)")
utilitaires.execute_command(
    f"PG_USE_COPY=yes ogr2ogr -f pgdump /vsistdout/ IRIS-GE*RGR92UTM40S*/*/*/*/*.SHP -nln iris_ge -nlt geometry -t_srs EPSG:4326 | {database_command_line}"
)

run("TRUNCATE iris_ge")

for shp in glob.glob("IRIS-GE*/*/*/*/*.SHP"):
    utilitaires.execute_command(
        f"PG_USE_COPY=yes ogr2ogr -f pgdump /vsistdout/ {shp} -nln iris_ge -nlt geometry -t_srs EPSG:4326 -LCO CREATE_TABLE=OFF | {database_command_line}"
    )


print("Correspondance codes Insee de la commune / circonscription (temps : 8 minutes)")
run(
    """
DROP TABLE IF EXISTS circo_commune;
CREATE TABLE circo_commune AS
	select
	ci.ref as circo,
		insee_com,
		nom,
		round((st_area(st_intersection(co.wkb_geometry, ci.wkb_geometry)::geography) / st_area(co.wkb_geometry::geography))::numeric,2) as pct_surf
	from ign_commune co
	join zone_circo ci on (st_intersects(co.wkb_geometry, ci.wkb_geometry) and st_area(st_intersection(co.wkb_geometry, ci.wkb_geometry)::geography) / st_area(co.wkb_geometry::geography) > 0.1 )
	UNION
	select
		ci.ref as circo,
		insee_arm,
		regexp_replace(nom,'[0-9].*$',''),
		round((st_area(st_intersection(co.wkb_geometry, ci.wkb_geometry)::geography) / st_area(co.wkb_geometry::geography))::numeric,2) as pct_surf
	from ign_arrondissement_municipal co
	join zone_circo ci on (st_intersects(co.wkb_geometry, ci.wkb_geometry) and st_area(st_intersection(co.wkb_geometry, ci.wkb_geometry)::geography) / st_area(co.wkb_geometry::geography) > 0.1 )
"""
)

utilitaires.end("geographie.py")
