from datacirco import utilitaires
from datacirco.connexion_db import db, database_command_line, run

module = "entreprises-02.py"
utilitaires.start(module)


def append_selected_columns(input_file, selected_columns, output_file):
    """Convert a CSV separated by `;` to a CSV with `,` as separator."""
    with open(input_file, "r") as input_csv:
        lines = input_csv.readlines()[1:]
        selected_lines = []
        for line in lines:
            values = line.strip().split(";")
            selected_values = []
            for col in selected_columns:
                if col == 28 and input_file == "societes-immatriculees-2024.csv":
                    selected_values.append("")
                else:
                    selected_values.append(
                        values[col] if "," not in values[col] else f'"{values[col]}"'
                    )
            selected_lines.append(",".join(selected_values) + "\n")
        output_file.writelines(selected_lines)


# Infogreffe
print("------------- Infogreffe")
print("\tImport des sociétés immatriculées...")
with open("societes-immatriculees.csv", "w") as file:
    file.write(
        "denomination,siren,forme_juridique,adresse,code_postal,ville,region,greffe,fiche,geoloc,date_immatriculation,code_ape\n"
    )

    immatriculees_files = [
        {
            "file": "societes-immatriculees-2012.csv",
            "columns": [0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12],
        },
        {
            "file": "societes-immatriculees-2013.csv",
            "columns": [0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12],
        },
        {
            "file": "societes-immatriculees-2014.csv",
            "columns": [0, 1, 3, 4, 5, 6, 7, 9, 10, 11, 8, 12],
        },
        {
            "file": "societes-immatriculees-2015.csv",
            "columns": [0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 12, 11],
        },
        {
            "file": "societes-immatriculees-2016.csv",
            "columns": [0, 1, 3, 5, 6, 7, 10, 11, 16, 14, 12, 4],
        },
        {
            "file": "societes-immatriculees-2017.csv",
            "columns": [0, 1, 3, 5, 6, 7, 10, 12, 24, 16, 13, 4],
        },
        {
            "file": "societes-immatriculees-2018.csv",
            "columns": [0, 1, 3, 6, 7, 8, 11, 13, 28, 17, 14, 4],
        },
        {
            "file": "societes-immatriculees-2019.csv",
            "columns": [0, 1, 3, 6, 7, 8, 11, 13, 28, 17, 14, 4],
        },
        {
            "file": "societes-immatriculees-2020.csv",
            "columns": [0, 1, 3, 6, 7, 8, 11, 13, 28, 17, 14, 4],
        },
        {
            "file": "societes-immatriculees-2021.csv",
            "columns": [0, 1, 3, 6, 7, 8, 11, 13, 28, 17, 14, 4],
        },
        {
            "file": "societes-immatriculees-2022.csv",
            "columns": [0, 1, 3, 6, 7, 8, 11, 13, 28, 17, 14, 4],
        },
        {
            "file": "societes-immatriculees-2023.csv",
            "columns": [0, 1, 3, 6, 7, 8, 11, 13, 28, 17, 14, 4],
        },
        {
            "file": "societes-immatriculees-2024.csv",
            "columns": [0, 1, 3, 6, 7, 8, 11, 13, 28, 17, 14, 4],
        },
    ]

    for file_info in immatriculees_files:
        try:
            print("\tProcessing", file_info["file"])
            append_selected_columns(file_info["file"], file_info["columns"], file)
        except Exception as e:
            print("ERROR lors du traitement de ", file_info["file"], e)
            raise e

columns, _ = utilitaires.get_fields("societes-immatriculees.csv")
run(
    f"""
    DROP TABLE IF EXISTS rncs_immatriculations CASCADE;
    CREATE TABLE IF NOT EXISTS rncs_immatriculations ({columns});
    TRUNCATE rncs_immatriculations;
"""
)

with open("societes-immatriculees.csv", "r") as csv_file:
    db.copy_expert(
        "copy rncs_immatriculations from stdin with (format csv, header true)", csv_file
    )

run(
    """
    ALTER TABLE rncs_immatriculations ADD geom geometry;
    UPDATE rncs_immatriculations
    SET geom = ST_SetSRID(ST_MakePoint(
        regexp_replace(geoloc, '^.*,', '')::numeric,
        regexp_replace(geoloc, ',.*$', '')::numeric
    ), 4326)
    WHERE geoloc != '';
    CREATE INDEX ON rncs_immatriculations USING gist(geom);
"""
)
print("\tImport des sociétés radiées...")
with open("societes-radiees.csv", "w") as file:
    file.write(
        "denomination,siren,forme_juridique,adresse,code_postal,ville,region,greffe,geoloc,date_radiation,code_ape\n"
    )

    radiees_files = [
        {
            "file": "societes-radiees-2012.csv",
            "columns": [0, 1, 2, 5, 6, 7, 8, 9, 12, 10, 3],
        },
        {
            "file": "societes-radiees-2013.csv",
            "columns": [0, 1, 2, 5, 6, 7, 8, 9, 12, 10, 3],
        },
        {
            "file": "societes-radiees-2014.csv",
            "columns": [0, 1, 2, 5, 6, 7, 8, 10, 12, 9, 3],
        },
        {
            "file": "societes-radiees-2015.csv",
            "columns": [0, 1, 2, 5, 6, 7, 8, 12, 17, 13, 3],
        },
        {
            "file": "societes-radiees-2016.csv",
            "columns": [0, 1, 3, 5, 6, 7, 10, 11, 14, 12, 4],
        },
        {
            "file": "societes-radiees-2017.csv",
            "columns": [0, 1, 3, 6, 7, 8, 11, 13, 17, 15, 4],
        },
        {
            "file": "societes-radiees-2018.csv",
            "columns": [0, 1, 3, 6, 7, 8, 11, 13, 17, 15, 4],
        },
        {
            "file": "societes-radiees-2019.csv",
            "columns": [0, 1, 3, 6, 7, 8, 11, 13, 17, 15, 4],
        },
        {
            "file": "societes-radiees-2020.csv",
            "columns": [0, 1, 3, 6, 7, 8, 11, 13, 17, 15, 4],
        },
        {
            "file": "societes-radiees-2021.csv",
            "columns": [0, 1, 3, 6, 7, 8, 11, 13, 17, 15, 4],
        },
        {
            "file": "societes-radiees-2022.csv",
            "columns": [0, 1, 3, 6, 7, 8, 11, 13, 17, 15, 4],
        },
        {
            "file": "societes-radiees-2023.csv",
            "columns": [0, 1, 3, 6, 7, 8, 11, 13, 17, 15, 4],
        },
        {
            "file": "societes-radiees-2024.csv",
            "columns": [0, 1, 3, 6, 7, 8, 11, 13, 17, 15, 4],
        },
        # TODO: trouver un meilleur moyen de gérer les fichiers pour les années suivantes : faire une boucle sur les années et changer les colonnes en fonction.
    ]

    for file_info in radiees_files:
        try:
            print("\tProcessing", file_info["file"])
            append_selected_columns(file_info["file"], file_info["columns"], file)
        except Exception as e:
            print("ERROR lors du traitement de ", file_info["file"], e)
            raise e

"""
On retire la société L.A.F.S LAV' A FERE SERVICES car elle fait planter le script sinon.
https://www.pappers.fr/entreprise/lafs-lav-a-fere-services-498999358
"""
utilitaires.execute_command(
    "sed -i '/498999358/d' societes-radiees.csv"
)  # remove line that contains errors
columns, _ = utilitaires.get_fields("societes-radiees.csv")
run(
    f"""
    DROP TABLE IF EXISTS rncs_radiations CASCADE;
    CREATE TABLE IF NOT EXISTS rncs_radiations ({columns});
"""
)

with open("societes-radiees.csv", "r") as csv_file:
    db.copy_expert(
        "copy rncs_radiations from stdin with (format csv, header true)", csv_file
    )

run(
    """
    ALTER TABLE rncs_radiations ADD geom geometry;
    UPDATE rncs_radiations
    SET geom = ST_SetSRID(ST_MakePoint(
        regexp_replace(geoloc, '^.*,', '')::numeric,
        regexp_replace(geoloc, ',.*$', '')::numeric
    ), 4326)
    WHERE geoloc != '';
    CREATE INDEX ON rncs_radiations USING gist(geom);
"""
)


print("\t Import du référenciel des communes des greffes...")

utilitaires.execute_command(
    f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ referentiel-communes-greffes.geojson -nln greffes_communes | {database_command_line}"
)
'''
# Données non utilisées dans le pdf
with open('entreprises_procedures_collectives_2015.csv', 'r') as file:
    reader = csv.reader(file, delimiter=';')
    with open('procedures_collectives.csv', 'w') as output_file:
        writer = csv.writer(output_file, delimiter=',')
        for row in reader:
            writer.writerow([row[0], row[3], row[4], row[6], row[7], row[8], row[9], row[10], row[12], row[11], row[13]])

for year in range(2016, 2023):
    with open(f'entreprises_procedures_collectives_{year}.csv', 'r') as file:
        reader = csv.reader(file, delimiter=';')
        with open('procedures_collectives.csv', 'a') as output_file:
            writer = csv.writer(output_file, delimiter=',')
            next(reader)
            for row in reader:
                selected_columns = [row[0], row[3], row[4], row[6], row[7], row[8], row[9], row[10], row[11], row[18], row[12]]
                writer.writerow(selected_columns)

run("""
    DROP TABLE IF EXISTS rncs_procedures_collectives CASCADE;
    CREATE TABLE rncs_procedures_collectives(
        Periode text,
        Greffe text,
        dep text,
        reg text,
        demandes_ouvertures int,
        sauvegardes int,
        redressements int,
        liquidations int,
        resolution int,
        liquidation_simple int,
        Total int
    )
""")

with open('procedures_collectives.csv', 'r') as file:
    db.copy_expert("COPY rncs_procedures_collectives FROM STDIN WITH (format csv, header true)", file)
'''

run(
    """
    CREATE VIEW greffe_circo AS select ref,greffe,count(*) as nb_communes from greffes_communes g join zone_circo c on (st_intersects(g.wkb_geometry, c.wkb_geometry)) group by 1,2
"""
)

utilitaires.end(module)
