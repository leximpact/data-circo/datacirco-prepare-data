from datacirco import utilitaires
from datacirco.connexion_db import db, database_command_line, run, engine
import pandas as pd

utilitaires.start("population.py")

annee_carroyage = ["2015", "2019"]
# TODO: Le carroyage à 200m et 1km de 2019 n'est pas encore fonctionnel dans DataCirco ?

for annee in annee_carroyage:
    # Les archives contiennent les données pour les trois zones géographiques du champ (France métropolitaine, Martinique et Réunion).
    print(f"carroyage Insee {annee}")

    table = f"insee_population_carroyee_{annee}"
    utilitaires.unzip_file(f"Filosofi{annee}_carreaux_200m_shp.zip")
    if annee == "2015":
        utilitaires.extract_7z(f"Filosofi{annee}_carreaux_200m_reg02_shp.7z")
        utilitaires.execute_command(
            f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ Filosofi{annee}_carreaux_200m_reg02.shp -t_srs EPSG:4326 -nln {table} | {database_command_line}"
        )

        utilitaires.extract_7z(f"Filosofi{annee}_carreaux_200m_reg04_shp.7z")
        utilitaires.execute_command(
            f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ Filosofi{annee}_carreaux_200m_reg04.shp -t_srs EPSG:4326 -nln {table} -lco CREATE_TABLE=NO | {database_command_line}"
        )

        utilitaires.extract_7z(f"Filosofi{annee}_carreaux_200m_metropole_shp.7z")
        utilitaires.execute_command(
            f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ Filosofi{annee}_carreaux_200m_metropole.shp -t_srs EPSG:4326 -nln {table} -lco CREATE_TABLE=NO | {database_command_line}"
        )
    elif annee == "2019":
        # https://www.insee.fr/fr/statistiques/7655513?sommaire=7655515
        # https://www.insee.fr/fr/statistiques/fichier/7655513/documentation_donnees-carroyees_filosofi2019.pdf
        # utilitaires.unzip_file(f"Filosofi{annee}_carreaux_1km_shp.zip")
        utilitaires.unzip_file(f"Filosofi{annee}_carreaux_200m_shp.zip")
        utilitaires.extract_7z(f"Filosofi{annee}_carreaux_200m_shp.7z")
        # print(f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ carreaux_200m_met.shp -t_srs EPSG:4326 -nln {table} -lco CREATE_TABLE=YES | {database_command_line}")
        utilitaires.execute_command(
            f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ carreaux_200m_met.shp -t_srs EPSG:4326 -nln {table} -lco CREATE_TABLE=YES | {database_command_line}"
        )
        utilitaires.execute_command(
            f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ carreaux_200m_mart.shp -t_srs EPSG:4326 -nln {table} -lco CREATE_TABLE=NO | {database_command_line}"
        )
        utilitaires.execute_command(
            f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ carreaux_200m_reun.shp -t_srs EPSG:4326 -nln {table} -lco CREATE_TABLE=NO | {database_command_line}"
        )
        """
        lcog_geo : Code officiel géographique au 1er janvier 2020 de la ou des commune(s)
        dans laquelle se trouve le carreau. En effet, si le carreau est à cheval sur
        plusieurs communes (intersection géographique avec la BDTopo),
        lcog_geo est alors la concaténation des codes des différentes communes
        intersectées, triées par surface d’intersection décroissante. Exemple pour
        un carreau à cheval majoritairement sur la commune 01002 mais
        également sur la commune 01001 : « 0100201001 ».
        """
        # Création de la colonne depcom
        run(
            f"""
            ALTER TABLE {table} ADD depcom text;
            UPDATE {table} SET depcom = left(lcog_geo,5);
            ALTER TABLE {table} ADD idinspire text;
            UPDATE {table} SET idinspire = idcar_200m;
        """
        )

    run(
        f"""
        ALTER TABLE {table} DROP ogc_fid;
        CREATE INDEX IF NOT EXISTS {table}_depcom ON {table} (depcom);
        CREATE INDEX IF NOT EXISTS {table}_idinspire ON {table} (idinspire);
    """
    )

    print(f"  carroyage Insee {annee} - géométries en projections légales locales")

    run(
        f"""
        ALTER TABLE {table} ADD geom geometry(polygon);
        UPDATE {table} SET geom = ST_Transform(wkb_geometry,2154) WHERE depcom <= '95999';
        UPDATE {table} SET geom = ST_Transform(wkb_geometry,5489) WHERE depcom LIKE '972%';
        UPDATE {table} SET geom = ST_Transform(wkb_geometry,4627) WHERE depcom LIKE '974%';
    """
    )

    print(
        f"  carroyage Insee {annee} - table de lien zone_circo / carreau + pct du carreau"
    )

    print(f"Import insee_population_carroyee_1km pour {annee}...")
    if annee == "2015":
        utilitaires.unzip_file(f"Filosofi{annee}_carreaux_1000m_shp.zip")
        utilitaires.extract_7z(f"Filosofi{annee}_carreaux_1000m_shp.7z")
        table = f"insee_population_carroyee_{annee}_1k"
        utilitaires.execute_command(
            f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ Filosofi{annee}_carreaux_1000m_reg02.shp -t_srs EPSG:4326 -nln {table}| {database_command_line}"
        )
        utilitaires.execute_command(
            f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ Filosofi{annee}_carreaux_1000m_reg04.shp -t_srs EPSG:4326 -nln {table} -lco CREATE_TABLE=NO| {database_command_line}"
        )
        utilitaires.execute_command(
            f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ Filosofi{annee}_carreaux_1000m_metropole.shp -t_srs EPSG:4326 -nln {table} -lco CREATE_TABLE=NO| {database_command_line}"
        )
    elif annee == "2019":
        utilitaires.unzip_file(f"Filosofi{annee}_carreaux_1km_shp.zip")
        utilitaires.extract_7z(f"Filosofi{annee}_carreaux_1km_shp.7z")
        table = f"insee_population_carroyee_{annee}_1k"

        # Fix precision problem : Des valeurs ont la forme "1234054030.400000095367432" ce qui ne rentre pas dans le type numeric par défaut "NUMERIC(24,15)"
        # Il faut donc augmenter la précision de la colonne ind_snv
        # Pour cela on va convertir le shapefile en sql et modifier la définition de la colonne ind_snv
        # Puis importer le SQL dans la base de données

        # Export en SQL
        utilitaires.execute_command(
            f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ carreaux_1km_met.shp -t_srs EPSG:4326 -nln {table} -lco CREATE_TABLE=YES > carreaux_1km_met.sql"
        )
        # Modifier la création de la table
        utilitaires.execute_command(
            r"sed -i 's/ind_snv\" NUMERIC(24,15);/ind_snv\" NUMERIC(50,30);/' carreaux_1km_met.sql"
        )
        # Importer le SQL dans la base
        utilitaires.execute_command(
            f"{database_command_line} -b -f carreaux_1km_met.sql"
        )
        # Export en SQL de la Martinique
        utilitaires.execute_command(
            f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ carreaux_1km_mart.shp -t_srs EPSG:4326 -nln {table} -lco CREATE_TABLE=NO > carreaux_1km_mart.sql",
        )
        # Modifier la création de la table
        utilitaires.execute_command(
            r"sed -i 's/ind_snv\" NUMERIC(24,15);/ind_snv\" NUMERIC(50,30);/' carreaux_1km_mart.sql",
        )
        # Importer le SQL dans la base
        utilitaires.execute_command(
            f"{database_command_line} -b -f carreaux_1km_mart.sql"
        )

        # Export en SQL de la Réunion
        utilitaires.execute_command(
            f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ carreaux_1km_reun.shp -t_srs EPSG:4326 -nln {table} -lco CREATE_TABLE=NO > carreaux_1km_reun.sql",
        )
        # Modifier la création de la table
        utilitaires.execute_command(
            r"sed -i 's/ind_snv\" NUMERIC(24,15);/ind_snv\" NUMERIC(50,30);/' carreaux_1km_reun.sql"
        )
        # Importer le SQL dans la base
        utilitaires.execute_command(
            f"{database_command_line} -b -f carreaux_1km_reun.sql"
        )

        run(
            f"""
            ALTER TABLE {table} ADD COLUMN geom geometry(Polygon, 4326);
            UPDATE {table}
            SET geom = ST_Transform(wkb_geometry, 4326) ;
            CREATE INDEX {table}_geom ON {table} USING GIST (geom);
            CLUSTER {table} USING {table}_geom ;
            """
        )


print("carroyage Insee 2010")
table = "insee_population_carroyee_2010_car"
utilitaires.execute_command(
    f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ /vsizip/200m_carreaux_martinique_shapefile_wgs84.zip -nln {table} | {database_command_line}"
)
utilitaires.execute_command(
    f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ /vsizip/200m_carreaux_reunion_shapefile_wgs84.zip -nln {table} -lco CREATE_TABLE=NO | {database_command_line}"
)
utilitaires.execute_command(
    f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ /vsizip/200m_carreaux_metropole_shapefile_wgs84.zip -nln {table} -lco CREATE_TABLE=NO | {database_command_line}"
)
run(f"ALTER TABLE {table} DROP ogc_fid")

file = "200m-rectangles-metropole"
files = utilitaires.extract_dbf(file + ".zip")
csv_files = []
for file in files:
    csv_files.append(utilitaires.dbf_to_csv(file))
for file in csv_files:
    with open(file, "r") as f:
        header = f.readline().strip().split(",")
fields = header[0] + " text," + " numeric,".join(header[1:]) + " numeric"

run(
    f"""
    DROP TABLE IF EXISTS insee_population_carroyee_2010_rect CASCADE;
    CREATE TABLE IF NOT EXISTS insee_population_carroyee_2010_rect ({fields});
"""
)

with open(file, "r") as f:
    db.copy_expert(
        "COPY insee_population_carroyee_2010_rect FROM STDIN WITH (FORMAT CSV, HEADER TRUE)",
        f,
    )

file = "200m-rectangles-martinique"
files = utilitaires.extract_dbf(file + ".zip")
csv_files = []
for file in files:
    csv_files.append(utilitaires.dbf_to_csv(file))
for file in csv_files:
    with open(file, "r") as f:
        db.copy_expert(
            "COPY insee_population_carroyee_2010_rect FROM STDIN WITH (FORMAT CSV, HEADER TRUE)",
            f,
        )

file = "200m-rectangles-reunion"
files = utilitaires.extract_dbf(file + ".zip")
csv_files = []
for file in files:
    csv_files.append(utilitaires.dbf_to_csv(file))
for file in csv_files:
    with open(file, "r") as f:
        db.copy_expert(
            "COPY insee_population_carroyee_2010_rect FROM STDIN WITH (FORMAT CSV, HEADER TRUE)",
            f,
        )

print("On traite les données 2010 pour obtenir le format de 2015")

run(
    """
    DROP TABLE if exists insee_population_carroyee_2010 CASCADE;
"""
)

run(
    """
    CREATE TABLE insee_population_carroyee_2010 as
    select wkb_geometry,
        idinspire,
        (select depcom from insee_population_carroyee_2015 n where c.idinspire=n.idinspire) as depcom,
        ind_c as ind,
        men*ind_c/ind_r as men,
        men_surf*ind_c/ind_r as men_surf,
        men_coll*ind_c/ind_r as men_coll,
        men_5ind*ind_c/ind_r as men_5ind,
        men_1ind*ind_c/ind_r as men_1ind,
        men_prop*ind_c/ind_r as men_prop,
        men_basr*ind_c/ind_r as men_pauv,
        ind_age1*ind_c/ind_r as ind_0_3,
        ind_age2*ind_c/ind_r as ind_4_5,
        ind_age3*ind_c/ind_r as ind_6_10,
        ind_age4*ind_c/ind_r as ind_11_14,
        ind_age5*ind_c/ind_r as ind_15_17,
        (ind_r-ind_age1-ind_age2-ind_age3-ind_age4-ind_age5-ind_age6)*ind_c/ind_r as ind_18_24,
        ind_age6*ind_c/ind_r as ind_25p,
        ind_age7*ind_c/ind_r as ind_65p,
        ind_age8*ind_c/ind_r as ind_75p,
        ind_srf * ind_c/ind_r as ind_snv
    from insee_population_carroyee_2010_car c
    inner join insee_population_carroyee_2010_rect r on r.idk = c.idk and r.nbcar = c.nbcar;
"""
)

run(
    """
    create index if not exists insee_population_carroyee_2010_wkb_geometry_idx on insee_population_carroyee_2010 using gist(wkb_geometry);
"""
)


table = "insee_population_carroyee_2010_1k"
utilitaires.unzip_file("ECP1KM_09_MET.zip")
utilitaires.unzip_file("ECP1KM_09_R02.zip")
utilitaires.unzip_file("ECP1KM_09_R04.zip")

utilitaires.execute_command(
    f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ R_rfl09_LAEA1000.mif R_rfl09_LAEA1000 -t_srs EPSG:4326 -nln MET_{table}| {database_command_line}"
)
utilitaires.execute_command(
    f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ R02_rfl09_UTM20N1000.mif R02_rfl09_UTM20N1000 -t_srs EPSG:4326 -nln R02_{table}| {database_command_line}"
)
utilitaires.execute_command(
    f"PG_USE_COPY=YES ogr2ogr -f pgdump /vsistdout/ R04_rfl09_PDN1000.mif R04_rfl09_PDN1000 -t_srs EPSG:4326 -nln R04_{table}| {database_command_line}"
)

run(
    f"""
    DROP TABLE if exists {table} CASCADE;
    CREATE TABLE {table} AS
    SELECT wkb_geometry, ind, indxyne1 FROM met_{table}
    UNION
    SELECT wkb_geometry, ind, indxyne1 FROM r02_{table}
    UNION
    SELECT wkb_geometry, ind, indxyne1 FROM r04_{table} ORDER BY wkb_geometry;
    CREATE INDEX IF NOT EXISTS {table}_idx ON {table} USING spgist (wkb_geometry);
    DROP TABLE met_{table} CASCADE;
    DROP TABLE r02_{table} CASCADE;
    DROP TABLE r04_{table} CASCADE;
"""
)

annees_ic = ["2018", "2020"]
for annee in annees_ic:
    print(
        f"Recensement {annee} à l'IRIS : https://www.insee.fr/fr/statistiques/5650720"
    )
    utilitaires.unzip_file(f"base-ic-evol-struct-pop-{annee}_csv.zip")
    utilitaires.unzip_file(f"base-ic-evol-struct-pop-{annee}-com_csv.zip")

    fields, _ = utilitaires.get_fields(f"base-ic-evol-struct-pop-{annee}.CSV", ";")
    run(
        f"""
        DROP TABLE IF EXISTS insee_pop_{annee} CASCADE;
        CREATE TABLE insee_pop_{annee} ({fields});
    """
    )

    with open(f"base-ic-evol-struct-pop-{annee}.CSV", "r") as f:
        db.copy_expert(
            f"COPY insee_pop_{annee} FROM STDIN WITH (FORMAT CSV, HEADER TRUE, DELIMITER ';')",
            f,
        )

    run(
        f"""
        CREATE INDEX IF NOT EXISTS insee_pop_{annee}_iris ON insee_pop_{annee} (iris);
    """
    )

    with open(f"base-ic-evol-struct-pop-{annee}-com.CSV", "r") as f:
        header = f.readline().strip().split(";")
    fields = " ,".join(header)
    with open(f"base-ic-evol-struct-pop-{annee}-com.CSV", "r") as f:
        db.copy_expert(
            f"COPY insee_pop_{annee} ({fields}) FROM STDIN WITH (FORMAT CSV, HEADER TRUE, DELIMITER ';')",
            f,
        )

print("population des circonscriptions en 2013 et 2019")

table = "insee_circo_2019"
run(
    f"""
    DROP TABLE IF EXISTS {table};
    CREATE TABLE {table} (dep text,circo text,circo_lib text, population float);
"""
)

with open("population-circonscriptions-legislatives-2019.csv", "r") as file:
    data_df = pd.read_csv(file)
data_df.columns = ["dep", "circo", "circo_lib", "population"]
data_df.to_sql(table, con=engine, index=False, if_exists="replace")


table = "insee_circo_2013"
run(
    f"""
    DROP TABLE IF EXISTS {table};
    CREATE TABLE {table} (depcir text,p13_pop float);
"""
)

with open("indic-stat-2013-circonscriptions-legislatives.csv", "r") as file:
    data_df = pd.read_csv(file)
data_df.columns = ["depcir", "p13_pop"]
data_df.to_sql(table, con=engine, index=False, if_exists="replace")


for annee in annee_carroyage:
    print(
        f" {annee} / {annee_carroyage} Requête qui prend 1h30 (voir 4h) car elle calcul la part de la population de la circonscriptions dans le carroyage Insee"
    )

    run(
        f"""
        DROP TABLE if exists circo_population_{annee} CASCADE;
        CREATE TABLE circo_population_{annee} AS
        SELECT ref, idinspire,
            st_area(
                st_intersection(p.geom, 
                    st_transform(c.wkb_geometry,st_srid(p.geom))
                )
            )/st_area(p.geom) as pct
        FROM insee_population_carroyee_{annee} p
        JOIN zone_circo c ON (p.wkb_geometry && c.wkb_geometry and st_intersects(p.wkb_geometry, c.wkb_geometry));
    """
    )
    print(f"Création de l'index pour {annee}")
    run(
        f"""
        CREATE INDEX IF NOT EXISTS circo_population_{annee}_ref_idx ON circo_population_{annee} (ref);
        CLUSTER circo_population_{annee} USING circo_population_{annee}_ref_idx;
    """
    )
    print(f"Fin de {annee}")

print("carroyage Insee 2010 - table de lien zone_circo / carreau + pct du carreau")

run(
    """
    DROP TABLE if exists circo_population_2010  CASCADE;
    CREATE TABLE circo_population_2010 AS
    SELECT ref, idinspire,
        st_area(st_intersection(p.wkb_geometry, c.wkb_geometry)::geography)/st_area(p.wkb_geometry::geography) as pct
    FROM insee_population_carroyee_2010 p
    JOIN zone_circo c ON (p.wkb_geometry && c.wkb_geometry and st_intersects(p.wkb_geometry, c.wkb_geometry));
    CREATE INDEX IF NOT EXISTS circo_population_2010_ref_idx ON circo_population_2010 (ref);
    CLUSTER circo_population_2010 USING circo_population_2010_ref_idx;
"""
)
utilitaires.end("population.py")
