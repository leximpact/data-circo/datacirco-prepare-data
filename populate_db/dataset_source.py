from datacirco import utilitaires
from datacirco.connexion_db import engine, run, metadata

utilitaires.start("dataset_source.py")

run(
    """
    DROP TABLE IF EXISTS dataset_source;
    """
)
metadata.create_all(engine)

utilitaires.end("dataset_sourcess.py")
