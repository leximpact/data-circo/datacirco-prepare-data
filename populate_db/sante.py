from datacirco import utilitaires
from tqdm import tqdm
from datacirco.connexion_db import db, run, pg
import os

utilitaires.start("sante.py")
fields, _ = utilitaires.get_fields("finess.csv")

run(
    f"""
    DROP TABLE IF EXISTS finess CASCADE;
    CREATE TABLE IF NOT EXISTS finess ({fields});
    TRUNCATE finess;
"""
)

with open("finess.csv", "r") as f:
    db.copy_expert("COPY finess FROM STDIN WITH (FORMAT CSV, HEADER TRUE)", f)

run(
    """
    CREATE INDEX IF NOT EXISTS finess_idx ON finess (com_code)
"""
)

run(
    """
    DROP TABLE IF EXISTS finess_eml CASCADE;
    CREATE TABLE IF NOT EXISTS finess_eml (
        equipement text,
        nofinessej text,
        rsej text,
        eml text,
        libeml text,
        noautor text,
        noautorarhgos text,
        noimplarhgos text,
        dateautor text,
        nofinesset text,
        datemeo text,
        datefin text
    );
    TRUNCATE finess_eml
"""
)

with open("finess_eml.csv", "r") as f:
    db.copy_expert("COPY finess_eml FROM STDIN WITH (FORMAT CSV, DELIMITER ';')", f)

# CNAMTS le 20 octobre 2016 - Données complémentaires professionnels de Santé - https://static.data.gouv.fr/resources/annuaire-sante-de-la-cnam/20180404-182737/Annuaire_sante_-_Description_contenu_PS_InfosPratiques.pdf
run(
    """
    DROP TABLE IF EXISTS cnamts_prof_sante CASCADE;
    CREATE TABLE IF NOT EXISTS cnamts_prof_sante (
        sexe text,
        nom text,
        prenom text,
        adr1 text,
        adr2 text,
        adr3 text,
        adr4 text,
        cp text,
        ville text,
        telephone text,
        profession text,
        metier text,
        nature text,
        convention text,
        contrat text,
        sesam_vitale text
    );
    TRUNCATE cnamts_prof_sante;
"""
)

with open("ps-infospratiques.csv", "r") as f:
    db.copy_expert("COPY cnamts_prof_sante FROM STDIN WITH (FORMAT CSV)", f)

# CNAMTS le 20 octobre 2016 - Professionnels de santé au sein des établissements https://static.data.gouv.fr/resources/annuaire-sante-de-la-cnam/20180404-180544/Annuaire_sante_-_Description_contenu_PSDansEtablissements.pdf
run(
    """
    CREATE TABLE IF NOT EXISTS cnamts_prof_sante_etab (nom_etab text,
        adr1 text,
        adr2 text,
        adr3 text,
        cp text,
        ville text,
        telephone text,
        type_etab text,
        nom text,
        prenom text,
        profession text
    );
    TRUNCATE cnamts_prof_sante_etab;
"""
)

with open("psdansetablissements.csv", "r") as f:
    db.copy_expert(
        "COPY cnamts_prof_sante_etab FROM STDIN WITH (FORMAT CSV, DELIMITER ';', QUOTE e'\x01')",
        f,
    )

for table in tqdm(
    [
        "cnamts_codes_profession",
        "cnamts_codes_nature_exercice",
        "cnamts_codes_mode_exercice",
    ]
):
    # path = f"{os.environ.get('RACINE')}/data-cnamts/{table}.csv"
    path = f"../../data-cnamts/{table}.csv"
    fields, _ = utilitaires.get_fields(path)

    run(
        f"""
        DROP TABLE IF EXISTS {table} CASCADE;
        CREATE TABLE {table} ({fields});
        TRUNCATE {table};
    """
    )

    with open(path, "r") as f:
        db.copy_expert(f"COPY {table} FROM STDIN WITH (FORMAT CSV, HEADER TRUE)", f)

run(
    """
    CREATE OR REPLACE VIEW cnamts_prof_sante_lib AS
    SELECT
        ps.*, prof.profession as lib_profession, mode.mode_exercice, nat.nature_exercice
    FROM cnamts_prof_sante ps
    LEFT JOIN cnamts_codes_profession prof ON (prof.code=ps.profession)
    LEFT JOIN cnamts_codes_mode_exercice mode ON (mode.code=ps.metier)
    LEFT JOIN cnamts_codes_nature_exercice nat ON (nat.code=ps.nature);

    create or replace view cnamts_prof_sante_etab_lib as
    select ps.*, prof.profession AS lib_profession
    FROM cnamts_prof_sante_etab ps
    LEFT JOIN cnamts_codes_profession prof ON prof.code = ps.profession;
"""
)

run(
    """
    CREATE TABLE IF NOT EXISTS drees_acces_urgences_2015 (
        depcom text,
        urgences text,
        smur text,
        mcs text,
        helismur text,
        helico text,
        pop_2014 text,
        typ_equip text,
        tps_su_smur numeric,
        tps_su_smur_mcs numeric,
        tps_helismur numeric,
        tps_total numeric,
        com_equip text,
        densite_pop numeric
    );
    TRUNCATE drees_acces_urgences_2015;
"""
)

with open("drees/diagnostic_d_acces_aux_soins_urgents_au_31_12_2015.csv", "r") as f:
    db.copy_expert(
        "COPY drees_acces_urgences_2015 FROM STDIN WITH (FORMAT CSV, HEADER TRUE)", f
    )

run(
    """
    CREATE INDEX IF NOT EXISTS drees_acces_urgences_2015_idx ON drees_acces_urgences_2015 (depcom);
"""
)

run(
    """
    CREATE TABLE IF NOT EXISTS drees_acces_urgences_2019 (
        depcom text,
        urgences text,
        smur text,
        mcs text,
        helismur text,
        helico text,
        pop_2017 text,
        typ_equip text,
        tps_su_smur numeric,
        tps_su_smur_mcs numeric,
        tps_helismur numeric,
        tps_total numeric,
        com_equip text,
        densite_pop numeric
    );
    TRUNCATE drees_acces_urgences_2019;
"""
)

with open("drees/diagnostic_d_acces_aux_soins_urgents_au_31_12_2019.csv", "r") as f:
    db.copy_expert(
        "COPY drees_acces_urgences_2019 FROM STDIN WITH (FORMAT CSV, HEADER TRUE)", f
    )

run(
    """
    CREATE INDEX IF NOT EXISTS drees_acces_urgences_2019_idx ON drees_acces_urgences_2019 (depcom);
"""
)

run(
    """
    CREATE TABLE IF NOT EXISTS population_france (
        annee numeric,
        france numeric,
        france_metropolitaine numeric
    );
    TRUNCATE population_france;
"""
)

# Use sed to remove the lines containing "en milliers" in "population_france.csv"
# sed -i '/en milliers/d' population_france.csv
os.system("sed -i '/en milliers/d' population_france.csv")

with open("population_france.csv", "r") as f:
    db.copy_expert(
        "COPY population_france FROM STDIN WITH (FORMAT CSV, HEADER TRUE)", f
    )
pg.commit()

utilitaires.end("sante.py")
