"""Convert the downloaded meteo data to csv files."""

import os
import xarray as xr
from tqdm import tqdm
import pandas as pd
import multiprocessing as mp
import geopandas as gpd
from datacirco import utilitaires
from datetime import datetime

START_YEAR = 1960
# START_YEAR = 2024  # Attention, le fichier final ne contiendra pas les données précédentes !
END_YEAR = datetime.today().year - 1
MONTH = [str(i + 1).zfill(2) for i in range(12)]
years = range(START_YEAR, END_YEAR + 1)
# year_month = [("1962","05"), ("2020","05")]  # For testing
year_month = [(y, m) for y in years for m in MONTH]
print(f"Processing {len(year_month)} months.")


def convert_crs(ds):
    ds.rio.write_crs("epsg:4326", inplace=True)
    ds.coords["longitude"] = (ds.coords["longitude"] + 180) % 360 - 180
    ds = ds.sortby(ds.longitude)
    return ds


# https://gisco-services.ec.europa.eu/distribution/v2/nuts/shp/NUTS_RG_20M_2021_4326.shp.zip

frontieres_europe = gpd.clip(
    gpd.read_file("NUTS_RG_20M_2021_4326.shp"), (-10, 35, 35, 70), keep_geom_type=False
)
# https://gitlab.lre.epita.fr/cours/mooc-python/-/raw/master/data/FRA/FRA_adm0.shp?inline=false
frontieres_france_metro = gpd.read_file("FRA_adm0.shp")
# https://www.data.gouv.fr/en/datasets/carte-des-circonscriptions-legislatives-2012-et-2017/#/resources/91fd8357-c753-43d8-b722-760305614c92
# https://static.data.gouv.fr/resources/carte-des-circonscriptions-legislatives-2012-et-2017/20170721-135755/france-circonscriptions-legislatives-2012.zip
frontieres_france_toute_circo_detail = gpd.read_file(
    "france-circonscriptions-legislatives-2012.shp"
)
frontieres_france_toute_circo_exterieurs = (
    frontieres_france_toute_circo_detail.geometry.unary_union
)
frontieres_france_toute_circo = gpd.GeoDataFrame(
    geometry=[frontieres_france_toute_circo_exterieurs],
    crs=frontieres_france_toute_circo_detail.crs,
)


def process_one_month(
    year,
    month,
    frontieres_europe,
    frontieres_france_metro,
    frontieres_france_toute_circo,
):
    file = f"./ERA5/monthly_dl/{year}-{month}_global.netcdf"
    if os.path.isfile(file) is False:
        raise Exception(f"File {file} not found in {os.getcwd()}")
    ds_global = xr.open_dataset(file, engine="netcdf4")
    if ds_global is None:
        raise Exception(f"Error opening file {file}")
    # Conversion des coordonnées
    era = convert_crs(ds_global)
    # Filtrage sur l'Europe
    era_masked_eu = era.rio.clip(
        frontieres_europe.geometry.values, frontieres_europe.crs, all_touched=True
    )
    # Filtrage sur la France
    era_masked_fr = era.rio.clip(
        frontieres_france_toute_circo.geometry.values,
        frontieres_france_toute_circo.crs,
        all_touched=True,
    )
    era_masked_fr_metro = era_masked_eu.rio.clip(
        frontieres_france_metro.geometry.values,
        frontieres_france_metro.crs,
        all_touched=True,
    )
    jours_mois = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    nb_jour_mois = jours_mois[int(month) - 1]
    # Monde
    ds_global_t2m_celcius_world = era.t2m - 273.15  # K to Celcius
    ds_global_tp_mm_world = era.tp * 1_000 * nb_jour_mois  # m to mm and dy to month
    # Europe
    ds_global_t2m_celcius_eu = era_masked_eu.t2m - 273.15  # K to Celcius
    ds_global_tp_mm_eu = era_masked_eu.tp * 1_000 * nb_jour_mois  # m to mm
    # France Metropolitaine
    ds_global_t2m_celcius_fr_metro = era_masked_fr_metro.t2m - 273.15  # K to Celcius
    ds_global_tp_mm_fr_metro = era_masked_fr_metro.tp * 1_000 * nb_jour_mois  # m to mm
    # France toutes circos
    ds_global_t2m_celcius_fr = era_masked_fr.t2m - 273.15  # K to Celcius
    ds_global_tp_mm_fr = era_masked_fr.tp * 1_000 * nb_jour_mois  # m to mm

    return {
        "year": year,
        "month": month,
        "global": {
            "temp_celcius": float(ds_global_t2m_celcius_world.mean().values),
            "precipitation_mm": float(ds_global_tp_mm_world.mean().values),
        },
        "europe": {
            "data-temp": ds_global_t2m_celcius_eu,
            "data-rain": ds_global_tp_mm_eu,
            "temp_celcius": float(ds_global_t2m_celcius_eu.mean().values),
            "precipitation_mm": float(ds_global_tp_mm_eu.mean().values),
        },
        "france": {
            # Les données sont pour toutes les circos
            "data-temp": ds_global_t2m_celcius_fr,
            "data-rain": ds_global_tp_mm_fr,
            # Le chiffre de moyenne est celui de la France Metro
            "temp_celcius": float(ds_global_t2m_celcius_fr_metro.mean().values),
            "precipitation_mm": float(ds_global_tp_mm_fr_metro.mean().values),
        },
    }


def convert_to_pandas(ds):
    df = ds.to_dataframe().dropna().reset_index().drop(["spatial_ref"], axis=1)

    df.rename(
        {
            "time": "date",
            "valid_time": "date",
            "t2m": "temperature",
            "tp": "precipitation",
        },
        axis=1,
        inplace=True,
    )
    return df


def prepare_pandas(data):
    df_temp = convert_to_pandas(data["france"]["data-temp"])
    df_rain = convert_to_pandas(data["france"]["data-rain"])
    df_temp["date"] = df_temp["date"].dt.floor("d")
    df_rain["date"] = df_rain["date"].dt.floor("d")
    df = pd.merge(df_temp, df_rain, how="inner", on=["date", "latitude", "longitude"])
    assert len(df) == len(df_temp)
    df["annee"] = df["date"].dt.year
    df["mois"] = df["date"].dt.month
    df = df[
        [
            "longitude",
            "latitude",
            "date",
            "annee",
            "mois",
            "precipitation",
            "temperature",
        ]
    ]
    return df


def extract_data(data):
    world = {
        "year": data["year"],
        "month": data["month"],
        "temp_celcius": data["global"]["temp_celcius"],
        "precipitation_mm": data["global"]["precipitation_mm"],
    }
    europe = {
        "year": data["year"],
        "month": data["month"],
        "temp_celcius": data["europe"]["temp_celcius"],
        "precipitation_mm": data["europe"]["precipitation_mm"],
    }
    france = {
        "year": data["year"],
        "month": data["month"],
        "temp_celcius": data["france"]["temp_celcius"],
        "precipitation_mm": data["france"]["precipitation_mm"],
    }
    # Sauve les données ERA5 sur le périmètre des circonscriptions
    df = prepare_pandas(data)
    path_to_csv_file = f"./ERA5/FR_circo_ERA5_{data['year']}-{data['month']}.csv"
    df.to_csv(path_to_csv_file, sep=",", index=False)
    return {
        "world": world,
        "europe": europe,
        "france": france,
    }


world = []
europe = []
france = []


def store_data(data):
    world.append(data["world"])
    europe.append(data["europe"])
    france.append(data["france"])


def process_and_save(year_month):
    year, month = year_month
    if len(year) != 4 or len(month) != 2:
        raise Exception(f"Invalid year and month {year_month}")
    data = process_one_month(
        year,
        month,
        frontieres_europe,
        frontieres_france_metro,
        frontieres_france_toute_circo,
    )
    data = extract_data(data)
    return data


def worker(year_and_month, return_dict):
    """worker function
    Args:
        year_and_month : year and month in the form "YYYY-MM"
    """
    data = process_and_save((year_and_month[0:4], year_and_month[5:7]))
    return_dict[year_and_month] = {
        "world": data["world"],
        "europe": data["europe"],
        "france": data["france"],
    }


def prepare_monthly_mean(data) -> pd.DataFrame:
    df = pd.DataFrame(data)
    df["date_str"] = df["year"].astype(str) + df["month"].astype(str)
    df["date"] = pd.to_datetime(df["date_str"], format="%Y%m")
    df.rename(
        {
            "year": "annee",
            "month": "mois",
            "temp_celcius": "temperature",
            "precipitation_mm": "precipitation",
        },
        axis=1,
        inplace=True,
    )
    df = df[["date", "annee", "mois", "precipitation", "temperature"]]
    return df


def save_csv(df, area):
    path_to_csv_file = f"./ERA5/ERA5_moyenne_mensuelle_{area}.csv"
    df.to_csv(path_to_csv_file, sep=",", index=False)


if __name__ == "__main__":
    utilitaires.start("meteo_convert.py")
    manager = mp.Manager()
    return_dict = manager.dict()
    jobs = []
    i = 0
    for k in tqdm(year_month):
        p = mp.Process(target=worker, args=(str(k[0]) + "-" + str(k[1]), return_dict))
        jobs.append(p)
        p.start()
        i += 1
        if i > mp.cpu_count() + 1:
            for proc in jobs:
                proc.join()
            jobs = []
            i = 0
    for proc in jobs:
        proc.join()

    world = []
    europe = []
    france = []
    for d in tqdm(return_dict):
        # print("---------------------------")
        # print(d)
        # print(return_dict.get(d))
        world.append(return_dict.get(d).get("world"))
        europe.append(return_dict.get(d).get("europe"))
        france.append(return_dict.get(d).get("france"))

    df_world = prepare_monthly_mean(world)
    df_europe = prepare_monthly_mean(europe)
    df_france = prepare_monthly_mean(france)

    save_csv(df_world, "monde")
    save_csv(df_europe, "europe")
    save_csv(df_france, "france")
    utilitaires.end("meteo_convert.py")
