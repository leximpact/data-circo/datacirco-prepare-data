from datacirco import utilitaires
import os
from datacirco.connexion_db import db, pg, run

utilitaires.start("equipements.py")

print("base permanente des équipements (BPE) Insee")

utilitaires.unzip_file("bpe21_ensemble_xy_csv.zip")
fields, _ = utilitaires.get_fields("bpe21_ensemble_xy.csv", ";")
run(
    f"""
    DROP TABLE IF EXISTS insee_bpe CASCADE;
    CREATE TABLE insee_bpe ({fields});
    ALTER TABLE insee_bpe ALTER lambert_x type float using lambert_x::numeric;
    ALTER TABLE insee_bpe ALTER lambert_y type float using lambert_y::numeric;
"""
)

with open("bpe21_ensemble_xy.csv", "r") as f:
    db.copy_expert(
        "COPY insee_bpe FROM STDIN WITH (FORMAT CSV, HEADER TRUE, DELIMITER ';')", f
    )

with open("Varmod_bpe21_ensemble_xy.csv", "r") as src, open("output.csv", "w") as dest:
    for line in src:
        dest.write(line.replace('"', ""))
os.remove("Varmod_bpe21_ensemble_xy.csv")
os.rename("output.csv", "Varmod_bpe21_ensemble_xy.csv")

fields, _ = utilitaires.get_fields("Varmod_bpe21_ensemble_xy.csv", ";")
run(
    f"""
    DROP TABLE IF EXISTS insee_bpe_vars CASCADE;
    CREATE TABLE insee_bpe_vars ({fields});
"""
)

with open("Varmod_bpe21_ensemble_xy.csv", "r") as f:
    db.copy_expert(
        "COPY insee_bpe_vars FROM STDIN WITH (FORMAT CSV, HEADER TRUE, DELIMITER ';')",
        f,
    )
    pg.commit()

utilitaires.end("equipements.py")
