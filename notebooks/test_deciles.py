from datacirco.connexion_db import db
import pandas as pd


print("Début du calcul")
sql_estimation = """SELECT estim.ref, reel.decile_1_reel, reel.mediane_reel, reel.decile_9_reel, estim.decile_1_estime, estim.mediane_estime, estim.decile_9_estime
    FROM (
        SELECT valeurs.ref, 
    SUM(cast(valeurs.DISP_D120 as float)*pop*surface/somme_totale.pond_revenu) as decile_1_estime,
    SUM(cast(valeurs.DISP_MED20 as float)*valeurs.pop*valeurs.surface/somme_totale.pond_revenu) as mediane_estime,
    SUM(cast(valeurs.DISP_D920 as float)*valeurs.pop*valeurs.surface/somme_totale.pond_revenu) as decile_9_estime
        from (
            SELECT circo.ref, reveni.*, st_area(st_intersection(iris.wkb_geometry, circo.wkb_geometry)) as surface, popi.p20_pop::numeric as pop
            FROM insee_pop_2020 as popi
            JOIN insee_revenu_pauvrete_2020 as reveni ON popi.iris = reveni.iris
            JOIN iris_ge AS iris ON iris.code_iris = popi.iris
            JOIN zone_circo AS circo ON st_intersects(iris.wkb_geometry, circo.wkb_geometry)
            WHERE popi.p20_pop IS NOT NULL
            AND reveni.DISP_D120 IS NOT NULL
            AND reveni.DISP_D120 NOT IN ('ns','nd')) as valeurs
            JOIN (
                    SELECT circo2.ref, SUM(st_area(st_intersection(iris2.wkb_geometry, circo2.wkb_geometry))*popi2.p20_pop::numeric) as pond_revenu
                    FROM insee_pop_2020 as popi2
                    JOIN insee_revenu_pauvrete_2020 as reveni2 ON popi2.iris = reveni2.iris
                    JOIN iris_ge AS iris2 ON iris2.code_iris = popi2.iris
                    JOIN zone_circo AS circo2 ON st_intersects(iris2.wkb_geometry, circo2.wkb_geometry)
                    WHERE popi2.p20_pop IS NOT NULL
                    AND reveni2.DISP_D120 IS NOT NULL
                    AND reveni2.DISP_D120 NOT IN ('ns','nd')
                    GROUP BY 1) AS somme_totale
            ON somme_totale.ref = valeurs.ref
            GROUP BY 1) as estim
            LEFT JOIN (SELECT circo,
            "D1_diff" as decile_1_reel,
            "nivvie_median_diff" as mediane_reel,
            "D9_diff" as decile_9_reel FROM insee_indic_stat_circo_2022
            ) as reel
            ON reel.circo = estim.ref;"""
df_decile_estim = pd.read_sql(sql_estimation, con=db.connection)
print("Fin de l'import dans la base")
df_decile_estim["difference_d1"] = df_decile_estim["decile_1_reel"].astype(
    int
) - df_decile_estim["decile_1_estime"].astype(int)
df_decile_estim["difference_mediane"] = df_decile_estim["mediane_reel"].astype(
    int
) - df_decile_estim["mediane_estime"].astype(int)
df_decile_estim["difference_d9"] = df_decile_estim["decile_9_reel"].astype(
    int
) - df_decile_estim["decile_9_estime"].astype(int)
df_decile_estim.to_csv(
    "notebooks/estimation_decile_circo.csv", index_label="ref", index=False
)

print("Fin du programme")
