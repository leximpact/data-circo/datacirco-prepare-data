from datacirco.connexion_db import db
from datacirco.modules_data.data import DataCirco, GetDataCirco
from datacirco.modules_data.sante_data import GetSante
from datacirco import utilitaires
from tqdm import tqdm

# dept="15"
# circo = "015-02"
# dept = circo[1:3] if circo[0]=='0' else circo[:3]
# data = GetDataCirco(db=db, data=DataCirco(dept, circo))
# in_communes = data.data.in_communes
# sante = GetSante(db=db, data=DataCirco(dept, circo))
# sante.get_prof_sante(in_communes)
# sante.data.sante.prof_sante
utilitaires.db_exec(
    db.mogrify("""SELECT DISTINCT ref FROM zone_circo WHERE  ref not LIKE '9%';""")
)
liste_circo = list(i[0] for i in db.fetchall())
liste = list()
prop_min = 1
pop_min = 163019
prof_min = 163019
for circo in tqdm(liste_circo):
    try:
        print(prop_min)
        dept = circo[1:3] if circo[0] == "0" else circo[:3]
        data = GetDataCirco(db=db, data=DataCirco(dept, circo))
        in_communes = data.data.in_communes
        sante = GetSante(db=db, data=DataCirco(dept, circo))
        sante.get_prof_sante(in_communes)
        prof_sante = sante.data.sante.prof_sante
        sante.get_population(in_communes)
        population = sante.data.sante.population
        for prof in prof_sante:
            if (prof["nb_circo"] != 0) and prof["nb_circo"] / population < prop_min:
                prop_min = prof["nb_circo"] / population
                pop_min = population
                prof_min = prof["nb_circo"]
        with open("notebooks/prop_min.txt") as f:
            f.write(
                "Proportion minimale: "
                + str(prop_min)
                + "Population ,"
                + str(pop_min)
                + ", Nombre minimal de professionnels :"
                + str(prof_min)
            )
    except Exception as e:
        print(circo)
        print(e)
        print(prop_min)
        print(prof_min)
        print(pop_min)
