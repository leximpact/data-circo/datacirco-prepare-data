
SELECT * FROM sdes_occupation_sols_2012 LIMIT 5;

SELECT DISTINCT code_12, libelle_fr FROM sdes_occupation_sols_2012_metro_drom;

SELECT code_occupation, nom_occupation FROM sdes_occupation_sols_2012_metro_drom_aggregated;

WITH 
total_surface_circo AS (
    SELECT SUM(st_area(st_intersection(c.wkb_geometry, tp.wkb_geometry)::geography))/10000 as surface_totale_circo_ha
    FROM sdes_occupation_sols_2012_metro_drom as tp
    LEFT JOIN zone_circo c ON st_intersects(c.wkb_geometry, tp.wkb_geometry)
    WHERE c.ref = '091-07'
),
occupation_surfaces_circo AS (
    SELECT 
        t.code_12 as code_occupation, 
        MIN(t.libelle_fr) AS nom_occupation,
        SUM(st_area(st_intersection(c.wkb_geometry,t.wkb_geometry)::geography))/10000 as aire_oc_ha
    FROM sdes_occupation_sols_2012_metro_drom as t
    LEFT JOIN zone_circo c ON st_intersects(c.wkb_geometry, t.wkb_geometry)
    WHERE c.ref = '091-07'
    GROUP BY 1
)
SELECT 
    oc.code_occupation as code_occupation,
    clc.nom_occupation as nom_occupation, 
    tc.surface_totale_circo_ha as surface_circo,
    oc.aire_oc_ha as aire_oc_circo_ha,
    oc.aire_oc_ha / tc.surface_totale_circo_ha as part_superficie_circo,
    agg.surface_totale_france_ha,
    agg.aire_oc_ha_france,
    agg.part_superficie_france
FROM occupation_surfaces_circo oc
CROSS JOIN total_surface_circo tc
JOIN sdes_occupation_sols_2012_metro_drom_aggregated agg ON agg.code_occupation = oc.code_occupation
JOIN sdes_occupation_sols_2012_nomenclature clc ON clc.code_occupation = agg.code_occupation
ORDER BY clc.nom_occupation;


SELECT t.code_12 as code_occupation, t.libelle_fr AS nom_occupation, SUM(st_area(st_intersection(c.wkb_geometry,t.wkb_geometry)::geography))/10000 as aire_oc,
    SUM(st_area(t.wkb_geometry::geography))/10000 as aire_oc_full, SUM(t.area_ha) as sum_area_ha
        FROM sdes_occupation_sols_2012 as t
        LEFT JOIN zone_circo c ON st_intersects(c.wkb_geometry, t.wkb_geometry)
        WHERE c.ref = '976-01'
        GROUP BY 1, 2
        ORDER BY sum_area_ha;
/*
 code_occupation |                                    nom_occupation                                    |      aire_oc       |    aire_oc_full    |    sum_area_ha
-----------------+--------------------------------------------------------------------------------------+--------------------+--------------------+--------------------
 5230            | Mers et océans                                                                       | 45.603845774092775 | 166600.36259239021 | 166468.75891900000

=> Il faut découper les zones d'après la frontière de la circo et/ou calculer le prorata de surface
*/
SELECT SUM(area_ha) as somme_ha, SUM(st_area(wkb_geometry::geography)) as aire_oc FROM sdes_occupation_sols_2012;
       somme_ha       |      aire_oc
----------------------+-------------------
 66 092 395 ha | 661 144 111 288 m2


SELECT SUM(st_area(wkb_geometry::geography)) FROM zone_circo;
        sum
-------------------
 855 071 546 015 m2

https://fr.wikipedia.org/wiki/Superficie_de_la_France


SELECT oc.code_occupation, oc.nom_occupation, oc.aire_oc_ha
    FROM (
        SELECT t.code_12 as code_occupation, t.libelle_fr AS nom_occupation,
        SUM(st_area(st_intersection(c.wkb_geometry,t.wkb_geometry)::geography))/10000 as aire_oc_ha
        FROM sdes_occupation_sols_2012 as t
        LEFT JOIN zone_circo c ON st_intersects(c.wkb_geometry, t.wkb_geometry)
        WHERE c.ref = '976-01'
        GROUP BY 1, 2
        ) as oc
    ORDER BY oc.aire_oc;




WITH 
total_surface_france AS (
    SELECT SUM(st_area(tp.wkb_geometry::geography))/10000 as surface_totale_france_ha
    FROM sdes_occupation_sols_2012 as tp
),
total_surface_circo AS (
    SELECT SUM(st_area(st_intersection(c.wkb_geometry, tp.wkb_geometry)::geography))/10000 as surface_totale_circo_ha
    FROM sdes_occupation_sols_2012 as tp
    LEFT JOIN zone_circo c ON st_intersects(c.wkb_geometry, tp.wkb_geometry)
    WHERE c.ref = '976-01'
),
occupation_surfaces_circo AS (
    SELECT 
        t.code_12 as code_occupation, 
        t.libelle_fr AS nom_occupation,
        SUM(st_area(st_intersection(c.wkb_geometry,t.wkb_geometry)::geography))/10000 as aire_oc_ha
    FROM sdes_occupation_sols_2012 as t
    LEFT JOIN zone_circo c ON st_intersects(c.wkb_geometry, t.wkb_geometry)
    WHERE c.ref = '976-01'
    GROUP BY 1, 2
),
occupation_surfaces_france AS (
    SELECT 
        t.code_12 as code_occupation, 
        t.libelle_fr AS nom_occupation,
        SUM(st_area(t.wkb_geometry::geography))/10000 as aire_oc_ha_france
    FROM sdes_occupation_sols_2012 as t
    GROUP BY 1, 2
)
SELECT 
    oc.code_occupation as code_occupation, 
    oc.nom_occupation as code_occupation, 
    tc.surface_totale_circo_ha as surface_circo,
    oc.aire_oc_ha as aire_oc_circo_ha,
    oc.aire_oc_ha / tc.surface_totale_circo_ha as part_superficie_circo,
    ts.surface_totale_france_ha as surface_totale_france_ha,
    oc_fr.aire_oc_ha_france as aire_oc_france_ha,
    oc_fr.aire_oc_ha_france / ts.surface_totale_france_ha as part_superficie_france
FROM occupation_surfaces_circo oc, total_surface_france ts, total_surface_circo tc, occupation_surfaces_france oc_fr
WHERE oc_fr.code_occupation = oc.code_occupation
ORDER BY oc.aire_oc_ha;

 code_occupation |                                   code_occupation                                    |   surface_circo    |  aire_oc_circo_ha  | part_superficie_circo  | surface_totale_france_ha | aire_oc_france_ha  | part_superficie_france
-----------------+--------------------------------------------------------------------------------------+--------------------+--------------------+------------------------+--------------------------+--------------------+------------------------
 5210            | Lagunes littorales                                                                   | 11835.714038998289 | 0.1864170493815567 | 1.5750384705757393e-05 |        66114411.12880571 |  277.3201772691332 |  4.194549607782957e-06
 4210            | Marais maritimes                                                                     | 11835.714038998289 |  3.102083175376474 | 0.00026209514399851266 |        66114411.12880571 |  760.7927835300158 |  1.150721560610773e-05
 2111            | Terres arables hors périmètres d'irrigation                                          | 11835.714038998289 |  10.06496984774232 |   0.000850389745357025 |        66114411.12880571 |  950.4660351176818 | 1.4376079570094948e-05
 3112            | Mangroves                                                                            | 11835.714038998289 | 13.036383990442104 |  0.0011014446570344338 |        66114411.12880571 | 63126.102263321016 |  0.0009548009455962211
 1410            | Espaces verts urbains                                                                | 11835.714038998289 | 14.744207495464797 |   0.001245738740128658 |        66114411.12880571 |  750.8660116102188 | 1.1357070248230198e-05
 1330            | Chantiers                                                                            | 11835.714038998289 |  16.53939006699144 |   0.001397413794596143 |        66114411.12880571 |  194.5006661396602 | 2.9418800352124327e-06
 2310            | Prairies et autres surfaces toujours en herbe à usage agricole                       | 11835.714038998289 | 17.701809024378036 |  0.0014956266234594008 |        66114411.12880571 |  27281.42319020446 | 0.00041263958529486294
 2420            | Systèmes culturaux et parcellaires complexes                                         | 11835.714038998289 | 22.773820636076277 |  0.0019241611077318434 |        66114411.12880571 |  37016.92472131307 |   0.000559891922037932
 1230            | Zones portuaires                                                                     | 11835.714038998289 | 26.892311974733353 |   0.002272132622174215 |        66114411.12880571 |  658.3375339635363 |  9.957549688840865e-06
 5120            | Plans d'eau                                                                          | 11835.714038998289 |  41.81410150592959 |   0.003532875276316537 |        66114411.12880571 |  669.3906802225092 | 1.0124731791354625e-05
 5230            | Mers et océans                                                                       | 11835.714038998289 | 45.603845774092775 |   0.003853070936305964 |        66114411.12880571 | 1477643.2965010512 |    0.02234979138848065
 1310            | Extraction de matériaux                                                              | 11835.714038998289 |  65.84196542466063 |   0.005562990556185585 |        66114411.12880571 |  1420.418326478032 | 2.1484246811345537e-05
 2222            | Bananeraies                                                                          | 11835.714038998289 |    81.211221159181 |   0.006861539649538059 |        66114411.12880571 | 12845.876449018548 |  0.0001942976762508237
 1240            | Aéroports                                                                            | 11835.714038998289 |  90.94245876815012 |  0.0076837323433548415 |        66114411.12880571 | 1485.6910509720014 |  2.247151605234057e-05
 1110            | Tissu urbain continu                                                                 | 11835.714038998289 | 139.00074810669088 |   0.011744179324431798 |        66114411.12880571 |  517.3264928682426 |  7.824716034456913e-06
 1210            | Zones industrielles ou commerciales et installations publiques                       | 11835.714038998289 | 208.81374728026833 |    0.01764268269681355 |        66114411.12880571 |  4414.242331981626 |   6.67667193372061e-05
 3330            | Végétation clairsemée                                                                | 11835.714038998289 | 221.19843890730124 |   0.018689065837385024 |        66114411.12880571 |  11389.11881043897 | 0.00017226378660849614
 3240            | Forêt et végétation arbustive en mutation                                            | 11835.714038998289 | 255.31367747922795 |   0.021571463845609803 |        66114411.12880571 |  82022.64974169366 |   0.001240616808669673
 3230            | Végétation sclérophylle                                                              | 11835.714038998289 |  371.9575805458194 |    0.03142671234876336 |        66114411.12880571 | 21792.649852130802 |  0.0003296202670500056
 2223            | Palmeraies                                                                           | 11835.714038998289 |   854.364999698089 |    0.07218533642186559 |        66114411.12880571 |  2464.495125410169 |  3.727621683884893e-05
 1120            | Tissu urbain discontinu                                                              | 11835.714038998289 |  984.2810790207453 |    0.08316195168095238 |        66114411.12880571 |  63618.61572449972 |  0.0009622503571960489
 2430            | Surfaces essentiellement agricoles, interrompues par des espaces naturels importants | 11835.714038998289 |   3029.20826879184 |    0.25593793993422775 |        66114411.12880571 |  75352.83837559426 |   0.001139733941345859
 3111            | Forêts de feuillus                                                                   | 11835.714038998289 |  5321.120513275713 |     0.4495817063290644 |        66114411.12880571 |  728279.7851773998 |   0.011015446900956998


SELECT tot_circo.nom_occupation, tot_circo.nb_ha_circo::numeric, tot_circo.part_circo, tot_france.part_france, aire_oc, somme_circo  FROM (
                    WITH somme_aire_circo AS (
                        SELECT SUM(st_area(st_intersection(c1.wkb_geometry, oc1.wkb_geometry)::geography)) as somme_circo
                        FROM sdes_occupation_sols_2012 as oc1
                        LEFT JOIN zone_circo c1
                        ON st_intersects(c1.wkb_geometry, oc1.wkb_geometry)
                        WHERE c1.ref = '976-01')
                            SELECT oc.code_occupation, oc.nom_occupation, SUM(oc.area_ha) as nb_ha_circo, SUM(oc.aire_oc/somme_aire_circo.somme_circo) as part_circo
                            FROM somme_aire_circo, (
                                SELECT t.code_12 as code_occupation, t.libelle_fr AS nom_occupation,
                                st_area(st_intersection(c.wkb_geometry,t.wkb_geometry)::geography) as aire_oc, t.area_ha
                                FROM sdes_occupation_sols_2012 as t
                                LEFT JOIN zone_circo c ON st_intersects(c.wkb_geometry, t.wkb_geometry)
                                WHERE c.ref = '976-01'
                                ) as oc
                            GROUP BY 1, 2
                            ORDER BY part_circo) as tot_circo
                            LEFT JOIN (
                                WITH somme_aire_france AS (
                                    SELECT SUM(st_area(wkb_geometry::geography)) as somme FROM sdes_occupation_sols_2012
                                    )
                                SELECT o.code_12 as type_france, SUM(o.area_ha/somme_aire_france.somme) as part_france
                                FROM sdes_occupation_sols_2012 as o, somme_aire_france
                                GROUP BY 1
                                ) as tot_france
                                ON tot_circo.code_occupation = tot_france.type_france
                                WHERE tot_circo.nom_occupation='Mers et océans';






SELECT tot_circo.nom_occupation, tot_circo.part_circo, tot_france.part_france FROM (
WITH somme_aire_circo AS (
    SELECT SUM(st_area(st_intersection(c1.wkb_geometry, oc1.wkb_geometry))) as somme_circo 
    FROM sdes_occupation_sols_2012 as oc1
    LEFT JOIN zone_circo c1
    ON st_intersects(c1.wkb_geometry, oc1.wkb_geometry)
    WHERE c1.ref = '084-05')
                SELECT oc.code_occupation, oc.nom_occupation, SUM(oc.aire_oc/somme_aire_circo.somme_circo) as part_circo
                    FROM somme_aire_circo, (
                        SELECT t.code_12 as code_occupation, t.libelle_fr AS nom_occupation,
                        st_area(st_intersection(c.wkb_geometry,t.wkb_geometry)) as aire_oc
                        FROM sdes_occupation_sols_2012 as t
                        LEFT JOIN zone_circo c ON st_intersects(c.wkb_geometry, t.wkb_geometry)
                        WHERE c.ref = '084-05'
                        ) as oc
                    GROUP BY 1, 2
                    ORDER BY part_circo

) as tot_circo
LEFT JOIN (
    WITH somme_aire_france AS (SELECT SUM(area_ha) as somme FROM sdes_occupation_sols_2012)
                SELECT o.code_12 as type_france, SUM(o.area_ha/somme_aire_france.somme) as part_france
                    FROM sdes_occupation_sols_2012 as o, somme_aire_france
                    GROUP BY 1
) as tot_france
ON tot_circo.code_occupation = tot_france.type_france;

-------------------------------------------

DROP TABLE sdes_occupation_sols_2012_nomenclature;
CREATE TABLE sdes_occupation_sols_2012_nomenclature (
    code_occupation varchar(3),
    nom_occupation text,
    PRIMARY KEY (code_occupation)
);

INSERT INTO sdes_occupation_sols_2012_nomenclature (code_occupation, nom_occupation) VALUES
('111', 'Tissu urbain continu'),
('112', 'Tissu urbain discontinu'),
('121', 'Zones industrielles ou commerciales et installations publiques'),
('122', 'Réseaux routier et ferroviaire et espaces associés'),
('123', 'Zones portuaires'),
('124', 'Aéroports'),
('131', 'Extraction de matériaux'),
('132', 'Décharges'),
('133', 'Chantiers'),
('141', 'Espaces verts urbains'),
('142', 'Equipements sportifs et de loisirs'),
('211', 'Terres arables hors périmètres d''irrigation'),
('212', 'Périmètres irrigués en permanence'),
('213', 'Rizières'),
('221', 'Vignobles'),
('222', 'Vergers et petits fruits'),
('223', 'Oliveraies'),
('231', 'Prairies et autres surfaces toujours en herbe à usage agricole'),
('241', 'Cultures annuelles associées à des cultures permanentes'),
('242', 'Systèmes culturaux et parcellaires complexes'),
('243', 'Surfaces essentiellement agricoles, interrompues par des espaces naturels importants'),
('244', 'Territoires agroforestiers'),
('311', 'Forêts de feuillus'),
('312', 'Forêts de conifères'),
('313', 'Forêts mélangées'),
('321', 'Pelouses et pâturages naturels'),
('322', 'Landes et broussailles'),
('323', 'Végétation sclérophylle'),
('324', 'Forêt et végétation arbustive en mutation'),
('331', 'Plages, dunes et sable'),
('332', 'Roches nues'),
('333', 'Végétation clairsemée'),
('334', 'Zones incendiées'),
('335', 'Glaciers et neiges éternelles'),
('411', 'Marais intérieurs'),
('412', 'Tourbières'),
('421', 'Marais maritimes'),
('422', 'Marais salants'),
('423', 'Zones intertidales'),
('511', 'Cours et voies d''eau'),
('512', 'Plans d''eau'),
('521', 'Lagunes littorales'),
('522', 'Estuaires'),
('523', 'Mers et océans');
