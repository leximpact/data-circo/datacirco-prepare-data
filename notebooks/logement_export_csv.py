from datacirco.connexion_db import db
import pandas as pd


print("Début du calcul")
sql_estimation = """SELECT estim.*, reel.*,
        estim.pct_res_estim - reel.log_res as difference_pct_res,
        estim.pct_sec_estim - reel.log_sec as difference_pct_sec, 
        estim.pct_maison_estim - maison as difference_pct_maison, 
        estim.pct_proprio_estim - reel.proprio as difference_pct_proprio,
        estim.pct_loc_estim - reel.locatai as difference_pct_locatai,
        estim.pct_gratuit_estim - reel.gratuit as difference_pct_gratuit,
        estim.pct_vacant_estim - reel.log_vac as difference_pct_vacant,
        estim.pct_ach90_estim - reel.acH90 as difference_pct_avant90
FROM (
    SELECT t.ref, SUM(cast(t.P20_RP as float)*t.surface/h.pond_logement)*100 as pct_res_estim,
        SUM(cast(t.P20_RSECOCC as float)*t.surface/h.pond_logement)*100 as pct_sec_estim,
        SUM(cast(t.P20_RPMAISON as float)*t.surface/h.pond_residences_p)*100 as pct_maison_estim,
        SUM(cast(t.P20_RP_PROP as float)*t.surface/h.pond_residences_p)*100 as pct_proprio_estim,
        SUM(cast(t.P20_RP_LOC  as float)*t.surface/h.pond_residences_p)*100 as pct_loc_estim,
        SUM(cast(t.P20_RP_GRAT as float)*t.surface/h.pond_residences_p)*100 as pct_gratuit_estim,
        SUM(cast(t.P20_LOGVAC  as float)*t.surface/h.pond_logement)*100 as pct_vacant_estim,
        SUM((t.P20_RP_ACH19::numeric + t.P20_RP_ACH45::numeric + t.P20_RP_ACH70::numeric + t.P20_RP_ACH90::numeric)*t.surface/h.pond_residences_p)*100 as pct_ach90_estim
            from (
                SELECT log.*, c.ref, st_area(st_intersection(iris.wkb_geometry, c.wkb_geometry))/st_area(iris.wkb_geometry) as surface
                FROM insee_logement_2020 AS log
                JOIN iris_ge AS iris ON iris.code_iris = log.iris
                JOIN zone_circo AS c ON st_intersects(iris.wkb_geometry, c.wkb_geometry)
                WHERE log.P20_NBPI_RP IS NOT NULL) as t
                JOIN (
                    SELECT c2.ref, SUM(st_area(st_intersection(iris2.wkb_geometry, c2.wkb_geometry))*log2.p20_log::numeric /st_area(iris2.wkb_geometry)) as pond_logement,
                    SUM(st_area(st_intersection(iris2.wkb_geometry, c2.wkb_geometry))*log2.P20_RP::numeric /st_area(iris2.wkb_geometry)) as pond_residences_p
                    FROM insee_logement_2020 as log2
                    JOIN iris_ge AS iris2 ON iris2.code_iris = log2.iris
                    JOIN zone_circo AS c2 ON st_intersects(iris2.wkb_geometry, c2.wkb_geometry)
                    WHERE log2.P20_NBPI_RP IS NOT NULL
                    GROUP BY 1) AS h ON t.ref = h.ref
                GROUP BY 1) as estim
            LEFT JOIN (
                SELECT circo, log_res::numeric, log_sec::numeric, log_vac::numeric,
                proprio::numeric, locatai::numeric, gratuit::numeric, maison::numeric, ach90::numeric
                FROM insee_indic_stat_circo_2022
            ) AS reel
            on estim.ref = reel.circo;"""
df_logement_estim = pd.read_sql(sql_estimation, con=db.connection)
print("Fin du calcul")
df_logement_estim.to_csv(
    "notebooks/estimation_logement_circo.csv", index_label="ref", index=False
)
print("Fin du programme")
