SELECT t.ref, SUM(cast(t.P20_RP as float)*t.pop*t.surface/h.pond_logement),
    SUM(cast(t.P20_NBPI_RP as float)*t.pop*t.surface/h.pond_logement),
    SUM(cast(t.P20_RPMAISON as float)*t.pop*t.surface/h.pond_logement),
    SUM(cast(t.P20_RPAPPART as float)*t.pop*t.surface/h.pond_logement),
    SUM(cast(t.P20_RP_PROP as float)*t.pop*t.surface/h.pond_logement),
    SUM(cast(t.P20_RP_LOC  as float)*t.pop*t.surface/h.pond_logement),
    SUM(cast(t.P20_RP_GRAT as float)*t.pop*t.surface/h.pond_logement),
    SUM(cast(t.P20_LOGVAC  as float)*t.pop*t.surface/h.pond_logement),
    SUM((t.P20_RP_ACH19::numeric + t.P20_RP_ACH45::numeric + t.P20_RP_ACH70::numeric + t.P20_RP_ACH90::numeric)*t.pop*t.surface/h.pond_logement)
        from (
            SELECT log.*, c.ref, st_area(st_intersection(iris.wkb_geometry, c.wkb_geometry)) as surface, popi.p20_pop::numeric as pop
            FROM insee_pop_2020 as popi
            JOIN insee_logement_2020 AS log ON popi.iris = log.iris
            JOIN iris_ge AS iris ON iris.code_iris = popi.iris
            JOIN zone_circo AS c ON st_intersects(iris.wkb_geometry, c.wkb_geometry)
            WHERE popi.p20_pop IS NOT NULL AND log.P20_NBPI_RP IS NOT NULL) as t
        JOIN (
                    SELECT c2.ref, SUM(st_area(st_intersection(iris2.wkb_geometry, c2.wkb_geometry))*popi2.p20_pop::numeric) as pond_logement
                    FROM insee_pop_2020 as popi2
                    JOIN insee_logement_2020 as log2 ON popi2.iris = log2.iris
                    JOIN iris_ge AS iris2 ON iris2.code_iris = popi2.iris
                    JOIN zone_circo AS c2 ON st_intersects(iris2.wkb_geometry, c2.wkb_geometry)
                    WHERE popi2.p20_pop IS NOT NULL
                    AND log2.P20_NBPI_RP IS NOT NULL
                    GROUP BY 1) AS h ON t.ref = h.ref
        GROUP BY 1;

SELECT estim.*, reel.* FROM (
    SELECT h.ref, SUM(cast(t.P20_RP as float)*t.pop*t.surface/h.pond_logement) as pct_res_estim,
        SUM(cast(t.P20_RSECOCC as float)*t.pop*t.surface/h.pond_logement) as pct_sec_estim,
        SUM(cast(t.P20_RPMAISON as float)*t.pop*t.surface/h.pond_logement) as pct_maison_estim,
        SUM(cast(t.P20_RP_PROP as float)*t.pop*t.surface/h.pond_logement) as pct_proprio_estim,
        SUM(cast(t.P20_RP_LOC  as float)*t.pop*t.surface/h.pond_logement) as pct_loc_estim,
        SUM(cast(t.P20_RP_GRAT as float)*t.pop*t.surface/h.pond_logement) as pct_gratuit_estim,
        SUM(cast(t.P20_LOGVAC  as float)*t.pop*t.surface/h.pond_logement) as pct_vacant_estim,
        SUM((t.P20_RP_ACH19::numeric + t.P20_RP_ACH45::numeric + t.P20_RP_ACH70::numeric + t.P20_RP_ACH90::numeric)*t.pop*t.surface/h.pond_logement) as pct_ach90_estim
            from (
                SELECT log.*, c.ref, st_area(st_intersection(iris.wkb_geometry, c.wkb_geometry)) as surface, popi.p20_pop::numeric as pop
                FROM insee_pop_2020 as popi
                JOIN insee_logement_2020 AS log ON popi.iris = log.iris
                JOIN iris_ge AS iris ON iris.code_iris = popi.iris
                JOIN zone_circo AS c ON st_intersects(iris.wkb_geometry, c.wkb_geometry)
                WHERE popi.p20_pop IS NOT NULL AND log.P20_NBPI_RP IS NOT NULL) as t
            JOIN (
                        SELECT c2.ref, SUM(st_area(st_intersection(iris2.wkb_geometry, c2.wkb_geometry))*popi2.p20_pop::numeric*log2.P20_log::numeric) as pond_logement
                        FROM insee_pop_2020 as popi2
                        JOIN insee_logement_2020 as log2 ON popi2.iris = log2.iris
                        JOIN iris_ge AS iris2 ON iris2.code_iris = popi2.iris
                        JOIN zone_circo AS c2 ON st_intersects(iris2.wkb_geometry, c2.wkb_geometry)
                        WHERE popi2.p20_pop IS NOT NULL
                        AND log2.P20_NBPI_RP IS NOT NULL
                        GROUP BY 1) AS h ON t.ref = h.ref 
                GROUP BY 1) as estim
            LEFT JOIN (
                SELECT circo, log_res::numeric, log_sec::numeric, log_vac::numeric,
                proprio::numeric, locatai::numeric, gratuit::numeric, maison::numeric, ach90::numeric
                FROM insee_indic_stat_circo_2022
            ) AS reel
            on estim.ref = reel.circo;

select
SUM(p20_log::numeric)
from ign_departement dep
join iris_ge i on (st_intersects(dep.wkb_geometry, i.wkb_geometry))
join insee_logement_2020 p on (p.iris=i.code_iris)
where dep.insee_dep='03';


-- WITH somme_totale AS (
--                     SELECT SUM(st_area(st_intersection(iris2.wkb_geometry, dep2.wkb_geometry))*popi2.p20_pop::numeric) as pond_logement
--                     FROM insee_pop_2020 as popi2
--                     JOIN insee_logement_2020 as log2 ON popi2.iris = log2.iris
--                     JOIN iris_ge AS iris2 ON iris2.code_iris = popi2.iris
--                     JOIN ign_departement  AS dep2 ON st_intersects(iris2.wkb_geometry, dep2.wkb_geometry)
--                     WHERE popi2.p20_pop IS NOT NULL
--                     AND log2.P20_NBPI_RP IS NOT NULL
--                     AND dep2.insee_dep = '02')
--     SELECT SUM(cast(t.P20_RP as float)*t.pop*t.surface/pond_logement)
--         from somme_totale, (
--             SELECT log.*, st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry)) as surface, popi.p20_pop::numeric as pop
--             FROM insee_pop_2020 as popi
--             JOIN insee_logement_2020 AS log ON popi.iris = log.iris
--             JOIN iris_ge AS iris ON iris.code_iris = popi.iris
--             JOIN ign_departement AS dep ON st_intersects(iris.wkb_geometry, dep.wkb_geometry)
--             WHERE popi.p20_pop IS NOT NULL AND log.P20_NBPI_RP IS NOT NULL AND dep.insee_dep = '02') as t;

SELECT SUM(p20_log::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry))/st_area(iris.wkb_geometry))
        FROM insee_logement_2020 AS log 
        JOIN iris_ge AS iris ON iris.code_iris = log.iris
        JOIN ign_departement AS dep ON st_intersects(iris.wkb_geometry, dep.wkb_geometry)
        WHERE log.P20_NBPI_RP IS NOT NULL AND dep.insee_dep = '03';


SELECT SUM(p20_log::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry))/st_area(iris.wkb_geometry)),
SUM(P20_RP::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry))/st_area(iris.wkb_geometry)),
SUM(P20_NBPI_RP::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry))/st_area(iris.wkb_geometry)),
SUM(P20_RPMAISON::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry))/st_area(iris.wkb_geometry)),
SUM(P20_RPAPPART::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry))/st_area(iris.wkb_geometry)),
SUM(P20_RP_PROP::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry))/st_area(iris.wkb_geometry)),
SUM(P20_RP_LOC::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry))/st_area(iris.wkb_geometry)),
SUM(P20_RP_GRAT::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry))/st_area(iris.wkb_geometry)),
SUM(P20_LOGVAC::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry))/st_area(iris.wkb_geometry)),
SUM((P20_RP_ACH19::numeric + P20_RP_ACH45::numeric + P20_RP_ACH70::numeric + P20_RP_ACH90::numeric)*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry))/st_area(iris.wkb_geometry))
        FROM insee_logement_2020  AS log 
        JOIN iris_ge AS iris ON iris.code_iris = log.iris
        JOIN ign_departement AS dep ON st_intersects(iris.wkb_geometry, dep.wkb_geometry)
        WHERE log.P20_NBPI_RP IS NOT NULL AND dep.insee_dep = '{dept}';
