SHELL := /bin/bash

install:
	poetry install
	poetry run python -m ipykernel install --name datacirco --user
	poetry run pre-commit install

precommit:
	-poetry run pre-commit run --all-files
	-echo "XXXXXXXX Last check XXXXXXXXXXX"
	-poetry run pre-commit run --all-files

