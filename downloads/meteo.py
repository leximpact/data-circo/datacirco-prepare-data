"""This file uses the CDS API to download ERA5 data.

We get the monthly averaged reanalysis data for 2m temperature and total
precipitation. The data is downloaded in netcdf format. We handle both
the old, before 2024, and new file format.
"""

import os
import shutil
import multiprocessing as mp
import urllib3
import zipfile

import xarray as xr

# import zarr
import cdsapi
import humanize
import requests
from datacirco import utilitaires

# Disable warning
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

START_YEAR = 2024
END_YEAR = 2024

DATA_DIR = "ERA5/monthly_dl"
"""c.retrieve( 'reanalysis-era5-single-levels-monthly-means', { 'format':

'netcdf', 'product_type': 'monthly_averaged_reanalysis', 'variable': [
'2m_temperature', 'total_precipitation', ], 'year': '2022', 'month': [
'01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12',
], 'time': '00:00', }, 'download.nc')
"""


# FORMAT = "netcdf"
FORMAT = "netcdf"
# Daily
# PRODUCT = 'reanalysis-era5-single-levels'
# TYPE = 'reanalysis'
# TYPE = 'ensemble_members'
TIME = [f"{i}:00".zfill(5) for i in range(24)]

# Monthly
PRODUCT = "reanalysis-era5-single-levels-monthly-means"
VARIABLE = ["total_precipitation", "2m_temperature"]

TYPE = "monthly_averaged_reanalysis"

MONTH = [str(i + 1).zfill(2) for i in range(12)]
DAY = [str(i + 1).zfill(2) for i in range(31)]
TIME = "00:00"


def get_url(year, month):
    cds_client = cdsapi.Client(
        url=os.environ.get("CDS_API_URL"), key=os.environ.get("CDS_API_KEY")
    )
    query = {
        "variable": VARIABLE,
        "product_type": TYPE,
        "year": year,
        "month": month,
        "day": DAY,
        "time": TIME,
        "format": FORMAT,
    }
    cds_r = cds_client.retrieve(PRODUCT, query)
    url = cds_r.location
    file_size = int(cds_r.content_length)
    return url, file_size


def handle_new_file_format(disk_url, temp_dir):
    if zipfile.is_zipfile(disk_url):
        print("C'est le nouveau format. Extracting zip file...")
    else:
        return
    with zipfile.ZipFile(disk_url, "r") as zip_ref:
        shutil.rmtree(temp_dir, ignore_errors=True)
        os.makedirs(temp_dir, exist_ok=True)
        shutil.copy(disk_url, temp_dir)
        zip_ref.extractall(temp_dir)
    files = os.listdir(temp_dir)
    tp_dataset = None
    t2m_dataset = None
    for f in files:
        if f.endswith(".nc"):
            file = os.path.join(temp_dir, f)
            print(file)
            nc_dataset = xr.open_dataset(file, engine="netcdf4")
            if "tp" in nc_dataset.variables:
                tp_dataset = nc_dataset
            elif "t2m" in nc_dataset.variables:
                t2m_dataset = nc_dataset
    if tp_dataset and t2m_dataset:
        merged_dataset = xr.merge([tp_dataset, t2m_dataset])
        # Overwrite the original file
        merged_dataset.to_netcdf(disk_url)
    else:
        raise ValueError(
            "Both tp and t2m variables are not found in the new format files."
        )
    shutil.rmtree(temp_dir, ignore_errors=True)


def dl_cdsapi(year_month):
    """Download one month of data as a single file."""
    # saved filename on disk
    filename = f"{year_month[0]}-{year_month[1]}_global.{FORMAT}"
    disk_url = os.path.join(DATA_DIR, filename)
    if os.path.exists(disk_url):
        print(f"{filename} already exists, skipping...")
        return
    # Make sure that the TCP request returns the right file size:
    size_diff = 1
    while size_diff != 0:
        url, file_size = get_url(str(year_month[0]), year_month[1])
        print(url)
        resp = requests.get(url, stream=True, timeout=(5, 5))
        total_length = int(resp.headers.get("content-length"))
        size_diff = file_size - total_length

    # Retry until the file is complete
    print(f"Writing to {disk_url}")
    dl_size = 0
    while dl_size < file_size:
        with open(disk_url, "ab") as f:
            dl_size = f.tell()
            print(
                "{}, full size:{} DL starting at {}".format(
                    filename,
                    humanize.naturalsize(file_size),
                    humanize.naturalsize(dl_size),
                )
            )
            # If timeout, re-initiate the download
            headers = {"Range": f"bytes={dl_size}-"}
            try:
                resp = requests.get(url, stream=True, timeout=(5, 5), headers=headers)
                shutil.copyfileobj(resp.raw, f)
            except (
                requests.exceptions.ReadTimeout,
                urllib3.exceptions.ReadTimeoutError,
            ):
                continue
            dl_size = f.tell()
    temp_dir = os.path.join(DATA_DIR, f"temp_{year_month[0]}_{year_month[1]}")
    handle_new_file_format(disk_url, temp_dir)


if __name__ == "__main__":
    # Download monthly data
    if not os.path.exists(DATA_DIR):
        print(f"{DATA_DIR} don't exist !")
        exit(1)
    years = range(START_YEAR, END_YEAR + 1)
    year_month = [(y, m) for y in years for m in MONTH]
    module = "meteo"
    utilitaires.clear_dataset_source(module)
    utilitaires.add_dataset_source(
        "meteo-era5",
        module,
        "Données météo de réanalyse ERA5",
        "ECMWF",
        "2024",
        url_information="https://www.ecmwf.int/en/forecasts/dataset/ecmwf-reanalysis-v5",
        url_data="https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels-monthly-means?tab=overview",
        id_reference="ERA5",
    )

    pool = mp.Pool(8)
    pool.map(dl_cdsapi, year_month)
