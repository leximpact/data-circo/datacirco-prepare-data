from datacirco import utilitaires
from tqdm import tqdm
import os
import subprocess
import shutil

module = "elus"

files = [
    {
        "url": "https://www.data.gouv.fr/fr/datasets/r/601ef073-d986-4582-8e1a-ed14dc857fba",
        "name": "elus-conseiller-departemental.csv",
        "description": "Les conseillers départementaux",
    },
    {
        "url": "https://www.data.gouv.fr/fr/datasets/r/d5f400de-ae3f-4966-8cb6-a85c70c6c24a",
        "name": "elus-municipal.csv",
        "description": "Les conseillers municipaux",
    },
    {
        "url": "https://www.data.gouv.fr/fr/datasets/r/430e13f9-834b-4411-a1a8-da0b4b6e715c",
        "name": "elus-regional.csv",
        "description": "Les conseillers régionaux",
    },
    {
        "url": "https://www.data.gouv.fr/fr/datasets/r/1ac42ff4-1336-44f8-a221-832039dbc142",
        "name": "elus-depute.csv",
        "description": "Les députés",
    },
    {
        "url": "https://www.data.gouv.fr/fr/datasets/r/41d95d7d-b172-4636-ac44-32656367cdc7",
        "name": "elus-epci.csv",
        "description": "Les conseillers communautaires",
    },
    {
        "url": "https://www.data.gouv.fr/fr/datasets/r/a595be27-cfab-4810-b9d4-22e193bffe35",
        "name": "elus-membre-assemblees.csv",
        "description": "Les membres des assemblées des collectivités à statut particulier",
    },
    {
        "url": "https://www.data.gouv.fr/fr/datasets/r/2876a346-d50c-4911-934e-19ee07b0e503",
        "name": "elus-maire.csv",
        "description": "Les maires",
    },
    {
        "url": "https://www.data.gouv.fr/fr/datasets/r/70957bb0-f19f-40c5-b97b-90b3d4d71f9e",
        "name": "elus-representant-parlement-europeen.csv",
        "description": "Les représentants au Parlement européen",
    },
    {
        "url": "https://www.data.gouv.fr/fr/datasets/r/b78f8945-509f-4609-a4a7-3048b8370479",
        "name": "elus-senateur.csv",
        "description": "Les sénateurs",
    },
]

utilitaires.start(f"{module}.py")
utilitaires.clear_dataset_source(module)
# Important : bien effacer avant car sinon on garde les députés qui ont quitté l'Assemblée.
print("Députés actifs :")
subprocess.run(
    "rm -rf acteur deport organe pays rne", cwd=os.environ.get("PWD"), shell=True
)
liste_deputes_url = "https://data.assemblee-nationale.fr/static/openData/repository/17/amo/deputes_actifs_mandats_actifs_organes_divises/AMO40_deputes_actifs_mandats_actifs_organes_divises.json.zip"
local_filename = liste_deputes_url.split("/")[-1]
assert utilitaires.download_file(liste_deputes_url, overwrite=True)
utilitaires.unzip_file(local_filename)

datetime = utilitaires.get_last_modified(liste_deputes_url)
utilitaires.add_dataset_source(
    local_filename,
    module,
    "Liste des députés avec un mandat actif",
    "Assemblée nationale",
    datetime.strftime("%Y"),
    liste_deputes_url,
    "https://data.assemblee-nationale.fr/acteurs/deputes-en-exercice",
    id_reference="AN-deputes-en-exercice",
)

dirpath = "./rne"
if os.path.exists(dirpath):
    shutil.rmtree(dirpath)
os.makedirs(dirpath)

print("Données RNE :")
for file in tqdm(files):
    assert utilitaires.download_file(
        file["url"], "./rne/", file["name"], overwrite=True
    )
    datetime = utilitaires.get_last_modified(file["url"])
    if not datetime:
        datetime = utilitaires.get_current_time()
utilitaires.add_dataset_source(
    "rne",
    module,
    "Répertoire national des élus",
    "Ministère de l'Intérieur",
    datetime.strftime("%Y"),
    url_data=file["url"],
    url_information="https://www.data.gouv.fr/en/datasets/repertoire-national-des-elus-1/",
    id_reference="interieur-resultats-elections",
)

utilitaires.end(f"{module}.py")
