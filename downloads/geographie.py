from datacirco import utilitaires
from tqdm import tqdm
import glob
import dateparser
import re

module = "geographie"

utilitaires.start(f"{module}.py")
utilitaires.clear_dataset_source(module)

print("Découpage géographique des circonscriptions :")
# TODO: Ce site répond par une 404 depuis plusieurs mois. https://data.europa.eu/data/datasets/59131e7088ee3809491e322b?locale=fr
file = "circonscriptions-legislatives.zip"
url = "http://panneaux-election.fr/carte/circonscriptions-legislatives.json.zip"
# assert utilitaires.download_file(url, filename=file)
# datetime = utilitaires.get_last_modified(url)
datetime = dateparser.parse("2023-06-21").date()
utilitaires.add_dataset_source(
    file,
    module,
    "Contours détaillés des circonscriptions législatives",
    "Mapotempo",
    datetime.strftime("%Y"),
    url_data=url,
    url_information="https://panneaux-election.fr/data/",
    licence="Licence ODbl",
    id_reference=f"Mapotempo-{file}",
)

print("Codes postaux :")
file = "laposte_cp.csv"
# Le 04/09/2023 ce fichier ne contenait pas le code postal (!) donc on est passé sur la version small sans données géographiques.
# url = "https://datanova.laposte.fr/data-fair/api/v1/datasets/laposte-hexasmal/lines?size=10000&page=1&q_mode=simple&finalizedAt=2023-08-08T02:31:18.825Z&format=geojson"
url = "https://koumoul.com/data-fair/api/v1/datasets/laposte-hexasmal/data-files/019HexaSmal.csv"
assert utilitaires.download_file(url, filename=file)
url_information = "https://datanova.laposte.fr/datasets/laposte-hexasmal"
soup = utilitaires.get_soup(url_information)
soup = soup.find_all("div", class_="v-subheader pa-0 theme--light")[-1]
datetime = soup.text[re.search(r"\d", soup.text).start() :].strip()
datetime = dateparser.parse(datetime).date()

utilitaires.add_dataset_source(
    file,
    module,
    "La correspondance entre les codes postaux et les codes Insee des communes, de France (métropole et DOM), des TOM, ainsi que de MONACO",
    "La Poste - Service National de l'Adresse",
    datetime.strftime("%Y"),
    url_data=url,
    url_information=url_information,
    last_modified=datetime.strftime("%Y-%m-%d %H:%M:%S"),
    id_reference=f"LaPoste-{file}",
)

print("Données IRIS :")
iris_urls = [
    "https://data.cquest.org/ign/iris_ge/IRIS-GE_2-0__SHP_LAMB93_FXX_2021-01-01.7z",
    "https://data.cquest.org/ign/iris_ge/IRIS-GE_2-0__SHP_RGAF09UTM20_D971_2022-01-01.7z",
    "https://data.cquest.org/ign/iris_ge/IRIS-GE_2-0__SHP_RGAF09UTM20_D972_2022-01-01.7z",
    "https://data.cquest.org/ign/iris_ge/IRIS-GE_2-0__SHP_UTM22RGFG95_D973_2022-01-01.7z",
    "https://data.cquest.org/ign/iris_ge/IRIS-GE_2-0__SHP_RGR92UTM40S_D974_2022-01-01.7z",
    "https://data.cquest.org/ign/iris_ge/IRIS-GE_2-0__SHP_RGM04UTM38S_D976_2022-01-01.7z",
]

for url in iris_urls:
    assert utilitaires.download_file(url)

print("Extraction fichiers IRIS :")
files = glob.glob("IRIS-GE*.7z")
for file, url in zip(tqdm(files), iris_urls):
    utilitaires.extract_7z(file)
    datetime = utilitaires.get_last_modified(url)
    utilitaires.add_dataset_source(
        file,
        module,
        "Découpage géographique",
        "Insee",
        "2022",
        url_data=url,
        url_information="https://geoservices.ign.fr/irisge",
        id_reference=f"Insee-{file}-2022",
    )

utilitaires.end(f"{module}.py")
