from datacirco import utilitaires
from datacirco.connexion_db import db
from urllib.request import urlopen
from tqdm import tqdm
import dateutil.parser as dparser
import pandas as pd
import glob
import os
import shutil
import json


module = "sante"


def finess():
    print("Établissements FINESS :")
    finess = json.loads(
        urlopen(
            "https://www.data.gouv.fr/api/1/datasets/5dad52986f44413aca65df95/"
        ).read()
    )
    url_finess = finess["resources"][0]["url"]

    assert utilitaires.download_file(url_finess)
    with open(f"{url_finess.split('/')[-1]}", encoding="ISO-8859-1") as src, open(
        "finess.csv", "w", encoding="UTF-8"
    ) as target:
        target.write(src.read())
    # os.remove(f"{url_finess.split('/')[-1]}")

    utilitaires.add_dataset_source(
        "finess.csv",
        module,
        "Référentiel des structures sanitaires, sociales et médico-sociales",
        "Atlasanté",
        utilitaires.get_last_modified(url_finess).strftime("%Y"),
        url_data=url_finess,
        url_information="https://www.data.gouv.fr/fr/datasets/referentiel-finess-t-finess/",
        id_reference="Insee-finess",
    )


def equipement_materiel_lourd():
    print("Équipements matériels lourds :")
    emlhisto = json.loads(
        urlopen(
            "https://www.data.gouv.fr/api/1/datasets/5369956ba3a729239d2046ff/"
        ).read()
    )
    emlhisto["resources"][2]["url"]  # unused
    url_eml = emlhisto["resources"][0]["url"]
    assert utilitaires.download_file(url_eml)
    with open(f"{url_eml.split('/')[-1]}") as src, open(
        "finess_eml.csv", "w"
    ) as target:
        lines = src.readlines()
        for line in lines:
            if line.startswith("equipmateriellourd"):
                target.write(line)
    # os.remove(f"{url_eml.split('/')[-1]}")

    utilitaires.add_dataset_source(
        "finess_eml.csv",
        module,
        "Liste des équipements matériels lourds",
        "Ministère des Solidarités et de la Santé",
        utilitaires.get_last_modified(url_eml).strftime("%Y"),
        url_data=url_eml,
        url_information="https://www.data.gouv.fr/fr/datasets/finess-extraction-des-equipements-materiels-lourds/",
        id_reference="Insee-finess_eml",
    )


def annuaire_sante():
    print("Annuaire santé CNAMTS :")
    # On veut lire les fichier "Infos pratiques professionnels de sante" et "Professionnels exercant en etablissements de sante" de https://www.data.gouv.fr/fr/datasets/annuaire-sante-de-la-cnam/
    ps = json.loads(
        urlopen(
            "https://www.data.gouv.fr/api/1/datasets/5ac4f20fc751df5567500c0a/"
        ).read()
    )
    if ps["resources"][9]["title"] == "Infos pratiques professionnels de sante":
        url_ps = ps["resources"][9]["url"]
    else:
        raise ValueError(
            f'Le fichier attendu n\'est pas le bon : {ps["resources"][9]["title"]}'
        )
    if (
        ps["resources"][10]["title"]
        == "Professionnels exercant en etablissements de sante"
    ):
        url_psetab = ps["resources"][10]["url"]
    else:
        raise ValueError(
            f'Le fichier attendu n\'est pas le bon : {ps["resources"][10]["title"]}'
        )
    filename_ps = "ps-infospratiques-raw.csv"
    assert utilitaires.download_file(url_ps, filename=filename_ps)

    file_ps = "ps-infospratiques.csv"
    print(f"Lecture du fichier CSV {file_ps}...")
    df = pd.read_csv(
        filename_ps,
        encoding="unicode_escape",
        delimiter=";",  # Passé au ; en 2024
        quoting=3,
        usecols=[i for i in range(16)],
        header=None,
        dtype="str",
    )
    sorted_df = df.sort_values(
        [df.columns[i] for i in range(16)],
        ascending=[True for i in range(16)],
        inplace=False,
        na_position="first",
    )
    sorted_df.drop_duplicates(subset=None, keep="first", inplace=True)
    sorted_df.to_csv(file_ps, index=False, header=False)
    utilitaires.add_dataset_source(
        file_ps,
        module,
        "Annuaire des professionnels de santé",
        "CNAM",
        utilitaires.get_last_modified(url_ps).strftime("%Y"),
        url_data=url_ps,
        url_information="https://www.data.gouv.fr/fr/datasets/annuaire-sante-de-la-cnam/",
        id_reference=f"Insee-{file_ps}",
    )

    assert utilitaires.download_file(url_psetab)
    filename = url_psetab.split("/")[-1]
    utilitaires.add_dataset_source(
        filename,
        module,
        "Annuaire des professionnels de santé",
        "CNAM",
        utilitaires.get_last_modified(url_psetab).strftime("%Y"),
        url_data=url_psetab,
        url_information="https://www.data.gouv.fr/fr/datasets/annuaire-sante-de-la-cnam/",
        id_reference=f"Insee-{filename}",
    )


def atih():
    dirpath = "atih"
    if os.path.exists(dirpath):
        shutil.rmtree(dirpath)
    os.makedirs(dirpath)

    print("Données d'activité hospitalière (ATIH) :")
    # A noter que les sites https://www.scansante.fr et https://www.atih.sante.fr/ sont instables.
    db.execute(db.mogrify("select insee_reg from ign_region"))
    regions = db.fetchall()
    regions = [int(x[0]) for x in regions]

    for annee in tqdm(range(2017, 2019)):
        for region in tqdm(regions):
            url = f"https://www.scansante.fr/applications/analyse-activite-regionale/submit?snatnav=&mbout=dummy&annee={annee}&region={str(region).zfill(2)}"
            file = f"atih-{annee}-{str(region).zfill(2)}.xlsx"
            assert utilitaires.download_file(url, "atih/", file, overwrite=False)
            url_info = (
                f"https://www.atih.sante.fr/analyse-de-l-activite-hospitaliere-{annee}"
            )
            soup = utilitaires.get_soup(url_info)
            results = soup.find_all("div", class_="block block-atih-content last odd")[
                0
            ]
            results = results.find_all("li")[1]
            last_modified = dparser.parse(results.text, fuzzy=True)

        # extraction XLSX > CSV + fichiers globaux annuels
        for typ in tqdm(["MCO", "HAD", "SSR", "PSY"]):
            for excel in tqdm(glob.glob("atih/*.xlsx")):
                try:
                    df = pd.read_excel(excel, sheet_name=f"{typ}_finess", skiprows=2)
                    filename = f"{excel.split('.')[-2]}-{typ}.csv"
                    df.to_csv(filename, index=False, header=True)
                    with open(filename, "r") as src:
                        lines = src.read().replace(" ,", ",")
                    with open(filename, "w") as dst:
                        dst.write(lines)
                except ValueError:
                    print(f"Worksheet '{typ}_finess' not found in {excel}.")

            df_concat = pd.concat(
                [pd.read_csv(f) for f in glob.glob(f"atih/*{annee}*{typ}.csv")],
                ignore_index=True,
            )
            df_concat.to_csv(f"atih/{annee}-{typ}.csv", index=False, header=True)

    utilitaires.add_dataset_source(
        file,
        module,
        "Analyse nationale de l'activité hospitalière des établissements de santé",
        "ATIH",
        annee,
        url_data=url,
        url_information=url_info,
        last_modified=last_modified,
        id_reference=f"ATIH-{file}",
    )


def diagnostic_d_acces_aux_soins_urgents():
    dirpath = "drees"
    if os.path.exists(dirpath):
        shutil.rmtree(dirpath)
    os.makedirs(dirpath)

    print("Diagnostic d'accès aux urgences :")
    url_data = "https://data.drees.solidarites-sante.gouv.fr/api/datasets/1.0/2943_diagnostic-d-acces-aux-soins-urgents/attachments/diagnostic_d_acces_aux_soins_urgents_au_31_12_2015_xls"
    filename = "diagnostic_d_acces_aux_soins_urgents_au_31_12_2015"
    url_info = "https://data.drees.solidarites-sante.gouv.fr/explore/dataset/2943_diagnostic-d-acces-aux-soins-urgents/information/"

    assert utilitaires.download_file(url_data, "drees/", filename + ".xls")
    df = pd.read_excel(
        f"drees/{filename}.xls",
        sheet_name="BASECOM_URGENCES_2015",
        usecols=[1] + [i for i in range(6, 19)],
        skiprows=5,
    )
    df.to_csv(f"drees/{filename}.csv", index=False, header=True)
    os.remove(f"drees/{filename}.xls")
    millesime = dparser.parse(filename, fuzzy=True, dayfirst=True).strftime("%Y")
    utilitaires.add_dataset_source(
        filename + ".csv",
        module,
        "Diagnostic d'accès aux soins urgents",
        "DREES",
        millesime,
        url_data=url_data,
        url_information=url_info,
        last_modified="2015-12-31 00:00:00",
        id_reference=None,
    )

    filename = "diagnostic_d_acces_aux_soins_urgents_au_31_12_2019"
    url = "https://data.drees.solidarites-sante.gouv.fr/api/datasets/1.0/2943_diagnostic-d-acces-aux-soins-urgents/attachments/diagnostic_d_acces_aux_soins_urgents_au_31_12_2019_xlsx"
    assert utilitaires.download_file(url, "drees/", filename + ".xlsx")
    df = pd.read_excel(
        f"drees/{filename}.xlsx",
        sheet_name="BASECOM_URGENCES_2019",
        usecols=[1] + [i for i in range(5, 18)],
        skiprows=5,
    )
    df.to_csv(f"drees/{filename}.csv", index=False, header=True)
    os.remove(f"drees/{filename}.xlsx")
    millesime = dparser.parse(filename, fuzzy=True, dayfirst=True).strftime("%Y")
    utilitaires.add_dataset_source(
        filename + ".csv",
        module,
        "Diagnostic d'accès aux soins urgents",
        "DREES",
        millesime,
        url_data=url,
        url_information=url_data,
        last_modified="2019-12-31 00:00:00",
        id_reference=f"DREES-{filename}",
    )


def population_france():
    print("Population France :")
    url = "https://www.insee.fr/fr/statistiques/fichier/5225246/demo-pop-france.xlsx"
    url_data = "https://www.insee.fr/fr/statistiques/5225246"
    soup = utilitaires.get_soup(url_data)
    soup = soup.find(
        "div", class_="date-diffusion hidden-impression-information-rapide"
    )
    datetime_object = dparser.parse(soup.text, fuzzy=True)

    assert utilitaires.download_file(url)
    df = pd.read_excel("demo-pop-france.xlsx", skiprows=2, skipfooter=5)
    df.to_csv("population_france.csv", index=False, header=True)
    os.remove("demo-pop-france.xlsx")

    utilitaires.add_dataset_source(
        "population_france.csv",
        module,
        "Population au 1er janvier (données annuelles de 1990 à 2024)",
        "Insee",
        datetime_object.strftime("%Y"),
        url_data=url,
        url_information=url_data,
        last_modified=datetime_object.strftime("%Y-%m-%d %H:%M:%S"),
        id_reference="Insee-population_france",
    )


if __name__ == "__main__":
    utilitaires.start(f"{module}.py")
    utilitaires.clear_dataset_source(module)
    finess()
    equipement_materiel_lourd()
    annuaire_sante()
    atih()
    diagnostic_d_acces_aux_soins_urgents()
    population_france()
    utilitaires.end(f"{module}.py")
