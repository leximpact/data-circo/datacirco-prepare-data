from datacirco import utilitaires
import dateutil.parser as dparser
from tqdm import tqdm
import datetime
import os

module = "entreprises"
utilitaires.start(f"{module}.py")
utilitaires.clear_dataset_source(module)

INFOGREFFE_API_KEY = os.getenv("INFOGREFFE_API_KEY")

if INFOGREFFE_API_KEY is None or len(INFOGREFFE_API_KEY) < 13:
    print(f"API key invalid {INFOGREFFE_API_KEY=}")

print("Radiations / Immatriculations Infogreffe :")
# Le 15/03/2024 : plus de données avant 2018 !!!
# for annee in tqdm(range(2012, 2022)):
#     # print(f"Dowloading {annee} :")
#     name = "entreprises"
#     if annee > 2017:
#         name = "societes"
#     for status in ["radiees", "immatriculees"]:
#         url = f"https://opendata.datainfogreffe.fr/explore/dataset/{name}-{status}-{annee}/download/?format=csv&timezone=Europe/Berlin&lang=fr&use_labels_for_header=true&csv_separator=%3B"
#         file = f"societes-{status}-{annee}.csv"
#         if os.path.exists(file):
#             continue
#         url_information = f"https://opendata.datainfogreffe.fr/explore/dataset/{name}-{status}-{annee}"
#         # print(url, url_information)
#         soup = utilitaires.get_soup(url_information)
#         soup = str(soup).split("data_processed")
#         try:
#             datetime_object = dparser.parse(soup[1], fuzzy=True)
#         except IndexError:
#             datetime_object = datetime.datetime.now()
#         # Traceback (most recent call last):
#         #   File "/home/datacirco/datacirco/downloads/entreprises.py", line 60, in <module>
#         #     datetime_object = dparser.parse(soup[1], fuzzy=True)
#         #                                     ~~~~^^^
#         # IndexError: list index out of range
#         assert utilitaires.download_file(url, filename=file, overwrite=False)
#         utilitaires.add_dataset_source(
#             file,
#             module,
#             "Registre du Commerce et des Sociétés",
#             "INFOGREFFE",
#             annee,
#             url_data=url,
#             url_information=url_information,
#             last_modified=datetime_object.strftime("%Y-%m-%d %H:%M:%S"),
#             id_reference=f"INFOGREFFE-{file}",
#         )

for annee in tqdm(range(2022, datetime.datetime.now().year + 1)):
    # print(f"Dowloading {annee} :")
    for status in ["radiees", "immatriculees"]:
        url = f"https://opendata.datainfogreffe.fr/api/explore/v2.1/catalog/datasets/entreprises-{status}-en-{annee}/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&csv_separator=%3B"
        file = f"societes-{status}-{annee}.csv"
        url_info = f"https://opendata.datainfogreffe.fr/explore/dataset/entreprises-{status}-en-{annee}"
        try:
            soup = utilitaires.get_soup(url_info)
            soup = str(soup).split("data_processed")
            datetime_object = dparser.parse(soup[1], fuzzy=True)
            if annee == datetime.datetime.now().year:
                overwrite = True
            else:
                overwrite = False
            assert utilitaires.download_file(
                url, filename=file, overwrite=overwrite, api_key=INFOGREFFE_API_KEY
            )
            utilitaires.add_dataset_source(
                file,
                module,
                "Registre du Commerce et des Sociétés",
                "INFOGREFFE",
                annee,
                url_data=url,
                url_information=url_info,
                last_modified=datetime_object.strftime("%Y-%m-%d %H:%M:%S"),
                id_reference=f"INFOGREFFE-{file}",
            )
        except IndexError:
            # Cela arrive en début d'année quand les données ne sont pas encore disponibles
            print(f"Error {url_info=}")


print("Communes - Greffes :")
url = "https://opendata.datainfogreffe.fr/explore/dataset/referentiel-communes-greffes/download/?format=geojson"
file = "referentiel-communes-greffes.geojson"
url_info = (
    "https://opendata.datainfogreffe.fr/explore/dataset/referentiel-communes-greffes"
)

soup = utilitaires.get_soup(url_info)
soup = str(soup).split("data_processed")
datetime_object = dparser.parse(soup[1], fuzzy=True)

assert utilitaires.download_file(
    url, filename=file, overwrite=True, api_key=INFOGREFFE_API_KEY
)
utilitaires.add_dataset_source(
    file,
    module,
    "Référentiel de correspondance communes - greffes",
    "INFOGREFFE",
    datetime_object.strftime("%Y"),
    url_information=url_info,
    url_data=url,
    last_modified=datetime_object.strftime("%Y-%m-%d %H:%M:%S"),
    id_reference=f"INFOGREFFE-{file}",
)

print("Insee siren et siret :")
# On le prends chez Christian Quest car il fait un travail de géolocalisation
# Dans le fichier de l'Insee il manque 1 million de coordonnées.
# En plus cette version ne contient que les établissements encore actif, ce qui nous intéresse.
# TODO: Changer de source
stock_urls = [
    # "http://data.cquest.org/geo_sirene/v2019/last/StockEtablissementActif_utf8_geo.csv.gz",  # Le 21/10/2024 le lien last n'existe plus
    "http://data.cquest.org/geo_sirene/v2019/2024-03/StockEtablissementActif_utf8_geo.csv.gz",
    "https://files.data.gouv.fr/insee-sirene/StockUniteLegale_utf8.zip",
]

for url in stock_urls:
    # Filename do not change when updated so we have to overwrite older files
    assert utilitaires.download_file(url)
    file = url.split("/")[-1]
    utilitaires.add_dataset_source(
        file,
        module,
        "Base Sirene",
        "Insee",
        "mars 2024",
        url_data=url,
        url_information="https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret/",
        id_reference=f"Insee-{file}",
    )

"""
Données non utilisées dans le pdf
print("Dossier complet Insee :")
dossier_complet_url = "https://www.insee.fr/fr/statistiques/fichier/5359146/dossier_complet.zip"
assert utilitaires.download_file(dossier_complet_url)
utilitaires.unzip_file(dossier_complet_url.split('/')[-1])
"""

"""
# Données non utilisées dans le pdf
# Procédures collectives
print("Procédures collectives Infogreffe :")
for annee in tqdm(range(2017, 2023)): 
    assert utilitaires.download_file(f"https://opendata.datainfogreffe.fr/explore/dataset/statistiques-jugements-de-procedures-collectives-en-{annee}/download/?format=csv&timezone=Europe/Berlin&lang=fr&use_labels_for_header=true&csv_separator=%3B", filename=f"entreprises_procedures_collectives_{annee}.csv")
for annee in tqdm(range(2015, 2017)):
    assert utilitaires.download_file(f"https://opendata.datainfogreffe.fr/explore/dataset/statistiques-jugements-de-procedures-collectives-{annee}/download/?format=csv&timezone=Europe/Berlin&lang=fr&use_labels_for_header=true&csv_separator=%3B", filename=f"entreprises_procedures_collectives_{annee}.csv")
"""

utilitaires.end(f"{module}.py")
