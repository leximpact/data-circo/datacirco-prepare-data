from datacirco import utilitaires

module = "emplois"

utilitaires.start(f"{module}.py")
utilitaires.clear_dataset_source(module)

file = "etablissements-et-effectifs-salaries-au-niveau-commune-x-ape.csv"
url_data = "https://open.urssaf.fr/explore/dataset/etablissements-et-effectifs-salaries-au-niveau-commune-x-ape-last/download/?format=csv&timezone=Europe/Berlin&lang=fr&use_labels_for_header=true&csv_separator=%3B"
url = "https://open.urssaf.fr/explore/dataset/etablissements-et-effectifs-salaries-au-niveau-commune-x-ape-last/information/"
print(f"Dowloading {file}...")
assert utilitaires.download_file(url_data, filename=file)
utilitaires.add_dataset_source(
    file,
    module,
    "Nombre d'établissements employeurs et effectifs salariés",
    "URSSAF",
    "2023",
    url_information=url,
    url_data=url_data,
    id_reference="urssaf_etablissements_et_effectifs_salaries_au_niveau_commune_x_ape",
)

utilitaires.end(f"{module}.py")
