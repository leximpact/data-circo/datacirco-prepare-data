from datacirco import utilitaires
from datetime import date
from tqdm import tqdm

module = "education"

datasets_education = [
    {
        "url": "https://data.education.gouv.fr/api/explore/v2.1/catalog/datasets/fr-en-adresse-et-geolocalisation-etablissements-premier-et-second-degre/exports/geojson?lang=fr&timezone=Europe%2FBerlin",
        "filename": "adresse-et-geolocalisation-etablissements-premier-et-second-degre.geojson",
        "table_name": "educ_etablissements_premier_et_second_degre",
        "description": "Adresse et géolocalisation des établissements d'enseignement du premier et second degrés.",
        "source": "https://data.education.gouv.fr/explore/dataset/fr-en-adresse-et-geolocalisation-etablissements-premier-et-second-degre/information/",
        "producteur": "DEPP – Ministère de l’Éducation nationale",
        "id_reference": None,
    },
    {
        "url": "https://data.education.gouv.fr/api/explore/v2.1/catalog/datasets/fr-en-etablissements-ep/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B",
        "filename": "etab_prioritaires.csv",
        "table_name": "educ_etab_prioritaires",
        "description": "Etablissements de l'éducation prioritaire",
        "source": "https://data.education.gouv.fr/explore/dataset/fr-en-etablissements-ep/information/",
        "producteur": "DEPP – Ministère de l’Éducation nationale",
        "id_reference": None,
    },
    {
        "url": "https://data.education.gouv.fr/api/explore/v2.1/catalog/datasets/fr-en-ips-ecoles-ap2022/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B",
        "filename": "ips-ecoles.csv",
        "table_name": "educ_ips_ecoles",
        "description": "Indices de position sociale dans les écoles (à partir de 2022)",
        "source": "https://data.education.gouv.fr/explore/dataset/fr-en-ips-ecoles-ap2022/information/",
        "producteur": "DEPP – Ministère de l’Éducation nationale",
        "id_reference": None,
    },
    {
        "url": "https://data.education.gouv.fr/explore/dataset/dataviz-ips-colleges/download/?format=csv&",
        "filename": "ips-colleges.csv",
        "table_name": "educ_ips_colleges",
        "description": "Indices de position sociale dans les collèges de France métropolitaine et DROM.",
        "source": "https://data.education.gouv.fr/explore/dataset/fr-en-ips-colleges-ap2022/information/",
        "producteur": "DEPP – Ministère de l’Éducation nationale",
        "id_reference": None,
    },
    {
        "url": "https://data.education.gouv.fr/api/explore/v2.1/catalog/datasets/fr-en-ips-lycees-ap2022/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B",
        "filename": "ips-lycees.csv",
        "table_name": "educ_ips_lycees",
        "description": "Indices de position sociale dans les lycées (à partir de 2022)",
        "source": "https://data.education.gouv.fr/explore/dataset/fr-en-ips-lycees-ap2022/information/",
        "producteur": "DEPP – Ministère de l’Éducation nationale",
        "id_reference": None,
    },
    {
        "url": "https://data.education.gouv.fr/api/explore/v2.1/catalog/datasets/fr-en-ips-erea-ap2022/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B",
        "filename": "ips-erea.csv",
        "table_name": "educ_ips_erea",
        "description": "Indices de position sociale dans les EREA (à partir de 2022)",
        "source": "https://data.education.gouv.fr/explore/dataset/fr-en-ips-erea-ap2022/information/",
        "producteur": "DEPP – Ministère de l’Éducation nationale",
        "id_reference": None,
    },
    {
        "url": "https://data.education.gouv.fr/api/explore/v2.1/catalog/datasets/fr-en-ecoles-effectifs-nb_classes/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B",
        "filename": "ecoles_effectifs_nb_classes.csv",
        "table_name": "educ_ecoles_effectifs_nb_classes",
        "description": "Effectifs d’élèves par niveau et nombre de classes par école",
        "source": "https://data.education.gouv.fr/explore/dataset/fr-en-ecoles-effectifs-nb_classes/information/",
        "producteur": "DEPP – Ministère de l’Éducation nationale",
        "id_reference": None,
    },
    {
        "url": "https://data.education.gouv.fr/api/explore/v2.1/catalog/datasets/fr-en-college-effectifs-niveau-sexe-lv/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B",
        "filename": "college_effectifs.csv",
        "table_name": "educ_college_effectifs",
        "description": "Effectifs d’élèves par niveau, sexe, langues vivantes 1 et 2 les plus fréquentes, par collège",
        "source": "https://data.education.gouv.fr/explore/dataset/fr-en-college-effectifs-niveau-sexe-lv/information/",
        "producteur": "DEPP – Ministère de l’Éducation nationale",
        "id_reference": None,
    },
    {
        "url": "https://data.education.gouv.fr/api/explore/v2.1/catalog/datasets/fr-en-lycee_gt-effectifs-niveau-sexe-lv/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B",
        "filename": "lycee_gt_effectifs.csv",
        "table_name": "educ_lycee_gt_effectifs",
        "description": "Effectifs d’élèves par niveau, sexe, langues vivantes 1 et 2 les plus fréquentes, par lycée d’enseignement général et technologique",
        "source": "https://data.education.gouv.fr/explore/dataset/fr-en-lycee_gt-effectifs-niveau-sexe-lv/information/",
        "producteur": "DEPP – Ministère de l’Éducation nationale",
        "id_reference": None,
    },
    {
        "url": "https://data.education.gouv.fr/api/explore/v2.1/catalog/datasets/fr-en-lycee_pro-effectifs-niveau-sexe-lv/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B",
        "filename": "lycee_pro-effectifs.csv",
        "table_name": "educ_lycee_pro_effectifs",
        "description": "Effectifs d’élèves par niveau, sexe, langues vivantes 1 et 2 les plus fréquentes, par lycée professionnel",
        "source": "https://data.education.gouv.fr/explore/dataset/fr-en-lycee_pro-effectifs-niveau-sexe-lv/information/",
        "producteur": "DEPP – Ministère de l’Éducation nationale",
        "id_reference": None,
    },
    {
        "url": "https://data.education.gouv.fr/api/explore/v2.1/catalog/datasets/fr-en-dnb-par-etablissement/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B",
        "filename": "dnb_par_etablissement.csv",
        "table_name": "educ_dnb_par_etablissement",
        "description": "Diplôme national du brevet par établissement",
        "source": "https://data.education.gouv.fr/explore/dataset/fr-en-dnb-par-etablissement/information/",
        "producteur": "DEPP – Ministère de l’Éducation nationale",
        "id_reference": None,
    },
    {
        "url": "https://data.education.gouv.fr/api/explore/v2.1/catalog/datasets/fr-en-indicateurs-de-resultat-des-lycees-denseignement-general-et-technologique/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B",
        "filename": "indicateurs_resultat_lycees.csv",
        "table_name": "educ_indicateurs_resultat_lycees",
        "description": "Indicateurs de valeur ajoutée des lycées d'enseignement général et technologique",
        "source": "https://data.education.gouv.fr/explore/dataset/fr-en-indicateurs-de-resultat-des-lycees-denseignement-general-et-technologique/information/",
        "producteur": "DEPP – Ministère de l’Éducation nationale",
        "id_reference": None,
    },
    {
        "url": "https://data.education.gouv.fr/api/explore/v2.1/catalog/datasets/fr-en-indicateurs-de-resultat-des-lycees-denseignement-professionnels/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B",
        "filename": "indicateurs_resultat_lycees_pro.csv",
        "table_name": "educ_indicateurs_resultat_lycees_pro",
        "description": "Indicateurs de valeur ajoutée des lycées d'enseignement professionnel",
        "source": "https://data.education.gouv.fr/explore/dataset/fr-en-indicateurs-de-resultat-des-lycees-denseignement-professionnels/information/",
        "producteur": "DEPP – Ministère de l’Éducation nationale",
        "id_reference": None,
    },
    {
        "url": "https://data.education.gouv.fr/api/explore/v2.1/catalog/datasets/fr-en-indicateurs-valeur-ajoutee-colleges/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B",
        "filename": "indicateurs_valeur_ajoutee_colleges.csv",
        "table_name": "educ_indicateurs_valeur_ajoutee_colleges",
        "description": "Indicateurs de valeur ajoutée des collèges",
        "source": "https://data.education.gouv.fr/explore/dataset/fr-en-indicateurs-valeur-ajoutee-colleges/information/",
        "producteur": "DEPP – Ministère de l’Éducation nationale",
        "id_reference": None,
    },
    {
        "url": "https://data.education.gouv.fr/explore/?sort=modified&exclude.keyword=hors%20catalogue&exclude.publisher=Minist%C3%A8re%20des%20Sports",
        "filename": "depp_donnees_multiples",
        "table_name": "",
        "description": "Données multiples",
        "source": "https://data.education.gouv.fr/explore/?sort=modified&exclude.keyword=hors%20catalogue&exclude.publisher=Minist%C3%A8re%20des%20Sports",
        "producteur": "DEPP – Ministère de l’Éducation nationale",
        "id_reference": "depp-donnees-multiples",
    },
    # Enseignement supérieur
    {
        "url": "https://data.enseignementsup-recherche.gouv.fr/api/explore/v2.1/catalog/datasets/fr-esr-principaux-etablissements-enseignement-superieur/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B",
        "filename": "principaux-etablissements-enseignement-superieur.csv",
        "table_name": "educ_principaux_etablissements_enseignement_superieur",
        "description": "Principaux établissements d'enseignement supérieur ",
        "source": "https://data.enseignementsup-recherche.gouv.fr/explore/dataset/fr-esr-principaux-etablissements-enseignement-superieur/information/",
        "producteur": "Ministère de l'Enseignement supérieur, de la Recherche et de l'Innovation",
        "id_reference": "mesr-principaux-etablissements-enseignement-superieur",
    },
    {
        "url": "https://api.opendata.onisep.fr/downloads/5fa586da5c4b6/5fa586da5c4b6.csv",
        "filename": "onisep-structures_denseignement_superieur.csv",
        "table_name": "educ_onisep_structures_denseignement_superieur",
        "description": "Structures d'enseignement supérieur",
        "source": "https://opendata.onisep.fr/data/5fa586da5c4b6/2-ideo-structures-d-enseignement-superieur.htm",
        "producteur": "Onisep",
        "id_reference": "onisep-structures_denseignement_superieur",
    },
]

if __name__ == "__main__":
    utilitaires.start(f"{module}.py")
    utilitaires.clear_dataset_source(module)
    for data in tqdm(datasets_education):
        url = data["url"]
        local_filename = data["filename"]
        assert utilitaires.download_file(url=url, filename=local_filename)

        datetime = utilitaires.get_last_modified(url)
        if not datetime:
            datetime = date.today()
        utilitaires.add_dataset_source(
            local_filename,
            module,
            data["description"],
            data["producteur"],
            datetime.strftime("%Y"),
            url_data=url,
            url_information=data["source"],
            id_reference=data["id_reference"],
        )
    utilitaires.end(f"{module}.py")
