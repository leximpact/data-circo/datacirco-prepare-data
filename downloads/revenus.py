from datacirco import utilitaires
import pandas as pd

module = "revenus"
utilitaires.start(f"{module}.py")
utilitaires.clear_dataset_source(module)

print("Données de l'INSEE sur les circonscriptions :")

path_insee = "https://www.insee.fr/fr/statistiques/fichier/6436476/indic-stat-circonscriptions-legislatives-2022.xlsx"
fichier = pd.read_excel(path_insee, header=7, sheet_name="indicateurs_circonscriptions")
assert utilitaires.download_file(path_insee)
filename = "insee_indic_stat_circo_2022"
utilitaires.add_dataset_source(
    filename=filename,
    module=module,
    description="Circonscriptions législatives, Indicateurs socio-économiques",
    producteur="Insee",
    version="2022",
    url_data=path_insee,
    url_information="https://www.insee.fr/fr/statistiques/6436315?sommaire=6436478",
    last_modified="2023-01-01 00:00:00",
    id_reference="insee_indic_stat_circonscriptions_legislatives_2022",
)

# On utilise les données de revenus à l'échelle nationale sur les déciles :
print("Données sur les déciles à l'échelle nationale :")
path_insee_revenu_nat = (
    "https://www.insee.fr/fr/statistiques/fichier/2417897/reve-niv-vie-decile.xlsx"
)
fichier = pd.read_excel(path_insee, header=3)
assert utilitaires.download_file(path_insee_revenu_nat)
utilitaires.add_dataset_source(
    filename="insee_decile_natio",
    module=module,
    description="Niveau de vie moyen par décile - Données annuelles de 1996 à 2022",
    producteur="Insee",
    version="2022",
    url_data=path_insee_revenu_nat,
    url_information="https://www.insee.fr/fr/statistiques/6436315?sommaire=6436478",
    last_modified="2024-07-11 00:00:00",
    licence="Licence ouverte",
    id_reference="insee_decile_natio_2022",
)

print("Données sur les revenus à l'échelle départementale :")
path_insee_revenu_dep = "https://www.insee.fr/fr/statistiques/fichier/6036902/base-cc-filosofi-2019_XLSX_histo.zip"
assert utilitaires.download_file(path_insee_revenu_dep)
utilitaires.add_dataset_source(
    filename="insee_revenu_dep_2019",
    module=module,
    description="Principaux résultats sur les revenus et la pauvreté des ménages",
    producteur="Insee",
    version="2020",
    url_data=path_insee_revenu_dep,
    url_information="https://www.insee.fr/fr/statistiques/6692414?sommaire=6692394",
    last_modified="2023-01-23 00:00:00",
    id_reference="base-cc-filosofi-2019",
)

print("Données sur les revenus à l'échelle IRIS :")
# Pour insee_revenu_pauvrete_2020
insee_revenu_pauvrete_2020 = "https://www.insee.fr/fr/statistiques/fichier/7233950/BASE_TD_FILO_DISP_IRIS_2020_CSV.zip"
assert utilitaires.download_file(insee_revenu_pauvrete_2020)
utilitaires.add_dataset_source(
    filename="insee_revenu_pauvrete_2020",
    module=module,
    description="Revenus, pauvreté et niveau de vie",
    producteur="Insee",
    version="2020",
    url_data=insee_revenu_pauvrete_2020,
    url_information="https://www.insee.fr/fr/statistiques/7233950",
    last_modified="2023-03-31 00:00:00",
    id_reference="insee_revenu_pauvrete_2020",
)

utilitaires.end(f"{module}.py")
