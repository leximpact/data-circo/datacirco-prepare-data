from datacirco import utilitaires
import pandas as pd
import dateparser
import re

module = "population"
utilitaires.start(f"{module}.py")
utilitaires.clear_dataset_source(module)

print("Carroyage Insee 2010 :")
carroyage_200m = [
    "http://data.cquest.org/insee_carroyage/200m_carreaux_metropole_shapefile_wgs84.zip",
    "http://data.cquest.org/insee_carroyage/200m_carreaux_martinique_shapefile_wgs84.zip",
    "http://data.cquest.org/insee_carroyage/200m_carreaux_reunion_shapefile_wgs84.zip",
    "https://www.insee.fr/fr/statistiques/fichier/2520034/200m-rectangles-metropole.zip",
    "https://www.insee.fr/fr/statistiques/fichier/2520034/200m-rectangles-martinique.zip",
    "https://www.insee.fr/fr/statistiques/fichier/2520034/200m-rectangles-reunion.zip",
]
url_data = "https://www.insee.fr/fr/statistiques/2520034"
soup = utilitaires.get_soup(url_data)
result = soup.find("p", class_="paragraphe").text[-20:]
datetime = result[re.search(r"\d", result).start() :].strip()
datetime = dateparser.parse(datetime).date()

for file in carroyage_200m:
    assert utilitaires.download_file(file)
    filename = file.split("/")[-1]
    utilitaires.add_dataset_source(
        filename,
        module,
        "Données carroyées à 200m",
        "Insee",
        datetime.strftime("%Y"),
        url_data=file,
        url_information=url_data,
        last_modified=datetime.strftime("%Y-%m-%d %H:%M:%S"),
        id_reference=f"Insee-{filename}",
    )

carroyage_1km = [
    "https://www.insee.fr/fr/statistiques/fichier/1405815/ECP1KM_09_MET.zip",
    "https://www.insee.fr/fr/statistiques/fichier/1405815/ECP1KM_09_R02.zip",
    "https://www.insee.fr/fr/statistiques/fichier/1405815/ECP1KM_09_R04.zip",
]
for file in carroyage_1km:
    assert utilitaires.download_file(file)
    filename = file.split("/")[-1]
    utilitaires.add_dataset_source(
        filename,
        module,
        "Données carroyées à 1km",
        "Insee",
        datetime.strftime("%Y"),
        url_data=file,
        url_information="https://www.insee.fr/fr/statistiques/1405815",
        last_modified=datetime.strftime("%Y-%m-%d %H:%M:%S"),
        id_reference=f"Insee-{filename}",
    )

print("Carroyage Insee 2015 :")
carroyage_2015 = [
    "http://data.cquest.org/insee_carroyage/Filosofi2015_carreaux_200m_shp.zip",
    "https://www.insee.fr/fr/statistiques/fichier/4176293/Filosofi2015_carreaux_1000m_shp.zip",
]
assert utilitaires.download_file(carroyage_2015[0])
filename = carroyage_2015[0].split("/")[-1]
utilitaires.add_dataset_source(
    filename,
    module,
    "Données carroyées à 200m",
    "Insee",
    "2015",
    url_data=carroyage_2015[0],
    url_information="https://www.insee.fr/fr/statistiques/4176290",
    last_modified="2015-01-01 00:00:00",
    id_reference=None,
)
assert utilitaires.download_file(carroyage_2015[1])
filename = carroyage_2015[1].split("/")[-1]
utilitaires.add_dataset_source(
    filename,
    module,
    "Données carroyées à 1km",
    "Insee",
    "2015",
    url_data=carroyage_2015[1],
    url_information="https://www.insee.fr/fr/statistiques/4176293",
    last_modified="2015-01-01 00:00:00",
    id_reference=None,
)

print("Carroyage Insee 2019 :")
carroyage_2019 = [
    "https://www.insee.fr/fr/statistiques/fichier/7655475/Filosofi2019_carreaux_200m_shp.zip",
    "https://www.insee.fr/fr/statistiques/fichier/7655464/Filosofi2019_carreaux_1km_shp.zip",
]
assert utilitaires.download_file(carroyage_2019[0])
filename = carroyage_2019[0].split("/")[-1]
utilitaires.add_dataset_source(
    filename,
    module,
    "Données carroyées à 200m",
    "Insee",
    "2019",
    url_data=carroyage_2019[0],
    url_information="https://www.insee.fr/fr/statistiques/4176290",
    last_modified="2019-01-01 00:00:00",
    id_reference=f"Insee-{filename}",
)
assert utilitaires.download_file(carroyage_2019[1])
filename = carroyage_2019[1].split("/")[-1]
utilitaires.add_dataset_source(
    filename,
    module,
    "Données carroyées à 1km",
    "Insee",
    "2019",
    url_data=carroyage_2019[1],
    url_information="https://www.insee.fr/fr/statistiques/4176293",
    last_modified="2019-01-01 00:00:00",
    id_reference=f"Insee-{filename}",
)

print("Indicateurs stats 2013 par circo :")
file = "https://www.insee.fr/fr/statistiques/fichier/2542357/indic-stat-2013-circonscriptions-legislatives.xls"
filename = file.split("/")[-1].split(".")[0]
assert utilitaires.download_file(file)
df = pd.read_excel(filename + ".xls", usecols=[0, 2], skiprows=6)
df.to_csv(filename + ".csv", index=None, header=True)
utilitaires.add_dataset_source(
    filename + ".csv",
    module,
    "Indicateurs statistiques 2013 sur les circonscriptions législatives",
    "Insee",
    "2013",
    url_data=file,
    url_information="https://www.insee.fr/fr/statistiques/2542357",
    last_modified="2013-01-01 00:00:00",
    id_reference=f"Insee-{filename}",
)

print("Population par circo 2021 :")
file = "https://www.insee.fr/fr/statistiques/fichier/2508230/population-circonscriptions-legislatives-2021.xlsx"
filename = file.split("/")[-1].split(".")[0]
assert utilitaires.download_file(file)
df = pd.read_excel(
    filename + ".xlsx",
    sheet_name="Population par circonscription",
    usecols=[0, 1, 2, 3],
    skiprows=7,
)
df.to_csv(filename + ".csv", index=None, header=True)

url_data = "https://www.insee.fr/fr/statistiques/2508230"

utilitaires.add_dataset_source(
    filename + ".csv",
    module,
    "Populations légales des circonscriptions législatives pour les élections de 2024",
    "Insee",
    "2021",
    url_data=file,
    url_information=url_data,
    last_modified="2024-06-26 00:00:00",
    id_reference=f"Insee-{filename}",
)

print("Recensement 2018 à l'IRIS :")
recensement = [
    "https://www.insee.fr/fr/statistiques/fichier/5650720/base-ic-evol-struct-pop-2018_csv.zip",
    "https://www.insee.fr/fr/statistiques/fichier/5650720/base-ic-evol-struct-pop-2018-com_csv.zip",
]
for file in recensement:
    assert utilitaires.download_file(file)
    filename = file.split("/")[-1]
    utilitaires.add_dataset_source(
        filename,
        module,
        "Recensement de la population - Base infracommunale (IRIS)",
        "Insee",
        "2018",
        url_data=file,
        url_information="https://www.insee.fr/fr/statistiques/5650720",
        last_modified="2018-01-01 00:00:00",
        id_reference=f"Insee-{filename}",
    )

print("Recensement 2020 à l'IRIS :")
recensement = [
    "https://www.insee.fr/fr/statistiques/fichier/7704076/base-ic-evol-struct-pop-2020_csv.zip",
    "https://www.insee.fr/fr/statistiques/fichier/7704076/base-ic-evol-struct-pop-2020-com_csv.zip",
]
for file in recensement:
    assert utilitaires.download_file(file)
    filename = file.split("/")[-1]
    utilitaires.add_dataset_source(
        filename,
        module,
        "Recensement de la population - Base infracommunale (IRIS)",
        "Insee",
        "2020",
        url_data=file,
        url_information="https://www.insee.fr/fr/statistiques/7704076",
        last_modified="2023-01-01 00:00:00",
        id_reference=f"Insee-{filename}",
    )

utilitaires.end(f"{module}.py")
