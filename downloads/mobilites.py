from datacirco import utilitaires

module = "mobilites"
utilitaires.start(f"{module}.py")
utilitaires.clear_dataset_source(module)

base_mobilites = [
    # Base des Bornes de Recharge pour Véhicules Électriques
    {
        "annee": "2024",
        "filename": "irve_2024",
        "url": "https://www.data.gouv.fr/fr/datasets/r/eb76d20a-8501-400e-b336-d85724de5435",
        "url_data": "https://www.data.gouv.fr/fr/datasets/fichier-consolide-des-bornes-de-recharge-pour-vehicules-electriques/",
        "id_reference": "bornes_elec_2024",
        "producteur": "data.gouv",
        "description": "Répertoire national des Infrastructures de recharge pour véhicules électriques (IRVE),",
    },
    # Base des stations de carburants
    {
        "annee": "2024",
        "filename": "carburants",
        "url": "http://donnees.roulez-eco.fr/opendata/annee",
        "url_data": "http://donnees.roulez-eco.fr/opendata",
        "id_reference": "stations_carburant",
        "producteur": "Ministère de l'Economie",
        "description": "Répertoire des stations services dont le volume de vente à l'année est supérieur à cinq cents mètres cubes (tous produits confondus).",
    },
    # Base des structures de transport en communs
    {
        "annee": "2024",
        "filename": "transports_commun_2024",
        "url": "https://www.data.gouv.fr/fr/datasets/r/eb76d20a-8501-400e-b336-d85724de5435",
        "url_data": "https://www.data.gouv.fr/fr/datasets/r/ba635ef6-b506-4381-9dc9-b51ad3c482ab",
        "id_reference": "transport_commun",
        "producteur": "Point d'Accès National transport.data.gouv.fr ",
        "description": "Position des arrêts et tracés de lignes de tous les jeux de données de transport en commun",
    },
    # Base des véhicules et leur type :
    {
        "annee": "2024",
        "filename": "sdes_vehicules_2024",
        "url": "https://www.statistiques.developpement-durable.gouv.fr/media/7228/download?inline",
        "url_data": "https://www.statistiques.developpement-durable.gouv.fr/donnees-2023-sur-les-immatriculations-des-vehicules",
        "id_reference": "vehicules",
        "producteur": " service des données et études statistiques (SDES)",
        "description": "Données sur les immatriculations des véhicules neufs et d'occasion à l'échelon communal.",
    },
]

for base_mobilite in base_mobilites:
    # pour chaque référence dans la base
    print(f"Base mobilités {base_mobilite['id_reference']} :")
    url = base_mobilite["url"]
    millesime = base_mobilite["annee"]
    last_modified = None
    file = base_mobilite["filename"]
    print(file)
    utilitaires.add_dataset_source(
        file,
        module,
        base_mobilite["description"],
        producteur=base_mobilite["producteur"],
        version=millesime,
        url_data=base_mobilite["url"],
        url_information=base_mobilite["url_data"],
        last_modified=last_modified,
        id_reference=base_mobilite["id_reference"],
    )


# print('Téléchargement des données de véhicules')
# file = requests.get("https://www.statistiques.developpement-durable.gouv.fr/media/7228/download?inline")
# if file.status_code == 200:
#     # Ouvrir le contenu du fichier ZIP depuis la mémoire
#     with ZipFile(io.BytesIO(file.content)) as thezip:
#         for dezip_file in thezip.namelist()[1:]:
#             name = dezip_file[:-5]
#             print(name)
#             with thezip.open(dezip_file) as excel_file:
#                 df_neuf = pd.read_excel(excel_file, sheet_name= 'neuf_annuel', header = 3, dtype = {'code commune' : str})
#                 df_neuf['type_achat'] = 'neuf'
#                 df_occasion = pd.read_excel(excel_file, sheet_name= 'occasion_annuel', header = 3, dtype = {'code commune' : str})
#                 df_occasion['type_achat'] = 'occasion'
#                 df = pd.concat([df_neuf, df_occasion], axis = 0, ignore_index = True)
#                 df['code commune'] = df['code commune'].apply(lambda x : x.zfill(5))
#                 df.to_csv('notebooks/'+name + '.csv', index=False)
# else:
#     print(f"Failed to download file: {response.status_code}")


utilitaires.end(f"{module}.py")
