from datacirco import utilitaires
import dateutil.parser as dparser

module = "equipements"
utilitaires.start("equipements.py")
utilitaires.clear_dataset_source(module)

# url = "https://www.insee.fr/fr/statistiques/fichier/3568638/bpe21_ensemble_xy_csv.zip"
url = "https://www.insee.fr/fr/statistiques/fichier/8217525/BPE23.zip"
file = url.split("/")[-1]
assert utilitaires.download_file(url, filename=file)

# url_info = "https://www.insee.fr/fr/statistiques/3568638?sommaire=3568656"
url_info = "https://www.insee.fr/fr/statistiques/8217525?sommaire=8217537"
soup = utilitaires.get_soup(url_info)
soup = soup.find(name="div", class_="date-diffusion")
results = soup.text
datetime_object = dparser.parse(results, fuzzy=True)
utilitaires.add_dataset_source(
    file,
    module,
    "Équipements géolocalisés (Commerce, service, santé, action sociale, enseignement, sport-loisir, transports et tourisme)",
    "Insee",
    "2023",
    url_data=url,
    url_information=url_info,
    last_modified=datetime_object,
    id_reference=f"Insee-{file}",
)

utilitaires.end("equipements.py")
