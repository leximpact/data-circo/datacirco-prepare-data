from datacirco import utilitaires
from datetime import datetime

module = "environnement"
utilitaires.start(f"{module}.py")
utilitaires.clear_dataset_source(module)

print("=== Import pour la section Occupation des sols et paysages ===")
print("Import des données d'occupation des sols")
path_occup_sols_fr = "https://www.donnees.statistiques.developpement-durable.gouv.fr/donneesCLC/CLC/millesime/CLC12_FR_RGF_SHP.zip"
path_occup_sols_drom = "https://www.donnees.statistiques.developpement-durable.gouv.fr/donneesCLC/CLC/millesime/CLC12_DOM_UTM_SHP.zip"
assert utilitaires.download_file(path_occup_sols_fr, overwrite=False)
assert utilitaires.download_file(path_occup_sols_drom, overwrite=False)
utilitaires.add_dataset_source(
    filename="sdes_occupation_sols_2012",
    module=module,
    description="Inventaire biophysique de l'occupation des sols",
    producteur="Ministère chargé de l'écologie",
    version="2012",
    url_data=path_occup_sols_fr,
    url_information="https://www.statistiques.developpement-durable.gouv.fr/corine-land-cover-0?rubrique=348&dossier=1759",
    last_modified=datetime.now(),
    id_reference="geoloc_occupation_sols",
)
print("Import des données d'artificialisation des sols du CEREMA")

# Pour avoir les données zippées et géographiques (géographie des communes)
# TODO: Vérifier si nous avons les DROM/COM dans le fichier unique
# path_artif = {
#     'metro' : "",
#     '974' : "",
#     '973' : "",
#     '972' : "",
#     '971' : ""
#     }
# for path in path_artif.values :
#     assert utilitaires.download_file(path)

# Préférer la source Data.gouv.fr que celle de Box qui est plus compliquée à récupérer
path_artif = (
    "https://www.data.gouv.fr/fr/datasets/r/f87abde2-f0ea-4d1d-86bd-eb2988d8a415"
)
assert utilitaires.download_file(
    path_artif, filename="conso_sols_2009_2023_resultats_com.csv"
)
utilitaires.add_dataset_source(
    filename="cerema_artificialisation_2023",
    module=module,
    description="Indicateurs communaux des flux de consommation des sols de 2009 à 2023",
    producteur="CEREMA",
    version="2023",
    url_data="https://artificialisation.developpement-durable.gouv.fr/mesurer-la-consommation-despaces/telecharger-les-donnees",
    url_information="https://outil2amenagement.cerema.fr/outils/lobservatoire-lartificialisation-des-sols",
    last_modified="2024-04-25 10:23",
    id_reference="cerema_artificialisation_2023",
)

print("=== Import pour la section Risques ===")
print("Import des données des sites pollués")
path_sitespollues = (
    "https://www.georisques.gouv.fr/webappReport/ws/infosols/export/excel?national=true"
)
# 'https://www.georisques.gouv.fr/webappReport/ws/infosols/export/shapefile?national=true'
assert utilitaires.download_file(path_sitespollues, filename="site_pollues.xlsx")
# 'site_pollues.zip'
utilitaires.add_dataset_source(
    filename="sites_pollues",
    module=module,
    description="Sites et sols pollués",
    producteur="Ministère de la Transition écologique et de la Cohésion des territoires",
    version=datetime.now().year,  # La fréquence de mise à jour de ce jeu de données est quotidienne.
    url_data=path_sitespollues,
    url_information="https://www.georisques.gouv.fr/articles-risques/pollutions-sols-sis-anciens-sites-industriels/basol",
    last_modified=datetime.now(),
    id_reference="basol_sites_pollues",
)


print("Import de la localisation retrait-gonflement des argiles")
path_argiles = (
    "https://www.statistiques.developpement-durable.gouv.fr/media/4553/download?inline"
)
assert utilitaires.download_file(
    path_argiles, filename="indicateur_retrait_gonflement_argiles.xlsx"
)
utilitaires.add_dataset_source(
    filename="sdes_argiles_2021",
    module=module,
    description="Nouveau zonage d'exposition au retrait-gonflement des argiles",
    producteur="Ministère de la Transition écologique et de la Cohésion des territoires",
    version="2021",
    url_data=path_argiles,
    url_information="https://www.statistiques.developpement-durable.gouv.fr/nouveau-zonage-dexposition-au-retrait-gonflement-des-argiles-plus-de-104-millions-de-maisons",
    last_modified="2021-06-23 00:00:00",
    id_reference="indicateur_retrait_gonflement_argiles",
)

print("Import des données liées au risque radon")
path_radon = (
    "https://www.data.gouv.fr/fr/datasets/r/817114f8-9b61-48fa-b7a4-0e3c1331a44c"
)
assert utilitaires.download_file(path_radon, filename="irsn_radon_2019.csv")
utilitaires.add_dataset_source(
    filename="irsn_radon_2019",
    module=module,
    description="Potentiel radon de sa commune",
    producteur="Institut de radioprotection et de sûreté nucléaire (IRSN) ",
    version="2019",
    url_data=path_radon,
    url_information="https://www.data.gouv.fr/fr/datasets/connaitre-le-potentiel-radon-de-ma-commune/",
    last_modified="2019-05-06 00:00:00",
    id_reference="irsn_radon",
)

print("Import des données liées aux inondations")
path_inondables = "https://files.georisques.fr/di_2020/tri_2020_sig_di.zip"
assert utilitaires.download_file(path_inondables, overwrite=False)
utilitaires.add_dataset_source(
    filename="risques_inondations_2020",
    module=module,
    description="Données COVADIS sur les risques d'inondation",
    producteur="MAA (Ministère de l'Agriculture), MTES/MCT (Ministère Chargé de la Transition Ecologique et de la Cohésion des Territoires)",
    version="2020",
    url_data=path_inondables,
    url_information="https://www.georisques.gouv.fr/donnees/bases-de-donnees/zonages-inondation-rapportage-2020",
    last_modified="2018-10-01 00:00:00",
    id_reference="risques_inondations_2020",
)

utilitaires.end(f"{module}.py")
