from datacirco import utilitaires
from tqdm import tqdm
import requests
import json
import pandas as pd
import dateparser
import re
import gzip
import os
import shutil

module = "logements"
utilitaires.start(f"{module}.py")
utilitaires.clear_dataset_source(module)


# https://www.insee.fr/fr/statistiques/7704078
# https://www.insee.fr/fr/statistiques/fichier/7704078/base-ic-logement-2020_csv.zip
# https://www.insee.fr/fr/statistiques/fichier/7704078/base-ic-logement-2020-com_csv.zip pour Mayotte

base_logements = [
    {
        "annee": "2018",
        "url": "https://www.insee.fr/fr/statistiques/fichier/5650749/base-ic-logement-2018_csv.zip",
        "url_data": "https://www.insee.fr/fr/statistiques/5650749",
    },
    {
        "annee": "2020",
        "url": "https://www.insee.fr/fr/statistiques/fichier/7704078/base-ic-logement-2020_csv.zip",
        "url_data": "https://www.insee.fr/fr/statistiques/7704078",
    },
]

for base_logement in base_logements:
    print(f"Base logement {base_logement['annee']} :")
    url = base_logement["url"]
    url_data = base_logement["url_data"]
    assert utilitaires.download_file(url)
    soup = utilitaires.get_soup(url_data)
    result = soup.find_all("strong")[2]
    result = soup.find("li", class_="item").text
    result = result[re.search(r"\d", result).start() :].strip()
    millesime = base_logement["annee"]
    try:
        last_modified = dateparser.parse(result.text).date()
        last_modified = last_modified.strftime("%Y-%m-%d %H:%M:%S")
    except Exception:
        last_modified = None
    file = url.split("/")[-1]
    utilitaires.add_dataset_source(
        file,
        module,
        "Recensement de la population - Base infracommunale (IRIS)",
        "Insee",
        millesime,
        url_data=url,
        url_information=url_data,
        last_modified=last_modified,
        id_reference=f"Insee-{file}-{millesime}",
    )

# Données non utilisées dans le pdf
# assert utilitaires.download_file("https://www.insee.fr/fr/statistiques/fichier/6454155/base-ccc-logement-2019.zip")


print("DPE avant 2021:")

# Données antérieure à 2021 :

url_info = "https://data.ademe.fr/datasets/dpe-france"
url = "https://data.ademe.fr/datasets/dpe-france"
soup = utilitaires.get_soup(url_info)
soup = soup.find_all("div", class_="v-list-item__title")[-1]
soup = soup.text.lstrip()
millesime = soup[re.search(r"\d", soup).start() :].strip()
millesime = dateparser.parse(millesime).date()
if os.path.exists("dpe_2b.csv") is False:
    for num in tqdm([*range(1, 20), "2a", "2b", *range(21, 96)]):
        file = f"dpe_{str(num).zfill(2)}"
        url = f"https://data.ademe.fr/data-fair/api/v1/datasets/dpe-{str(num).zfill(2)}/raw"
        assert utilitaires.download_file(url, filename=f"{file}.gz")
        with gzip.open(f"{file}.gz", "r") as f_in, open(f"{file}.csv", "wb") as f_out:
            shutil.copyfileobj(f_in, f_out)
        os.remove(f"{file}.gz")
utilitaires.add_dataset_source(
    "ademe-dpe-france.csv",
    module,
    "Diagnostics de performance énergétique pour les logements",
    "DPE - ADEME",
    millesime.strftime("%Y"),
    url_data=url,
    url_information=url_info,
    id_reference="ademe_dpe_avant_2021",
)

print("DPE après 2021:")
# Données postérieures à 2021 et actualisées le 25 mars 2024 :
if os.path.exists("dpe_logement_ademe_9900.csv") is False:
    liste_colonnes = [
        "N°DPE",
        "Adresse_(BAN)",
        "Adresse_brute",
        "Code_postal_(brut)",
        "Code_postal_(BAN)",
        "Code_INSEE_(BAN)",
        "Nom__commune_(BAN)",
        "N°_département_(BAN)",
        "N°_région_(BAN)",
        "Coordonnée_cartographique_X_(BAN)",
        "Coordonnée_cartographique_Y_(BAN)",
        "Période_construction",
        "Surface_habitable_logement",
        "Type_énergie_principale_chauffage",
        "Conso_é_finale_installation_ECS",
        "Conso_5_usages_par_m²_é_primaire",
        "Coût_total_5_usages",
        "Emission_GES_5_usages_par_m²",
        "Emission_GES_5_usages",
        "Date_établissement_DPE",
        "Date_réception_DPE",
        "Méthode_application_DPE",
        "Date_fin_validité_DPE",
        "Version_DPE",
        "Etiquette_GES",
        "Etiquette_DPE",
    ]
    df_logement = pd.DataFrame({}, columns=liste_colonnes)
    pbar = tqdm(total=8700)
    taille = 8689982
    with open("infos_telechargement.txt", "w") as file:
        nv_url = "https://data.ademe.fr/data-fair/api/v1/datasets/dpe-v2-logements-existants/lines?size=1000"
        file.write(f"{nv_url} \n")
        i = 1
        continuer = True
        while continuer is True:
            i = i + 1
            try:
                if i % 100 == 0:
                    try:
                        df_logement.to_csv(
                            "dpe_logement_ademe_" + str(i) + ".csv",
                            index=False,
                        )
                        taille = taille + len(df_logement)
                        df_logement = pd.DataFrame({}, columns=liste_colonnes)
                    except Exception as pb_enregistrement:
                        print(pb_enregistrement)
                        print("url_enregistrement", nv_url)
                    file.write(
                        f"Taille : {taille} \n Nouveau cycle de 100 enregistrements \n"
                    )
                else:
                    query = requests.get(nv_url)
                    ajout = json.loads(query.text)
                    continuer = "after" in ajout["next"]
                    table_merge = pd.DataFrame(ajout["results"]).loc[:, liste_colonnes]
                    df_logement = pd.concat([df_logement, table_merge])
                    file.write(f"Telechargement de : {nv_url} \n")
                    if continuer is False:
                        df_logement.to_csv(
                            "dpe_logement_ademe_" + str(i) + ".csv",
                            index=False,
                        )
                        taille = taille + len(df_logement)
                        file.write(f"Taille : {taille} \n FIN !")
                    else:
                        nv_url = ajout["next"]
                    pbar.update(1)
            except Exception as e:
                print(e)
                print(nv_url)
    pbar.close()

utilitaires.add_dataset_source(
    filename="ademe_dpe_logements_2024",
    module=module,
    description="Diagnostics de performance énergétique pour les logements ",
    producteur="ADEME",
    version="2024",
    url_data="https://data.ademe.fr/datasets/dpe-v2-logements-existants",
    url_information="https://data.ademe.fr/datasets/dpe-v2-logements-existants/full",
    id_reference="ademe_dpe_apres_2021",
)

utilitaires.end(f"{module}.py")


# # Données non utilisées dans le pdf
# filename = 'dpe_logement_202103.sql'
# url = f"https://files.data.gouv.fr/ademe/{filename}"
# assert utilitaires.download_file(url)
# with open(f'{filename}', 'rb') as src, gzip.open(f'{filename}.gz', 'wb') as dst:
#     dst.writelines(src)
# utilitaires.add_dataset_source(
#     filename,
#     module,
#     "",
#     "ADEME",
#     utilitaires.get_last_modified(url).strftime("%Y"),
#     url,
#     "https://www.data.gouv.fr/fr/datasets/dpe-logements-existants-depuis-juillet-2021/",
#     id_reference=f"ADEME-{filename}",
# )
"""
# Données non utilisées dans le pdf (aucune table créée)
assert utilitaires.download_file("https://www.statistiques.developpement-durable.gouv.fr/sites/default/files/2019-01/RPLS%202016%20-%20Donnees%20detaillees%20au%20logement.zip")
assert utilitaires.download_file("https://www.statistiques.developpement-durable.gouv.fr/sites/default/files/2019-01/RPLS%202017%20-%20Donnees%20detaillees%20et%20geolocalisees%20au%20logement_0.zip")
assert utilitaires.download_file("https://www.statistiques.developpement-durable.gouv.fr/sites/default/files/2019-04/rpls-%202018-geolocalise-donnees-detaillees-au-logement_0.zip")
assert utilitaires.download_file("https://www.statistiques.developpement-durable.gouv.fr/sites/default/files/2020-06/rpls2019_donnees_detaillees_geoloc_logement.zip")
assert utilitaires.download_file("https://www.statistiques.developpement-durable.gouv.fr/sites/default/files/2021-07/rpls2020_donnees_detaillees_geolocalisees_logement.zip")
assert utilitaires.download_file("https://data.statistiques.developpement-durable.gouv.fr/dido/api/files/f1b4b62e-0fa8-4874-a21a-fa52dee8164c", filename="rpls2021_donnees_detaillees_geoloc_logement.7z")
# assert utilitaires.download_file("https://data.statistiques.developpement-durable.gouv.fr/dido/api/v1/datafiles/0b83eee4-14c1-4f21-b749-54d6739a65e5/csv?millesime=2022-01&withColumnName=true&withColumnDescription=true&withColumnUnit=false", filename="rpls2022_donnees_detaillees_geoloc_logement.csv")

files = glob.glob('RPLS*.zip')
files += glob.glob('rpls*.zip')

for file in tqdm(files):
    utilitaires.unzip_file(file)
utilitaires.extract_7z('rpls2021_donnees_detaillees_geoloc_logement.7z')
utilitaires.extract_7z('RPLS_geoloc2021_OpenData.7z')

os.makedirs("open_data", exist_ok=True)

files = glob.glob('2018/*.csv')
files += glob.glob('RPLS*/*.csv')
files += glob.glob('Region/*.csv')
files += glob.glob('Open_Data/Region/*.csv')
#files.append('rpls2022_donnees_detaillees_geoloc_logement.csv')

for file in tqdm(files):
    shutil.move(file, f"open_data/{file.split('/')[-1]}")

files = glob.glob('open_data/*.csv')
for file in tqdm(files):
    subprocess.run(f"python3 {os.environ.get('RACINE')}/utf8fix.py {file} ';'", cwd=os.environ.get("PWD"), shell=True)
    subprocess.run(f"csvclean -d ';' {file}", cwd=os.environ.get("PWD"), shell=True)

files = glob.glob('open_data/*_out.csv')
for file in files:
    shutil.move(file, f"clean_{file.split('/')[-1]}")

for annee in tqdm(range(2016, 2022)):
    subprocess.run(f'csvstack clean*{annee}* | gzip -9 > rpls{annee}.csv.gz', cwd=os.environ.get("PWD"), shell=True)
    # print(glob.glob(f'*{annee}*.csv'))
    for file in glob.glob(f'*{annee}*.csv'):
        os.remove(file)

walk = list(os.walk(utilitaires.current_dir))
for path, _, _ in tqdm(walk[::-1]):
    if len(os.listdir(path)) == 0:
        os.rmdir(path)

"""
