
- Documentation générale sur le carroyage : https://www.insee.fr/fr/statistiques/fichier/2520034/donnee-carroyees-documentation-generale.pdf
- Documentation synthétique sur les données carroyées 2010 (diffusées en 2013) https://www.insee.fr/fr/statistiques/fichier/2520034/documentation-synthetique-donnees-a-200m.pdf

```sql
CREATE TABLE insee_population_carroyee_2010_bco as
select n.wkb_geometry,
    c.idinspire,
    n.depcom,
    c.ind_c as ind,
    r.men*c.ind_c/ind_r as men,
    r.men_surf*c.ind_c/ind_r as men_surf,
    r.men_coll*c.ind_c/ind_r as men_coll,
    r.men_5ind*c.ind_c/ind_r as men_5ind,
    r.men_1ind*c.ind_c/ind_r as men_1ind,
    r.men_prop*c.ind_c/ind_r as men_prop,
    r.men_basr*c.ind_c/ind_r as men_pauv,
    r.ind_age1*c.ind_c/ind_r as ind_0_3,
    r.ind_age2*c.ind_c/ind_r as ind_4_5,
    r.ind_age3*c.ind_c/ind_r as ind_6_10,
    r.ind_age4*c.ind_c/ind_r as ind_11_14,
    r.ind_age5*c.ind_c/ind_r as ind_15_17,
    (ind_r-ind_age1-ind_age2-ind_age3-ind_age4-ind_age5-ind_age6)*c.ind_c/ind_r as ind_18_24,
    r.ind_age6*c.ind_c/ind_r as ind_25p,
    r.ind_age7*c.ind_c/ind_r as ind_65p,
    r.ind_age8*c.ind_c/ind_r as ind_75p,
    r.ind_srf * c.ind_c/ind_r as ind_snv
from insee_population_carroyee_2015 n inner join  insee_population_carroyee_2010_car c on c.idinspire=n.idinspire
inner join insee_population_carroyee_2010_rect r on r.idk = c.idk;



select c.wkb_geometry,
    c.idinspire,
    n.depcom as depcom,
    c.ind_c as ind
from insee_population_carroyee_2010_car c inner join insee_population_carroyee_2015 n on c.idinspire=n.idinspire;
```

```
infocirco=# \d insee_population_carroyee_2010_car
           Table "public.insee_population_carroyee_2010_car"
    Column    |          Type          | Collation | Nullable | Default
--------------+------------------------+-----------+----------+---------
 wkb_geometry | geometry(Polygon,4326) |           |          |
 idinspire    | character varying(30)  |           |          |
 id           | character varying(23)  |           |          |
 idk          | character varying(25)  |           |          |
 ind_c        | numeric(16,4)          |           |          |
 nbcar        | numeric(16,4)          |           |          |
Indexes:
    "insee_population_carroyee_2010_car_wkb_geometry_geom_idx" gist (wkb_geometry)

infocirco=# \d insee_population_carroyee_2010_rect
 Table "public.insee_population_carroyee_2010_rect"
  Column  |  Type   | Collation | Nullable | Default
----------+---------+-----------+----------+---------
 idk      | text    |           |          |
 men      | numeric |           |          |
 men_surf | numeric |           |          |
 men_occ5 | numeric |           |          |
 men_coll | numeric |           |          |
 men_5ind | numeric |           |          |
 men_1ind | numeric |           |          |
 i_1ind   | numeric |           |          |
 men_prop | numeric |           |          |
 i_prop   | numeric |           |          |
 men_basr | numeric |           |          |
 i_basr   | numeric |           |          |
 ind_r    | numeric |           |          |
 ind_age1 | numeric |           |          |
 ind_age2 | numeric |           |          |
 ind_age3 | numeric |           |          |
 ind_age4 | numeric |           |          |
 ind_age5 | numeric |           |          |
 ind_age6 | numeric |           |          |
 ind_age7 | numeric |           |          |
 i_age7   | numeric |           |          |
 ind_age8 | numeric |           |          |
 i_age8   | numeric |           |          |
 ind_srf  | numeric |           |          |
 nbcar    | numeric |           |          |

infocirco=# \d insee_population_carroyee_2015
                                         Table "public.insee_population_carroyee_2015"
    Column    |          Type          | Collation | Nullable |                             Default
--------------+------------------------+-----------+----------+-----------------------------------------------------------------
 ogc_fid      | integer                |           | not null | nextval('insee_population_carroyee_2015_ogc_fid_seq'::regclass)
 wkb_geometry | geometry(Polygon,4326) |           |          |
 idinspire    | character varying(30)  |           |          |
 id_carr1km   | character varying(31)  |           |          |
 i_est_cr     | numeric(1,0)           |           |          |
 id_carr_n    | character varying(32)  |           |          |
 groupe       | numeric(7,0)           |           |          |
 depcom       | character varying(5)   |           |          |
 i_pauv       | numeric(1,0)           |           |          |
 id_car2010   | character varying(30)  |           |          |
 ind          | numeric(6,1)           |           |          |
 men          | numeric(6,1)           |           |          |
 men_pauv     | numeric(5,1)           |           |          |
 men_1ind     | numeric(5,1)           |           |          |
 men_5ind     | numeric(5,1)           |           |          |
 men_prop     | numeric(6,1)           |           |          |
 men_fmp      | numeric(5,1)           |           |          |
 ind_snv      | numeric(10,1)          |           |          |
 men_surf     | numeric(8,1)           |           |          |
 men_coll     | numeric(6,1)           |           |          |
 men_mais     | numeric(5,1)           |           |          |
 log_av45     | numeric(6,1)           |           |          |
 log_45_70    | numeric(6,1)           |           |          |
 log_70_90    | numeric(6,1)           |           |          |
 log_ap90     | numeric(6,1)           |           |          |
 log_inc      | numeric(5,1)           |           |          |
 log_soc      | numeric(6,1)           |           |          |
 ind_0_3      | numeric(5,1)           |           |          |
 ind_4_5      | numeric(5,1)           |           |          |
 ind_6_10     | numeric(5,1)           |           |          |
 ind_11_17    | numeric(5,1)           |           |          |
 ind_18_24    | numeric(5,1)           |           |          |
 ind_25_39    | numeric(6,1)           |           |          |
 ind_40_54    | numeric(5,1)           |           |          |
 ind_55_64    | numeric(5,1)           |           |          |
 ind_65_79    | numeric(5,1)           |           |          |
 ind_80p      | numeric(5,1)           |           |          |
 ind_inc      | numeric(6,1)           |           |          |
 i_est_1km    | numeric(1,0)           |           |          |
Indexes:
    "insee_population_carroyee_2015_pk" PRIMARY KEY, btree (ogc_fid)
    "insee_population_carroyee_2015_wkb_geometry_geom_idx" gist (wkb_geometry)

```