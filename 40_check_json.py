from pathlib import Path

circo_errors = []


def check_folder_contents(base_folder="data-json"):
    base_path = Path(base_folder)
    # Check if base folder exists
    if not base_path.exists():
        print(f"Error: {base_folder} does not exist")
        return
    # Get all subfolders
    subfolders = sorted([f for f in base_path.iterdir() if f.is_dir()])
    for subfolder in subfolders:
        # Get all files in subfolder
        files = list(subfolder.glob("*"))
        # Check number of files
        # 971 : Guadeloupe.
        # 973 : Guyane.
        # 977 : Saint-Barthélemy.
        expected_len = 45
        if (
            "971" in subfolder.__str__()
            or "973" in subfolder.__str__()
            or "977" in subfolder.__str__()
        ):
            expected_len = 37
        # 972 : Martinique.
        # 974 : La Réunion.
        if "972" in subfolder.__str__() or "974" in subfolder.__str__():
            expected_len = 43
        # 976 : Mayotte.
        if "976" in subfolder.__str__():
            expected_len = 31
        if len(files) != expected_len:
            if (len(files) + 1 == expected_len) and (
                len(list(subfolder.glob("*.log"))) == 1
            ):
                # C'est parcequ'il a été généré manuellment
                pass
            else:
                print(
                    f"{subfolder} Warning: Found {len(files)} files, expected {expected_len}"
                )
                circo_errors.append(subfolder.__str__().split("/")[-1])
        # Check file sizes
        for file in files:
            size_kb = file.stat().st_size
            if size_kb < 17:
                print(
                    f"{subfolder} Warning: {file.name} is smaller than 17 bytes ({size_kb:.2f}B)"
                )


if __name__ == "__main__":
    check_folder_contents()
    print(f"Found {len(circo_errors)} errors :")
    for error in circo_errors:
        print(error)
