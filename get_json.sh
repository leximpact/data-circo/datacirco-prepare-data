#! /bin/bash
export RACINE=`pwd`
CIRCO=$1
echo -e "\n$CIRCO Extracting json data for $CIRCO - `date`"
rm -rf data-json/$CIRCO/*
mkdir -p data-json/$CIRCO
cd data-json/$CIRCO
umask 0000
# Fail on first error
set -e
source ../../.venv/bin/activate

echo "$CIRCO Extracting data in `pwd` "
# Test if $2 is set
if [ -z "$2" ]
then
    python ../../extract_json.py $CIRCO $2 $3 $4 2>extract_json_$CIRCO.log
else
    echo "Debug mode"
    python ../../extract_json.py $CIRCO $2 $3 $4
    
fi


zip -q $CIRCO.zip *
# rm *.csv
qr "https://leximpact.an.fr/datacirco/circo/$CIRCO.pdf" > qr-code-circo.png
echo -e "\n$CIRCO Extracting json data for $CIRCO done in $SECONDS s at `date`"
# Sources n'est généré que pour la première circonscription
if [ $CIRCO == "001-01" ]
then
    mv sources.json $RACINE/data-json/sources.json
    mv circos_list.json $RACINE/data-json/circos_list.json
fi