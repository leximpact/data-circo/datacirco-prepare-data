#! /bin/bash
# Stop at first error
set -e

echo "Starting script at $(date)"

for c in $(cat missing_circo.txt)
do
    echo "------------ $c -----------------------------"
    ./get_json.sh $c
done
echo "Done script $(date) in $SECONDS s at $(date)"
