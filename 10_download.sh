#!/bin/bash

echo "Starting script at `date`"
# Stop at first error
set -e

export RACINE=`pwd`
export DATA=$RACINE/data
mkdir -p $DATA

METEO=$DATA/meteo

# Meteo
# mkdir -p $METEO/ERA5/monthly_dl && cd $METEO
# wget https://gisco-services.ec.europa.eu/distribution/v2/nuts/shp/NUTS_RG_20M_2021_4326.shp.zip
# unzip -o NUTS_RG_20M_2021_4326.shp.zip
# wget https://data.biogeo.ucdavis.edu/data/diva/adm/FRA_adm.zip
# unzip -o FRA_adm.zip
# wget https://static.data.gouv.fr/resources/carte-des-circonscriptions-legislatives-2012-et-2017/20170721-135755/france-circonscriptions-legislatives-2012.zip
# unzip -o france-circonscriptions-legislatives-2012.zip

for i in entreprises revenus environnement education equipements logements emplois sante population geographie elus meteo
do
    echo "Downloading $i"
    bash ci/download.sh $i
    echo "################################"
done

echo "Done script at `date`"
