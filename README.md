# Data Circo

## Installation

Voir [INSTALL.md](INSTALL.md) pour l'installation sur un serveur.

Pour l'installation pour développer en local, voir [CONTRIBUTING.md](CONTRIBUTING.md).

## Récupération des données

- `bash 05_download_ign.sh` Pour initialiser la base avec des données IGN. __rapide__
- `bash 10_download.sh` Pour télécharger toutes les données. __Il faut plusieurs heures__

Ce script fait appel à `utf8fix.py`.

## Contours des circonscriptions

Le fichier `circonscriptions-legislatives.json` provient de MapoTempo mais l'[url](http://panneaux-election.fr/carte/circonscriptions-legislatives.json.zip) n'est plus disponible depuis plusieurs mois.

Ce fichier est trop gros (96Mo) pour être utilisé sur le site web. Sa taille a été réduite à moins de 1Mo en passant par le site https://mapshaper.org/ et le menu "Simplify" pour obtenir `circonscriptions-legislatives-smaller.json`.

## Import des données dans la base Postgresql

Ce script réalise à la fois l'import des fichiers brutes, mais aussi les post-traitements pour créer toutes les tables.

Attention, cela prend une dizaine d'heures. Il est donc conseillé de le lancer avec `nohup` (ou tmux) pour ne pas arrêter l'import en cas de problème de connexion SSH.

`nohup bash 20_populate_db.sh &`

Pour voir l'avancement:

`tail -f nohup.out`

Le téléchargement des données météo complètes prend plusieurs jours !!! Il faut donc changer la date de début pour commencer seulement par les données manquantes.

## Generation des PDF

Il faut lancer `30_get_all_json.sh`, dans le projet DataCirco-prepare-data, qui appel `get_json.sh`, qui lui-même appel `extract_json.py`.

Attention, cela prend 6 heures. Il est donc conseillé de le lancer avec `tmux` ou `nohup` pour ne pas arrêter l'import en cas de problème de connexion SSH.

`nohup bash 30_get_all_json.sh &`

Pour voir l'avancement:

`tail -f nohup.out`

Cela va exporter les fichiers JSON dans le dossier `./data-json/`.

Ensuite c'est le projet [DataCirco-Web](https://git.leximpact.dev/leximpact/data-circo/datacirco-web) qui va lire ces fichiers pour générer le site web.

Pour le détail de la production LexImpact, voir [La doc DevOps](https://git.leximpact.dev/leximpact/communs/devops/-/blob/master/notes_installations/datacirco.md) (accès restreint).

### Mode "kit des déptés"

Ce mode est destiné à générer une plaquette LexImpact avec un extrait de DataCirco :
- On ajoute un encarté sur la page de garde. C'est le fichier `kit-premiere-couverture.png`.
- On indique le nom du député(e) pour donner un aspect personnalisé. => Penser à mettre à jour la liste, il y a souvent des changements.
- On affiche une capture d'écran du sommaire. => Penser à la refaire avant s'il y a eu des ajouts. C'est le fichier `kit-table_matieres.png`.
- On affiche le début du chapitre _population_.
- On affiche une page montrant un apperçu des autres pages, avec un QR Code pour le télécharger. C'est le fichier `kit-image-qr-code.png`. => Bien vérifier que c'est le QR Code dynamique qui est dans le document et pas celui de la maquette !!!
- On affiche une page sur les autres outils LexImpact. C'est le fichier `kit-quatrieme-couverture.png`.

Pour l'activer il faut modifier le fichier `data2latex.py` et mettre la variable `mode_kit_depute` à _True_.

#### Génération des étiquettes

Extraction de la liste des député-e-s en CSV.

```sql
SELECT i.nom as dep_nom, dep, circo, civ, REPLACE(REPLACE(civ, 'Mme', 'Madame'),'M.','Monsieur') as civilite,
	REPLACE(REPLACE(civ, 'Mme', 'Députée'),'M.','Député') as depute, prenom, an.nom
FROM ign_departement i, an_mailing an
WHERE REGEXP_REPLACE(an.dep,'^0','','g') = i.insee_dep
AND i.insee_dep <= '974'
ORDER BY 2, 3
```

Publipostage des étiquettes avec LibreOffice :
- Il faut LibreOffice Base qui n'est pas installé par défaut sous Ubuntu : `sudo apt-get install libreoffice`
- Il faut ajouter la base dans le module de publipostage
- Il faut ajouter un champ invisible "enregistrement suivant"

 https://lofurol.fr/joomla/attachments/article/128/Tutoriel_LibreOffice_%C3%89tiquettes_Fusion_Mailing.pdf

### Test

Si ça ne fonctionne pas, un test de la partie Python peut se lancer par:
```sh
source .venv/bin/activate
python data2latex.py 006-08
```

Pour tester aussi la partie PDF, il faut utiliser `bash makepdf.sh 006-08`

## Verification

Lancer `40_check.sh` qui va afficher les circos sans PDF. Si rien ne s'affiche c'est que c'est bon. _A noter que le scripts n'affichent pas les circos de l'étranger, ni St Pierre et Miquelon._

## Envois des mails aux députés

L'envoi d'email a été codé mais jamais utilisé, faute d'autorisation hierarchique.

- Pour la métropole : `50_emailing.py`
- Pour les Collectivité d'outre-mer: `60_emailing_COM.py`
- Pour les Départements d'outre-mer: `70_emailing_DOM.py`

## Documentations

- [Les données carroyées, des outils et méthodes innovants](https://www.insee.fr/fr/statistiques/5008701?sommaire=5008710&q=carroyage)
