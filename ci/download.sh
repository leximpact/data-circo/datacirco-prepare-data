#!/bin/bash
# Stop at first error
set -e

set -a # automatically export all variables
source .env
set +a

RACINE=`pwd`
echo "RACINE: $RACINE"
DATA=$RACINE/data
echo "DATA: $DATA"
source .venv/bin/activate
MODULE_NAME=$1

echo "Starting script at `date` for $MODULE_NAME"

MODULE_FOLDER=$DATA/$MODULE_NAME
echo "MODULE_FOLDER: $MODULE_FOLDER"

cd $MODULE_FOLDER
echo "Running python3 $RACINE/downloads/$MODULE_NAME.py"
python3 $RACINE/downloads/$MODULE_NAME.py

echo "Done script at `date` for $MODULE_NAME"
