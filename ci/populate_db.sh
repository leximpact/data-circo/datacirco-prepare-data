#!/bin/bash
# Stop at first error
set -e

RACINE=`pwd`
echo "RACINE: $RACINE"
DATA=$RACINE/data
echo "DATA: $DATA"
source .venv/bin/activate
MODULE_NAME=$1

echo "Starting script at `date` for $MODULE_NAME"

MODULE_FOLDER=$DATA/`echo $MODULE_NAME | sed 's/-.*//'`
echo "MODULE_FOLDER: $MODULE_FOLDER"

cd $MODULE_FOLDER
# if the module name is 'entreprises', then we have to run 'entreprises-01.py' and 'entreprises-02.py'
if [ $MODULE_NAME == "entreprises" ]; then
    echo "Running python3 $RACINE/populate_db/entreprises-01.py"
    python3 $RACINE/populate_db/entreprises-01.py
    echo "Running python3 $RACINE/populate_db/entreprises-02.py"
    python3 $RACINE/populate_db/entreprises-02.py
    echo "Done script at `date` for $MODULE_NAME"
elif [ $MODULE_NAME == "meteo" ]; then
    echo "Running python3 $RACINE/populate_db/meteo_convert.py"
    python3 $RACINE/populate_db/meteo_convert.py
    echo "Running python3 $RACINE/populate_db/meteo_import.py"
    python3 $RACINE/populate_db/meteo_import.py
else
    echo "Running python3 $RACINE/populate_db/$MODULE_NAME.py"
    python3 $RACINE/populate_db/$MODULE_NAME.py

fi

echo "Done script at `date` for $MODULE_NAME"
