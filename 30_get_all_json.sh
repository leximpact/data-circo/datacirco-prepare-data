#! /bin/bash

echo "Starting script at $(date)"

# Get the full command line, assuming the password part is always first if it exists
FULL_COMMAND=$(poetry run python datacirco/connexion_db.py | tail -1)
# Extract the password value if it exists
PGPASSWORD=$(echo $FULL_COMMAND | grep -oP 'PGPASSWORD=\K[^ ]+')

# Conditionally export PGPASSWORD if it is not empty
if [ -n "$PGPASSWORD" ]; then
    export PGPASSWORD
fi

# Remove the PGPASSWORD part from the command to avoid execution error
PSQL_COMMAND_LINE=$(echo "$FULL_COMMAND" | sed 's/PGPASSWORD=[^ ]* //')
echo PSQL_COMMAND_LINE=$PSQL_COMMAND_LINE
# 976 = Mayotte
$PSQL_COMMAND_LINE -tAc "SELECT ref FROM zone_circo WHERE ref <'975' OR ref LIKE '976%' ORDER BY 1" | parallel -j 75% --halt now,fail=1 ./get_json.sh {}

echo "Done script $(date) in $SECONDS s at $(date)"
