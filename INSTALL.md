
# Installation de DataCirco

Pour l'installation en local, voir CONTRIBUTING.md

Il faut un conteneur de plus de 4 Go de RAM, sinon:
```
Jun 07 08:35:00 data-circo systemd[1]: postgresql@14-main.service: A process of this unit has been killed by the OOM killer.
Jun 07 08:35:00 data-circo systemd[1]: postgresql@14-main.service: Failed with result 'oom-kill'.
```

Pour l'espace disque il y a un montage vers le disque principal du serveur pour accèder aux données d'ailleurs et ne pas les stocker dans le conteneur. Seule le code est dans le conteneur, ni les données, ni les pdf.

`cat /etc/pve/lxc/134.conf `
```
arch: amd64
cores: 6
features: nesting=1
hostname: datacirco-prepare-data
memory: 24000
mp0: /data/private-data/output/datacirco/data,mp=/home/datacirco/datacirco/data,replicate=0
mp1: /data/private-data/output/datacirco/data-json,mp=/home/datacirco/datacirco/data-json,replicate=0
mp2: /data/private-data/output/datacirco/pdf,mp=/home/datacirco/datacirco/pdf,replicate=0
mp3: /data/private-data/output/datacirco/database-postgresql-15/main,mp=/var/lib/postgresql/15/main,replicate=0
mp4: /data/private-data/output/datacirco/backup,mp=/home/datacirco/datacirco/backup,replicate=0
net0: name=eth0,bridge=vmbr1,firewall=1,gw=192.168.0.254,hwaddr=BC:24:11:3C:67:53,ip=192.168.0.134/24,type=veth
onboot: 1
ostype: debian
rootfs: data-proxmox:134/vm-134-disk-0.raw,size=48G
swap: 512
unprivileged: 1

```
## Eléments de base

```
apt update && apt upgrade -y
apt -y install python3.11-venv python3-wheel python3-pip sudo tmux
apt -y install gnupg2 wget nano git curl csvkit p7zip-full jq optipng imagemagick-6.q16hdri patool ghostscript python3-cairo libcairo2-dev gdal-bin zip parallel python3-dev  build-essential
```
Explication des paquets :
* `python3.11-venv` permet de créer un environnement virtuel Python.
* `python3-wheel` permet d'installer des librairies Python.
* `python3-pip` permet d'installer des librairies Python.
* `python3-cairo` permet de générer des cartes.
* `libcairo2-dev` permet de générer des cartes.
* `gdal-bin` permet d'installer `ogr2ogr`.
* `tmux` permet de lancer des commandes dans un terminal et de les laisser tourner même si on ferme le terminal.
* `optipng` permet de compresser les images PNG.
* `imagemagick-6.q16hdri` permet de convertir la première page du PDF en image.
* `ghostscript` est utilisé par imagemagick pour convertir les PDF en image.
* `patool` permet de décompresser les archives.
* `jq` permet de manipuler les fichiers JSON.
* `csvkit` permet de manipuler les fichiers CSV.
* `curl` permet de télécharger des fichiers.
* `git` permet de télécharger le code source de DataCirco.
* `nano` est un éditeur de texte.
* `wget` permet de télécharger des fichiers.
* `gnupg2` permet de vérifier les signatures des paquets.
* `sudo` permet d'exécuter des commandes en tant que root et n'est pas installé par défaut sur Debian 11.
* `p7zip-full` permet de décompresser les archives 7z.
* `parallel` permet d'exécuter des commandes en parallèle.
* `python3-dev` permet de compiler des librairies Python.
* `build-essential` permet de compiler des programmes (nécessaire pour pyCairo).
    
#### Locales pour éviter des messages d'erreurs PERL

`dpkg-reconfigure locales`

Cocher `en_US.UTF8` et `fr_FR.UTF8` et choisir `en_US.UTF8` par défaut sur l'écran suivant.

#### Paramétrage de la Policy d'ImageMagick

`sed -i '/PDF/d' /etc/ImageMagick-6/policy.xml`


### Installation de PostgreSQL 15

Sur Debian 12, c'est Postgres-15 par défaut.
```bash
apt install -y postgresql postgresql-postgis-scripts
```

Pour vérifier qu'il fonctionne:

`pg_lsclusters`


## Créer un compte dédié

Créer un compte Linux `datacirco`:

`useradd datacirco -m`

Créer un compte PostgreSQL `datacirco`:

`sudo -u postgres createuser --superuser datacirco`

Créer une base datacirco:

`sudo -u postgres createdb infocirco`

Donner les droits


```sql
sudo -u postgres psql
GRANT ALL PRIVILEGES ON DATABASE infocirco TO datacirco;
```

## Environnement Python DataCirco

```bash
su - datacirco
git clone https://git.leximpact.dev/leximpact/datacirco.git
cd datacirco
curl -sSL https://install.python-poetry.org | python3 -
/home/datacirco/.local/bin/poetry config virtualenvs.in-project true
/home/datacirco/.local/bin/poetry install
# pip install -r requirements.txt
```

## Clef d'API pour la météo

Il faut créer un fichier `~/.cdsapirc` contenant la clef d'API pour la météo, renseignée dans le Keypass:
```
url: https://cds.climate.copernicus.eu/api/v2
key: <UID>:<API key>
verify: 0
```

## Installation et configuration d'un serveur mail

Le script `emailing.py` nécessite un serveur SMTP, nous utilisons celui de `mail.leximpact.dev`.

Attention : l'envoie de mail n'a finalement jamais été utilisé.
