import os
import sys
import subprocess
import time
import zipfile
import gzip
import dbfread
import csv
import shutil
import staticmaps
import requests
import json
from pyunpack import Archive
import datetime
import numpy as np
from bs4 import BeautifulSoup
from decimal import Decimal
from pandas import DataFrame
from retry import retry
from datacirco.connexion_db import db, run


class CustomJsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            try:
                return float(obj)
            except Exception:
                if len(str(obj)) == 0:
                    return None
                else:
                    return str(obj)
        if isinstance(obj, DataFrame):
            obj = obj.replace({np.nan: None})
            return obj.to_dict("records")
        if isinstance(obj, datetime.date):
            return obj.isoformat()
        try:
            return json.JSONEncoder.default(self, obj)
        except TypeError as e:
            print(e)
            print(obj)
            print(f"Type: {type(obj)}")
            raise e


current_dir = "./"
begin = None


def trace(msg):
    """Prints message in log file."""
    print(msg, file=sys.stderr)


def start(filename):
    """Records time at the beginning of the file."""
    global begin
    begin = time.perf_counter()
    print(f"============ Starting {filename} ============")


def end(filename):
    """Records time at the end of the file."""
    global begin
    print(f"\nFin de {filename} en {time.perf_counter() - begin} secondes.")


def execute_command(command, debug: bool = False):
    """Executes command in shell."""
    if debug:
        trace("Executing : " + command)
    p = subprocess.run(command, cwd=os.environ.get("PWD"), shell=True)
    if p.returncode != 0:
        raise Exception(f"Invalid result for {command}: { p.returncode= }")


def db_exec(query, name=None):
    """Executes a query on the database."""
    start = time.time()
    trace(query.decode())
    try:
        db.execute(query)
    except Exception as e:
        # TODO: implement a better exception handling with pg.rollback()
        raise e
    if name:
        data = db.fetchall()
        with open(name + ".csv", "w") as csvfile:
            out_csv = csv.writer(csvfile)
            out_csv.writerow([desc[0] for desc in db.description])
            for line in data:
                out_csv.writerow(line)
        if db.rowcount > 0:
            db.scroll(0, mode="absolute")
    trace(f"SQL Execution time: {time.time() - start}")


def db_exec_get_first_value(query):
    """Executes a query on the database and returns the first value of the
    first row."""
    db_exec(db.mogrify(query))
    return db.fetchall()[0][0]


def clean_string(string):
    """Remove accent, spaces and special characters from a string."""
    string = string.lower()
    to_replace = [" ", "-", "'", "(", ")"]
    for char in to_replace:
        string = string.replace(char, "_")
    # Accent
    string = string.replace("é", "e")
    string = string.replace("è", "e")
    # U+FEFF is the byte order mark, or BOM, and is used to tell the difference between big- and little-endian UTF-16 encoding
    # Il est présent dans la colonne "Code commune" de data/emplois/etablissements-et-effectifs-salaries-au-niveau-commune-x-ape.csv
    string = string.replace("\uFEFF", "")
    to_keep = ["+", ".", "_", "-"]
    string = "".join(c for c in string if c.isascii() and (c.isalnum() or c in to_keep))
    return string


def get_fields(file, delimiter=","):
    """Returns the name of the columns of the file each followed by 'text'."""
    with open(file, "r") as f:
        header = f.readline().strip().split(delimiter)
    header = [clean_string(h) for h in header]
    fields = '"' + '" text, "'.join(header) + '" text'
    return fields, header


def fix_number_infile(filename):
    """Removes spaces in numbers like '1 234' in a file."""
    sed_cmd = r"sed -i 's/\([0-9]\) \([0-9]\{3\}\)/\1\2/g' " + filename
    execute_command(sed_cmd)


def replace_in_file(filename: str, find: str, replace: str):
    """Replaces a string in a file."""
    # Try using sed
    try:
        new_find = find.replace("/", "\\/")
        new_find = new_find.replace(".", r"\.")
        sed_cmd = f"sed -i 's/{new_find}/{replace}/g' {filename}"
        # trace(f"Executing : {sed_cmd}")
        execute_command(sed_cmd)
        return
    except Exception as e:
        trace(f"WARNING : Executing 'sed' failed : {e}\n Using Python")
    # If sed failed, do it in python
    with open(filename, "r") as file:
        filedata = file.read()
    # Replace the target string
    filedata = filedata.replace(find, replace)
    # Write the file out again
    with open(filename, "w") as file:
        file.write(filedata)


def get_sources(module):
    """Returns the sources for the module given in parameter."""
    db.execute(
        db.mogrify(
            """
            SELECT *
            FROM dataset_source
            WHERE module=%s
            """,
            (module,),
        )
    )
    sources = db.fetchall()
    columns = [desc[0] for desc in db.description]
    return [dict(zip(columns, row)) for row in sources]


@retry(
    (requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError),
    tries=3,
    delay=2,
)
def download_file(
    url: str,
    path: str = None,
    filename: str = None,
    overwrite: bool = True,
    api_key: str = None,
    debug: bool = False,
):
    """Downloads a file in path from URL.

    param: url: URL of the file to download
    param: path: Path where to download the file
    param: filename: Name of the file after download
    param: overwrite: If False, do not download the file if it already exists
    param: api_key: API key for the download
    param: debug: If True, prints debug messages
    """
    if path is None:
        path = current_dir

    if filename is None:
        filename = url.split("/")[-1]
        filename = filename.replace("%20", " ")
        trace(f"Utilisation du nom de fichier : {filename}")

    # Do not download if file already exists
    if (
        os.path.exists(path + filename)
        and os.path.getsize(path + filename) > 0
        and not overwrite
    ):
        trace(f"{path + filename} already exists, skipping...")
        return True
    elif debug:
        trace(f"Downloading {url} to {path + filename}...")

    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
    }
    if api_key:
        headers["Authorization"] = f"Apikey {api_key}"
    with requests.get(
        url,
        stream=True,
        headers=headers,
    ) as r:
        r.raise_for_status()
        with open(path + filename, "wb") as f:
            f.write(r.content)
            return True


def unzip_file(path):
    """Unzips .zip file."""
    with zipfile.ZipFile(path, "r") as zip_ref:
        zip_ref.extractall(current_dir)


def extract_7z(path):
    """Unzips .7z file."""
    Archive(path).extractall(current_dir)


def extract_gz(path):
    """Unzisp .gz file."""
    with gzip.open(path, "r") as src, open(os.path.splitext(path)[0], "wb") as dest:
        shutil.copyfileobj(src, dest)


def extract_dbf(path):
    """Extracts dbf from zip file."""
    files = []
    with zipfile.ZipFile(path, "r") as zip_file:
        dbf_files = [name for name in zip_file.namelist() if name.endswith(".dbf")]
        for file in dbf_files:
            filename = file.split("/")[-1]
            with zip_file.open(file) as dbf_data, open(filename, "wb") as output_file:
                output_file.write(dbf_data.read())
            files.append(filename)
    return files


def dbf_to_csv(path):
    """Converts a dbf file to csv."""
    table = dbfread.DBF(path)
    filename = path.split(".")[0] + ".csv"
    with open(filename, "w", newline="") as f:
        writer = csv.writer(f)
        writer.writerow(table.field_names)
        for record in table:
            writer.writerow(record.values())
    return filename


def get_last_modified(url):
    """Returns last modified data of file in url."""
    last_modified = None
    try:
        response = requests.head(url)
        if "Last-Modified" in response.headers:
            last_modified = response.headers["Last-Modified"]
        else:
            return last_modified
    except requests.RequestException as e:
        trace(f"An error occurred: {e}")
        return None
    return datetime.datetime.strptime(last_modified, "%a, %d %b %Y %H:%M:%S GMT")


def get_current_time():
    """Returns the current datetime."""
    return datetime.datetime.now()


def clear_dataset_source(module):
    """Deletes all the sources of the module."""
    run(
        db.mogrify(
            """
            DELETE FROM dataset_source
            WHERE module=%s
            """,
            (module,),
        )
    )


def add_dataset_source(
    filename,
    module,
    description,
    producteur,
    version,
    url_data,
    url_information,
    last_modified="",
    licence="Licence Ouverte",
    id_reference=None,
):
    if last_modified == "":
        last_modified = get_last_modified(url_information)
    if last_modified is None:
        last_modified = get_current_time()
    if id_reference is None:
        id_reference = f"{producteur}{filename}{version}"
    id_reference = f"'{clean_string(id_reference).replace('.csv', '').replace('.geojson', '').replace('.zip', '')}'"
    sql = f"""INSERT INTO dataset_source (filename, module, description, producteur, version, licence, url, url_data, last_modified, date_download, id_reference) VALUES (
        '{filename}', '{module}', '{description.replace("'", "''")}', '{producteur.replace("'", "''")}', '{version}', '{licence.replace("'", "''")}', '{url_information.replace("'", "''")}', '{url_data.replace("'", "''")}', '{last_modified}', '{get_current_time()}', {id_reference})
        ON CONFLICT (filename) DO UPDATE SET
            description = excluded.description,
            version = excluded.version,
            url_data = excluded.url_data,
            last_modified = excluded.last_modified,
            date_download = excluded.date_download,
            licence = excluded.licence,
            id_reference = excluded.id_reference;
    """
    run(sql)


def get_soup(url):
    page = requests.get(url)
    soup = BeautifulSoup(page.content, "html.parser")
    return soup


def draw_poly(
    carte,
    geo,
    poly_fill=staticmaps.parse_color("#FFFFFF00"),
    poly_width=2,
    poly_color=staticmaps.BLACK,
):
    """Draws polygons on maps."""
    if geo[0]["type"] == "Polygon":
        for polygon in geo[0]["coordinates"]:
            carte.add_object(
                staticmaps.Area(
                    [staticmaps.create_latlng(lat, lng) for lng, lat in polygon],
                    fill_color=poly_fill,
                    width=poly_width,
                    color=poly_color,
                )
            )
    else:
        for mp in geo[0]["coordinates"]:
            for polygon in mp:
                carte.add_object(
                    staticmaps.Area(
                        [staticmaps.create_latlng(lat, lng) for lng, lat in polygon],
                        fill_color=poly_fill,
                        width=poly_width,
                        color=poly_color,
                    )
                )


def list_to_dict(liste, keys):
    """Converts a list to a dictionary."""
    return {keys[i]: liste[i] for i in range(0, len(liste))}


def int_list_to_dict(liste, keys):
    """Converts a list of int to a dictionary."""
    return {keys[i]: int(liste[i]) for i in range(0, len(liste))}


def float_list_to_dict(liste, keys):
    """Converts a list of float to a dictionary."""
    return {keys[i]: float(liste[i]) for i in range(0, len(liste))}


def to_int(value):
    """Converts a string to an int."""
    try:
        return int(value)
    except (ValueError, TypeError):
        return None


def get_str_circo(circo_num):
    if circo_num[2] == "0":
        return "0" + circo_num[:2] + "-" + circo_num[3:]
    else:
        return circo_num[:3] + "-" + circo_num[3:]


def get_circo_communes(circo):
    db.execute(
        db.mogrify(
            "SELECT communes FROM zone_circo WHERE ref = %s;",
            (circo,),
        )
    )
    communes = db.fetchone()[0]
    return communes
