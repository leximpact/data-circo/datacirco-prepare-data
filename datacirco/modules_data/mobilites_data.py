from dataclasses import dataclass
from datacirco import utilitaires
from datetime import datetime
from datacirco.modules_data.data import DataCirco, GetDataCirco


@dataclass()
class DataMobilites:
    nb_bornes_recharges_circo = None
    nb_stations_carburants = None
    dict_types_energie_annee_circo = None
    dict_type_energie_tot_circo = None


class GetMobilites:
    """To retreive data from the database."""

    def __init__(self, db, data: DataCirco):
        """
        Args:
            db: Connection to the database
            data: A DataCirco object
        """
        self.db = db
        utilitaires.db = db
        data.mobilites = DataMobilites()
        self.data = data
        self.dept = data.dept
        self.circo = data.circo
        self.data.mobilites.sources = utilitaires.get_sources("mobilites")

    def get_irve(self, annee=datetime.today().year):
        "To obtain electric charging stations for electric vehicles inside the constituency"
        sql = f"""SELECT COUNT(t.*) FROM (
            SELECT * FROM irve_2024 AS i
            LEFT JOIN zone_circo AS c
            ON ST_INTERSECTS(c.wkb_geometry, i.geom)
            WHERE c.ref = '{self.circo}'
            AND i.date_mise_en_service != '{annee}') as t;"""
        utilitaires.db_exec(self.db.mogrify(sql))
        self.data.mobilites.nb_bornes_recharges_circo = self.db.fetchone()[0]

    def get_carburants(self):
        "To obtain gas station inside the constituency"
        sql = f"""SELECT COUNT(t.*) FROM (
            SELECT * FROM carburants_2024 AS i
            LEFT JOIN zone_circo AS c
            ON ST_INTERSECTS(c.wkb_geometry, i.geom)
            WHERE c.ref = '{self.circo}') as t;"""
        utilitaires.db_exec(self.db.mogrify(sql))
        self.data.mobilites.nb_stations_carburants_circo = self.db.fetchone()[0]
        # Merge avec les communes
        # sql = f"""SELECT COUNT(t.*) FROM (
        #     SELECT * FROM carburants_2024 AS i
        #     WHERE i.cp in (%s)) as t;"""%in_communes
        # utilitaires.db_exec(
        #     self.db.mogrify(sql))
        # self.data.mobilites.nb_stations_carburants = self.db.fetchone()[0]

    def get_vehicules(self):
        in_communes = GetDataCirco(db=self.db, data=self.data).data.in_communes
        sql = (
            """SELECT t.energie, SUM(2010) as sum_2010, SUM(2011) as sum_2011, SUM(2012) as sum_2012, SUM(2013) as sum_2013,
        SUM(2014) as sum_2014, SUM(2015) as sum_2015, SUM(2016) as sum_2016, SUM(2017) as sum_2017, SUM(2018) as sum_2018, SUM(2019) as sum_2019,
        SUM(2020) as sum_2020, SUM(2021) as sum_2021, SUM(2022) as sum_2022, SUM(2023) as sum_2023 FROM (
            SELECT * FROM sdes_vehicules_2024 AS i
            WHERE i.code_commune in (%s)
            AND i.categorie = 'VP') as t GROUP BY 1;"""
            % in_communes
        )
        utilitaires.db_exec(self.db.mogrify(sql))
        self.data.mobilites.dict_types_energie_annee_circo = {
            row[0]: {
                "annee_2010": utilitaires.to_int(row[1]),
                "annee_2011": utilitaires.to_int(row[2]),
                "annee_2012": utilitaires.to_int(row[3]),
                "annee_2013": utilitaires.to_int(row[4]),
                "annee_2014": utilitaires.to_int(row[5]),
                "annee_2015": utilitaires.to_int(row[6]),
                "annee_2016": utilitaires.to_int(row[7]),
                "annee_2017": utilitaires.to_int(row[8]),
                "annee_2018": utilitaires.to_int(row[9]),
                "annee_2019": utilitaires.to_int(row[10]),
                "annee_2020": utilitaires.to_int(row[11]),
                "annee_2021": utilitaires.to_int(row[12]),
                "annee_2022": utilitaires.to_int(row[13]),
                "annee_2023": utilitaires.to_int(row[14]),
            }
            for row in self.db.fetchall()
        }
        dict_type_energie_tot_circo = dict()
        for energie in self.data.mobilites.dict_types_energie_annee_circo.keys():
            dict_type_energie_tot_circo[energie] = sum(
                self.data.mobilites.dict_types_energie_annee_circo[energie].values()
            )
        self.dict_type_energie_tot_circo = dict_type_energie_tot_circo
