from dataclasses import dataclass

from datacirco import utilitaires


@dataclass()
class DataElections:
    """Classe représentant une donnée de type élections."""

    elus = None
    sources = None


class GetElections:
    """Classe permettant d'extraire d'une base de données des informations sur
    les élections."""

    def __init__(self, db, data):
        """
        Args:
            db: Connection to the database
            data: A DataCirco object
        """
        self.db = db
        utilitaires.db = db
        data.elections = DataElections()
        self.data = data
        self.dept = data.dept
        self.data.elections.sources = utilitaires.get_sources("elus")

    def get_elus(self):
        """Calcule les parités des élus dans un département donné."""
        if self.data.dept.startswith("0"):
            sans_zero = f"or rne.code_du_departement='{self.data.dept[-1]}'"
        else:
            sans_zero = ""
        utilitaires.db_exec(
            self.db.mogrify(
                f"""
                select d.*, n.femmes_nat, n.total_nat from
                (select elu, ordre,
                    sum(case when code_sexe='F' then 1 else 0 end) as femmes,
                    count(*) as total
                from rne
                where rne.code_du_departement='{self.data.dept}' {sans_zero}
                group by 1, 2) as d
                join
                (select elu, ordre,
                    sum(case when code_sexe='F' then 1 else 0 end) as femmes_nat,
                    count(*) as total_nat
                from rne
                group by 1, 2) as n ON (n.elu=d.elu)
                order by d.ordre desc;
                """
            ),
            "parite_elus",
        )
        elus = [
            {
                "organisation": elu[0],
                "ordre": elu[1],
                "femmes_dep": elu[2],
                "total_dep": elu[3],
                "femmes_nat": elu[-2],
                "total_nat": elu[-1],
            }
            for elu in utilitaires.db.fetchall()
        ]
        self.data.elections.elus = elus
