from dataclasses import dataclass
from datacirco.connexion_db import db
from datacirco import utilitaires
import json


@dataclass()
class DataEnvironnement:
    dictionnaire_sites_pollues = None
    nb_sites_pollues_circo = None
    nb_sites_pollues_dep = None
    nb_sites_pollues_nat = None
    loc_occup_sols = None
    nb_occup_sols = None
    tableau_artificialisation = None
    tableau_artificialisation_pop = None
    serie_tmp_artif = None


class GetEnvironnement:
    """To retreive data from the database."""

    def __init__(self, db, data):
        """
        Args:
            db: Connection to the database
            data: A DataCirco object
        """
        self.db = db
        utilitaires.db = db
        data.environnement = DataEnvironnement()
        self.data = data
        self.dept = data.dept
        self.circo = data.circo
        self.data.environnement.sources = utilitaires.get_sources("environnement")

    def get_occupation_sols(self):
        # Données cartographiques
        # On supprime les données trop lourde pour l'export :(
        # utilitaires.db_exec(
        #     self.db.mogrify(
        #         f"""
        # SELECT t.libelle_fr, t.couleur, st_asgeojson(t.wkb_geometry)
        # FROM sdes_occupation_sols_2012 as t
        # LEFT JOIN zone_circo c
        # ON st_intersects(t.wkb_geometry, c.wkb_geometry)
        # WHERE c.ref = '{self.circo}';"""
        #     )
        # )
        self.data.environnement.loc_occup_sols = [
            # {
            #     "code_occupation": r[0],
            #     "couleur": r[1],
            #     "geojson": dict(
            #         type="Feature", properties=dict(), geometry=json.loads(r[2])
            #     ),
            # }
            # for r in self.db.fetchall()
        ]
        """
        SELECT t.code_12 as code_occupation, t.libelle_fr AS nom_occupation, SUM(st_area(st_intersection(c.wkb_geometry,t.wkb_geometry)::geography))/10000 as aire_oc,
            SUM(st_area(t.wkb_geometry::geography))/10000 as aire_oc_full, SUM(t.area_ha) as sum_area_ha
                FROM sdes_occupation_sols_2012 as t
                LEFT JOIN zone_circo c ON st_intersects(c.wkb_geometry, t.wkb_geometry)
                WHERE c.ref = '976-01'
                GROUP BY 1, 2
                ORDER BY sum_area_ha;
        code_occupation |                                    nom_occupation                                    |      aire_oc       |    aire_oc_full    |    sum_area_ha
        -----------------+--------------------------------------------------------------------------------------+--------------------+--------------------+--------------------
        5230            | Mers et océans                                                                       | 45.603845774092775 | 166600.36259239021 | 166468.75891900000

        => Il faut découper les zones d'après la frontière de la circo avant de calculer la surface
        """
        # Données pour le tableau au niveau des circonscriptions
        utilitaires.db_exec(
            self.db.mogrify(
                f"""
            WITH 
            total_surface_circo AS (
                SELECT SUM(st_area(st_intersection(c.wkb_geometry, tp.wkb_geometry)::geography))/10000 as surface_totale_circo_ha
                FROM sdes_occupation_sols_2012_metro_drom as tp
                LEFT JOIN zone_circo c ON st_intersects(c.wkb_geometry, tp.wkb_geometry)
                WHERE c.ref = '{self.circo}'
            ),
            occupation_surfaces_circo AS (
                SELECT 
                    t.code_12 as code_occupation, 
                    MIN(t.libelle_fr) AS nom_occupation,
                    SUM(st_area(st_intersection(c.wkb_geometry,t.wkb_geometry)::geography))/10000 as aire_oc_ha
                FROM sdes_occupation_sols_2012_metro_drom as t
                LEFT JOIN zone_circo c ON st_intersects(c.wkb_geometry, t.wkb_geometry)
                WHERE c.ref = '{self.circo}'
                GROUP BY 1
            )
            SELECT 
                oc.code_occupation as code_occupation,
                clc.nom_occupation as nom_occupation, 
                tc.surface_totale_circo_ha as surface_circo,
                oc.aire_oc_ha as aire_oc_circo_ha,
                oc.aire_oc_ha / tc.surface_totale_circo_ha as part_superficie_circo,
                agg.surface_totale_france_ha,
                agg.aire_oc_ha_france,
                agg.part_superficie_france
            FROM occupation_surfaces_circo oc
            CROSS JOIN total_surface_circo tc
            JOIN sdes_occupation_sols_2012_metro_drom_aggregated agg ON agg.code_occupation = oc.code_occupation
            JOIN sdes_occupation_sols_2012_nomenclature clc ON clc.code_occupation = agg.code_occupation
            ORDER BY clc.nom_occupation;
                """
            )
        )
        columns = [col[0] for col in utilitaires.db.description]
        self.data.environnement.nb_occup_sols = [
            dict(zip(columns, row)) for row in self.db.fetchall()
        ]
        # self.data.environnement.nb_occup_sols = [
        #     {
        #         "code_occupation": r[0],
        #         "nb_ha_circo": int(r[1]),
        #         "part_superficie_circo": float(r[2]),
        #         "part_superficie_france": float(r[3]),
        #     }
        #     for r in self.db.fetchall()
        # ]

    def get_sites_pollues(self):
        utilitaires.db_exec(
            db.mogrify(
                f"""
                SELECT t.nom_instruction, t.statut_instruction, t.description, t.nom_commune,
                t.action, t.milieu, st_asgeojson(t.geom)
                FROM sites_pollues AS t
                JOIN zone_circo AS c 
                ON st_intersects(c.wkb_geometry, t.geom)
                WHERE c.ref = '{self.circo}'
                """
            )
        )
        self.data.environnement.dictionnaire_sites_pollues = [
            {
                "nom": r[0],
                "statut": r[1],
                "description": r[2],
                "nom_commune": r[3],
                "action": r[4],
                "milieu": r[5],
                "geojson": dict(
                    type="Feature", properties=dict(), geometry=json.loads(r[6])
                ),
            }
            for r in self.db.fetchall()
        ]

        # Pour compter les sites pollués de la circonscription, du département :
        utilitaires.db_exec(
            db.mogrify(
                f"""
                SELECT COUNT(*) FROM sites_pollues AS t
                JOIN zone_circo AS c 
                ON st_intersects(c.wkb_geometry, t.geom)
                WHERE c.ref = '{self.circo}';
                """
            )
        )
        self.data.environnement.nb_sites_pollues_circo = db.fetchone()[0]

        utilitaires.db_exec(
            db.mogrify(
                f"""
                SELECT COUNT(*) FROM sites_pollues AS t
                JOIN ign_departement AS dep 
                ON st_intersects(dep.wkb_geometry, t.geom)
                WHERE dep.insee_dep = '{self.dept}';
                """
            )
        )
        self.data.environnement.nb_sites_pollues_dep = db.fetchone()[0]

        utilitaires.db_exec(
            db.mogrify(
                """
                SELECT COUNT(*) FROM sites_pollues;
                """
            )
        )
        self.data.environnement.nb_sites_pollues_nat = db.fetchone()[0]

    def get_artificialisation(self, in_communes):
        if in_communes.startswith("'75"):
            in_communes = "'75056'"
        # Données sur la circonscription
        utilitaires.db_exec(
            self.db.mogrify(
                f"""SELECT nb_mixte, nb_routiere, nb_ferroviaire, nb_inconnue, nb_activite, nb_habitat, art_totale
                        FROM (
                            SELECT SUM(art11mix12 + art12mix13 + art13mix14 + art14mix15 + art15mix16 + art16mix17 + art17mix18 + art18mix19 + art19mix20 + art20mix21) as nb_mixte,
                        SUM(art11rou12 + art12rou13 + art13rou14 + art14rou15 + art15rou16 + art16rou17 + art17rou18 + art18rou19 + art19rou20 + art20rou21) as nb_routiere,
                        SUM(art11fer12 + art12fer13 + art13fer14 + art14fer15 + art15fer16 + art16fer17 + art17fer18 + art18fer19 + art19fer20 + art20fer21) as nb_ferroviaire,
                        SUM( art11inc12 + art12inc13 + art13inc14 + art14inc15 + art15inc16 + art16inc17 + art17inc18 + art18inc19 + art19inc20 + art20inc21) as nb_inconnue,
                        SUM(art11act12 + art12act13 + art13act14 + art14act15 + art15act16 + art16act17 + art17act18 + art18act19 + art19act20 + art20act21) as nb_activite,
                        SUM(art11hab12 + art12hab13 + art13hab14 + art14hab15 + art15hab16 + art16hab17 + art17hab18 + art18hab19 + art19hab20 + art20hab21) as nb_habitat,
                        SUM(naf11art12 + naf12art13 + naf13art14 + naf14art15 + naf15art16 + naf16art17 + naf17art18 + naf18art19 + naf19art20 + naf20art21) as art_totale
                            FROM cerema_artificalisation_2023
                            WHERE idcom in ({in_communes})
                        ) as t; """
            )
        )
        nom_colonnes = [desc[0] for desc in self.db.description]
        resultat = self.db.fetchone()
        valeurs_circonscription = dict(
            zip(nom_colonnes, [float(i if i is not None else 0) for i in resultat])
        )

        # Données artificalisation/population sur le département
        utilitaires.db_exec(
            self.db.mogrify(
                f"""SELECT nb_mixte, nb_routiere, nb_ferroviaire, nb_inconnue, nb_activite, nb_habitat, art_totale
                        FROM (
                            SELECT SUM(art11mix12 + art12mix13 + art13mix14 + art14mix15 + art15mix16 + art16mix17 + art17mix18 + art18mix19 + art19mix20 + art20mix21) as nb_mixte,
                        SUM(art11rou12 + art12rou13 + art13rou14 + art14rou15 + art15rou16 + art16rou17 + art17rou18 + art18rou19 + art19rou20 + art20rou21) as nb_routiere,
                        SUM(art11fer12 + art12fer13 + art13fer14 + art14fer15 + art15fer16 + art16fer17 + art17fer18 + art18fer19 + art19fer20 + art20fer21) as nb_ferroviaire,
                        SUM( art11inc12 + art12inc13 + art13inc14 + art14inc15 + art15inc16 + art16inc17 + art17inc18 + art18inc19 + art19inc20 + art20inc21) as nb_inconnue,
                        SUM(art11act12 + art12act13 + art13act14 + art14act15 + art15act16 + art16act17 + art17act18 + art18act19 + art19act20 + art20act21) as nb_activite,
                        SUM(art11hab12 + art12hab13 + art13hab14 + art14hab15 + art15hab16 + art16hab17 + art17hab18 + art18hab19 + art19hab20 + art20hab21) as nb_habitat,
                        SUM(naf11art12 + naf12art13 + naf13art14 + naf14art15 + naf15art16 + naf16art17 + naf17art18 + naf18art19 + naf19art20 + naf20art21) as art_totale
                            FROM cerema_artificalisation_2023
                            WHERE iddep = '{self.dept}'
                        ) as t; """
            )
        )
        nom_colonnes = [desc[0] for desc in self.db.description]
        resultat = self.db.fetchone()
        valeurs_departement = dict(
            zip(nom_colonnes, [float(i if i is not None else 0) for i in resultat])
        )

        # Données artificalisation/population sur la France entière
        utilitaires.db_exec(
            self.db.mogrify(
                """SELECT nb_mixte, nb_routiere, nb_ferroviaire, nb_inconnue, nb_activite, nb_habitat, art_totale
                        FROM (
                            SELECT SUM(art11mix12 + art12mix13 + art13mix14 + art14mix15 + art15mix16 + art16mix17 + art17mix18 + art18mix19 + art19mix20 + art20mix21) as nb_mixte,
                        SUM(art11rou12 + art12rou13 + art13rou14 + art14rou15 + art15rou16 + art16rou17 + art17rou18 + art18rou19 + art19rou20 + art20rou21) as nb_routiere,
                        SUM(art11fer12 + art12fer13 + art13fer14 + art14fer15 + art15fer16 + art16fer17 + art17fer18 + art18fer19 + art19fer20 + art20fer21) as nb_ferroviaire,
                        SUM( art11inc12 + art12inc13 + art13inc14 + art14inc15 + art15inc16 + art16inc17 + art17inc18 + art18inc19 + art19inc20 + art20inc21) as nb_inconnue,
                        SUM(art11act12 + art12act13 + art13act14 + art14act15 + art15act16 + art16act17 + art17act18 + art18act19 + art19act20 + art20act21) as nb_activite,
                        SUM(art11hab12 + art12hab13 + art13hab14 + art14hab15 + art15hab16 + art16hab17 + art17hab18 + art18hab19 + art19hab20 + art20hab21) as nb_habitat,
                        SUM(naf11art12 + naf12art13 + naf13art14 + naf14art15 + naf15art16 + naf16art17 + naf17art18 + naf18art19 + naf19art20 + naf20art21) as art_totale
                            FROM cerema_artificalisation_2023
                        ) as t; """
            )
        )
        nom_colonnes = [desc[0] for desc in self.db.description]
        resultat = self.db.fetchone()
        valeurs_national = dict(
            zip(nom_colonnes, [float(i if i is not None else 0) for i in resultat])
        )

        self.data.environnement.tableau_artificialisation = dict(
            circonscription=valeurs_circonscription,
            departement=valeurs_departement,
            national=valeurs_national,
        )

        # Données artificalisation/population sur la circo
        """
        Documentation : https://www.data.gouv.fr/fr/datasets/r/61d393d4-a24e-4236-9104-5602d6b2bd66

        Pour la France :
        nb_habitat1420 | naf14art20    | pop1420  | men1420   | emp1420  | artpop1420 | menhab1420 | mepart1420 | mepart1420_calcule | mepart1420_calcule_indiv
        ----------------+------------+---------+---------+---------+--------------------+-------------------+---------------------+---------------------+------
        818 808 683    | 1 306 518 482 | 125 4947 | 1 454 736 |  711 270 | 1 433 979.8 | 987 361.1 | -406 172.8 | 16.5               |  238 834

         1 306 518 482 m² d'artificialisation total, dont 818 808 683 m² pour l'habitat
         Donc 818 808 683 m² d'habitat pour 1 254 947 habitants, 1 454 736 ménages et 711 270 emplois supplémentaires
         
         Soit Nombre de m² artificialisés par habitant supplémentaire = 1 306 518 482 / 1 254 947 = 1 040 m²

        
        naf14art20 Flux entre NAF (Naturel, Agricole et Forestier) et artificialisé, sur la période 2014-2020 en m²
        nb_habitat1420 Flux NAF (Naturel, Agricole et Forestier) vers artificialisé destiné à l’habitat sur la période 2014-2020 en m²
        mepart1420 Nombre de ménages + emplois supplémentaire par ha artificialisé (période 2014-2020). Égal à (men1420 + emp1420) / (naf14art20/10000)
        menhab1420 Nombre de ménages par ha artificialisé à destination de l’habitat (période 2014-2020).
        artpop1420 Nombre de m² artificialisé par habitant supplémentaire (période 2014-2020).
        Égal à naf14art20 / pop1420
        pop1420 Variation de population entre 2014 et 2020
        men1420 Variation du nombre de ménages entre 2014 et 2020
        emp1420 Variation du nombre d’emplois entre 2014 et 2020
        artpop1420 Nombre de m² artificialisé par habitant supplémentaire (période 2014-2020).
        """
        debut_requete_artificialisation = """
        SELECT
            t.nb_habitat1420 as nb_habitat1420,
            t.naf14art20 as naf14art20,
            t.pop1420 as pop1420,
            t.men1420 as men1420,
            t.emp1420 as emp1420,
            t.artpop1420 as artpop1420,
            t.menhab1420 as menhab1420,
            t.mepart1420 as mepart1420,
            t.mepart1420_calcule as mepart1420_calcule
        FROM (
            SELECT 
                SUM(art14hab15 + art15hab16 + art16hab17 + art17hab18 + art18hab19 + art19hab20) as nb_habitat1420,
                SUM(naf14art15 + naf15art16 + naf16art17 + naf17art18 + naf18art19 + naf19art20) as naf14art20,
                SUM(pop1420) as pop1420,
                SUM(men1420) as men1420,
                SUM(emp1420) as emp1420,
                SUM(artpop1420) as artpop1420,
                SUM(menhab1420) as menhab1420,
                SUM(mepart1420) as mepart1420,
                SUM(
                    (men1420 + emp1420) / 
                    NULLIF(
                        ((naf14art15 + naf15art16 + naf16art17 + naf17art18 + naf18art19 + naf19art20)/10000), 0
                    )
                ) as mepart1420_calcule
            FROM cerema_artificalisation_2023
                        """
        utilitaires.db_exec(
            self.db.mogrify(
                f"""{debut_requete_artificialisation}
                        WHERE idcom in ({in_communes})
                    ) as t; """
            )
        )
        nom_colonnes = [desc[0] for desc in self.db.description]
        resultat = self.db.fetchone()
        valeurs_circonscription = dict(
            zip(nom_colonnes, [float(i if i is not None else 0) for i in resultat])
        )

        # Données sur le département
        utilitaires.db_exec(
            self.db.mogrify(
                f"""{debut_requete_artificialisation}
                            WHERE iddep = '{self.dept}'
                        ) as t; """
            )
        )
        nom_colonnes = [desc[0] for desc in self.db.description]
        resultat = self.db.fetchone()
        valeurs_departement = dict(
            zip(nom_colonnes, [float(i if i is not None else 0) for i in resultat])
        )

        # Données sur la France entière
        utilitaires.db_exec(
            self.db.mogrify(
                f"""{debut_requete_artificialisation}
                        ) as t; """
            )
        )
        nom_colonnes = [desc[0] for desc in self.db.description]
        resultat = self.db.fetchone()
        valeurs_national = dict(
            zip(nom_colonnes, [float(i if i is not None else 0) for i in resultat])
        )

        self.data.environnement.tableau_artificialisation_pop = dict(
            circonscription=valeurs_circonscription,
            departement=valeurs_departement,
            national=valeurs_national,
        )

    def get_valeurs_graphiques_artif(self, in_communes):
        if in_communes.startswith("'75"):
            in_communes = "'75056'"
        serie_tmp_artif = dict()
        for usage in ["mix", "rou", "fer", "inc", "act", "hab", "mix"]:
            chaine = ""
            for date in range(10, 23, 1):
                date_deb = str(date).rjust(2, "0")
                date_fin = str(date + 1).rjust(2, "0")
                chaine_usage = "art" + date_deb + usage + date_fin
                chaine = chaine + ", " + chaine_usage
            chaine = chaine + ","
            sql = f"""SELECT {chaine[1:-1]} FROM cerema_artificalisation_2023 WHERE idcom in ({in_communes});"""
            utilitaires.db_exec(self.db.mogrify(sql))
            valeurs = self.db.fetchone()
            serie_tmp_artif[usage] = valeurs
        chaine = ""
        for date in range(10, 23, 1):
            date_deb = str(date).rjust(2, "0")
            date_fin = str(date + 1).rjust(2, "0")
            chaine_usage = "naf" + date_deb + "art" + date_fin
            chaine = chaine + ", " + chaine_usage
            sql = f"""SELECT {chaine[1:]} FROM cerema_artificalisation_2023 WHERE idcom in ({in_communes});"""
            utilitaires.db_exec(self.db.mogrify(sql))
            valeurs = self.db.fetchone()
            serie_tmp_artif["total"] = valeurs
        self.data.environnement.serie_tmp_artif = serie_tmp_artif
