from dataclasses import dataclass
from datacirco import utilitaires
from datetime import datetime
from sqlalchemy import text
import pandas as pd
import numpy as np


def comparaison_normale(df, annee_debut: int = 1991, annee_fin: int = 2020):
    """Réalise le calcul de la différence entre la valeur d'une année par
    rapport à la période 1991_2020."""
    # selection des années précédant 2020 et succédant à 1991
    df_norm = df.loc[
        (df["annee"] < datetime(annee_fin, 12, 31))
        & (datetime(annee_debut, 1, 1) < df["annee"])
    ]
    # différence de la température et des précipitations aux moyennes de ces années
    df[f"temperature_delta_{annee_debut}_{annee_fin}"] = (
        df.temperature - df_norm.temperature.mean()
    )
    df[f"precipitation_delta_{annee_debut}_{annee_fin}"] = (
        df.precipitation - df_norm.precipitation.mean()
    )
    return df


def fusion_echelle(df_circo, df_france, df_europe, df_monde, var_merge: str = "annee"):
    """Fusionne les dataframe de la France, Europe et monde sur la variable
    d'année."""
    # On choisit de préciser une variable merge au cas où l'on voudrait écrire
    # Une fonction plus générale. ici àa ne vaut que pour les années.
    df = df_france.merge(
        df_circo, on=[var_merge], how="inner", suffixes=("_fr", "_circo")
    )
    if var_merge == "annee":
        df = df.merge(df_europe, on=[var_merge], how="inner")
        df.rename(
            {
                "temperature": "temperature_europe",
                "precipitation": "precipitation_europe",
                "temperature_delta_1991_2020": "temperature_delta_1991_2020_europe",
                "precipitation_delta_1991_2020": "precipitation_delta_1991_2020_europe",
            },
            inplace=True,
            axis=1,
        )
        df = df.merge(df_monde, on=[var_merge], how="inner")
        df.rename(
            {
                "temperature": "temperature_monde",
                "precipitation": "precipitation_monde",
                "temperature_delta_1991_2020": "temperature_delta_1991_2020_monde",
                "precipitation_delta_1991_2020": "precipitation_delta_1991_2020_monde",
            },
            inplace=True,
            axis=1,
        )
        # Change order
        df = df[
            [
                "annee",
                "temperature_circo",
                "temperature_fr",
                "temperature_europe",
                "temperature_monde",
                "temperature_delta_1991_2020_circo",
                "temperature_delta_1991_2020_fr",
                "temperature_delta_1991_2020_europe",
                "temperature_delta_1991_2020_monde",
                "precipitation_circo",
                "precipitation_fr",
                "precipitation_europe",
                "precipitation_monde",
                "precipitation_delta_1991_2020_circo",
                "precipitation_delta_1991_2020_fr",
                "precipitation_delta_1991_2020_europe",
                "precipitation_delta_1991_2020_monde",
            ]
        ]
    return df


@dataclass()
class DataMeteo:
    """Classe représentant une donnée de type meteo."""

    historique: pd.DataFrame = None
    historique_precip_mens = None
    historique_temp_mens = None
    df_temperature = None
    df_precipitation = None
    # Les variable temp_2024_* servent à générer l'exemple du graphique su le site.
    temp_2024_circo: float = None
    temp_2024_fr: float = None
    temp_2024_monde: float = None
    precip_2024_circo: float = None
    precip_2024_fr: float = None
    sources = None


class GetMeteo:
    def __init__(self, db, data, engine):
        """
        Args:
            db: Connection to the database
            data: A DataCirco object
            engine: Objet basé sur une URL
        """
        # assert isinstance(data, DataCirco)
        self.engine = engine
        self.db = db
        data.meteo = DataMeteo()
        self.data = data
        self.current_year = datetime.today().year
        self.data.meteo.sources = utilitaires.get_sources("meteo")

    def get_mean_as_df(self, table, var_merge: str = "annee"):
        """Fonction qui calcule la moyenne des températures sur toutes les
        variables.

        Faire la somme des précipitations n'a pas de sens lorsqu'on
        aggrège par mois.
        """
        agg = "sum" if var_merge == "annee" else "avg"
        sql = f"""select
                m.{var_merge},
                avg(m.temperature) as temperature,
                {agg}(m.precipitation) as precipitation
            from {table} as m
            where annee != {self.current_year}
            group by 1
            order by 1;
            """
        with self.engine.connect() as con:
            df = pd.read_sql_query(text(sql), con)
        if var_merge == "annee":
            df["annee"] = pd.to_datetime(df["annee"], format="%Y")
            return comparaison_normale(df)
        if var_merge == "mois":
            df["mois"] = pd.to_datetime(df["mois"], format="%m")
            # pour ajouter l'écart de chaque année à la moyenne d'un intervalle
            return df

    def get_meteo(self):
        """Fonction permettant de récupérer les données de météo."""
        current_year = self.current_year
        circo = self.data.circo
        engine = self.engine
        # Monde
        df_monde = self.get_mean_as_df("meteo_era5_monde")
        df_europe = self.get_mean_as_df("meteo_era5_europe")
        df_france = self.get_mean_as_df("meteo_era5_france")

        # Circo
        # Il faut faire la moyenne des différents points de la circo
        # pour un même mois, puis en faire la somme pour l'année.
        sql = f"""SELECT monthly.annee, avg(monthly.temperature) as temperature, sum(monthly.precipitation) as precipitation
        FROM (select m.annee, m.mois,
                avg(m.temperature) as temperature,
                avg(m.precipitation) as precipitation
            from zone_circo c
            join meteo_era5_circo m on (st_intersects(c.wkb_geometry, m.geom))
                where ref = '{circo}'
                and annee != {current_year}
            group by 1,2) as monthly
            group by 1
            order by 1
            """
        with engine.connect() as con:
            df_circo = pd.read_sql_query(text(sql), con)
        if len(df_circo) < 1:
            # Si ne contient pas d'informations, on considère les stations météo qui seraient dans un périmètre plus large
            # C'est par exemple le cas pour la 5 ème circonscription de Paris 075-05
            sql = f"""SELECT monthly.annee, avg(monthly.temperature) as temperature, sum(monthly.precipitation) as precipitation
        FROM (select m.annee, m.mois,
                avg(m.temperature) as temperature,
                avg(m.precipitation) as precipitation
            from zone_circo c
                join meteo_era5_circo m on (ST_DWithin(c.wkb_geometry, m.geom, 0.2))
                where ref = '{circo}'
                and annee != {current_year}
            group by 1,2) as monthly
            group by 1
            order by 1
                """
            with engine.connect() as con:
                df_circo = pd.read_sql_query(text(sql), con)
        df_circo["annee"] = pd.to_datetime(df_circo["annee"], format="%Y")
        df_circo = comparaison_normale(df_circo)
        df = fusion_echelle(df_circo, df_france, df_europe, df_monde)
        self.data.meteo.historique = df

        if len(df) != 0:
            # L'année est en dur car c'est un exemple qui sert à la légende des graphique sur le web.
            self.data.meteo.temp_2024_circo = df[df.annee == "2024-01-01"][
                "temperature_delta_1991_2020_circo"
            ].iloc[0]
            self.data.meteo.temp_2024_fr = df[df.annee == "2024-01-01"][
                "temperature_delta_1991_2020_fr"
            ].iloc[0]
            self.data.meteo.temp_2024_monde = df[df.annee == "2024-01-01"][
                "temperature_delta_1991_2020_monde"
            ].iloc[0]
            self.data.meteo.precip_2024_circo = df[df.annee == "2024-01-01"][
                "precipitation_circo"
            ].iloc[0]
            self.data.meteo.precip_2024_fr = df[df.annee == "2024-01-01"][
                "precipitation_fr"
            ].iloc[0]
        else:
            self.data.meteo.historique = None

    def get_temperatures(self, df):
        self.data.meteo.df_temperature = df.copy()
        self.data.meteo.df_temperature["ecart_temp_fr_color"] = np.where(
            self.data.meteo.df_temperature["temperature_delta_1991_2020_circo"] < 0,
            "#8691FD",
            "#F29B92",
        )
        self.data.meteo.df_temperature["MA10_circo"] = (
            self.data.meteo.df_temperature["temperature_delta_1991_2020_circo"]
            .rolling(10)
            .mean()
        )
        self.data.meteo.df_temperature["MA10_fr"] = (
            self.data.meteo.df_temperature["temperature_delta_1991_2020_fr"]
            .rolling(10)
            .mean()
        )
        self.data.meteo.df_temperature["MA10_europe"] = (
            self.data.meteo.df_temperature["temperature_delta_1991_2020_europe"]
            .rolling(10)
            .mean()
        )
        self.data.meteo.df_temperature["MA10_monde"] = (
            self.data.meteo.df_temperature["temperature_delta_1991_2020_monde"]
            .rolling(10)
            .mean()
        )
        self.data.meteo.df_temperature = self.data.meteo.df_temperature.drop(
            columns=[
                "precipitation_circo",
                "precipitation_fr",
                "precipitation_europe",
                "precipitation_monde",
                "precipitation_delta_1991_2020_circo",
                "precipitation_delta_1991_2020_fr",
                "precipitation_delta_1991_2020_europe",
                "precipitation_delta_1991_2020_monde",
            ]
        )

    def get_precipitation_cumul(self, df):
        self.data.meteo.df_precipitation = df.copy()
        self.data.meteo.df_precipitation["MA10_circo"] = (
            self.data.meteo.df_precipitation["precipitation_circo"].rolling(10).mean()
        )
        self.data.meteo.df_precipitation["MA10_fr"] = (
            self.data.meteo.df_precipitation["precipitation_fr"].rolling(10).mean()
        )
        self.data.meteo.df_precipitation["MA10_europe"] = (
            self.data.meteo.df_precipitation["precipitation_europe"].rolling(10).mean()
        )
        self.data.meteo.df_precipitation["MA10_monde"] = (
            self.data.meteo.df_precipitation["precipitation_monde"].rolling(10).mean()
        )
        self.data.meteo.df_precipitation = self.data.meteo.df_precipitation.drop(
            columns=[
                "temperature_circo",
                "temperature_fr",
                "temperature_europe",
                "temperature_monde",
                "temperature_delta_1991_2020_circo",
                "temperature_delta_1991_2020_fr",
                "temperature_delta_1991_2020_europe",
                "temperature_delta_1991_2020_monde",
            ]
        )

    def get_climat_mens_avg_1991_2020(self):
        """Récupère les données de température et de précipitation moyennes
        mensuelles, sur la période 1991_2020."""
        circo = self.data.circo
        engine = self.engine
        sql = f"""SELECT monthly.mois, avg(monthly.temperature) as temperature_circo_avg_1991_2020, avg(monthly.precipitation) as precipitation_circo_avg_1991_2020
        FROM (select m.annee, m.mois,
                avg(m.temperature) as temperature,
                avg(m.precipitation) as precipitation
            from zone_circo c
            join meteo_era5_circo m on (st_intersects(c.wkb_geometry, m.geom))
                where ref = '{circo}'
                and annee between 1991 and 2020
            group by 1,2) as monthly
            group by 1
            order by 1;
            """
        with engine.connect() as con:
            df_circo_avg = pd.read_sql_query(text(sql), con)
        # Si ne contient pas d'informations, on considère les stations météo qui seraient dans un périmètre plus large
        if len(df_circo_avg) < 1:
            sql = f"""SELECT monthly.mois, avg(monthly.temperature) as temperature_circo_avg_1991_2020, avg(monthly.precipitation) as precipitation_circo_avg_1991_2020
        FROM (select m.annee, m.mois,
                avg(m.temperature) as temperature,
                avg(m.precipitation) as precipitation
            from zone_circo c
                join meteo_era5_circo m on (ST_DWithin(c.wkb_geometry, m.geom, 0.20))
                where ref = '{circo}'
                and annee between 1991 and 2020
            group by 1,2) as monthly
            group by 1
            order by 1;
                """
            with engine.connect() as con:
                df_circo_avg = pd.read_sql_query(text(sql), con)
        return df_circo_avg

    def get_climat_mens_avg(self):
        """Récupère les données de température et de précipitation moyennes
        mensuelles, sur toutes la durée des données."""
        circo = self.data.circo
        engine = self.engine
        sql = f"""SELECT monthly.mois, avg(monthly.temperature) as temperature_circo, avg(monthly.precipitation) as precipitation_circo
        FROM (select m.annee, m.mois,
                avg(m.temperature) as temperature,
                avg(m.precipitation) as precipitation
            from zone_circo c
            join meteo_era5_circo m on (st_intersects(c.wkb_geometry, m.geom))
                where ref = '{circo}'
            group by 1,2) as monthly
            group by 1
            order by 1;
            """
        with engine.connect() as con:
            df_circo_avg = pd.read_sql_query(text(sql), con)
        # Si ne contient pas d'informations, on considère les stations météo qui seraient dans un périmètre plus large
        if len(df_circo_avg) < 1:
            sql = f"""SELECT monthly.mois, avg(monthly.temperature) as temperature_circo, avg(monthly.precipitation) as precipitation_circo
        FROM (select m.annee, m.mois,
                avg(m.temperature) as temperature,
                avg(m.precipitation) as precipitation
            from zone_circo c
                join meteo_era5_circo m on (ST_DWithin(c.wkb_geometry, m.geom, 0.20))
                where ref = '{circo}'
            group by 1,2) as monthly
            group by 1
            order by 1;
                """
            with engine.connect() as con:
                df_circo_avg = pd.read_sql_query(text(sql), con)
        utilitaires.trace("get_climat_mens_avg:\n%s" % sql)
        return df_circo_avg

    def get_circo_temp(self):
        circo = self.data.circo
        engine = self.engine
        colonnes = "mois TEXT, " + ", ".join(
            [f'"{year}" FLOAT' for year in range(1960, self.current_year)]
        )
        sql = f"""SELECT *
        FROM crosstab(
            $$SELECT meteo.mois, meteo.annee, avg(meteo.temperature) FROM (
                SELECT m.* FROM meteo_era5_circo m
                JOIN zone_circo c on (st_intersects(c.wkb_geometry, m.geom))
                where c.ref = '{circo}') as meteo
            GROUP BY 1,2
            ORDER BY 1,2$$,
            $$SELECT generate_series(1960,{self.current_year-1})$$)
            AS final_result({colonnes}); """
        with engine.connect() as con:
            df_circo_temp = pd.read_sql_query(text(sql), con)
        # Si taille réduite
        if len(df_circo_temp) < 1:
            sql = f"""SELECT *
            FROM crosstab(
                $$SELECT meteo.mois, meteo.annee, avg(meteo.temperature) as temperature FROM (
                    SELECT m.* FROM meteo_era5_circo m
                    JOIN zone_circo c on (ST_DWithin(c.wkb_geometry, m.geom, 0.20))
                    where c.ref = '{circo}') as meteo
                GROUP BY 1,2
                ORDER BY 1,2$$,
                $$SELECT generate_series(1960,{self.current_year-1})$$)
                AS final_result({colonnes}); """
            with engine.connect() as con:
                df_circo_temp = pd.read_sql_query(text(sql), con)
        return df_circo_temp

    def get_circo_precip(self):
        circo = self.data.circo
        engine = self.engine
        colonnes = "mois TEXT, " + ", ".join(
            [f'"{year}" FLOAT' for year in range(1960, self.current_year)]
        )
        sql = f"""SELECT *
        FROM crosstab(
            $$SELECT meteo.mois, meteo.annee, avg(meteo.precipitation) as precipitation FROM (
                SELECT m.* FROM meteo_era5_circo m
                JOIN zone_circo c on (st_intersects(c.wkb_geometry, m.geom))
                where c.ref = '{circo}') as meteo
            GROUP BY 1, 2
            ORDER BY 1,2$$,
            $$SELECT generate_series(1960,{self.current_year-1})$$)
            AS final_result({colonnes}); """
        with engine.connect() as con:
            df_circo_precip = pd.read_sql_query(text(sql), con)
        # Si taille réduite
        if len(df_circo_precip) < 1:
            sql = f"""SELECT *
            FROM crosstab(
                $$SELECT meteo.mois, meteo.annee, avg(meteo.precipitation) as precipitation FROM (
                    SELECT m.* FROM meteo_era5_circo m
                    JOIN zone_circo c on (ST_DWithin(c.wkb_geometry, m.geom, 0.20))
                    where c.ref = '{circo}') as meteo
                GROUP BY 1,2
                ORDER BY 1,2$$,
                $$SELECT generate_series(1960,{self.current_year-1})$$)
                AS final_result({colonnes}); """
            with engine.connect() as con:
                df_circo_precip = pd.read_sql_query(text(sql), con)
        return df_circo_precip

    def get_meteo_mens(self):
        """Fonction permettant de récupérer les données de météo."""
        # On aggrège les données aux différentes échelles
        sql = """select
                m.mois,
                avg(m.temperature) as temperature_fr,
                avg(m.precipitation) as precipitation_fr
            from meteo_era5_france as m
            where annee != 2025
            group by 1
            order by 1;
            """
        with self.engine.connect() as con:
            df_france = pd.read_sql_query(text(sql), con)
        df_france["mois"] = pd.to_datetime(df_france["mois"], format="%m")

        # On s'intéresse aux données moyennes sur la circonscription entre 1960 et 2023
        df_circo_avg = self.get_climat_mens_avg()
        df_circo_avg["mois"] = pd.to_datetime(df_circo_avg["mois"], format="%m")
        df_avg = df_circo_avg.merge(df_france, on="mois")
        df_avg["mois"] = df_avg["mois"].apply(lambda x: x.strftime("%m"))
        df_avg["delta_circo_france_temperature"] = (
            df_avg["temperature_circo"] - df_avg["temperature_fr"]
        )
        df_avg["delta_circo_france_precipitation"] = (
            df_avg["precipitation_circo"] - df_avg["precipitation_fr"]
        )
        df_avg.sort_values(by=["mois"], inplace=True)

        # Moyenne mensuelle sur la période 1991_2020
        df_circo_avg_1991_2020 = self.get_climat_mens_avg_1991_2020()
        df_circo_avg_1991_2020["mois"] = pd.to_datetime(
            df_circo_avg_1991_2020["mois"], format="%m"
        )
        df_circo_avg_1991_2020["mois"] = df_circo_avg_1991_2020["mois"].apply(
            lambda x: x.strftime("%m")
        )

        # On importe les données de circonscirption année par année POUR LA TEMPERATURE
        df_circo_temp = self.get_circo_temp()
        df_circo_temp["mois"] = pd.to_datetime(df_circo_temp["mois"], format="%m")
        df_circo_temp["mois"] = df_circo_temp["mois"].apply(lambda x: x.strftime("%m"))
        dict_circo_temp = (
            df_circo_temp.sort_values(by=["mois"])
            .drop(columns=["mois"])
            .to_dict(orient="list")
        )
        dict_temp_mens = dict(
            temp_mois_annuelle=dict_circo_temp,
            avg_temperature_circo=df_avg["temperature_circo"].to_list(),
            avg_temperature_circo_1991_2020=df_circo_avg_1991_2020[
                "temperature_circo_avg_1991_2020"
            ].to_list(),
            avg_temperature_fr=df_avg["temperature_fr"].to_list(),
            delta_circo_france_temperature=df_avg["delta_circo_france_temperature"],
        )
        self.data.historique_temp_mens = dict_temp_mens

        # On importe les données de circonscirption année par année POUR LES PRECIPITATIONS
        df_circo_precip = self.get_circo_precip()
        df_circo_precip["mois"] = pd.to_datetime(df_circo_precip["mois"], format="%m")
        df_circo_precip["mois"] = df_circo_precip["mois"].apply(
            lambda x: x.strftime("%m")
        )
        dict_circo_precip = dict_circo_precip = (
            df_circo_precip.sort_values(by=["mois"])
            .drop(columns=["mois"])
            .to_dict(orient="list")
        )
        dict_precip_mens = dict(
            precip_mois_annuelle=dict_circo_precip,
            avg_precipitation_circo=df_avg["precipitation_circo"].to_list(),
            avg_precipitation_circo_1991_2020=df_circo_avg_1991_2020[
                "precipitation_circo_avg_1991_2020"
            ].to_list(),
            avg_precipitation_fr=df_avg["precipitation_fr"].to_list(),
            delta_circo_france_precipitation=df_avg["delta_circo_france_precipitation"],
        )
        self.data.historique_precip_mens = dict_precip_mens
