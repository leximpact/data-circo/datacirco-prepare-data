from dataclasses import dataclass
from datacirco import utilitaires
from datacirco.modules_data.pop_data import DataPopulation
from datacirco.modules_data.meteo_data import DataMeteo
import pandas as pd
import numpy as np
import plotly.graph_objects as go
import statsmodels.api as sm


@dataclass()
class DataCirco:
    dept: str
    circo: str
    geo = None
    superficie_circo = None
    in_communes = None
    communes = None
    population: DataPopulation = None
    meteo: DataMeteo = None


class GetDataCirco:
    db = None
    data = None

    def __init__(self, db, data: DataCirco):
        self.db = db
        utilitaires.db = db
        self.data = data
        self.dept = data.dept
        self.circo = data.circo

        # Limites géographiques de la circo
        utilitaires.db_exec(
            db.mogrify(
                "SELECT ST_AsGeojson(wkb_geometry)::json FROM zone_circo WHERE ref= %s",
                (self.circo,),
            )
        )
        self.data.geo = db.fetchone()

        # superficie de la circonscription
        utilitaires.db_exec(
            db.mogrify(
                "select st_area(wkb_geometry::geography)/1000000 from zone_circo where ref = %s",
                (self.circo,),
            )
        )
        superficie_circo = db.fetchone()
        self.data.superficie_circo = superficie_circo[0]

        # superficie sauf Guyane
        utilitaires.db_exec(
            db.mogrify(
                "select sum(st_area(wkb_geometry::geography)::float)/1000000 from ign_region where insee_reg != '03';"
            )
        )
        superficie_nat = db.fetchone()
        self.data.superficie_nat = superficie_nat[0]

        # liste des communes couvertes par la circonscription
        db.execute(
            db.mogrify(
                """
            select
                insee_com,
                nom,
                round((st_area(st_intersection(co.wkb_geometry, ci.wkb_geometry)::geography) / st_area(co.wkb_geometry::geography))::numeric,2) as pct_surf
            from ign_commune co
            join zone_circo ci on (st_intersects(co.wkb_geometry, ci.wkb_geometry) and st_area(st_intersection(co.wkb_geometry, ci.wkb_geometry)::geography) / st_area(co.wkb_geometry::geography) > 0.1 )
            where ref = %s
            UNION
            select
                insee_arm,
                regexp_replace(nom,'[0-9].*$',''),
                round((st_area(st_intersection(co.wkb_geometry, ci.wkb_geometry)::geography) / st_area(co.wkb_geometry::geography))::numeric,2) as pct_surf
            from ign_arrondissement_municipal co
            join zone_circo ci on (st_intersects(co.wkb_geometry, ci.wkb_geometry) and st_area(st_intersection(co.wkb_geometry, ci.wkb_geometry)::geography) / st_area(co.wkb_geometry::geography) > 0.1 )
            where ref = %s
            order by 3 desc,2
        """,
                (self.circo, self.circo),
            )
        )
        self.data.communes = db.fetchall()
        in_communes = ""
        for com in self.data.communes:
            in_communes = in_communes + ("'%s'," % com[0])
        self.data.in_communes = in_communes[:-1]  # suppression virgule finale


def get_trendline(df: pd.DataFrame(), x: str, y: str, label: str) -> go.Scatter():
    """Draw trending line Trendline code from https://github.com/plotly/plotly.
    py/blob/7e1ee0ba29ebea5f947f81d5cddc1bdd86b08cb7/packages/python/plotly/plo
    tly/express/_core.py#L288.

    Args:
        df (_type_): _description_
    """

    sorted_trace_data = df.sort_values(by=[x])

    # Convert date to unix epoch seconds
    if sorted_trace_data[x].dtype.type == np.datetime64:
        sorted_trace_data["x_for_statsmodels"] = (
            sorted_trace_data[x].astype(int) / 10**9
        )
    else:
        sorted_trace_data["x_for_statsmodels"] = sorted_trace_data[x]

    trendline = sm.nonparametric.lowess(
        sorted_trace_data[y],
        sorted_trace_data["x_for_statsmodels"],
        missing="drop",
        is_sorted=True,
    )
    sm_y = f"y_for_{y}"
    sorted_trace_data[sm_y] = trendline[:, 1]
    trace = go.Scatter(
        x=sorted_trace_data[x],
        y=sorted_trace_data[sm_y],
        mode="lines",
        name=label,
    )
    return (
        trace,
        sorted_trace_data[sm_y].iloc[0],
        sorted_trace_data[sm_y].iloc[-1],
        df.loc[df[x] == "2022-01-01"][y].values[0],
    )
