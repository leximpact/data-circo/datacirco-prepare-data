from dataclasses import dataclass

from datacirco import utilitaires
from datacirco.modules_data.data import DataCirco
import json


@dataclass()
class DataRevenu:
    liste_carreaux = None
    dict_general = None
    dict_deciles = None
    dict_decomposition_revenu = None
    dict_tx_pauvrete_age = None


class GetRevenu:
    """To retreive data from the database."""

    def __init__(self, db, data: DataCirco):
        """
        Args:
            db: Connection to the database
            data: A DataCirco object
        """
        self.db = db
        utilitaires.db = db
        data.revenu = DataRevenu()
        self.data = data
        self.dept = data.dept
        self.circo = data.circo
        self.data.revenu.sources = utilitaires.get_sources("revenu")

    def get_carreaux_revenu(self):
        """A partir de la table insee_pop_2020, récupérer la valeur Men_pauv du
        nombre de ménages pauvres et en obtenir la proportion en la divisant
        par le nombre de ménages."""
        # On vérifie d'abord le rapport entre valeurs imputées et nombres de carreaux total de la circonscription
        utilitaires.db_exec(
            self.db.mogrify(
                f"""select count(*), sum(p.i_est_1km::numeric)
            from zone_circo c
                right join insee_population_carroyee_2019 p on (st_intersects(c.wkb_geometry, p.wkb_geometry))
            where c.ref='{self.circo}';"""
            )
        )
        rapport_imputation = self.db.fetchone()
        if (
            rapport_imputation[1]
            and rapport_imputation[0]
            and (rapport_imputation[1] / rapport_imputation[0] < 1 / 10)
        ):
            # Si moins de 10% des carreaux sont imputés on prend les carreaux de 200m, sinon ceux de 1km
            utilitaires.db_exec(
                self.db.mogrify(
                    f"""select st_asgeojson(st_Transform(p.wkb_geometry, 4326)) as geometry,
                    round(100*(p.men_pauv::numeric)/p.men::numeric,2)::numeric as pct_men_pauv,
                    round(p.ind_snv::numeric/p.ind::numeric)::numeric as niveau_vie_moyen
                    from zone_circo c
                        right join insee_population_carroyee_2019 p on (st_intersects(c.wkb_geometry, p.wkb_geometry))
                    where c.ref='{self.circo}';"""
                )
            )
        else:
            utilitaires.db_exec(
                self.db.mogrify(
                    f"""select st_asgeojson(st_Transform(p.wkb_geometry, 4326)) as geometry,
                round(100*(p.men_pauv::numeric)/p.men::numeric,2)::numeric as pct_men_pauv,
                ((p.ind_snv::numeric)/p.ind::numeric)::numeric as niveau_vie_moyen
                from zone_circo c
                    right join insee_population_carroyee_2019_1k p on (st_intersects(c.wkb_geometry, p.wkb_geometry))
                where c.ref='{self.circo}';"""
                )
            )

        self.data.revenu.liste_carreaux = [
            dict(
                geometrie=json.loads(ligne[0]),
                pourcentage_menage_pauvre=float(ligne[1]),
                niveau_vie_moyen_hab=int(ligne[2]),
            )
            for ligne in self.db.fetchall()
        ]

    def get_revenu_agg(self):
        # Maintenant on retourne le taux de pauvreté et le revenu moyen aggrégés sur la circonscription ainsi que leur évolution
        # C'est à l'échelle des 1km que l'on a le plus d'individus :

        # On aggrège d'abord au niveau des circonscriptions
        # utilitaires.db_exec(
        #     self.db.mogrify(
        #         f"""WITH somme_totale AS (
        #         SELECT SUM(st_area(st_intersection(circo2.wkb_geometry, car2.wkb_geometry)wkb_geometry))*car2.ind) as pond_revenu,
        #         SUM(car2.men*st_area(st_intersection(circo2.wkb_geometry, car2.wkb_geometry)wkb_geometry))) as pond_pauv FROM
        #         insee_population_carroyee_2015_1k as car2
        #         JOIN zone_circo circo2 ON st_intersects(circo2.wkb_geometry, car2.wkb_geometry)
        #         WHERE circo2.ref = '{self.circo}' AND car2.ind_snv IS NOT NULL)
        #     SELECT SUM(st_area(st_intersection(circo.wkb_geometry, car.wkb_geometry)wkb_geometry))*(car.ind_snv)/pond_revenu),
        #     SUM((st_area(st_intersection(circo.wkb_geometry, car.wkb_geometry)wkb_geometry))*(round(100*(car.men_pauv::numeric)/car.men::numeric,2))*car.men)/(pond_pauv))
        #         FROM somme_totale, insee_population_carroyee_2015_1k as car
        #         JOIN zone_circo circo ON st_intersects(circo.wkb_geometry, car.wkb_geometry)
        #         WHERE circo.ref = '{self.circo}' AND car.ind_snv IS NOT NULL;"""
        #     )
        # )

        # Valeurs de la circonscription et de la France pour le revenu médian, premier et dernier déciles et taux de pauvreté
        # dict_general = dict()
        utilitaires.db_exec(
            self.db.mogrify(
                f""" SELECT tx_pauvrete60_diff::numeric, d1_diff::numeric, nivvie_median_diff::numeric, d9_diff::numeric
                FROM insee_indic_stat_circo_2022 WHERE circo in ('{self.circo}', '000-00')
                ORDER BY CASE circo
                WHEN '{self.circo}' THEN 1
                WHEN '000-00' THEN 2
                END;"""
            )
        )
        val_circo_fr = self.db.fetchall()
        utilitaires.db_exec(
            self.db.mogrify(
                f"""SELECT tp6019, d119, med19, d919
                FROM insee_revenu_dep_2019
                WHERE codgeo = '{self.dept}';"""
            )
        )
        val_dep = self.db.fetchone()
        dict_general = dict(
            taux_pauvrete=dict(), decile_1=dict(), mediane=dict(), decile_9=dict()
        )
        index = 0
        for variable in dict_general.keys():
            if val_circo_fr[0][index] is None:
                break
            dict_general[variable]["circonscription"] = float(val_circo_fr[0][index])
            dict_general[variable]["france"] = float(val_circo_fr[1][index])
            dict_general[variable]["departement"] = float(val_dep[index])
            index = index + 1
        self.data.revenu.dict_general = dict_general

    def get_pauvrete_age(self):
        utilitaires.db_exec(
            self.db.mogrify(
                f""" SELECT tx_pauvrete60_diff_tragerf1::numeric, tx_pauvrete60_diff_tragerf2::numeric, tx_pauvrete60_diff_tragerf3::numeric,
                tx_pauvrete60_diff_tragerf4::numeric, tx_pauvrete60_diff_tragerf5::numeric, tx_pauvrete60_diff_tragerf6::numeric
                FROM insee_indic_stat_circo_2022 WHERE circo in ('{self.circo}', '000-00')
                ORDER BY CASE circo
                WHEN '{self.circo}' THEN 1
                WHEN '000-00' THEN 2
                END;"""
            )
        )
        val_circo_fr = list(self.db.fetchall())

        utilitaires.db_exec(
            self.db.mogrify(
                f"""SELECT tp60age119::numeric, tp60age219::numeric, tp60age319::numeric, tp60age419::numeric, tp60age519::numeric, tp60age619::numeric
                FROM insee_revenu_dep_2019
                WHERE codgeo = '{self.dept}';"""
            )
        )
        val_dep = list(self.db.fetchone())
        dict_tx_pauvrete_age = dict(
            moins_30ans=dict(),
            age30_39=dict(),
            age40_49=dict(),
            age_50_59=dict(),
            age_60_74=dict(),
            plus_75=dict(),
        )
        index = 0
        for variable in dict_tx_pauvrete_age.keys():
            if val_circo_fr[0][index] is None:
                break
            dict_tx_pauvrete_age[variable]["circonscription"] = float(
                val_circo_fr[0][index]
            )
            dict_tx_pauvrete_age[variable]["france"] = float(val_circo_fr[1][index])
            dict_tx_pauvrete_age[variable]["departement"] = float(val_dep[index])
            index = index + 1
        self.data.revenu.dict_tx_pauvrete_age = dict_tx_pauvrete_age

    def get_decomposition_revenu(self):
        utilitaires.db_exec(
            self.db.mogrify(
                f""" SELECT pact::numeric, ppen::numeric, ppat::numeric, ppsoc::numeric, pimpot::numeric
                FROM insee_indic_stat_circo_2022 WHERE circo in ('{self.circo}', '000-00')
                ORDER BY CASE circo
                WHEN '{self.circo}' THEN 1
                WHEN '000-00' THEN 2
                END;"""
            )
        )
        val_circo_fr = self.db.fetchall()
        utilitaires.db_exec(
            self.db.mogrify(
                f"""SELECT pact19::numeric, ppen19::numeric, ppat19::numeric, ppsoc19::numeric, pimpot19::numeric
                FROM insee_revenu_dep_2019
                WHERE codgeo = '{self.dept}';"""
            )
        )
        val_dep = self.db.fetchone()
        dict_decomposition_revenu = dict(
            p_revenus_activites=dict(),
            p_pensions_retraites_rentes=dict(),
            p_patrimoine=dict(),
            p_presta_soc=dict(),
            p_impots=dict(),
        )
        index = 0
        for variable in dict_decomposition_revenu.keys():
            if val_circo_fr[0][index] is None:
                break
            dict_decomposition_revenu[variable]["circonscription"] = float(
                val_circo_fr[0][index]
            )
            dict_decomposition_revenu[variable]["france"] = float(
                val_circo_fr[1][index]
            )
            dict_decomposition_revenu[variable]["departement"] = float(val_dep[index])
            index = index + 1
        self.data.revenu.dict_decomposition_revenu = dict_decomposition_revenu

    def get_dict_deciles(self):
        # On aggrège les données de revenu sur les circonscriptions
        utilitaires.db_exec(
            self.db.mogrify(
                f"""WITH somme_totale AS (
                            SELECT SUM(st_area(st_intersection(iris2.wkb_geometry, circo2.wkb_geometry)::geography)*popi2.p20_pop::numeric) as pond_revenu
                            FROM insee_pop_2020 as popi2
                            JOIN insee_revenu_pauvrete_2020 as reveni2 ON popi2.iris = reveni2.iris
                            JOIN iris_ge AS iris2 ON iris2.code_iris = popi2.iris
                            JOIN zone_circo AS circo2 ON st_intersects(iris2.wkb_geometry, circo2.wkb_geometry)
                            WHERE circo2.ref = '{self.circo}'
                            AND popi2.p20_pop IS NOT NULL
                            AND reveni2.DISP_D120 IS NOT NULL
                            AND reveni2.DISP_D120 NOT IN ('ns','nd'))    
            SELECT SUM(cast(t.DISP_D120 as float)*pop*surface/pond_revenu),
            SUM(cast(t.DISP_D220 as float)*t.pop*t.surface/pond_revenu),
            SUM(cast(t.DISP_D320 as float)*t.pop*t.surface/pond_revenu),
            SUM(cast(t.DISP_D420 as float)*t.pop*t.surface/pond_revenu),
            SUM(cast(t.DISP_MED20 as float)*t.pop*t.surface/pond_revenu),
            SUM(cast(t.DISP_D620 as float)*t.pop*t.surface/pond_revenu),
            SUM(cast(t.DISP_D720 as float)*t.pop*t.surface/pond_revenu),
            SUM(cast(t.DISP_D820 as float)*t.pop*t.surface/pond_revenu),
            SUM(cast(t.DISP_D920 as float)*t.pop*t.surface/pond_revenu)
                from somme_totale, (
                    SELECT reveni.*, st_area(st_intersection(iris.wkb_geometry, circo.wkb_geometry)::geography) as surface, popi.p20_pop::numeric as pop
                    FROM insee_pop_2020 as popi
                    JOIN insee_revenu_pauvrete_2020 as reveni ON popi.iris = reveni.iris
                    JOIN iris_ge AS iris ON iris.code_iris = popi.iris
                    JOIN zone_circo AS circo ON st_intersects(iris.wkb_geometry, circo.wkb_geometry)
                    WHERE circo.ref = '{self.circo}'
                    AND popi.p20_pop IS NOT NULL
                    AND reveni.DISP_D120 IS NOT NULL
                    AND reveni.DISP_D120 NOT IN ('ns','nd')) as t;"""
            )
        )
        dict_deciles = dict(liste_circo=list(self.db.fetchone()))

        # On remplace par les valeurs de l'INSEE en 2020
        # Source : https://www.insee.fr/fr/statistiques/2417897#tableau-figure1
        dict_deciles["liste_nationale"] = [
            9980,
            15070,
            18140,
            20700,
            23140,
            25800,
            28640,
            32550,
            38550,
        ]
        self.data.revenu.dict_deciles = dict_deciles
