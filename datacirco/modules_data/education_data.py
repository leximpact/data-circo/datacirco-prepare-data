from dataclasses import dataclass
import pandas as pd
from datacirco import utilitaires


@dataclass()
class DataEducation:
    """Class that contains the data for the education module."""

    etablissements_premier_et_second_degre = None
    # Établissements d'enseignement supérieur en France
    etablissements_superieur = None
    effectifs_premier_et_second_france = None
    effectifs_premier_et_second_prive_france = None
    effectifs_primaire_france = None
    effectifs_primaire_prive_france = None
    effectifs_college_france = None
    effectifs_college_prive_france = None
    effectifs_lycee_france = None
    effectifs_lycee_prive_france = None
    effectifs_superieur_france = None
    effectifs_superieur_prive_france = None
    ips_premier_et_second_public_france = None
    ips_premier_et_second_prive_france = None
    ips_premier_et_second_france = None
    ips_premier_et_second_france_max = None
    ips_premier_et_second_france_min = None
    ips_premier_et_second_prive_france_max = None
    ips_premier_et_second_prive_france_min = None
    ips_premier_et_second_public_france_max = None
    ips_premier_et_second_public_france_min = None


class GetEtablissements:
    """To retreive data from the database."""

    def __init__(self, db, data):
        """
        Args:
            db: Connection to the database
            data: A DataCirco object
        """
        self.db = db
        utilitaires.db = db
        data.education = DataEducation()
        self.data = data
        self.dept = data.dept
        self.circo = data.circo

    def get_effectifs_france(self):
        """Get the effectifs from the database.

        SELECT nature_uai_libe, count(*) FROM
        public.educ_etablissements_premier_et_second_degre     GROUP BY
        nature_uai_libe     ORDER BY 2 DESC; "ECOLE DE NIVEAU
        ELEMENTAIRE"   36382 "ECOLE MATERNELLE"      12856 "COLLEGE"
        7619 "LYCEE PROFESSIONNEL"   1729 "SECTION ENSEIGNT GEN. ET
        PROF. ADAPTE" 1480 "LYCEE ENSEIGNT GENERAL ET TECHNOLOGIQUE"
        1225 "LYCEE POLYVALENT"      1014 "SECTION D ENSEIGNEMENT
        PROFESSIONNEL"  749 "LYCEE D ENSEIGNEMENT GENERAL"  600 "MAISON
        FAMILIALE RURALE EDUCATION ORIENT"      375 "ECOLE ELEMENTAIRE D
        APPLICATION"       191 "SECTION ENSEIGT GENERAL ET
        TECHNOLOGIQUE"      111 "LYCEE D ENSEIGNEMENT TECHNOLOGIQUE" 104
        ...
        """
        self.data.education.effectifs_premier_et_second_france = (
            utilitaires.db_exec_get_first_value(
                "SELECT sum(effectifs) from educ_etablissements_details"
            )
        )
        self.data.education.effectifs_premier_et_second_prive_france = utilitaires.db_exec_get_first_value(
            "SELECT sum(effectifs) from educ_etablissements_details WHERE secteur_public_prive_libe != 'public'"
        )
        # Filter on ECOLE
        # École primaire  = maternelle et élémentaire
        self.data.education.effectifs_primaire_france = utilitaires.db_exec_get_first_value(
            "SELECT sum(effectifs) from educ_etablissements_details WHERE nature_uai_libe IN ('ECOLE DE NIVEAU ELEMENTAIRE', 'ECOLE MATERNELLE')"
        )

        self.data.education.effectifs_primaire_prive_france = utilitaires.db_exec_get_first_value(
            "SELECT sum(effectifs) from educ_etablissements_details WHERE nature_uai_libe IN ('ECOLE DE NIVEAU ELEMENTAIRE', 'ECOLE MATERNELLE') and secteur_public_prive_libe != 'public'"
        )

        # Filter on COLLEGE
        self.data.education.effectifs_college_france = utilitaires.db_exec_get_first_value(
            "SELECT sum(effectifs) from educ_etablissements_details WHERE nature_uai_libe LIKE 'COLLEGE%'"
        )
        self.data.education.effectifs_college_prive_france = utilitaires.db_exec_get_first_value(
            "SELECT sum(effectifs) from educ_etablissements_details WHERE nature_uai_libe LIKE 'COLLEGE%' and secteur_public_prive_libe != 'public'"
        )
        # Filter on LYCEE
        self.data.education.effectifs_lycee_france = utilitaires.db_exec_get_first_value(
            "SELECT sum(effectifs) from educ_etablissements_details WHERE nature_uai_libe LIKE 'LYCEE%'"
        )
        self.data.education.effectifs_lycee_prive_france = utilitaires.db_exec_get_first_value(
            "SELECT sum(effectifs) from educ_etablissements_details WHERE nature_uai_libe LIKE 'LYCEE%' and secteur_public_prive_libe != 'public'"
        )

        # Filter on SUPERIEUR
        self.data.education.effectifs_superieur_france = int(
            utilitaires.db_exec_get_first_value(
                "SELECT sum(effectifs) from educ_etablissements_superieur;"
            )
        )
        self.data.education.effectifs_superieur_prive_france = utilitaires.db_exec_get_first_value(
            "SELECT sum(effectifs) from educ_etablissements_superieur WHERE LOWER(secteur_public_prive_libe) != 'public'"
        )

        # Autres
        self.data.education.effectifs_autres_france = utilitaires.db_exec_get_first_value(
            "SELECT sum(effectifs) from educ_etablissements_details WHERE nature_uai_libe NOT IN ('ECOLE DE NIVEAU ELEMENTAIRE', 'ECOLE MATERNELLE') AND nature_uai_libe NOT LIKE 'COLLEGE%' AND nature_uai_libe NOT LIKE 'LYCEE%'"
        )
        self.data.education.effectifs_autres_prive_france = utilitaires.db_exec_get_first_value(
            """SELECT sum(effectifs) from educ_etablissements_details WHERE nature_uai_libe NOT IN ('ECOLE DE NIVEAU ELEMENTAIRE', 'ECOLE MATERNELLE') AND nature_uai_libe NOT LIKE 'COLLEGE%' AND nature_uai_libe NOT LIKE 'LYCEE%'
             AND lower(secteur_public_prive_libe) != 'public'"""
        )
        # IPS
        self.data.education.ips_premier_et_second_public_france = utilitaires.db_exec_get_first_value(
            "SELECT avg(ips) from educ_etablissements_details WHERE lower(secteur_public_prive_libe) = 'public' AND ips > 1"
        )
        self.data.education.ips_premier_et_second_prive_france = utilitaires.db_exec_get_first_value(
            "SELECT avg(ips) from educ_etablissements_details WHERE lower(secteur_public_prive_libe) != 'public' AND ips > 1"
        )
        self.data.education.ips_premier_et_second_france = (
            utilitaires.db_exec_get_first_value(
                "SELECT avg(ips) from educ_etablissements_details WHERE ips > 1"
            )
        )
        self.data.education.ips_premier_et_second_france_max = (
            utilitaires.db_exec_get_first_value(
                "SELECT max(ips) from educ_etablissements_details WHERE ips > 1"
            )
        )
        self.data.education.ips_premier_et_second_france_min = (
            utilitaires.db_exec_get_first_value(
                "SELECT min(ips) from educ_etablissements_details WHERE ips > 1"
            )
        )
        self.data.education.ips_premier_et_second_prive_france_max = utilitaires.db_exec_get_first_value(
            "SELECT max(ips) from educ_etablissements_details WHERE lower(secteur_public_prive_libe) != 'public' AND ips > 1"
        )
        self.data.education.ips_premier_et_second_prive_france_min = utilitaires.db_exec_get_first_value(
            "SELECT min(ips) from educ_etablissements_details WHERE lower(secteur_public_prive_libe) != 'public' AND ips > 1"
        )
        self.data.education.ips_premier_et_second_public_france_max = utilitaires.db_exec_get_first_value(
            "SELECT max(ips) from educ_etablissements_details WHERE lower(secteur_public_prive_libe) = 'public' AND ips > 1"
        )
        self.data.education.ips_premier_et_second_public_france_min = utilitaires.db_exec_get_first_value(
            "SELECT min(ips) from educ_etablissements_details WHERE lower(secteur_public_prive_libe) = 'public' AND ips > 1"
        )

    def get_etablissements(self):
        """Get the etablissements from the database."""
        utilitaires.db_exec(
            utilitaires.db.mogrify(
                """
                SELECT * FROM educ_etablissements_details WHERE circo=%s
                """,
                (self.circo,),
            )
        )
        etablissements_premier_et_second_degre = utilitaires.db.fetchall()
        assert len(etablissements_premier_et_second_degre) > 0
        df = pd.DataFrame(etablissements_premier_et_second_degre)
        df.columns = [col[0] for col in utilitaires.db.description]
        df = df.astype(
            {
                "effectifs": "float32",
                "ips": "float32",
                "taux_de_reussite": "float32",
                "brevet_mentions": "float32",
                "valeur_ajoutee": "float32",
            }
        )
        self.data.education.etablissements_premier_et_second_degre = df

        utilitaires.db_exec(
            utilitaires.db.mogrify(
                """
                SELECT * FROM educ_etablissements_superieur WHERE circo=%s
                """,
                (self.circo,),
            )
        )

        etablissements_superieur = utilitaires.db.fetchall()
        # Il n'y a pas toujours d'établissements supérieur
        if len(etablissements_superieur) > 0:
            df = pd.DataFrame(etablissements_superieur)
            df.columns = [col[0] for col in utilitaires.db.description]
            df = df.astype(
                {
                    "latitude": "float32",
                    "longitude": "float32",
                    "effectifs": "float32",
                }
            )
            self.data.education.etablissements_superieur = df
