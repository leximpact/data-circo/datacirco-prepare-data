from dataclasses import dataclass
from datacirco import utilitaires
import json
import plotly as px


def get_color(
    valeur: int, valeur_min: int = 0, valeur_max: int = 80, colorscale="Electric"
):
    """Obtenir une couleur d'une palette de couleurs Plotly en fonction d'une
    valeur numérique.

    :param valeur: La valeur numérique pour laquelle on souhaite obtenir
        la couleur.
    :param valeur_minimale: La borne inférieure de la variable pour
        laquelle on souhaite une couleur.
    :param valeur_maximale: La borne supérieure de la variable pour
        laquelle on souhaite une couleur.
    :param colorscale: La palette de couleurs Plotly (par exemple,
        'Viridis', 'Cividis', etc.)
    :return: La couleur correspondant à la valeur dans la palette de
        couleurs.
    """
    # Normaliser la valeur entre 0 et 1
    valeur_norme = max(0, min(1, (valeur - valeur_min) / (valeur_max - valeur_min)))
    # Obtenir la couleur de la palette en fonction de la valeur normalisée
    color = px.colors.sample_colorscale(colorscale, [valeur_norme])[0]

    return color


@dataclass()
class DataSante:
    population_totale = None
    population = None
    etab_sante = None
    finess_eml = None
    equip_sante = None
    prof_sante = None
    urgences = None
    carte_urgences = None
    carte_urgences_detail = None
    geoloc_equip_urgence = None
    sources = None


class GetSante:
    """To retreive data from the database."""

    def __init__(self, db, data):
        """
        Args:
            db: Connection to the database
            data: A DataCirco object
        """
        self.db = db
        utilitaires.db = db
        data.sante = DataSante()
        self.data = data
        self.dept = data.dept
        self.circo = data.circo
        self.data.sante.sources = utilitaires.get_sources("sante")

    def get_population_totale(self):
        utilitaires.db_exec(
            self.db.mogrify(
                """
                SELECT france 
                FROM population_france
                WHERE annee=2023
                """
            )
        )
        total = self.db.fetchone()
        self.data.sante.population_totale = int(total[0])

    def get_population(self, in_communes):
        # Calcul de la population concernées par les communes et/ou arrondissements municipaux
        utilitaires.db_exec(
            self.db.mogrify(
                """
                SELECT sum(population) FROM (
                    SELECT population FROM ign_commune WHERE insee_com in (%s) AND insee_com not in ('13055','69123','75056')
                    UNION
                    SELECT population FROM ign_arrondissement_municipal WHERE insee_arm in (%s)
                ) territoire
                """
                % (in_communes, in_communes)
            )
        )
        population = self.db.fetchone()
        self.data.sante.population = population[0]

    def get_etab_sante(self, in_communes):
        """Récupère les établissements de santé."""
        utilitaires.db_exec(
            self.db.mogrify(
                """
            SELECT
                categ_niv3_code,
                categ_niv3_lib,
                count(*)
            FROM
                finess
            WHERE etat='ACTUEL'
                and com_code in (%s)
                and categ_niv3_lib != ''
            GROUP BY 1,2
            ORDER BY 1
            """
                % in_communes
            )
        )
        columns = [col[0] for col in utilitaires.db.description]
        self.data.sante.etab_sante = [
            dict(zip(columns, row)) for row in self.db.fetchall()
        ]

    def get_finess(self, in_communes):
        """Récupère les établissements de santé présents sur les communes ou
        arrondissements couverts au moins partiellement par la circonscription.

        EML = 'équipements matériels lourds'
        """
        utilitaires.db_exec(
            self.db.mogrify(
                """
        select
            eml.libeml,
            sum(case when ci.libeml is not null then 1 else 0 end) as nb_circo
        from (select libeml from finess_eml group by 1) as eml
        left join (
            select eml.libeml
            from finess et
            join finess_eml eml on (eml.nofinesset=finess)
            where com_code in (%s)
                and etat='ACTUEL'
            ) as ci
            on (eml.libeml=ci.libeml)
        group by 1
        """
                % in_communes
            )
        )
        self.data.sante.finess_eml = [
            {
                "equipement": r[0],
                "nb": r[1],
            }
            for r in self.db.fetchall()
        ]

    def get_equip_sante(self, in_communes):
        """Récupère les Equipements de santé."""
        utilitaires.db_exec(
            self.db.mogrify(
                """
        SELECT circo.*, nb FROM (
            SELECT
                categ_code,
                categ_lib,
                count(*)
            FROM
                finess
            WHERE etat='ACTUEL'
                and com_code in (%s)
                and categ_code >='600' and categ_code <='621'
            GROUP BY 1,2
        ) as circo left join (
            SELECT
                categ_code,
                categ_lib,
                count(*) as nb
            FROM
                finess
            WHERE etat='ACTUEL'
                and categ_code >='600' and categ_code <='621'
            GROUP BY 1,2
        ) as nat on (nat.categ_code = circo.categ_code)
            ORDER BY 1
            """
                % in_communes
            )
        )
        columns = [col[0] for col in utilitaires.db.description]
        self.data.sante.equip_sante = [
            dict(zip(columns, row)) for row in self.db.fetchall()
        ]

    def get_prof_sante(self, in_communes):
        """Récupère les professionnels de santé."""
        utilitaires.db_exec(
            self.db.mogrify(
                """
        SELECT
            nat.lib_profession, coalesce(circo.nb,0), nat.nb
        FROM (
            select
                lib_profession, count(*) as nb
            from (
                select
                    nom, prenom, lib_profession
                from laposte_cp cp
                join cnamts_prof_sante_lib ps on (code_postal=cp and ville in (nom_de_la_commune, ligne_5, libelle_d_acheminement))
                where sexe is not null and lib_profession is not null
                UNION
                select
                    nom, prenom, lib_profession
                from laposte_cp cp
                join cnamts_prof_sante_etab_lib ps on (code_postal=cp and ville in (nom_de_la_commune, ligne_5, libelle_d_acheminement))
                where lib_profession is not null
                group by 1,2,3
            ) ps group by 1
        ) as nat
        LEFT JOIN (
            select
                lib_profession, count(*) as nb
            from (
                select
                    nom, prenom, lib_profession
                from laposte_cp cp
                join cnamts_prof_sante_lib ps on (code_postal=cp and ville in (nom_de_la_commune, ligne_5, libelle_d_acheminement))
                where code_commune_insee in (%s) and sexe is not null and lib_profession is not null

                UNION

                select
                    nom, prenom, lib_profession
                from laposte_cp cp
                join cnamts_prof_sante_etab_lib ps on (code_postal=cp and ville in (nom_de_la_commune, ligne_5, libelle_d_acheminement))
                where code_commune_insee in (%s) and lib_profession is not null
                group by 1,2,3
            ) ps group by 1
        ) as circo
        ON (nat.lib_profession = circo.lib_profession)
        WHERE nat.nb > 500
        ORDER BY 3 DESC;
        """
                % (in_communes, in_communes)
            )
        )
        self.data.sante.prof_sante = [
            {
                "profession": r[0],
                "nb_circo": r[1],
                "nb_france": r[2],
            }
            for r in self.db.fetchall()
        ]

    def get_prox_urgence(self, in_communes):
        """Récupère la proximité aux services d’urgence."""
        utilitaires.db_exec(
            self.db.mogrify(
                """
        SELECT
            left(typ_equip,4),
            (tps_total+2.5)::int / 5 * 5,
            count(*),
            sum(pop_2017::int)
        FROM drees_acces_urgences_2019 ur
        WHERE depcom in (%s)
        GROUP BY 1,2
        ORDER BY 2,1 desc;
        """
                % (in_communes,)
            )
        )
        self.data.sante.urgences = [
            {
                "type": r[0],
                "tps": r[1],
                "nb": r[2],
                "pop": r[3],
            }
            for r in self.db.fetchall()
        ]
        # --carte accès urgences
        utilitaires.db_exec(
            self.db.mogrify(
                """
                        SELECT
                                ur.typ_equip,
                                ur.tps_total,
                                st_asgeojson(coalesce(co.wkb_geometry,ar.wkb_geometry)) as geojson
                        FROM drees_acces_urgences_2019 ur
                        LEFT JOIN ign_commune co ON (co.insee_com=ur.depcom)
                        LEFT JOIN ign_arrondissement_municipal ar ON (ar.insee_arm=ur.depcom)
                        WHERE ur.depcom in (%s);
                        """
                % (in_communes,)
            )
        )
        self.data.sante.carte_urgences = [
            {
                "type_equip": r[0],
                "temps": r[1],
                "geojson": r[2],
            }
            for r in self.db.fetchall()
        ]

        utilitaires.db_exec(
            self.db.mogrify(
                """
            SELECT y.depcom, y.nom, y.latitude, y.longitude, y.typ_equip
            FROM (
            SELECT
                    ur.typ_equip,
                    COALESCE(co.nom, ar.nom) as nom,
                    ur.depcom,
                    CAST(coalesce(ur.tps_su_smur, 0) as FLOAT) as temps,
                    st_x(st_asgeojson(ST_PointOnSurface(ST_astext(coalesce(co.wkb_geometry,ar.wkb_geometry), 4326)))) as longitude,
                    st_y(st_asgeojson(ST_PointOnSurface(ST_astext(coalesce(co.wkb_geometry,ar.wkb_geometry), 4326)))) as latitude
            FROM drees_acces_urgences_2019 ur
            LEFT JOIN ign_commune co ON (co.insee_com=ur.depcom)
            LEFT JOIN ign_arrondissement_municipal ar ON (ar.insee_arm=ur.depcom)
            WHERE ur.depcom in (%s)) as y
            WHERE (y.typ_equip IN ('MCS','HeliSMUR','HeliETAT')) OR (y.typ_equip='SU_SMUR' AND y.temps = 0);"""
                % (in_communes,)
            )
        )
        loc_communes_circo = [
            {
                "code_postal": r[0],
                "nom": r[1],
                "latitude": r[2],
                "longitude": r[3],
                "type_equip": r[4],
            }
            for r in self.db.fetchall()
        ]
        utilitaires.db_exec(
            self.db.mogrify(
                """
            SELECT DISTINCT ON(x.com_ref) x.com_ref, x.nom, x.latitude_ref, x.longitude_ref, x.type_equip_ref
            FROM (
            SELECT *
            FROM drees_acces_urgences_2019 ur
            LEFT JOIN ign_commune co ON (co.insee_com=ur.depcom)
            LEFT JOIN ign_arrondissement_municipal ar ON (ar.insee_arm=ur.depcom)
            WHERE ur.depcom in (%s)) as y 
            INNER JOIN ( 
                SELECT ur1.depcom as com_ref, co1.nom, ur1.typ_equip as type_equip_ref,
                CASE WHEN st_intersects(coalesce(co1.wkb_geometry,ar1.wkb_geometry), c.wkb_geometry) THEN st_x(st_asgeojson(ST_PointOnSurface(ST_astext(st_intersection(coalesce(co1.wkb_geometry,ar1.wkb_geometry), c.wkb_geometry), 4326))))
                ELSE  st_x(st_asgeojson(ST_PointOnSurface(ST_astext(coalesce(co1.wkb_geometry,ar1.wkb_geometry), 4326)))) END as longitude_ref,
                CASE WHEN st_intersects(coalesce(co1.wkb_geometry,ar1.wkb_geometry), c.wkb_geometry) THEN st_y(st_asgeojson(ST_PointOnSurface(ST_astext(st_intersection(coalesce(co1.wkb_geometry,ar1.wkb_geometry), c.wkb_geometry), 4326))))
                ELSE  st_y(st_asgeojson(ST_PointOnSurface(ST_astext(coalesce(co1.wkb_geometry,ar1.wkb_geometry), 4326)))) END as latitude_ref
                FROM drees_acces_urgences_2019 ur1
                LEFT JOIN  ign_commune co1 ON (co1.insee_com=ur1.depcom)
                LEFT JOIN ign_arrondissement_municipal ar1 ON (ar1.insee_arm=ur1.depcom)
                CROSS JOIN zone_circo c
                WHERE ur1.depcom not in (%s) AND c.ref = '%s') as x
            ON y.com_equip = x.com_ref;"""
                % (
                    in_communes,
                    in_communes,
                    self.circo,
                )
            )
        )
        loc_communes_ref = [
            {
                "code_commune": r[0],
                "nom": r[1],
                "latitude": r[2],
                "longitude": r[3],
                "type_equip": r[4],
            }
            for r in self.db.fetchall()
        ]

        # Liste des points de référence à l'intérieur et à l'extérieur de la circonscription
        self.data.sante.geoloc_equip_urgence = loc_communes_ref + loc_communes_circo

        utilitaires.db_exec(
            self.db.mogrify(
                """
            SELECT y.depcom, y.nom, y.typ_equip, y.temps, y.temps_heli, y.geojson
            FROM (
            SELECT
                    ur.typ_equip,
                    COALESCE(co.nom, ar.nom) as nom,
                    ur.depcom,
                    CAST(coalesce(ur.tps_su_smur, 0) as FLOAT) as temps,
                    CAST(coalesce(ur.tps_total, coalesce(ur.tps_su_smur_mcs, 0)) as FLOAT) as temps_heli,
                    st_asgeojson(coalesce(co.wkb_geometry,ar.wkb_geometry)) as geojson,
                    ur.com_equip
            FROM drees_acces_urgences_2019 AS ur
            LEFT JOIN ign_commune AS co ON (co.insee_com=ur.depcom)
            LEFT JOIN ign_arrondissement_municipal AS ar ON (ar.insee_arm=ur.depcom)
            WHERE ur.depcom in (%s)) as y;"""
                % (in_communes,)
            )
        )
        # La couleur est défini selon le chiffre dans la fonction qui catégorise
        # Hachure ssi le temps en hélicoptère est supérieur à celui sans hélicoptère
        carte_urgences_detail = [
            {
                "code_commune": r[0],
                "nom": r[1],
                "type_equip": r[2],
                "temps": r[3],
                "temps_heli": r[4],
                "geojson": dict(
                    type="Feature", properties=dict(), geometry=json.loads(r[5])
                ),
            }
            for r in self.db.fetchall()
        ]

        for commune in carte_urgences_detail:
            commune["geojson"]["properties"] = dict(
                color=get_color(commune["temps"]),
                hachure=(commune["temps"] < commune["temps_heli"]),
            )
        self.data.sante.carte_urgences_detail = carte_urgences_detail
