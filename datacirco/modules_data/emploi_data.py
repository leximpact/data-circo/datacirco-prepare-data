from dataclasses import dataclass

from datacirco import utilitaires


@dataclass()
class DataEmploi:
    secteurs = None
    salaries = None
    emplois = None
    total_salaries = None
    sources = None


class GetEmploi:
    """To retreive data from the database."""

    def __init__(self, db, data):
        """
        Args:
            db: Connection to the database
            data: A DataCirco object
        """
        self.db = db
        utilitaires.db = db
        data.emploi = DataEmploi()
        self.data = data
        self.dept = data.dept
        self.circo = data.circo
        self.data.emploi.sources = utilitaires.get_sources("emplois")

    def get_secteurs(self, in_communes):
        """Récupère les secteurs."""
        utilitaires.db_exec(
            self.db.mogrify(
                """
        select
            grand_secteur_d_activite,
            substring(grand_secteur_d_activite,5),
            coalesce(sum(effectifs_salaries_2021::int),0) as effectifs_2021,
            coalesce(sum(effectifs_salaries_2022::int),0) as effectifs_2022,
            coalesce(sum(effectifs_salaries_2023::int),0) as effectifs_2023
        from
            urssaf_salaries_prive
        where
            code_commune in (%s)
        group by 1,2 order by 1,2;
        """
                % (in_communes,)
            )
        )
        self.data.emploi.secteurs = [
            {
                "libelle_complet": secteur,
                "libelle": effectifs,
                "effectifs_2021": effectifs_2021,
                "effectifs_2022": effectifs_2022,
                "effectifs_2023": effectifs_2023,
            }
            for secteur, effectifs, effectifs_2021, effectifs_2022, effectifs_2023 in self.db.fetchall()
        ]

        self.data.emploi.total_salaries = sum(
            sect["effectifs_2023"] for sect in self.data.emploi.secteurs
        )

    def get_salaries(self, in_communes):
        """Récupère les salariés."""
        utilitaires.db_exec(
            self.db.mogrify(
                """
        select
            grand_secteur_d_activite,
            secteur_na38,
            substring(grand_secteur_d_activite,5),
            substring(secteur_na38,4),
            coalesce(sum(effectifs_salaries_2006::int),0)  as effectifs_2006,
            coalesce(sum(effectifs_salaries_2021::int),0) as effectifs_2021,
            coalesce(sum(effectifs_salaries_2022::int),0) as effectifs_2022,
            coalesce(sum(effectifs_salaries_2023::int),0) as effectifs_2023
        from
            urssaf_salaries_prive
        where
            code_commune in (%s)
        group by 1,2,3,4 order by 1,2;
        """
                % (in_communes,)
            )
        )
        self.data.emploi.salaries = [
            {
                "secteur_full": secteur_full,
                "sous_secteur_full": sous_secteur_full,
                "secteur": secteur,
                "sous_secteur": sous_secteur,
                "effectifs_2006": effectifs_2006,
                "effectifs_2021": effectifs_2021,
                "effectifs_2022": effectifs_2022,
                "effectifs_2023": effectifs_2023,
            }
            for secteur_full, sous_secteur_full, secteur, sous_secteur, effectifs_2006, effectifs_2021, effectifs_2022, effectifs_2023 in self.db.fetchall()
        ]

    def get_emplois(self, in_communes):
        """Récupère les emplois."""
        utilitaires.db_exec(
            self.db.mogrify(
                """
        select
            st_asgeojson(wkb_geometry),
            nom,
            sum(coalesce(effectifs_salaries_2023,'0')::int - coalesce(effectifs_salaries_2006,'0')::int) as evolution,
            ST_X(St_centroid(wkb_geometry)),
            ST_Y(St_centroid(wkb_geometry))
        from
            urssaf_salaries_prive
        JOIN ign_commune co ON (co.insee_com=code_commune)
        where
            code_commune in (%s)
        GROUP BY 1,2,4,5 ORDER BY 3 DESC
        """
                % (in_communes,)
            )
        )
        self.data.emploi.emplois = [
            {
                "geojson": r[0],
                "nom": r[1],
                "evolution": r[2],
                "longitude": r[3],
                "latitude": r[4],
            }
            for r in self.db.fetchall()
        ]
