from dataclasses import dataclass

from datacirco import utilitaires


@dataclass()
class DataEntreprises:
    entreprises = None
    entreprises_sieges = None
    etablissements = None
    etablissements_par_categorie = None
    effectif0 = None
    effectif1 = None
    employeurs = None
    immat_rad = None
    rncs_immatriculations = None
    rncs_radiations = None
    rncs_immatriculations_nat = None
    rncs_radiations_nat = None
    tranche_effectifs = None
    sources = None


class GetEntreprises:
    """To retreive data from the database."""

    def __init__(self, db, data):
        """
        Args:
            db: Connection to the database
            data: A DataCirco object
        """
        self.db = db
        utilitaires.db = db
        data.entreprises = DataEntreprises()
        self.data = data
        self.dept = data.dept
        self.circo = data.circo
        self.data.entreprises.tranche_effectifs = {
            "00": "pas de salarié",
            "01": "de 1 à 2 salariés",
            "02": "de 3 à 4 salariés",
            "03": "de 5 à 9 salariés",
            "11": "de 10 à 19 salariés",
            "12": "de 20 à 49 salariés",
            "21": "de 50 à 99 salariés",
            "22": "de 100 à 199 salariés",
            "31": "de 200 à 249 salariés",
            "32": "de 250 à 499 salariés",
            "41": "de 500 à 999 salariés",
            "42": "de 1000 à 1999 salariés",
            "51": "de 2000 à 4999 salariés",
            "52": "de 5000 à 9999 salariés",
            "53": "plus de 10000 salariés",
        }
        self.data.entreprises.sources = utilitaires.get_sources("entreprises")

    def get_entreprises(self):
        """Récupère les entreprises."""
        utilitaires.db_exec(
            self.db.mogrify(
                """
        select
            case when left(categoriejuridiqueunitelegale,1) in ('4','7') then 'Public'
            else coalesce(categorieentreprise,'autre') end,
            count(*),
            sum(case when trancheeffectifsetablissement NOT IN ('NN','00') then 1 else 0 end) as employeur,
            count(distinct(et.siren))
        from zone_circo c
        join insee_siret et on (st_intersects(c.wkb_geometry, et.geom))
        join insee_siren en on (et.siren=en.siren)
        where
            ref = %s
            and etatadministratifetablissement = 'A'
        GROUP BY 1
        ORDER BY length(case when left(categoriejuridiqueunitelegale,1) in ('4','7') then 'Public'
            else coalesce(categorieentreprise,'autre') end),1
        """,
                (self.circo,),
            )
        )
        entreprises = [
            {
                "categorieentreprise": row[0],
                "nombre": row[1],
                "employeur": row[2],
                "sum_siren": row[3],
            }
            for row in self.db.fetchall()
        ]
        assert entreprises is not None
        self.data.entreprises.entreprises = entreprises

    def get_entreprises_sieges(self):
        """Récupère les sièges des entreprises."""
        utilitaires.db_exec(
            self.db.mogrify(
                """
        select
            case when left(categoriejuridiqueunitelegale,1) in ('4','7') then 'Public'
            else coalesce(categorieentreprise,'autre') end,
            count(*)
        from zone_circo c
        join insee_siret et on (st_intersects(c.wkb_geometry, et.geom))
        join insee_siren en on (et.siren=en.siren and nicsiegeunitelegale=et.nic)
        where
            ref = %s
            and etatadministratifetablissement = 'A'
        GROUP BY 1
        ORDER BY length(case when left(categoriejuridiqueunitelegale,1) in ('4','7') then 'Public'
            else coalesce(categorieentreprise,'autre') end),1
        """,
                (self.circo,),
            )
        )
        sieges = [
            {
                "categorieentreprise": row[0],
                "nombre": row[1],
            }
            for row in self.db.fetchall()
        ]
        assert sieges is not None
        self.data.entreprises.entreprises_sieges = sieges

    def get_etablissements(self):
        """Récupère les établissements."""
        utilitaires.db_exec(
            self.db.mogrify(
                """
        select
            categorieentreprise,
            count(*)
        from zone_circo c
        join insee_siret et on (st_intersects(c.wkb_geometry, et.geom))
        join insee_siren en on (et.siren=en.siren)
        where ref = %s
            and categoriejuridiqueunitelegale='1000'
            and etatadministratifetablissement = 'A'
        group by 1
        order by 1
        """,
                (self.circo,),
            )
        )
        self.data.entreprises.etablissements_par_categorie = [
            {
                "categorieentreprise": row[0],
                "nombre": row[1],
            }
            for row in self.db.fetchall()
        ]

        utilitaires.db_exec(
            self.db.mogrify(
                """
                select
                    trancheeffectifsetablissement,
                    count(*)
                from zone_circo c
                join insee_siret et on (st_intersects(c.wkb_geometry, et.geom))
                join insee_siren en on (et.siren=en.siren)
                where
                    ref = %s
                    and etatadministratifetablissement = 'A'
                    AND trancheeffectifsetablissement NOT IN ('NN','00')
                GROUP BY 1
                ORDER BY 1
                """,
                (self.circo,),
            )
        )

        self.data.entreprises.etablissements = [
            {
                "trancheeffectifsetablissement": row[0],
                "tranche_lib": self.data.entreprises.tranche_effectifs[row[0]],
                "nombre": row[1],
            }
            for row in self.db.fetchall()
        ]

    def get_employeurs_circo(self):
        """Récupère les établissements employeurs dans la circonscription."""
        utilitaires.db_exec(
            self.db.mogrify(
                """
        select
            et.siren,
            coalesce(denominationusuelleetablissement, denominationunitelegale, denominationusuelle1unitelegale) as nom,
            trancheeffectifsetablissement,
            case when left(categoriejuridiqueunitelegale,1) in ('4','7') then 'Public' else coalesce(categorieentreprise) end,
            case when coalesce(denominationusuelleetablissement, denominationunitelegale, denominationusuelle1unitelegale) ~ replace(libellecommuneetablissement,'-', ' ')
                then ''
                else '('||libellecommuneetablissement||')' end,
            latitude, longitude
        from zone_circo c
        join insee_siret et on (st_intersects(c.wkb_geometry, et.geom))
        join insee_siren en on (et.siren=en.siren)
        where ref = %s
            and trancheeffectifsetablissement not in ('NN')
            and trancheeffectifsetablissement >= '22'
            and etatadministratifetablissement = 'A'
        order by 3 desc, 2
        limit 50;
        """,
                (self.circo,),
            )
        )
        self.data.entreprises.employeurs = [
            {
                "siren": row[0],
                "nom": row[1],
                "trancheeffectifsetablissement": row[2],
                "categorieentreprise": row[3],
                "commune": row[4],
                "latitude": float(row[5]),
                "longitude": float(row[6]),
            }
            for row in self.db.fetchall()
        ]

    def get_radiations(self):
        """Récupère les immatriculations et radiations d'entreprises."""
        self.data.entreprises.rncs_immatriculations = None
        self.data.entreprises.rncs_immatriculations_nat = None
        self.data.entreprises.rncs_radiations = None
        self.data.entreprises.rncs_radiations_nat = None
        if self.circo < "099":
            # graphique immatriculations / radiations d'entreprises aux greffes
            utilitaires.db_exec(
                self.db.mogrify(
                    """
            select
                left(date_immatriculation,4)::int as annee,
                count(*) as immatriculations
            from rncs_immatriculations r
            join zone_circo c on (st_intersects(c.wkb_geometry, r.geom))
            where ref=%s
            and left(date_immatriculation,4)::int < date_part('year', CURRENT_DATE)
            group by 1
            order by 1;
            """,
                    (self.circo,),
                )
            )
            self.data.entreprises.rncs_immatriculations = [
                {
                    "annee": row[0],
                    "immatriculations": int(row[1]),
                }
                for row in self.db.fetchall()
            ]

            utilitaires.db_exec(
                self.db.mogrify(
                    """
            select
                left(date_radiation,4)::int as annee,
                count(*) as radiations
            from rncs_radiations r
            join zone_circo c on (st_intersects(c.wkb_geometry, r.geom))
            where ref=%s
            and left(date_radiation,4)::int < date_part('year', CURRENT_DATE)
            group by 1
            order by 1;
            """,
                    (self.circo,),
                )
            )
            self.data.entreprises.rncs_radiations = [
                {
                    "annee": row[0],
                    "radiations": int(row[1]),
                }
                for row in self.db.fetchall()
            ]

            # Immatriculations France entière
            utilitaires.db_exec(
                self.db.mogrify(
                    """
            select
                left(date_immatriculation,4)::int as annee,
                count(*) as immatriculations
            from rncs_immatriculations r
            join zone_circo c on (st_intersects(c.wkb_geometry, r.geom))
            where left(date_immatriculation,4)::int < date_part('year', CURRENT_DATE)
            group by 1
            order by 1;
            """
                )
            )
            self.data.entreprises.rncs_immatriculations_nat = [
                {
                    "annee": row[0],
                    "immatriculations": int(row[1]),
                }
                for row in self.db.fetchall()
            ]

            # Radiations France entière
            utilitaires.db_exec(
                self.db.mogrify(
                    """
            select
                left(date_radiation,4)::int as annee,
                count(*) as radiations
            from rncs_radiations r
            join zone_circo c on (st_intersects(c.wkb_geometry, r.geom))
            where left(date_radiation,4)::int < date_part('year', CURRENT_DATE)
            group by 1
            order by 1;
            """
                )
            )
            self.data.entreprises.rncs_radiations_nat = [
                {
                    "annee": row[0],
                    "radiations": int(row[1]),
                }
                for row in self.db.fetchall()
            ]

    def get_immat_rad_mens(self):
        """Récupère les immatriculations et radiations d'entreprise des 12
        derniers mois."""
        utilitaires.db_exec(
            self.db.mogrify(
                b"""
                select
                    i.annee_mois,
                    i.immatriculations,
                    r.radiations,
                    i.immatriculations - r.radiations as solde
                from (
                    select
                        left(date_radiation,7) as annee_mois,
                        count(r.*) as radiations
                    from zone_circo c 
                        join rncs_radiations r on (st_intersects(c.wkb_geometry, r.geom))
                    where ref=%s
                        and to_date(date_radiation,'YYYY-MM-DD')>date_trunc('month', CURRENT_DATE) - INTERVAL '1 year'
                        and to_date(date_radiation,'YYYY-MM-DD')<date_trunc('month', CURRENT_DATE)
                    group by 1
                ) as r,
                (
                    select
                        left(date_immatriculation,7) as annee_mois,
                        count(i.*) as immatriculations
                    from rncs_immatriculations i
                        join zone_circo c on (st_intersects(c.wkb_geometry, i.geom))
                    where ref=%s
                        and to_date(date_immatriculation,'YYYY-MM-DD')>date_trunc('month', CURRENT_DATE) - INTERVAL '1 year'
                        and to_date(date_immatriculation,'YYYY-MM-DD')<date_trunc('month', CURRENT_DATE)
                    group by 1
                ) as i
                where r.annee_mois=i.annee_mois
                order by 1
                """,
                (self.circo, self.circo),
            )
        )
        res = [
            {
                "annee_mois": row[0],
                "immatriculations": int(row[1]),
                "radiations": int(row[2]),
                "solde": int(row[3]),
            }
            for row in self.db.fetchall()
        ]
        assert res is not None
        self.data.entreprises.immat_rad = res
