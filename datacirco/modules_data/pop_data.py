from dataclasses import dataclass

from datacirco import utilitaires


@dataclass()
class DataPopulation:
    population_circo_detail_2010 = None
    population_circo_totale_2013 = None
    population_circo_detail_2015 = None
    population_circo_totale_2019 = None
    population_circo_detail_2018 = None
    population_departement_detail_2018 = None
    population_france_detail_2018 = None
    carreaux_evolution_population_2010_vs_2015 = None
    carreaux_densite_population_2015 = None
    carreaux_densite_population_2018 = None

    population_circo_totale = None
    population_france_2018 = None
    superficie_nat = None
    sources = None

    stats_menages_circo = None
    stats_menages_circo_2010 = None


class GetPopulation:
    def __init__(self, db, data):
        """
        Args:
            db: Connection to the database
            data: A DataCirco object
        """
        self.db = db
        utilitaires.db = db
        data.population = DataPopulation()
        self.data = data
        self.dept = data.dept
        self.circo = data.circo

        # population sauf Guyane
        # TODO: Pourquoi pas la Guyane ?
        utilitaires.db_exec(
            db.mogrify(
                "SELECT sum(p18_pop::float) from insee_pop_2018 where com not like '973%'"
            )
        )
        self.data.population.population_france_2018 = db.fetchone()[0]
        utilitaires.db_exec(
            db.mogrify(
                "SELECT sum(p20_pop::float) from insee_pop_2020 where com not like '973%'"
            )
        )
        self.data.population.population_france_2020 = db.fetchone()[0]
        self.data.population.sources = utilitaires.get_sources("population")
        self.column_names_repart_pop = [
            "annee",
            "ind",
            "ind_0_3",
            "ind_4_5",
            "ind_6_10",
            "ind_11_17",
            "ind_18_24",
            "ind_25_39",
            "ind_40_54",
            "ind_55_64",
            "ind_65_79",
            "ind_80p",
        ]
        self.column_names_repart_pop_2010 = [
            "annee",
            "ind",
            "ind_0_3",
            "ind_4_5",
            "ind_6_10",
            "ind_11_17",
            "ind_18_24",
            "ind_25_64",
            "ind_65p",
        ]
        self.column_names_population_carroyee_2010 = [
            "sum_men",
            "sum_men_pauv",
            "sum_men_1ind",
            "sum_men_5ind",
            "sum_men_prop",
            "sum_men_coll",
            "sum_ind_snv",
        ]
        self.column_names_population_carroyee_2015 = [
            "sum_men",
            "sum_men_pauv",
            "sum_men_1ind",
            "sum_men_5ind",
            "sum_men_fmp",
            "sum_men_prop",
            "sum_men_coll",
            "sum_men_mais",
            "sum_ind_snv",
        ]

    def set_pop_DROM_COM(self):
        utilitaires.db_exec(
            self.db.mogrify(
                b"""
        SELECT "p13_pop"::numeric
        FROM insee_circo_2013
        WHERE "depcir" = %s
        """,
                (self.dept + self.circo[-2:],),
            )
        )

        result = self.db.fetchone()
        self.data.population.population_circo_totale_2013 = (
            result[0] if result else None
        )
        utilitaires.db_exec(
            self.db.mogrify(
                """
            SELECT 2015 as annee,
            sum(ind*pct) AS ind,
                sum(ind_0_3 * pct) as ind_0_3,
                sum(ind_4_5 * pct) as ind_4_5,
                sum(ind_6_10 * pct) as ind_6_10,
                sum(ind_11_17 * pct) as ind_11_17,
                sum(ind_18_24 * pct) as ind_18_24,
                sum(ind_25_39 * pct) as ind_25_39,
                sum(ind_40_54 * pct) as ind_40_54,
                sum(ind_55_64 * pct) as ind_55_64,
                sum(ind_65_79 * pct) as ind_65_79,
                sum(ind_80p * pct) as ind_80p
            FROM circo_population_2015
            NATURAL JOIN insee_population_carroyee_2015
            WHERE ref = %s
            """,
                (self.circo,),
            ),
            "population_2015",
        )

        self.data.population.population_circo_detail_2015 = utilitaires.list_to_dict(
            self.db.fetchone(), self.column_names_repart_pop
        )
        if self.data.population.population_circo_detail_2015["ind"] is None:
            self.data.population.population_circo_detail_2015 = None
        utilitaires.db_exec(
            self.db.mogrify(
                b"""
        SELECT population::numeric
        FROM insee_circo_2019
        WHERE dep = %s and circo = %s
        """,
                (self.dept, str(int(self.circo[-2:]))),
            )
        )
        population_circo_totale_2019 = self.db.fetchone()[0]
        self.data.population.population_circo_totale_2019 = population_circo_totale_2019

        if population_circo_totale_2019:
            population_circo_totale = population_circo_totale_2019
        else:
            population_circo_totale = self.data.population.population_circo_detail_2015[
                "ind"
            ]
        self.data.population.population_circo_totale = population_circo_totale

        utilitaires.db_exec(
            self.db.mogrify(
                """
        SELECT 2018 as annee,
            sum(p18_pop::float) as ind,
            sum(p18_pop0002::float) as ind_0_2,
            sum(p18_pop0305::float) as ind_3_5,
            sum(p18_pop0610::float) as ind_6_10,
            sum(p18_pop1117::float) as ind_11_17,
            sum(p18_pop1824::float) as ind_18_24,
            sum(p18_pop2539::float) as ind_25_39,
            sum(p18_pop4054::float) as ind_40_54,
            sum(p18_pop5564::float) as ind_55_64,
            sum(p18_pop6579::float) as ind_65_79,
            sum(p18_pop80p::float) as ind_80p
        FROM insee_pop_2018
        WHERE com in (%s)
        """
                % self.data.in_communes
            ),
            "population_2018",
        )
        self.data.population.population_circo_detail_2018 = utilitaires.list_to_dict(
            self.db.fetchone(), self.column_names_repart_pop
        )
        if self.data.population.population_circo_detail_2018["ind"] is None:
            self.data.population.population_circo_detail_2018 = None

        utilitaires.db_exec(
            self.db.mogrify(
                """
        SELECT
            sum(p18_pop::float) as ind,
            sum(p18_pop0002::float) as ind_0_2,
            sum(p18_pop0305::float) as ind_3_5,
            sum(p18_pop0610::float) as ind_6_10,
            sum(p18_pop1117::float) as ind_11_17,
            sum(p18_pop1824::float) as ind_18_24,
            sum(p18_pop2539::float) as ind_25_39,
            sum(p18_pop4054::float) as ind_40_54,
            sum(p18_pop5564::float) as ind_55_64,
            sum(p18_pop6579::float) as ind_65_79,
            sum(p18_pop80p::float) as ind_80p
        FROM insee_pop_2018
        WHERE com LIKE %s
        """,
                (self.dept + "%",),
            )
        )
        self.data.population.population_departement_detail_2018 = (
            utilitaires.list_to_dict(
                self.db.fetchone(), self.column_names_repart_pop[1::]
            )
        )

        utilitaires.db_exec(
            b"""
        SELECT
            sum(p18_pop::float) as ind,
            sum(p18_pop0002::float) as ind_0_2,
            sum(p18_pop0305::float) as ind_3_5,
            sum(p18_pop0610::float) as ind_6_10,
            sum(p18_pop1117::float) as ind_11_17,
            sum(p18_pop1824::float) as ind_18_24,
            sum(p18_pop2539::float) as ind_25_39,
            sum(p18_pop4054::float) as ind_40_54,
            sum(p18_pop5564::float) as ind_55_64,
            sum(p18_pop6579::float) as ind_65_79,
            sum(p18_pop80p::float) as ind_80p
        FROM insee_pop_2018
        """
        )
        self.data.population.population_france_detail_2018 = utilitaires.list_to_dict(
            self.db.fetchone(), self.column_names_repart_pop[1::]
        )

    def _set_pop_2013_2019_metro(self):
        utilitaires.db_exec(
            self.db.mogrify(
                """
            SELECT 2015 as annee,
            sum(ind*pct) AS ind,
                sum(ind_0_3 * pct) as ind_0_3,
                sum(ind_4_5 * pct) as ind_4_5,
                sum(ind_6_10 * pct) as ind_6_10,
                sum(ind_11_17 * pct) as ind_11_17,
                sum(ind_18_24 * pct) as ind_18_24,
                sum(ind_25_39 * pct) as ind_25_39,
                sum(ind_40_54 * pct) as ind_40_54,
                sum(ind_55_64 * pct) as ind_55_64,
                sum(ind_65_79 * pct) as ind_65_79,
                sum(ind_80p * pct) as ind_80p
            FROM circo_population_2015
            NATURAL JOIN insee_population_carroyee_2015
            WHERE ref = %s
            """,
                (self.circo,),
            ),
            "population_2015",
        )

        self.data.population.population_circo_detail_2015 = utilitaires.list_to_dict(
            self.db.fetchone(), self.column_names_repart_pop
        )
        if self.data.population.population_circo_detail_2015["ind"] is None:
            self.data.population.population_circo_detail_2015 = None
        utilitaires.db_exec(
            self.db.mogrify(
                b"""
            SELECT "p13_pop"::numeric
            FROM insee_circo_2013
            WHERE "depcir" = %s
            """,
                (self.dept + self.circo[-2:],),
            )
        )
        self.data.population.population_circo_totale_2013 = int(self.db.fetchone()[0])

        utilitaires.db_exec(
            self.db.mogrify(
                b"""
            SELECT population::numeric
            FROM insee_circo_2019
            WHERE dep = %s and circo = %s
            """,
                (self.dept, str(int(self.circo[-2:]))),
            )
        )
        population_circo_totale_2019 = self.db.fetchone()
        self.data.population.population_circo_totale_2019 = int(
            population_circo_totale_2019[0]
        )

        if population_circo_totale_2019:
            population_circo_totale = int(population_circo_totale_2019[0])
        else:
            population_circo_totale = self.data.population.population_circo_detail_2015[
                "ind"
            ]
        self.data.population.population_circo_totale = population_circo_totale

        utilitaires.db_exec(
            self.db.mogrify(
                """
            SELECT sum(ind),
                sum(ind_0_3),
                sum(ind_4_5),
                sum(ind_6_10),
                sum(ind_11_17),
                sum(ind_18_24),
                sum(ind_25_39),
                sum(ind_40_54),
                sum(ind_55_64),
                sum(ind_65_79),
                sum(ind_80p)
            FROM insee_population_carroyee_2015
            WHERE depcom LIKE %s
            """,
                (self.dept + "%",),
            )
        )
        self.data.population.population_departement_detail_2018 = (
            utilitaires.float_list_to_dict(
                self.db.fetchone(), self.column_names_repart_pop[1::]
            )
        )

        utilitaires.db_exec(
            b"""
            SELECT sum(ind),
                sum(ind_0_3),
                sum(ind_4_5),
                sum(ind_6_10),
                sum(ind_11_17),
                sum(ind_18_24),
                sum(ind_25_39),
                sum(ind_40_54),
                sum(ind_55_64),
                sum(ind_65_79),
                sum(ind_80p)
            FROM insee_population_carroyee_2015
            """
        )
        self.data.population.population_france_detail_2018 = (
            utilitaires.float_list_to_dict(
                self.db.fetchone(), self.column_names_repart_pop[1::]
            )
        )

        utilitaires.db_exec(
            self.db.mogrify(
                """
            SELECT 2010 as annee,
                sum(ind * pct),
                sum(ind_0_3 * pct),
                sum(ind_4_5 * pct),
                sum(ind_6_10 * pct),
                sum(ind_11_14 * pct) + sum(ind_15_17 * pct),
                sum(ind_18_24 * pct),
                sum(ind_25p * pct) - sum(ind_65p * pct),
                sum(ind_65p * pct)
            FROM circo_population_2010
            NATURAL JOIN insee_population_carroyee_2010
            WHERE ref=%s
            """,
                (self.circo,),
            ),
            "population_2010",
        )
        self.data.population.population_circo_detail_2010 = (
            utilitaires.float_list_to_dict(
                self.db.fetchone(), self.column_names_repart_pop_2010
            )
        )
        if self.data.population.population_circo_detail_2010["ind"] is None:
            self.data.population.population_circo_detail_2010 = None
        # assert population_circo_detail_2010 is not None
        # self.data.population.population_circo_detail_2010 = population_circo_detail_2010

    def set_evolution(self):
        utilitaires.db_exec(
            self.db.mogrify(
                """
            select coalesce(p.ind,0)::int as pop_2010,
                coalesce(p2.ind,0)::int as pop_2015,
                st_asgeojson((st_dump(st_intersection(coalesce(p.wkb_geometry,p2.wkb_geometry),c.wkb_geometry))).geom)::json as geojson
            from zone_circo c
            left join insee_population_carroyee_2015_1k p on (st_intersects(c.wkb_geometry,p.wkb_geometry))
            left join insee_population_carroyee_2010_1k p2 on (p2.wkb_geometry&& p.wkb_geometry and st_intersects(p.wkb_geometry, st_centroid(p2.wkb_geometry)))
            where ref=%s
            """,
                (self.circo,),
            ),
            "carte_evolution_population",
        )
        self.data.population.carreaux_evolution_population_2010_vs_2015 = [
            {"pop_2010": r[0], "pop_2015": r[1], "geojson": r[2]}
            for r in self.db.fetchall()
        ]

    def set_densite_2015(self):
        """
        Attention : on lit la population 2015 alors qu'au niveau national on prend la population 2018 !
        SELECT coalesce(sum(ind * pct),0)
        FROM circo_population_2015
        NATURAL JOIN insee_population_carroyee_2015 p
        where ref='091-07';

        On divise par 0.04 car un carreau fait 200mx200m et pas 1km².

        """
        utilitaires.db_exec(
            self.db.mogrify(
                """
        SELECT coalesce(sum(ind * pct),0)
        FROM circo_population_2015
        NATURAL JOIN insee_population_carroyee_2015 p
        where ind/0.04>=%s and ind/0.04<=%s and ref=%s
        """,
                (0, 25, self.circo),
            )
        )
        self.data.population.densite_tres_peu_dense_2015 = self.db.fetchone()[0]

        utilitaires.db_exec(
            self.db.mogrify(
                """
        SELECT coalesce(sum(ind * pct),0)
        FROM circo_population_2015
        NATURAL JOIN insee_population_carroyee_2015 p
        where ind/0.04>%s and ind/0.04<=%s and ref=%s
        """,
                (25, 300, self.circo),
            )
        )
        self.data.population.densite_peu_dense_2015 = self.db.fetchone()[0]

        utilitaires.db_exec(
            self.db.mogrify(
                """
        SELECT coalesce(sum(ind * pct),0)
        FROM circo_population_2015
        NATURAL JOIN insee_population_carroyee_2015 p
        where ind/0.04>%s and ind/0.04<=%s and ref=%s
        """,
                (300, 1500, self.circo),
            )
        )
        self.data.population.densite_intermediaire_2015 = self.db.fetchone()[0]

        utilitaires.db_exec(
            self.db.mogrify(
                """
        SELECT coalesce(sum(ind * pct),0)
        FROM circo_population_2015
        NATURAL JOIN insee_population_carroyee_2015 p
        where ind/0.04>%s and ref=%s
        """,
                (1500, self.circo),
            )
        )
        self.data.population.densite_densement_peuple_2015 = self.db.fetchone()[0]

        # carte densité de population
        if self.data.superficie_circo > 400:
            utilitaires.db_exec(
                self.db.mogrify(
                    """
            SELECT ind as densite,
                st_asgeojson((st_dump(st_intersection(p.wkb_geometry,c.wkb_geometry))).geom)::json as geojson
            FROM
                insee_population_carroyee_2015_1k p
            JOIN zone_circo c on (st_intersects(c.wkb_geometry,p.wkb_geometry))
            WHERE ref=%s
            """,
                    (self.data.circo,),
                ),
                "carte_densite_population",
            )
        else:
            utilitaires.db_exec(
                self.db.mogrify(
                    """
            SELECT ind/0.04 as densite,
                st_asgeojson((st_dump(st_intersection(p.wkb_geometry,c.wkb_geometry))).geom)::json as geojson
            FROM
                insee_population_carroyee_2015 p
            JOIN zone_circo c on (st_intersects(c.wkb_geometry,p.wkb_geometry))
            WHERE ref=%s
            """,
                    (self.data.circo,),
                ),
                "carte_densite_population_2015",
            )
        self.data.population.carreaux_densite_population_2015 = [
            {"densite": int(float(r[0])), "geojson": r[1]} for r in self.db.fetchall()
        ]

    def set_densite_2018(self):
        """
        P18_POP : population
        """
        utilitaires.db_exec(
            self.db.mogrify(
                """
        SELECT coalesce(sum(pop_circo),0)
        FROM (
            select
                p.iris,
                p18_pop,
                p18_pop::numeric/st_area(i.wkb_geometry::geography)*1000000 as densite,
                (st_area(st_intersection(c.wkb_geometry, i.wkb_geometry)::geography)/st_area(i.wkb_geometry::geography)*p18_pop::numeric)::int as pop_circo
            from zone_circo c
                join iris_ge i on (st_intersects(c.wkb_geometry, i.wkb_geometry))
                join insee_pop_2018 p on (p.iris=i.code_iris)
            where ref=%s) p
        WHERE densite>=%s AND densite<%s
        """,
                (self.circo, 0, 25),
            )
        )
        self.data.population.densite_tres_peu_dense_2018 = self.db.fetchone()[0]

        utilitaires.db_exec(
            self.db.mogrify(
                """
        SELECT coalesce(sum(pop_circo),0)
        FROM (
            select
                p.iris,
                p18_pop,
                p18_pop::numeric/st_area(i.wkb_geometry::geography)*1000000 as densite,
                (st_area(st_intersection(c.wkb_geometry, i.wkb_geometry)::geography)/st_area(i.wkb_geometry::geography)*p18_pop::numeric)::int as pop_circo
            from zone_circo c
                join iris_ge i on (st_intersects(c.wkb_geometry, i.wkb_geometry))
                join insee_pop_2018 p on (p.iris=i.code_iris)
            where ref=%s) p
        WHERE densite>=%s AND densite<%s
        """,
                (self.circo, 25, 300),
            )
        )
        densite_peu_dense = self.db.fetchone()
        self.data.population.densite_peu_dense_2018 = densite_peu_dense[0]

        utilitaires.db_exec(
            self.db.mogrify(
                """
        SELECT coalesce(sum(pop_circo),0)
        FROM (
            select
                p.iris,
                p18_pop,
                p18_pop::numeric/st_area(i.wkb_geometry::geography)*1000000 as densite,
                (st_area(st_intersection(c.wkb_geometry, i.wkb_geometry)::geography)/st_area(i.wkb_geometry::geography)*p18_pop::numeric)::int as pop_circo
            from zone_circo c
                join iris_ge i on (st_intersects(c.wkb_geometry, i.wkb_geometry))
                join insee_pop_2018 p on (p.iris=i.code_iris)
            where ref=%s) p
        WHERE densite>=%s AND densite<%s
        """,
                (self.circo, 300, 1500),
            )
        )
        densite_intermediaire = self.db.fetchone()
        self.data.population.densite_intermediaire_2018 = densite_intermediaire[0]

        utilitaires.db_exec(
            self.db.mogrify(
                """
        SELECT coalesce(sum(pop_circo),0)
        FROM (
            select
                p.iris,
                p18_pop,
                p18_pop::numeric/st_area(i.wkb_geometry::geography)*1000000 as densite,
                (st_area(st_intersection(c.wkb_geometry, i.wkb_geometry)::geography)/st_area(i.wkb_geometry::geography)*p18_pop::numeric)::int as pop_circo
            from zone_circo c
                join iris_ge i on (st_intersects(c.wkb_geometry, i.wkb_geometry))
                join insee_pop_2018 p on (p.iris=i.code_iris)
            where ref=%s) p
        WHERE densite>=%s
        """,
                (self.circo, 1500),
            )
        )
        densite_densement_peuple = self.db.fetchone()
        self.data.population.densite_densement_peuple_2018 = densite_densement_peuple[0]

        # carte densité de population
        utilitaires.db_exec(
            self.db.mogrify(
                """
        select
            p18_pop::numeric/st_area(i.wkb_geometry::geography)*1000000 as densite,
            st_asgeojson((st_dump(st_intersection(i.wkb_geometry,c.wkb_geometry))).geom)::json as geojson
        from zone_circo c
            join iris_ge i on (st_intersects(c.wkb_geometry, i.wkb_geometry))
            join insee_pop_2018 p on (p.iris=i.code_iris)
        where ref=%s
        """,
                (self.circo,),
            ),
            "carte_densite_population_2018",
        )
        self.data.population.carreaux_densite_population_2018 = [
            {"densite": r[0], "geojson": r[1]} for r in self.db.fetchall()
        ]

    def set_densite(self, annee="2020"):
        """
        P20_POP : population
        Pas facile de faire un système générique car il y a des noms de variables spécifiques à l'année.
        """
        utilitaires.db_exec(
            self.db.mogrify(
                f"""
        SELECT coalesce(sum(pop_circo),0)
        FROM (
            select
                p.iris,
                p20_pop,
                p20_pop::numeric/st_area(i.wkb_geometry::geography)*1000000 as densite,
                (st_area(st_intersection(c.wkb_geometry, i.wkb_geometry)::geography)/st_area(i.wkb_geometry::geography)*p20_pop::numeric)::int as pop_circo
            from zone_circo c
                join iris_ge i on (st_intersects(c.wkb_geometry, i.wkb_geometry))
                join insee_pop_{annee} p on (p.iris=i.code_iris)
            where ref=%s) p
        WHERE densite>=%s AND densite<%s
        """,
                (self.circo, 0, 25),
            )
        )
        if annee == "2020":
            self.data.population.densite_tres_peu_dense_2020 = self.db.fetchone()[0]
        else:
            raise ValueError("Annee non gérée")

        utilitaires.db_exec(
            self.db.mogrify(
                f"""
        SELECT coalesce(sum(pop_circo),0)
        FROM (
            select
                p.iris,
                p20_pop,
                p20_pop::numeric/st_area(i.wkb_geometry::geography)*1000000 as densite,
                (st_area(st_intersection(c.wkb_geometry, i.wkb_geometry)::geography)/st_area(i.wkb_geometry::geography)*p20_pop::numeric)::int as pop_circo
            from zone_circo c
                join iris_ge i on (st_intersects(c.wkb_geometry, i.wkb_geometry))
                join insee_pop_{annee} p on (p.iris=i.code_iris)
            where ref=%s) p
        WHERE densite>=%s AND densite<%s
        """,
                (self.circo, 25, 300),
            )
        )
        densite_peu_dense = self.db.fetchone()
        if annee == "2020":
            self.data.population.densite_peu_dense_2020 = densite_peu_dense[0]
        else:
            raise ValueError("Annee non gérée")

        utilitaires.db_exec(
            self.db.mogrify(
                f"""
        SELECT coalesce(sum(pop_circo),0)
        FROM (
            select
                p.iris,
                p20_pop,
                p20_pop::numeric/st_area(i.wkb_geometry::geography)*1000000 as densite,
                (st_area(st_intersection(c.wkb_geometry, i.wkb_geometry)::geography)/st_area(i.wkb_geometry::geography)*p20_pop::numeric)::int as pop_circo
            from zone_circo c
                join iris_ge i on (st_intersects(c.wkb_geometry, i.wkb_geometry))
                join insee_pop_{annee} p on (p.iris=i.code_iris)
            where ref=%s) p
        WHERE densite>=%s AND densite<%s
        """,
                (self.circo, 300, 1500),
            )
        )
        densite_intermediaire = self.db.fetchone()
        if annee == "2020":
            self.data.population.densite_intermediaire_2020 = densite_intermediaire[0]
        else:
            raise ValueError("Annee non gérée")

        utilitaires.db_exec(
            self.db.mogrify(
                f"""
        SELECT coalesce(sum(pop_circo),0)
        FROM (
            select
                p.iris,
                p20_pop,
                p20_pop::numeric/st_area(i.wkb_geometry::geography)*1000000 as densite,
                (st_area(st_intersection(c.wkb_geometry, i.wkb_geometry)::geography)/st_area(i.wkb_geometry::geography)*p20_pop::numeric)::int as pop_circo
            from zone_circo c
                join iris_ge i on (st_intersects(c.wkb_geometry, i.wkb_geometry))
                join insee_pop_{annee} p on (p.iris=i.code_iris)
            where ref=%s) p
        WHERE densite>=%s
        """,
                (self.circo, 1500),
            )
        )
        densite_densement_peuple = self.db.fetchone()
        if annee == "2020":
            self.data.population.densite_densement_peuple_2020 = (
                densite_densement_peuple[0]
            )
        else:
            raise ValueError("Annee non gérée")
        # self.data.population.densite_densement_peuple_2018 = densite_densement_peuple[0]

        # carte densité de population
        utilitaires.db_exec(
            self.db.mogrify(
                f"""
        select
            p20_pop::numeric/st_area(i.wkb_geometry::geography)*1000000 as densite,
            st_asgeojson((st_dump(st_intersection(i.wkb_geometry,c.wkb_geometry))).geom)::json as geojson
        from zone_circo c
            join iris_ge i on (st_intersects(c.wkb_geometry, i.wkb_geometry))
            join insee_pop_{annee} p on (p.iris=i.code_iris)
        where ref=%s
        """,
                (self.circo,),
            ),
            f"carte_densite_population_{annee}",
        )
        if annee == "2020":
            self.data.population.carreaux_densite_population_2020 = [
                {"densite": r[0], "geojson": r[1]} for r in self.db.fetchall()
            ]
        elif annee == "2018":
            self.data.population.carreaux_densite_population_2018 = [
                {"densite": r[0], "geojson": r[1]} for r in self.db.fetchall()
            ]
        else:
            raise ValueError("Annee non gérée")

    def set_stat_menages(self):
        utilitaires.db_exec(
            self.db.mogrify(
                """
    select
        sum(men),
        sum(men_pauv),
        sum(men_1ind),
        sum(men_5ind),
        sum(men_fmp),
        sum(men_prop),
        sum(men_coll),
        sum(men_mais),
        sum(ind_snv)
    from insee_population_carroyee_2015 p
    join zone_circo c on (st_intersects(c.wkb_geometry,p.wkb_geometry))
    where ref=%s
    """,
                (self.circo,),
            ),
            "types_menages",
        )

        self.data.population.stats_menages_circo_2015 = utilitaires.float_list_to_dict(
            self.db.fetchone(), self.column_names_population_carroyee_2015
        )

        utilitaires.db_exec(
            self.db.mogrify(
                """
        select
            sum(men),
            sum(men_pauv),
            sum(men_1ind),
            sum(men_5ind),
            sum(men_fmp),
            sum(men_prop),
            sum(men_coll),
            sum(men_mais),
            sum(ind_snv)
        from insee_population_carroyee_2015 p
        join zone_circo c on (st_intersects(c.wkb_geometry,p.wkb_geometry))
        where ref LIKE %s
        """,
                (self.circo[0:3] + "%",),
            ),
            "types_menages_dep",
        )
        self.data.population.stats_menages_departement_2015 = (
            utilitaires.float_list_to_dict(
                self.db.fetchone(), self.column_names_population_carroyee_2015
            )
        )

        utilitaires.db_exec(
            b"""
        select
            sum(men),
            sum(men_pauv),
            sum(men_1ind),
            sum(men_5ind),
            sum(men_fmp),
            sum(men_prop),
            sum(men_coll),
            sum(men_mais),
            sum(ind_snv)
        from insee_population_carroyee_2015 p
        """,
            "types_menages_nat",
        )
        self.data.population.stats_menages_france_2015 = utilitaires.float_list_to_dict(
            self.db.fetchone(), self.column_names_population_carroyee_2015
        )

        utilitaires.db_exec(
            self.db.mogrify(
                """
        select
            sum(men),
            sum(men_pauv),
            sum(men_1ind),
            sum(men_5ind),
            sum(men_prop),
            sum(men_coll),
            sum(ind_snv)
        from insee_population_carroyee_2010 p
        join zone_circo c on (st_intersects(c.wkb_geometry,p.wkb_geometry))
        where ref=%s
        """,
                (self.circo,),
            ),
            "types_menages_2010",
        )
        self.data.population.stats_menages_circo_2010 = utilitaires.float_list_to_dict(
            self.db.fetchone(), self.column_names_population_carroyee_2010
        )

        utilitaires.db_exec(
            self.db.mogrify(
                """
        select
            sum(men),
            sum(men_pauv),
            sum(men_1ind),
            sum(men_5ind),
            sum(men_prop),
            sum(men_coll),
            sum(ind_snv)
        from insee_population_carroyee_2010 p
        join zone_circo c on (st_intersects(c.wkb_geometry,p.wkb_geometry))
        where ref LIKE %s
        """,
                (self.circo[0:3] + "%",),
            )
        )
        self.data.population.stats_menages_departement_2010 = (
            utilitaires.float_list_to_dict(
                self.db.fetchone(), self.column_names_population_carroyee_2010
            )
        )

        utilitaires.db_exec(
            b"""
        select
            sum(men),
            sum(men_pauv),
            sum(men_1ind),
            sum(men_5ind),
            sum(men_prop),
            sum(men_coll),
            sum(ind_snv)
        from insee_population_carroyee_2010 p
        """
        )

        self.data.population.stats_menages_france_2010 = utilitaires.float_list_to_dict(
            self.db.fetchone(), self.column_names_population_carroyee_2010
        )
