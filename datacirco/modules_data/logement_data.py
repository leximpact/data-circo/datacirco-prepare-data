from dataclasses import dataclass
from datacirco import utilitaires


@dataclass()
class DataLogement:
    carreaux = None
    data_logements_2018 = None
    data_logements_2020 = None
    dpe = None
    dpe_dep = None
    dpe_nat = None
    total_dpe = None
    total_dpe_dep = None
    total_dpe_nat = None
    dpe_communes = None
    dpe_communes_post_2021 = None
    ges_circo = None
    ges_dep = None
    ges_nat = None
    total_ges = None
    total_ges_dep = None
    total_ges_nat = None
    dpe_annee = None
    ges_annee = None
    sources = None


class GetLogement:
    """To retreive data from the database."""

    def __init__(self, db, data):
        """
        Args:
            db: Connection to the database
            data: A DataCirco object
        """
        self.db = db
        utilitaires.db = db
        data.logements = DataLogement()
        self.data = data
        self.dept = data.dept
        self.circo = data.circo
        self.data.logements.sources = utilitaires.get_sources("logements")

    def get_logements(self, superficie_circo):
        """Récupère la répartition des logements collectifs / individuels."""
        if superficie_circo > 400:
            utilitaires.db_exec(
                self.db.mogrify(
                    """
                    SELECT
                        st_asgeojson((st_dump(st_intersection(p.wkb_geometry,c.wkb_geometry))).geom)::json as geojson,
                        round(100.0*men_coll/men,1) as pct_logement_collectifs,
                        round(100.0*men_prop/men,1) as pct_proprietaires
                    FROM
                        insee_population_carroyee_2015_1k p
                    JOIN zone_circo c on (st_intersects(c.wkb_geometry,p.wkb_geometry))
                    WHERE ref=%s
                    """,
                    (self.circo,),
                ),
                "cartes_logement",
            )
        else:
            utilitaires.db_exec(
                self.db.mogrify(
                    """
            SELECT
                st_asgeojson((st_dump(st_intersection(p.wkb_geometry,c.wkb_geometry))).geom)::json as geojson,
                round(100.0*men_coll/men,1) as pct_logement_collectifs,
                round(100.0*men_prop/men,1) as pct_proprietaires
            FROM
                insee_population_carroyee_2015 p
            JOIN zone_circo c on (st_intersects(c.wkb_geometry,p.wkb_geometry))
            WHERE ref=%s
            """,
                    (self.circo,),
                ),
                "cartes_logement",
            )
        self.data.logements.carreaux = [
            {
                "geojson": row[0],
                "pct_logement_collectifs": float(row[1]),
                "pct_proprietaires": float(row[2]),
            }
            for row in self.db.fetchall()
        ]

        if len(self.data.logements.carreaux) == 0:
            utilitaires.db_exec(
                self.db.mogrify(
                    """
            select
                st_asgeojson((st_dump(st_intersection(i.wkb_geometry,c.wkb_geometry))).geom)::json as geojson,
                round(100*(p20_log::numeric-p20_maison::numeric)/p20_log::numeric,1) as pct_logement_collectifs,
                round(100 * P20_RP_PROP::numeric / P20_RP::numeric,1) as pct_proprietaires,
                p20_log::numeric as nb_logements,
                p20_maison as nb_logements_individuels,
                p20_RP::numeric as nb_residences_principales,
                p20_RP_prop::numeric as nb_rp_prop,
                P20_LOGVAC::numeric as nb_logements_vacants,
                P20_RSECOCC::numeric as nb_residences_secondaires_ou_occ
            from zone_circo c
                join iris_ge i on (st_intersects(c.wkb_geometry, i.wkb_geometry))
                join insee_logement_2020 p on (p.iris=i.code_iris)
            where ref=%s
            """,
                    (self.circo,),
                ),
                "cartes_logement",
            )
            self.data.logements.carreaux = [
                {
                    "geojson": row[0],
                    "pct_logement_collectifs": float(row[1]),
                    "pct_proprietaires": float(row[2]),
                    "nb_logements": int(row[3]),
                    "nb_logements_individuels": float(row[4]),
                    "nb_residences_principales": float(row[5]),
                    "nb_rp_prop": int(row[6]),
                    "nb_logements_vacants": int(row[7]),
                    "nb_residences_secondaires_ou_occ": int(row[8]),
                }
                for row in self.db.fetchall()
            ]

    def get_descriptif_logement(self):
        # descriptif des données de 2019
        # mfuel est null pour '971-03'
        utilitaires.db_exec(
            self.db.mogrify(
                f""" SELECT log_res::numeric, log_sec::numeric, log_vac::numeric,
                proprio::numeric, locatai::numeric, gratuit::numeric, maison::numeric, ach90::numeric, COALESCE(mfuel::numeric, 0) as mfuel
                FROM insee_indic_stat_circo_2022 WHERE circo in ('{self.circo}', '000-00')
                ORDER BY CASE circo
                WHEN '{self.circo}' THEN 1
                WHEN '000-00' THEN 2
                END;"""
            )
        )
        data_logements_2018 = dict()
        liste = [
            dict(
                log_res=float(row[0]),
                log_sec=float(row[1]),
                log_vac=float(row[2]),
                proprio=float(row[3]),
                locatai=float(row[4]),
                gratuit=float(row[5]),
                maison=float(row[6]),
                ach90=float(row[7]),
                mfuel=float(row[8]),
            )
            for row in self.db.fetchall()
        ]
        data_logements_2018["circonscription"] = liste[0]
        data_logements_2018["national"] = liste[1]
        self.data.logements.data_logements_2018 = data_logements_2018

        # descriptif des données de 2020 :
        # à l'échelle de la circonscription :
        utilitaires.db_exec(
            self.db.mogrify(
                f"""SELECT SUM(p20_log::numeric*st_area(st_intersection(iris.wkb_geometry, circo.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography)),
            SUM(P20_RP::numeric*st_area(st_intersection(iris.wkb_geometry,circo.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography)),
            SUM(P20_RSECOCC::numeric*st_area(st_intersection(iris.wkb_geometry,circo.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography)),
            SUM(P20_RPMAISON::numeric*st_area(st_intersection(iris.wkb_geometry,circo.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography)),
            SUM(P20_RPAPPART::numeric*st_area(st_intersection(iris.wkb_geometry,circo.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography)),
            SUM(P20_RP_PROP::numeric*st_area(st_intersection(iris.wkb_geometry,circo.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography)),
            SUM(P20_RP_LOC::numeric*st_area(st_intersection(iris.wkb_geometry,circo.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography)),
            SUM(P20_RP_GRAT::numeric*st_area(st_intersection(iris.wkb_geometry,circo.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography)),
            SUM(P20_LOGVAC::numeric*st_area(st_intersection(iris.wkb_geometry,circo.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography)),
            SUM((P20_RP_ACH19::numeric + P20_RP_ACH45::numeric + P20_RP_ACH70::numeric + P20_RP_ACH90::numeric)*st_area(st_intersection(iris.wkb_geometry,circo.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography))
            FROM insee_logement_2020  AS log 
            JOIN iris_ge AS iris ON iris.code_iris = log.iris
            JOIN zone_circo AS circo ON st_intersects(iris.wkb_geometry, circo.wkb_geometry)
            WHERE log.P20_RSECOCC IS NOT NULL AND circo.ref = '{self.circo}';"""
            )
        )
        valeurs_circo = [float(i) for i in self.db.fetchone()]

        # à l'échelle du dépatrtement :
        utilitaires.db_exec(
            self.db.mogrify(
                f"""SELECT SUM(p20_log::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography)),
                    SUM(P20_RP::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography)),
                    SUM(P20_RSECOCC::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography)),
                    SUM(P20_RPMAISON::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography)),
                    SUM(P20_RPAPPART::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography)),
                    SUM(P20_RP_PROP::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography)),
                    SUM(P20_RP_LOC::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography)),
                    SUM(P20_RP_GRAT::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography)),
                    SUM(P20_LOGVAC::numeric*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography)),
                    SUM((P20_RP_ACH19::numeric + P20_RP_ACH45::numeric + P20_RP_ACH70::numeric + P20_RP_ACH90::numeric)*st_area(st_intersection(iris.wkb_geometry, dep.wkb_geometry)::geography)/st_area(iris.wkb_geometry::geography))
                            FROM insee_logement_2020  AS log 
                            JOIN iris_ge AS iris ON iris.code_iris = log.iris
                            JOIN ign_departement AS dep ON st_intersects(iris.wkb_geometry, dep.wkb_geometry)
                            WHERE log.P20_RSECOCC IS NOT NULL AND dep.insee_dep = '{self.dept}';"""
            )
        )
        valeurs_dept = [float(i) for i in self.db.fetchone()]

        # à l'échelle nationale
        utilitaires.db_exec(
            self.db.mogrify(
                """SELECT SUM(p20_log::numeric),
                    SUM(P20_RP::numeric),
                    SUM(P20_RSECOCC::numeric),
                    SUM(P20_RPMAISON::numeric),
                    SUM(P20_RPAPPART::numeric),
                    SUM(P20_RP_PROP::numeric),
                    SUM(P20_RP_LOC::numeric),
                    SUM(P20_RP_GRAT::numeric),
                    SUM(P20_LOGVAC::numeric),
                    SUM((P20_RP_ACH19::numeric + P20_RP_ACH45::numeric + P20_RP_ACH70::numeric + P20_RP_ACH90::numeric))
                            FROM insee_logement_2020  AS log 
                            JOIN iris_ge AS iris ON iris.code_iris = log.iris
                            JOIN ign_departement AS dep ON st_intersects(iris.wkb_geometry, dep.wkb_geometry);"""
            )
        )
        valeurs_nat = [float(i) for i in self.db.fetchone()]
        liste_echelle = ["circonscription", "departement", "national"]
        dictionnaire_final = dict()

        for i, liste in enumerate([valeurs_circo, valeurs_dept, valeurs_nat]):
            dico = {
                "nb_logements": liste[0],
                "nb_residences_principales": liste[1],
                "nb_residences_secondaires": liste[2],
                "nb_maisons": liste[3],
                "nb_appart": liste[4],
                "nb_propretaires": liste[5],
                "nb_locataires": liste[6],
                "nb_gratuit": liste[7],
                "nb_vacants": liste[8],
                "nb_construits_avant90": liste[9],
            }
            dictionnaire_final[liste_echelle[i]] = dico

        self.data.logements.data_logements_2020 = dictionnaire_final

    def get_dpe(self, in_communes):
        """Récupère le diagnostic de performance énergétique (DPE) d’un
        logement."""
        print(self.circo)
        utilitaires.db_exec(
            self.db.mogrify(
                f"""
            SELECT enum.etiquette_dpe as classe_consommation_energie, coalesce(compte.nb_dpe, 0) as nb
            FROM (
                SELECT DISTINCT etiquette_dpe
                FROM ademe_dpe_logements_2024
                GROUP BY 1) as enum
                NATURAL LEFT JOIN (
                    SELECT d.etiquette_dpe, count(DISTINCT d.ndpe) as nb_dpe
                    FROM ademe_dpe_logements_2024 as d
                    LEFT JOIN zone_circo c
                    ON st_intersects(c.wkb_geometry, d.geom)
                    WHERE c.ref = '{self.circo}' AND CAST(d.date_etablissement_dpe as VARCHAR) NOT LIKE '2024%'
                    AND d.etiquette_dpe between 'A' and 'G'
                    GROUP BY 1
                    ) as compte
                ORDER BY 1;"""
            )
        )
        self.data.logements.dpe = [
            {
                "classe_consommation_energie": row[0],
                "nb": int(row[1]),
            }
            for row in self.db.fetchall()
        ]

        if len(self.dept) == 3:
            departement = self.dept
        else:
            departement = "0" + self.dept
        utilitaires.db_exec(
            self.db.mogrify(
                f"""SELECT enum.etiquette_dpe as classe_consommation_energie, coalesce(compte.nb_dpe, 0) as sum
                FROM (
                    SELECT DISTINCT etiquette_dpe
                    FROM ademe_dpe_logements_2024
                    GROUP BY 1) as enum
                NATURAL LEFT JOIN (
                    SELECT d.etiquette_dpe, count(DISTINCT d.ndpe) as nb_dpe
                    FROM ademe_dpe_logements_2024 as d
                    LEFT JOIN zone_circo c
                    ON st_intersects(c.wkb_geometry, d.geom)
                    WHERE LPAD(RTRIM(RTRIM(CAST(d.n_departement__ban_ AS VARCHAR),'.'),'.0'),3,'0') = '{departement}'
                    AND CAST(d.date_etablissement_dpe as VARCHAR) NOT LIKE '2024%'
                    AND d.etiquette_dpe between 'A' and 'G'
                    GROUP BY d.etiquette_dpe) as compte;"""
            )
        )
        self.data.logements.dpe_dep = [
            {
                "classe_consommation_energie": row[0],
                "nb": int(row[1]),
            }
            for row in self.db.fetchall()
        ]

        utilitaires.db_exec(
            self.db.mogrify(
                """
                SELECT *
                FROM ademe_dpe_france_2024;"""
            )
        )
        self.data.logements.dpe_nat = [
            {
                "classe_consommation_energie": row[0],
                "nb": int(row[1]),
            }
            for row in self.db.fetchall()
        ]

        if len(self.data.logements.dpe) > 4 and len(self.data.logements.dpe_dep) > 4:
            # Totaux pour pourcentages
            self.data.logements.total_dpe = 0
            for d in self.data.logements.dpe:
                self.data.logements.total_dpe += d["nb"]

            self.data.logements.total_dpe_dep = 0
            for d in self.data.logements.dpe_dep:
                self.data.logements.total_dpe_dep += d["nb"]

            self.data.logements.total_dpe_nat = 0
            for d in self.data.logements.dpe_nat:
                self.data.logements.total_dpe_nat += d["nb"]

    def get_dpe_communes(self, in_communes):
        """Récupère la répartition des notes DPE sur les communes couvertes au
        moins partiellement par la circonscription."""
        self.data.logements.dpe_communes = None
        if len(self.data.logements.dpe) > 4 and len(self.data.logements.dpe_dep) > 4:
            sql1 = f"""
                SELECT classe_consommation_energie, 
                    annee_2013, annee_2014, annee_2015, 
                    annee_2016, annee_2017, annee_2018,
                    annee_2019, annee_2020, annee_2021
                FROM dpe_stats_by_circo
                WHERE circonscription = '{self.circo}'
                ORDER BY classe_consommation_energie;
                """
            utilitaires.db_exec(self.db.mogrify(sql1))
            self.data.logements.dpe_communes = [
                {
                    "classe": row[0],
                    "2013": utilitaires.to_int(0 if row[1] is None else row[1]),
                    "2014": utilitaires.to_int(0 if row[2] is None else row[2]),
                    "2015": utilitaires.to_int(0 if row[3] is None else row[3]),
                    "2016": utilitaires.to_int(0 if row[4] is None else row[4]),
                    "2017": utilitaires.to_int(0 if row[5] is None else row[5]),
                    "2018": utilitaires.to_int(0 if row[6] is None else row[6]),
                    "2019": utilitaires.to_int(0 if row[7] is None else row[7]),
                    "2020": utilitaires.to_int(0 if row[8] is None else row[8]),
                    "2021": utilitaires.to_int(0 if row[9] is None else row[9]),
                }
                for row in self.db.fetchall()
            ]
        sql2 = f"""SELECT classe_consommation_energie, coalesce(a.annee_2021, 0) as annee_2021, coalesce(a.annee_2022, 0) as annee_2022, coalesce(a.annee_2023, 0) as annee_2023 FROM (
                select * from crosstab(
                    $$SELECT d.etiquette_dpe, LEFT(d.date_etablissement_dpe,4) as annee_etablissement_DPE, coalesce(count(DISTINCT d.ndpe), 0) as nb_dpe
                    FROM ademe_dpe_logements_2024 as d
                    LEFT JOIN zone_circo c
                    ON st_intersects(c.wkb_geometry, d.geom)
                    WHERE c.ref = '{self.circo}'
                    AND CAST(d.date_etablissement_dpe as VARCHAR) NOT LIKE '2024%'
                    AND d.etiquette_dpe between 'A' and 'G'
                    GROUP BY 1,2 ORDER BY 1,2$$,
                    $$SELECT generate_series(2021,2023)$$
                ) as T (
                    classe_consommation_energie text,
                    "annee_2021" int,
                    "annee_2022" int,
                    "annee_2023" int
                )) as a
                ORDER BY 1;
                """
        utilitaires.db_exec(self.db.mogrify(sql2))
        self.data.logements.dpe_post_2021 = [
            {
                "classe": row[0],
                "2021": utilitaires.to_int(0 if row[1] is None else row[1]),
                "2022": utilitaires.to_int(0 if row[2] is None else row[2]),
                "2023": utilitaires.to_int(0 if row[3] is None else row[3]),
            }
            for row in self.db.fetchall()
        ]

        liste_avant_2021 = self.data.logements.dpe_communes
        liste_apres_2021 = self.data.logements.dpe_post_2021

        resultat = []
        index_liste1 = {d["classe"]: d for d in liste_apres_2021}
        for classe_avant_2021 in liste_avant_2021:
            id_valeur = classe_avant_2021["classe"]
            if id_valeur in index_liste1:
                # Si la clé existe dans les deux listes, fusionner les dictionnaires
                valeur_2021_avant = int(classe_avant_2021.get("2021"))
                valeur_2021_apres = int(index_liste1[id_valeur].get("2021"))
                dictionnaire_fusionne = {**index_liste1[id_valeur], **classe_avant_2021}
                dictionnaire_fusionne["2021"] = valeur_2021_avant + valeur_2021_apres
                dictionnaire_fusionne["annee_2021_avant"] = valeur_2021_avant
                dictionnaire_fusionne["annee_2021_apres"] = valeur_2021_apres
                resultat.append(dictionnaire_fusionne)
            else:
                # Si la clé n'existe que dans la deuxième liste, ajouter le dictionnaire tel quel
                resultat.append(classe_avant_2021)
        self.data.logements.dpe_annee = resultat

    def get_emission_ges(self, in_communes):
        utilitaires.db_exec(
            self.db.mogrify(
                f"""
            SELECT enum.etiquette_ges as classe_ges, coalesce(compte.nb_ges, 0) as nb
            FROM (
                SELECT DISTINCT etiquette_ges
                FROM ademe_dpe_logements_2024
                GROUP BY 1) as enum
                NATURAL LEFT JOIN (
                    SELECT d.etiquette_ges, count(DISTINCT d.ndpe) as nb_ges
                    FROM ademe_dpe_logements_2024 as d
                    LEFT JOIN zone_circo c
                    ON st_intersects(c.wkb_geometry, d.geom)
                    WHERE c.ref = '{self.circo}'
                    AND CAST(d.date_etablissement_dpe as VARCHAR) NOT LIKE '2024%'
                    AND d.etiquette_ges IN ('A', 'B', 'C', 'D', 'E', 'F', 'G')
                    GROUP BY 1
                    ) as compte
                ORDER BY 1;"""
            )
        )
        self.data.logements.ges_circo = [
            {"classe_ges": row[0], "nb": utilitaires.to_int(row[1])}
            for row in self.db.fetchall()
        ]

        if len(self.dept) == 3:
            departement = self.dept
        else:
            departement = "0" + self.dept
        utilitaires.db_exec(
            self.db.mogrify(
                f"""SELECT enum.etiquette_ges as classe_ges, coalesce(compte.nb_ges, 0) as sum
                FROM (
                    SELECT DISTINCT etiquette_ges
                    FROM ademe_dpe_logements_2024
                    GROUP BY 1) as enum
                NATURAL LEFT JOIN (
                    SELECT d.etiquette_ges, count(DISTINCT d.ndpe) as nb_ges
                    FROM ademe_dpe_logements_2024 as d
                    LEFT JOIN zone_circo c
                    ON st_intersects(c.wkb_geometry, d.geom)
                    WHERE LPAD(RTRIM(RTRIM(CAST(d.n_departement__ban_ AS VARCHAR),'.'),'.0'),3,'0') = '{departement}'
                    AND CAST(d.date_etablissement_dpe as VARCHAR) NOT LIKE '2024%'
                    AND d.etiquette_ges IN ('A', 'B', 'C', 'D', 'E', 'F', 'G')
                    GROUP BY d.etiquette_ges) as compte;"""
            )
        )
        self.data.logements.ges_dep = [
            {
                "classe_ges": row[0],
                "nb": int(row[1]),
            }
            for row in self.db.fetchall()
        ]

        utilitaires.db_exec(
            self.db.mogrify(
                """
                SELECT *
                FROM ademe_ges_france_2024;"""
            )
        )
        self.data.logements.ges_nat = [
            {
                "classe_ges": row[0],
                "nb": int(row[1]),
            }
            for row in self.db.fetchall()
        ]

        if len(self.data.logements.dpe) > 4 and len(self.data.logements.dpe_dep) > 4:
            # Totaux pour pourcentages
            self.data.logements.total_ges = 0
            for d in self.data.logements.ges_circo:
                self.data.logements.total_ges += d["nb"]

            self.data.logements.total_ges_dep = 0
            for d in self.data.logements.ges_dep:
                self.data.logements.total_ges_dep += d["nb"]

            self.data.logements.total_ges_nat = 0
            for d in self.data.logements.ges_nat:
                self.data.logements.total_ges_nat += d["nb"]

        utilitaires.db_exec(
            self.db.mogrify(
                f"""SELECT classe_ges, coalesce(a.annee_2021, 0) as annee_2021, coalesce(a.annee_2022, 0) as annee_2022, coalesce(a.annee_2023, 0) as annee_2023 FROM (
                select * from crosstab(
                    $$SELECT d.etiquette_ges, LEFT(d.date_etablissement_dpe,4) as annee_etablissement_GES, coalesce(count(DISTINCT d.ndpe), 0) as nb_dpe
                    FROM ademe_dpe_logements_2024 as d
                    LEFT JOIN zone_circo c
                    ON st_intersects(c.wkb_geometry, d.geom)
                    WHERE c.ref = '{self.circo}'
                    AND CAST(d.date_etablissement_dpe as VARCHAR) NOT LIKE '2024%'
                    AND d.etiquette_ges IN ('A', 'B', 'C', 'D', 'E', 'F', 'G')
                    GROUP BY 1,2 ORDER BY 1,2$$,
                    $$SELECT generate_series(2021,2023)$$
                ) as T (
                    classe_ges text,
                    "annee_2021" int,
                    "annee_2022" int,
                    "annee_2023" int
                )) as a
                ORDER BY 1;
                """
            )
        )

        liste_ges_apres_2021 = [
            {
                "classe": row[0],
                "2021": utilitaires.to_int(row[1]),
                "2022": utilitaires.to_int(row[2]),
                "2023": utilitaires.to_int(row[3]),
            }
            for row in self.db.fetchall()
        ]

        if len(self.data.logements.dpe) > 4 and len(self.data.logements.dpe_dep) > 4:
            sql1 = f"""
            SELECT classe_ges,
                coalesce(annee_2013, 0) as annee_2013, coalesce(annee_2014, 0) as annee_2014, coalesce(annee_2015, 0) as annee_2015,
                        coalesce(annee_2016, 0) as annee_2016, coalesce(annee_2017, 0) as annee_2017, coalesce(annee_2018, 0) as annee_2018,
                        coalesce(annee_2019, 0) as annee_2019, coalesce(annee_2020, 0) as annee_2020, coalesce(annee_2021, 0) as annee_2021
            FROM crosstab(
                $$SELECT 
                    classe_estimation_ges,
                    annee,
                    SUM(nb_ges)
                FROM mv_dpe_stats_2013_2021
                WHERE circonscription = '{self.circo}'
                GROUP BY 1,2
                ORDER BY 1,2$$,
                $$SELECT generate_series(2013,2021)$$
            ) AS T (
                                classe_ges text,
                                "annee_2013" int,
                                "annee_2014" int,
                                "annee_2015" int,
                                "annee_2016" int,
                                "annee_2017" int,
                                "annee_2018" int,
                                "annee_2019" int,
                                "annee_2020" int,
                                "annee_2021" int
                            );
                            """
        utilitaires.db_exec(self.db.mogrify(sql1))
        liste_ges_avant_2021 = [
            {
                "classe": row[0],
                "2013": utilitaires.to_int(0 if row[1] is None else row[1]),
                "2014": utilitaires.to_int(0 if row[2] is None else row[2]),
                "2015": utilitaires.to_int(0 if row[3] is None else row[3]),
                "2016": utilitaires.to_int(0 if row[4] is None else row[4]),
                "2017": utilitaires.to_int(0 if row[5] is None else row[5]),
                "2018": utilitaires.to_int(0 if row[6] is None else row[6]),
                "2019": utilitaires.to_int(0 if row[7] is None else row[7]),
                "2020": utilitaires.to_int(0 if row[8] is None else row[8]),
                "2021": utilitaires.to_int(0 if row[9] is None else row[9]),
            }
            for row in self.db.fetchall()
        ]

        resultat_ges = []
        index_liste1 = {d["classe"]: d for d in liste_ges_apres_2021}
        for classe_avant_2021 in liste_ges_avant_2021:
            id_valeur = classe_avant_2021["classe"]
            if id_valeur in index_liste1:
                # Si la clé existe dans les deux listes, fusionner les dictionnaires
                valeur_2021_avant = int(classe_avant_2021.get("2021"))
                valeur_2021_apres = int(index_liste1[id_valeur].get("2021"))
                dictionnaire_fusionne = {**index_liste1[id_valeur], **classe_avant_2021}
                dictionnaire_fusionne["2021"] = valeur_2021_avant + valeur_2021_apres
                dictionnaire_fusionne["annee_2021_avant"] = valeur_2021_avant
                dictionnaire_fusionne["annee_2021_apres"] = valeur_2021_apres
                resultat_ges.append(dictionnaire_fusionne)
            else:
                # Si la clé n'existe que dans la deuxième liste, ajouter le dictionnaire tel quel
                resultat_ges.append(classe_avant_2021)
        self.data.logements.ges_annee = resultat_ges
