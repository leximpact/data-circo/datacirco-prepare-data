import staticmaps
from staticmaps.tile_provider import TileProvider

from datacirco import utilitaires

""" UNUSED
tile_provider_FR = TileProvider(
    "osmfr",
    url_pattern="https://$s.tile.openstreetmap.fr/osmfr/$z/$x/$y.png",
    shards=["a", "b", "c"],
    attribution="© OpenStreetMap / OSM-FR",
    max_zoom=20,
)
# tile_provider_forte = tile_provider_FR
# tile_provider_piano = tile_provider_FR
"""
tile_provider_forte = TileProvider(
    "forte",
    url_pattern="https://$s.forte.tiles.quaidorsay.fr/fr/$z/$x/$y.png",
    shards=["a", "b", "c"],
    attribution="© OpenStreetMap / quaidorsay.fr",
    max_zoom=18,
)
tile_provider_piano = TileProvider(
    "piano",
    url_pattern="https://$s.piano.tiles.quaidorsay.fr/fr/$z/$x/$y.png",
    shards=["a", "b", "c"],
    attribution="© OpenStreetMap / quaidorsay.fr",
    max_zoom=18,
)


class Carto:
    data = None

    def __init__(self, data):
        self.data = data

    def evolution_population(self):
        """Carte de l'évolution de la population de la circo."""
        carte = staticmaps.Context()
        carte.set_tile_provider(tile_provider_piano)
        carreaux = self.data.population.carreaux_evolution_population_2010_vs_2015
        assert carreaux is not None
        for carreau in carreaux:
            if carreau["pop_2010"] == carreau["pop_2015"]:
                color = staticmaps.parse_color("#88888880")  # gris
            elif carreau["pop_2010"] > carreau["pop_2015"] * 1.10:
                color = staticmaps.parse_color("#0000ff80")  # bleu
            elif carreau["pop_2010"] > carreau["pop_2015"]:
                color = staticmaps.parse_color("#0000ff40")  # bleu
            elif carreau["pop_2010"] * 1.10 < carreau["pop_2015"]:
                color = staticmaps.parse_color("#ff000080")  # rouge
            elif carreau["pop_2010"] < carreau["pop_2015"]:
                color = staticmaps.parse_color("#ff000040")  # rouge
            carte.add_object(
                staticmaps.Area(
                    [
                        staticmaps.create_latlng(lat, lng)
                        for lng, lat in carreau["geojson"]["coordinates"][0]
                    ],
                    fill_color=color,
                    width=0,
                    color=staticmaps.BLACK,
                )
            )
        # contour de la circo
        utilitaires.draw_poly(carte, self.data.geo)
        try:
            image = carte.render_cairo(900, 800)
            image.write_to_png("circo_evolution_population.png")
        except RuntimeError as e:
            raise e

    def densite_population(self):
        """Carreaux de densité de population."""
        if self.data.population.carreaux_densite_population_2020:
            carreaux = self.data.population.carreaux_densite_population_2020
        else:
            raise ValueError("Pas de données de densité de population pour 2020 !")
        # elif self.data.population.carreaux_densite_population_2018:
        #     carreaux = self.data.population.carreaux_densite_population_2018
        # else:
        #     carreaux = self.data.population.carreaux_densite_population_2015
        carte = staticmaps.Context()
        carte.set_tile_provider(tile_provider_piano)
        for carreau in carreaux:
            if carreau["densite"] < 25:
                color = staticmaps.parse_color("#88888880")  # gris
            elif carreau["densite"] < 300:
                color = staticmaps.parse_color("#0000ff80")  # bleu
            elif carreau["densite"] < 1500:
                color = staticmaps.parse_color("#ff00ff80")  # mauve
            else:
                color = staticmaps.parse_color("#ff000080")  # rouge
            carte.add_object(
                staticmaps.Area(
                    [
                        staticmaps.create_latlng(lat, lng)
                        for lng, lat in carreau["geojson"]["coordinates"][0]
                    ],
                    fill_color=color,
                    width=0,
                    color=staticmaps.BLACK,
                )
            )
        # contour de la circo
        utilitaires.draw_poly(carte, self.data.geo)
        try:
            image = carte.render_cairo(900, 900)
            image.write_to_png("circo_densite.png")
        except RuntimeError as e:
            raise e
