import psycopg2
import psycopg2.extras
from sqlalchemy import create_engine
from sqlalchemy import URL as sqlalchemy_URL, Table, MetaData, String, Column, DateTime
from decouple import config
import sys

# Pour debug des requêtes SQL
# import logging
# logging.basicConfig()
# logging.getLogger('sqlalchemy.engine').setLevel(logging.DEBUG)

db_host = config("DATABASE_HOST", default="postgres_datacirco")
db_name = config("DATABASE_NAME", default="infocirco")
db_pass = config("DATABASE_PASS", default="datacirco")
db_port = config("DATABASE_PORT", default="5432")
db_user = config("DATABASE_USER", default="datacirco")


# connexion à la base postgres
database_command_line = None
try:
    # Local connection
    pg = psycopg2.connect(
        dbname=db_name,
    )
    db_url = sqlalchemy_URL.create(
        "postgresql",
        database=db_name,
    )
    engine = create_engine(db_url, client_encoding="utf-8")
    print("DB Connected with name only", file=sys.stderr)
    database_command_line = f"psql -d {db_name}"
except psycopg2.OperationalError:
    try:
        pg = psycopg2.connect(
            dbname=db_name,
            user=db_user,
            password=db_pass,
            host=db_host,
            port=db_port,
        )
        db_url = f"postgresql://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}"
        engine = create_engine(db_url, client_encoding="utf-8")
        print("DB Connected with env variable", file=sys.stderr)
    except psycopg2.OperationalError:
        # We try the outside Docker config
        db_name = "infocirco"
        db_user = "datacirco"
        db_pass = "datacirco"
        db_host = "localhost"
        db_port = "5491"
        pg = psycopg2.connect(
            dbname=db_name,
            user=db_user,
            password=db_pass,
            host=db_host,
            port=db_port,
        )
        db_url = f"postgresql://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}"
        engine = create_engine(db_url, client_encoding="utf-8", echo=True)
        print(
            "DB Connected to localhost with default docker parameters", file=sys.stderr
        )

metadata = MetaData()

dataset_source = Table(
    "dataset_source",
    metadata,
    Column("filename", String, primary_key=True),
    Column("module", String),
    Column("description", String),
    Column("producteur", String),
    Column("version", String),
    Column("licence", String),
    Column("url", String),
    Column("url_data", String),
    Column("last_modified", DateTime),
    Column("date_download", DateTime),
    Column("id_reference", String),
)


# Check db is connected
with engine.connect() as con:
    if database_command_line is None:
        database_command_line = f"PGPASSWORD={db_pass} psql -h {con.engine.url.host} -p {con.engine.url.port} -U {con.engine.url.username} -d {con.engine.url.database}"
    print(
        f"INFO : Connected to database {con.engine.url.database} on the host {con.engine.url.host}:{con.engine.url.port}",
        file=sys.stderr,
    )
    pass

pg.set_client_encoding("UTF8")
db = pg.cursor()


def run(sql):
    """Execute sql command."""
    db.execute(db.mogrify(sql))
    pg.commit()


if __name__ == "__main__":
    print(database_command_line)
