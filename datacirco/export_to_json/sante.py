from datacirco.modules_data.sante_data import GetSante
from datacirco import utilitaires
from datacirco.map import tile_provider_piano
import staticmaps
import json


def export_sante(db, data, in_communes, geo):
    """Génère le fichier json concernant la santé.

    Args:
        db (_type_): Objet de connexion
        data: Données
        in_communes: Communes
        geo: Geo
    """
    sante = GetSante(db, data)
    sante.get_population_totale()
    sante.get_population(in_communes)
    sante.get_etab_sante(in_communes)
    sante.get_finess(in_communes)
    sante.get_equip_sante(in_communes)
    sante.get_prof_sante(in_communes)
    sante.get_prox_urgence(in_communes)

    # TODO: A retirer en septembre 2024 quand la carte Leaflet sera en prod dans les PDF.
    carte = staticmaps.Context()
    carte.set_tile_provider(tile_provider_piano)
    for urgence in sante.data.sante.carte_urgences:
        geom = json.loads(urgence["geojson"])
        if urgence["type_equip"] == "MCS":
            color = staticmaps.parse_color("#ff000080")  # rouge foncé
        elif urgence["temps"] == 0:
            color = staticmaps.parse_color("#0000ff80")  # bleu foncé
        elif (
            urgence["type_equip"] == "SU_SMUR"
            and urgence["temps"] is not None
            and urgence["temps"] <= 25
        ):
            color = staticmaps.parse_color("#0000ff40")  # bleu
        elif urgence["type_equip"] == "SU_SMUR":
            color = staticmaps.parse_color("#0000ff20")  # bleu clair
        elif urgence["temps"] > 25:
            color = staticmaps.parse_color("#ff000040")  # rouge
        else:
            color = staticmaps.parse_color("#ff000020")  # rouge clair
        utilitaires.draw_poly(carte, [geom], color, 0)
    # contour de la circo
    utilitaires.draw_poly(carte, geo)
    try:
        image = carte.render_cairo(900, 800)
        image.write_to_png("circo_access_urgences.png")
    except RuntimeError as e:
        raise e

    return sante.data.sante.__dict__
