from datacirco.modules_data.environnement_data import GetEnvironnement


def export_environnement(db, data, in_communes):
    """Génère le fichier json concernant l'environnement.

    Args:
        db (_type_): Objet de connexion
        data: Données
        in_communes: Communes
    """
    environnement = GetEnvironnement(db, data)
    # in_communes = environnement.data.in_communes
    environnement.get_sites_pollues()
    environnement.get_occupation_sols()
    environnement.get_artificialisation(in_communes)
    environnement.get_valeurs_graphiques_artif(in_communes)
    # environnement.get_argiles(in_communes)
    return environnement.data.environnement.__dict__
