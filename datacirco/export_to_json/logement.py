from datacirco.modules_data.logement_data import GetLogement
from datacirco import utilitaires
from datacirco.map import tile_provider_piano
import staticmaps


def export_logement(db, data, in_communes, superficie_circo, geo):
    """Génère le fichier json concernant le logement.

    Args:
        db (_type_): Objet de connexion
        data: Données
        in_communes: Communes
        superficie_circo: Superficie de la circonscription
        geo: Geo
    """

    logement = GetLogement(db, data)
    logement.get_logements(superficie_circo)
    logement.get_descriptif_logement()
    logement.get_dpe(in_communes)
    logement.get_dpe_communes(in_communes)
    logement.get_emission_ges(in_communes)
    # création d'une carte statique pour les données carroyées
    carte = staticmaps.Context()
    carte.set_tile_provider(tile_provider_piano)
    for carreau in logement.data.logements.carreaux:
        if carreau["pct_logement_collectifs"] < 25:
            color = staticmaps.parse_color("#0000ff80")  # bleu
        elif carreau["pct_logement_collectifs"] < 50:
            color = staticmaps.parse_color("#0000ff40")  # bleu clair
        elif carreau["pct_logement_collectifs"] < 75:
            color = staticmaps.parse_color("#ff000040")  # rouge clair
        else:
            color = staticmaps.parse_color("#ff000080")  # rouge
        carte.add_object(
            staticmaps.Area(
                [
                    staticmaps.create_latlng(lat, lng)
                    for lng, lat in carreau["geojson"]["coordinates"][0]
                ],
                fill_color=color,
                width=0,
                color=staticmaps.BLACK,
            )
        )

    # contour de la circo
    utilitaires.draw_poly(carte, geo)
    try:
        image = carte.render_cairo(900, 900)
        image.write_to_png("circo_logement_collectif.png")
    except RuntimeError as e:
        raise e

    carte = staticmaps.Context()
    carte.set_tile_provider(tile_provider_piano)
    for carreau in logement.data.logements.carreaux:
        if carreau["pct_proprietaires"] < 25:
            color = staticmaps.parse_color("#ff000080")  # bleu
        elif carreau["pct_proprietaires"] < 50:
            color = staticmaps.parse_color("#ff000040")  # bleu clair
        elif carreau["pct_proprietaires"] < 75:
            color = staticmaps.parse_color("#0000ff40")  # rouge clair
        else:
            color = staticmaps.parse_color("#0000ff80")  # rouge
        carte.add_object(
            staticmaps.Area(
                [
                    staticmaps.create_latlng(lat, lng)
                    for lng, lat in carreau["geojson"]["coordinates"][0]
                ],
                fill_color=color,
                width=0,
                color=staticmaps.BLACK,
            )
        )

    # contour de la circo
    utilitaires.draw_poly(carte, geo)
    try:
        image = carte.render_cairo(900, 900)
        image.write_to_png("circo_logement_locataires.png")
    except RuntimeError as e:
        raise e

    if (
        len(logement.data.logements.carreaux) > 0
        and len(logement.data.logements.carreaux[0]) > 4
    ):
        # carte résidences secondaires
        carte = staticmaps.Context()
        carte.set_tile_provider(tile_provider_piano)
        for carreau in logement.data.logements.carreaux:
            if (
                carreau["nb_residences_secondaires_ou_occ"]
                / carreau["nb_logements"]
                * 100
                < 5
            ):
                color = staticmaps.parse_color("#ff000080")  # bleu
            elif (
                carreau["nb_residences_secondaires_ou_occ"]
                / carreau["nb_logements"]
                * 100
            ):
                color = staticmaps.parse_color("#ff000040")  # bleu clair
            elif (
                carreau["nb_residences_secondaires_ou_occ"]
                / carreau["nb_logements"]
                * 100
            ):
                color = staticmaps.parse_color("#0000ff40")  # rouge clair
            else:
                color = staticmaps.parse_color("#0000ff80")  # rouge
            carte.add_object(
                staticmaps.Area(
                    [
                        staticmaps.create_latlng(lat, lng)
                        for lng, lat in carreau["geojson"]["coordinates"][0]
                    ],
                    fill_color=color,
                    width=0,
                    color=staticmaps.BLACK,
                )
            )
        # contour de la circo
        utilitaires.draw_poly(carte, geo)
        try:
            image = carte.render_cairo(900, 900)
            image.write_to_png("circo_logement_residences_secondaires.png")
        except RuntimeError as e:
            raise e
    return logement.data.logements.__dict__
