from datacirco.modules_data.mobilites_data import GetMobilites


def export_mobilites(db, data):
    """Génère le fichier json concernant les mobilites.

    Args:
        db (_type_): Objet de connexion
        data: Données
    """
    mobilites = GetMobilites(db, data)
    mobilites.get_irve()
    mobilites.get_carburants()
    mobilites.get_vehicules()
    return mobilites.data.mobilites.__dict__
