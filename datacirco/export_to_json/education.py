from datacirco.modules_data.education_data import GetEtablissements
from datacirco import utilitaires
from datacirco.map import tile_provider_piano
import staticmaps
import os
import sys


def get_script_path():
    return os.path.dirname(os.path.realpath(sys.argv[0]))


picto_dir = os.path.join(get_script_path(), "template/picto_png")


def get_size_from_effectifs(effectifs: int):
    if effectifs < 100:
        size = 11
    elif effectifs < 200:
        size = 16
    elif effectifs < 300:
        size = 21
    elif effectifs < 500:
        size = 26
    elif effectifs < 800:
        size = 31
    elif effectifs < 1000:
        size = 36
    else:
        size = 46
    return size


def get_color_from_effectifs(effectifs: int):
    """Create color from effectifs."""
    if effectifs is None:
        return "grey"
    try:
        effectifs = int(effectifs)
    except ValueError:
        return "grey"
    if effectifs < 100:
        color = "goldenrod"
    elif effectifs < 200:
        color = "orange"
    elif effectifs < 300:
        color = "red"
    elif effectifs < 500:
        color = "fushia"
    elif effectifs < 800:
        color = "violet"
    elif effectifs < 1000:
        color = "blue"
    else:
        color = "cyan"
    return color


def get_color_from_ips(ips: int):
    """Create color from ips."""
    if ips is None:
        return "grey"
    try:
        ips = int(ips)
    except ValueError:
        return "grey"
    if ips < 25:
        color = "grey"
    elif ips < 90:
        color = "goldenrod"
    elif ips < 100:
        color = "orange"
    elif ips < 110:
        color = "red"
    elif ips < 120:
        color = "fushia"
    elif ips < 130:
        color = "violet"
    elif ips < 140:
        color = "blue"
    else:
        color = "cyan"
    return color


def get_marker_ips(row):
    """Create marker from ips."""
    color = get_color_from_ips(row["ips"])
    if row["secteur_public_prive_libe"].lower() == "public":
        statut = "public"
    else:
        statut = "prive"
    png_file = statut + "-" + color + ".png"
    return staticmaps.ImageMarker(
        staticmaps.create_latlng(float(row["latitude"]), float(row["longitude"])),
        os.path.join(picto_dir, png_file),
        origin_x=11,
        origin_y=11,
    )


def get_marker_effectifs(row):
    """Create marker from effectifs."""
    color = get_color_from_effectifs(row["effectifs"])
    if row["secteur_public_prive_libe"].lower() == "public":
        statut = "public"
    else:
        statut = "prive"
    # size = get_size_from_effectifs(row["effectifs"])
    png_file = statut + "-" + color + ".png"
    return staticmaps.ImageMarker(
        staticmaps.create_latlng(float(row["latitude"]), float(row["longitude"])),
        os.path.join(picto_dir, png_file),
        origin_x=11,
        origin_y=11,
    )


def carte_effectifs(geo, etab_ps, etab_supp):
    """Carte des établissements avec une taille d'effectifs par type
    d'établissement."""

    liste_etab = [
        "ECOLE MATERNELLE",
        "ECOLE DE NIVEAU ELEMENTAIRE",
        "COLLEGE",
        "LYCEE",
        "autres",
        "superieur",
    ]

    for etab in liste_etab:
        type_etab = etab.replace(" ", "_").lower()
        carte = staticmaps.Context()
        carte.set_tile_provider(tile_provider_piano)
        utilitaires.draw_poly(carte, geo)
        if etab == "superieur":
            df_filtered = etab_supp
        elif etab == "autres":
            df_filtered = etab_ps[
                ~etab_ps["nature_uai_libe"].str.startswith(
                    (
                        "ECOLE MATERNELLE",
                        "ECOLE DE NIVEAU ELEMENTAIRE",
                        "COLLEGE",
                        "LYCEE",
                    )
                )
            ]
        else:
            df_filtered = etab_ps[etab_ps["nature_uai_libe"].str.startswith(etab)]
        if df_filtered is None or len(df_filtered) == 0:
            markers = []
        else:
            markers = df_filtered.apply(get_marker_effectifs, axis=1)

        for marker in markers:
            carte.add_object(marker)
        try:
            image = carte.render_cairo(450, 450)
            image.write_to_png(f"carte_education_effectifs_{type_etab}.png")
        except RuntimeError as e:
            raise e


def carte_ips(geo, etab_ps):
    """Carte des établissements avec IPS par établissement."""

    # Pas d'IPS pour le supérieur
    liste_etab = [
        "ECOLE MATERNELLE",
        "ECOLE DE NIVEAU ELEMENTAIRE",
        "COLLEGE",
        "LYCEE",
        "autres",
    ]

    for etab in liste_etab:
        type_etab = etab.replace(" ", "_").lower()
        carte = staticmaps.Context()
        carte.set_tile_provider(tile_provider_piano)
        utilitaires.draw_poly(carte, geo)
        if etab == "autres":
            # Remove "ECOLE MATERNELLE", "ECOLE DE NIVEAU ELEMENTAIRE", "COLLEGE", "LYCEE"
            df_filtered = etab_ps[
                ~etab_ps["nature_uai_libe"].str.startswith(
                    (
                        "ECOLE MATERNELLE",
                        "ECOLE DE NIVEAU ELEMENTAIRE",
                        "COLLEGE",
                        "LYCEE",
                    )
                )
            ]
        else:
            df_filtered = etab_ps[etab_ps["nature_uai_libe"].str.startswith(etab)]
        if df_filtered is None or len(df_filtered) == 0:
            markers = []
        else:
            markers = df_filtered.apply(get_marker_ips, axis=1)
        for marker in markers:
            carte.add_object(marker)
        try:
            image = carte.render_cairo(450, 450)
            image.write_to_png(f"carte_education_ips_{type_etab}.png")
        except RuntimeError as e:
            raise e


def export_education(db, data, geo):
    """Génère le fichier json concernant l'éducation.

    Args:
        db (_type_): Objet de connexion
        data: Données
        geo: Geo
    """
    # Récupérer les données concernant l'éducation
    etablissements = GetEtablissements(db, data)
    etablissements.get_effectifs_france()
    etablissements.get_etablissements()

    etab_ps = etablissements.data.education.etablissements_premier_et_second_degre
    etab_supp = etablissements.data.education.etablissements_superieur
    carte_ips(geo, etab_ps)
    carte_effectifs(geo, etab_ps, etab_supp)

    return etablissements.data.education.__dict__
