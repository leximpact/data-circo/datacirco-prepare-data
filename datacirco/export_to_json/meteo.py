from datacirco.modules_data.meteo_data import GetMeteo


def export_meteo(db, data, engine):
    """Génère le fichier json concernant le climat.

    Args:
        db (_type_): Objet de connexion
        data: Données
        engine: Objet basé sur une URL
    """
    meteo_data = GetMeteo(db, data, engine)
    meteo_data.get_meteo()
    meteo_data.get_meteo_mens()
    if meteo_data.data.meteo.historique is not None:
        meteo_data.get_temperatures(meteo_data.data.meteo.historique)
        meteo_data.get_precipitation_cumul(meteo_data.data.meteo.historique)

        meteo_data.data.meteo.historique["annee"] = meteo_data.data.meteo.historique[
            "annee"
        ].dt.strftime("%Y-%m-%d")
        meteo_data.data.meteo.df_temperature["annee"] = (
            meteo_data.data.meteo.df_temperature["annee"].dt.strftime("%Y-%m-%d")
        )
        meteo_data.data.meteo.df_precipitation["annee"] = (
            meteo_data.data.meteo.df_precipitation["annee"].dt.strftime("%Y-%m-%d")
        )
        meteo_data.data.meteo.historique = meteo_data.data.meteo.historique.to_dict()
        meteo_data.data.meteo.df_temperature = (
            meteo_data.data.meteo.df_temperature.to_dict()
        )
        meteo_data.data.meteo.df_precipitation = (
            meteo_data.data.meteo.df_precipitation.to_dict()
        )
        meteo_data.data.meteo.historique_temp_mens = (
            meteo_data.data.historique_temp_mens
        )
        meteo_data.data.meteo.historique_precip_mens = (
            meteo_data.data.historique_precip_mens
        )
        return meteo_data.data.meteo.__dict__
    return None
