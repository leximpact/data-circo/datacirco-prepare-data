from datacirco.modules_data.entreprises_data import GetEntreprises
from datacirco import utilitaires
from datacirco.map import tile_provider_forte
import staticmaps


def export_entreprises(db, data, geo):
    """Génère le fichier json concernant les entreprises.

    Args:
        db (_type_): Objet de connexion
        data: Données
        geo: Geo
    """
    # Récupérer les données concernant les entreprises
    entreprises = GetEntreprises(db, data)
    entreprises.get_entreprises()
    entreprises.get_entreprises_sieges()
    entreprises.get_etablissements()
    entreprises.get_employeurs_circo()
    entreprises.get_radiations()
    entreprises.get_immat_rad_mens()

    carte = staticmaps.Context()
    carte.set_tile_provider(tile_provider_forte)
    utilitaires.draw_poly(carte, geo)

    for employeur in entreprises.data.entreprises.employeurs:
        if employeur["latitude"] != "":
            carte.add_object(
                staticmaps.Marker(
                    staticmaps.create_latlng(
                        float(employeur["latitude"]), float(employeur["longitude"])
                    ),
                    color=staticmaps.BLACK,
                    size=6,
                )
            )
    try:
        image = carte.render_cairo(900, 800)
        image.write_to_png("carte_employeurs.png")
    except RuntimeError as e:
        raise e

    return entreprises.data.entreprises.__dict__
