from datacirco.modules_data.revenu_data import GetRevenu


def export_revenu(db, data):
    """Génère le fichier json concernant les revenus.

    Args:
        db (_type_): Objet de connexion
        data: Données
    """
    revenu = GetRevenu(db, data)
    revenu.get_carreaux_revenu()
    revenu.get_revenu_agg()
    revenu.get_decomposition_revenu()
    revenu.get_pauvrete_age()
    revenu.get_dict_deciles()
    return revenu.data.revenu.__dict__
