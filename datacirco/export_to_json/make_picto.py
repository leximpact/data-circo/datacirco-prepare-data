"""Ce script permet de générer les différentes variantes de couleur des
pictogrammes.

Il utilise cairosvg pour convertir les svg en png. Pour l'utiliser, il
faut exécuter la commande suivante: poetry run python
datacirco/export_to_json/make_picto.py
"""

import glob
import os
import cairosvg
from pathlib import Path

colors = {
    "cyan": "#00E0D7",
    "blue": "#1132FC",
    "violet": "#7800F1",
    "fushia": "#D40EC3",
    "red": "#C50140",
    "orange": "#FF553E",
    "goldenrod": "#FFAB01",
    "grey": "#686868",
}

if __name__ == "__main__":
    source_dir = os.path.abspath("template/picto_svg")
    temp_dir = os.path.join(source_dir, "tmp")
    destination_dir = os.path.abspath("template/picto_png")
    # Create subfolder tmp
    if not os.path.exists(temp_dir):
        os.makedirs(temp_dir)
    # Empty tmp folder
    for file in glob.glob(temp_dir + "/*"):
        os.remove(file)
    # Empty destination_dir
    [f.unlink() for f in Path(destination_dir).glob("*.png") if f.is_file()]
    # Iterate over all svg in template/picto_svg with glob
    for file in glob.glob(source_dir + "/*.svg"):
        filepath = os.path.abspath(file)
        path_only = os.path.dirname(filepath)
        filename = os.path.basename(file)
        filename_only = os.path.splitext(filename)[0]

        for color_name, color in colors.items():
            dest_tmp = os.path.join(temp_dir, filename_only + "-" + color_name + ".svg")
            dest_png = os.path.join(
                destination_dir, filename_only + "-" + color_name + ".png"
            )
            print(f"file: {file} to {dest_png} for color {color}")

            # read input file
            with open(file, "rt") as fin:
                # read file contents to string
                data = fin.read()
                # replace all occurrences of the required string
                data = data.replace('fill="#FFB800"', f'fill="{color}"')
                data = data.replace('fill="#FF0000"', f'fill="{color}"')
            with open(dest_tmp, "wt") as fout:
                # overrite the input file with the resulting data
                fout.write(data)

            # Create png from svg
            cairosvg.svg2png(
                url=dest_tmp, write_to=dest_png, output_width=22, output_height=22
            )

            # destination.replace("svg", 'png')
