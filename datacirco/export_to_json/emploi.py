from datacirco.modules_data.emploi_data import GetEmploi
from datacirco import utilitaires
from datacirco.map import tile_provider_piano
import staticmaps
import json
import math


def export_emploi(db, data, in_communes, superficie_circo, geo):
    """Génère le fichier json concernant l'emploi.

    Args:
        db (_type_): Objet de connexion
        data: Données
        in_communes: Communes
        superficie_circo: Superficie de la circonscription
        geo: Geo
    """
    emploi = GetEmploi(db, data)
    emploi.get_secteurs(in_communes)
    emploi.get_salaries(in_communes)
    emploi.get_emplois(in_communes)

    if len(emploi.data.emploi.emplois) > 0:
        carte = staticmaps.Context()
        carte.set_tile_provider(tile_provider_piano)
        echelle = math.sqrt(200000.0 / superficie_circo)
        for emp in emploi.data.emploi.emplois:
            json.loads(emp["geojson"])
            if emp["evolution"] > 0:
                color = staticmaps.parse_color("#0000ff40")  # bleu
            else:
                color = staticmaps.parse_color("#ff000080")  # rouge foncé
            # utilitaires.draw_poly(carte, [geom], color, 0)
            carte.add_object(
                staticmaps.Circle(
                    staticmaps.create_latlng(emp["latitude"], emp["longitude"]),
                    math.sqrt(abs(emp["evolution"]) * 4 / 3.14) / echelle,
                    fill_color=color,
                    color=color,
                    width=0,
                )
            )
        # contour de la circo
        utilitaires.draw_poly(carte, geo)
        try:
            image = carte.render_cairo(900, 800)
            image.write_to_png("circo_evolution_emplois.png")
        except RuntimeError as e:
            raise e

    return emploi.data.emploi.__dict__
