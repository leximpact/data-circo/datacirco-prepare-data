from datacirco.modules_data.pop_data import GetPopulation
from datacirco.map import Carto


def export_population(db, data, dept):
    """Génère le fichier json concernant le climat.

    Args:
        db (_type_): Objet de connexion
        data: Données
        dept: Département
    """
    map = Carto(data)
    pop = GetPopulation(db, data)
    if dept < "97" or dept in ("972", "974"):
        pop._set_pop_2013_2019_metro()
    else:
        pop.set_pop_DROM_COM()

    if dept < "97" or dept in ("972", "974"):
        pop.set_evolution()
        map.evolution_population()
        pop.set_densite_2015()
        pop.set_densite_2018()
        pop.set_densite(annee="2020")
        map.densite_population()
        pop.set_stat_menages()
    else:
        pop.set_densite_2018()
        pop.set_densite(annee="2020")
        if dept != "976":
            map.densite_population()

    return pop.data.population.__dict__
