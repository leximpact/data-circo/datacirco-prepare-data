from datacirco.modules_data.elections_data import GetElections


def export_elections(db, data):
    """Génère le fichier json concernant les élections.

    Args:
        db (_type_): Objet de connexion
        data: Données
    """
    # Récupérer les résultats des élections
    elections_data = GetElections(db, data)
    elections_data.get_elus()

    return elections_data.data.elections.__dict__
