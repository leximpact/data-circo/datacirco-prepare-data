import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate, make_msgid
from os import getenv
from dotenv import load_dotenv
import logging
import traceback


load_dotenv(dotenv_path=".env")
HOST = getenv("SMTP_SERVER")
PORT = 465  # For SSL
SMTP_USER = getenv("SMTP_USER")
PASSWD = getenv("SMTP_PASSWD")
MAIL_REPLY_TO = getenv("MAIL_REPLY_TO")


def send_mail(
    recipient_email,
    subject,
    content_text="Pas de mode texte.",
    content_html="<h3>Mail from LexImpact!</h3><br />",
    sender_address=MAIL_REPLY_TO,
    sender_name="LexImpact",
):
    """Send an email with the given parameters.

    Arg:
        recipient_email: str Email address of the recipient.
        subject: str Subject of the email.
        content_text: str Content of the email in text format.
        content_html: str Content of the email in html format.
        sender_address: str Email address for Reply-to.
        sender_name: str Name of the sender.
    """
    try:
        with smtplib.SMTP_SSL(host=HOST, port=PORT, timeout=5) as server:
            server.login(SMTP_USER, PASSWD)

            message = MIMEMultipart("alternative")
            message["Subject"] = subject
            # To be accepted by the recipient server, the sender email must use
            # the same domain as sender SMTP.
            # That's why we use the SMTP_USER email address.
            message["From"] = sender_name + " <" + SMTP_USER + ">"
            message["To"] = recipient_email
            # We use "Reply-to" as response address.
            message["Reply-to"] = sender_name + " <" + sender_address + ">"
            # "Date" and "Message-ID" are mandatory to avoid spam filters.
            message["Date"] = formatdate(localtime=True)
            message["Message-ID"] = make_msgid()

            # We add the content in text and html format.
            part1 = MIMEText(content_text, "plain")
            part2 = MIMEText(content_html, "html")

            # Add HTML/plain-text parts to MIMEMultipart message
            # The email client will try to render the last part first
            message.attach(part1)
            message.attach(part2)

            # Send the email with a BCC to the sender.
            result = server.sendmail(
                message["From"], [recipient_email, SMTP_USER], message.as_string()
            )
            logging.info(
                f'Message-ID {message["Message-ID"]} - From:{message["From"]} To {message["To"]} Reply-to:{message["Reply-to"]} : {message["Subject"]} \n result {result}'
            )
    except Exception as e:
        logging.error(f"Error in sending email to {recipient_email} : {e}")
        logging.error(traceback.print_exc())
        return False
