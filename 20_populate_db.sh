#!/bin/bash

# Fail on first error
set -e

echo "Starting script at `date`"


for i in revenus environnement education equipements logements emplois sante entreprises population geographie elus meteo
do
    echo "Chargement des données en base pour $i"
    bash ci/populate_db.sh $i
    echo "################################"
done

echo "Done script at `date`"
