#!/bin/bash

echo "Starting initialization at `date`"
# Stop at first error
set -e

# Get the full command line, assuming the password part is always first if it exists
FULL_COMMAND=$(poetry run python datacirco/connexion_db.py | tail -1)
# Extract the password value if it exists
PGPASSWORD=$(echo $FULL_COMMAND | grep -oP 'PGPASSWORD=\K[^ ]+' || true;)

# Conditionally export PGPASSWORD if it is not empty
if [ -n "$PGPASSWORD" ]; then
    export PGPASSWORD
fi

# Remove the PGPASSWORD part from the command to avoid execution error
PSQL_COMMAND_LINE=$(echo "$FULL_COMMAND" | sed 's/PGPASSWORD=[^ ]* //')
echo PSQL_COMMAND_LINE=$PSQL_COMMAND_LINE

# psql -d postgres -c "DROP DATABASE IF EXISTS $DB;"
# createdb $DB
$PSQL_COMMAND_LINE -c 'CREATE EXTENSION IF NOT EXISTS postgis;'
$PSQL_COMMAND_LINE -c 'CREATE EXTENSION IF NOT EXISTS tablefunc;'  # For crosstab()
$PSQL_COMMAND_LINE -c 'CREATE EXTENSION IF NOT EXISTS "unaccent";'

# Ask for confirmation
echo "We are going to DELETE all data on disk and in database !"
read -p "Are you sure? (YyOo)" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[YyOo]$ ]]
then
RACINE=`pwd`
DATA=$RACINE/data
rm -rf $DATA/*
GEO=$DATA/geographie

mkdir -p $GEO
cd $GEO

# Code officiel géographique au 1er janvier 2022 : https://www.insee.fr/fr/statistiques/fichier/6051727/cog_ensemble_2022_csv.zip

## découpage administratif
# -nc, --no-clobber: skip downloads that would download to existing files.
wget --no-clobber https://data.geopf.fr/telechargement/download/ADMIN-EXPRESS/ADMIN-EXPRESS_3-2__SHP_WGS84G_FRA_2024-01-22/ADMIN-EXPRESS_3-2__SHP_WGS84G_FRA_2024-01-22.7z


## découpages administrativs - ADMIN-EXPRESS (IGN)
7z x ADMIN-EXPRESS_3-2__SHP_WGS84G_FRA_2024-01-22.7z

for ADMIN in COMMUNE DEPARTEMENT REGION CANTON EPCI CHFLIEU_COMMUNE ARRONDISSEMENT_MUNICIPAL
do
  PG_USE_COPY=1 ogr2ogr -f pgdump /vsistdout/ ADMIN-EXPRESS_3-2__SHP_WGS84G_FRA_2024-01-22/ADMIN-EXPRESS/1_DONNEES_LIVRAISON_2024-01-00184/ADE_3-2_SHP_WGS84G_FRA-ED2024-01-22/$ADMIN.shp \
  -t_srs EPSG:4326 -nln ign_$ADMIN -nlt geometry | $PSQL_COMMAND_LINE
done

fi
echo "End with success at `date`"
