#! /usr/bin/python3

import smtplib
import sys
import time
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import psycopg2

if len(sys.argv) < 4:
    print("Usage: emailing.py smtphost user password")
    exit(1)

port = 465  # For SSL
port = 587

sender_email = "leximpact@assemblee-nationale.fr"

with smtplib.SMTP(sys.argv[1], port) as server:
    pg = psycopg2.connect("dbname=infocirco")
    db = pg.cursor()
    db.execute("SELECT * FROM an_mailing WHERE dep > '974' ORDER BY 1,2")
    recipients = db.fetchall()

    for recipient in recipients:
        recipient_email = recipient[2]
        circo = recipient[0] + "-" + recipient[1]
        if recipient[3] == "M.":
            civ = "Monsieur le député,"
        else:
            civ = "Madame la députée,"

        message = MIMEMultipart("alternative")
        message["Subject"] = "Première version de DataCirco disponible en PDF"
        message["From"] = "DataCirco-LexImpact <" + sender_email + ">"
        message["To"] = recipient_email

        # Create the plain-text and HTML version of your message
        text = """\
    %s

Le service LexImpact, de la direction du Contrôle et de l'évaluation de l'Assemblée nationale, a le plaisir de vous informer de la disponibilité de la première version de DataCirco !

Ce service agrège par circonscription des données publiques et ouvertes (opendata), et les restitue sous forme de PDF composés de cartes et de tableaux.

Ces documents sont accessibles publiquement en ligne via une carte navigable, à l'adresse https://leximpact.an.fr/datacirco/

Faute de données publiques suffisantes, le document PDF DataCirco n'est pas disponible pour votre circonscription. Certaines circonscriptions d'outre-mer et toutes celles des français de l'étranger sont malheureusement dans cette situation.

Cordialement,

--
Christian pour LexImpact
https://leximpact.an.fr/
    """ % (
            civ,
        )

        html = """\
<html>
<body>
    %s<p>

    Le service LexImpact, de la direction du Contrôle et de l'évaluation de l'Assemblée nationale, a le plaisir de vous informer de la disponibilité de la première version de DataCirco !<p>

    Ce service agrège par circonscription des données publiques et ouvertes (opendata), et les restitue sous forme de PDF composés de cartes et de tableaux.<p>

    Ces documents sont accessibles publiquement en ligne via une carte navigable :<p>

    <a href="https://leximpact.an.fr/datacirco/?s=m%s">
        <img width="90%%" style="border:1px solid black;" src="https://leximpact.an.fr/datacirco/circo/datacirco.png?s=m%s"><br>
    </a><br>
    https://leximpact.an.fr/datacirco/<p>

    Faute de données publiques suffisantes, le document PDF DataCirco n'est pas disponible pour votre circonscription. Certaines circonscriptions d'outre-mer et toutes celles des français de l'étranger sont malheureusement dans cette situation.<p>

    Cordialement,<p>

    <hr>
    Christian pour LexImpact<br>
    <a href="https://leximpact.an.fr/">https://leximpact.an.fr/</a><br>
    <hr>
    </p>
</body>
</html>
    """ % (
            civ,
            circo,
            circo,
        )

        # Turn these into plain/html MIMEText objects
        part1 = MIMEText(text, "plain")
        part2 = MIMEText(html, "html")

        # Add HTML/plain-text parts to MIMEMultipart message
        # The email client will try to render the last part first
        message.attach(part1)
        message.attach(part2)

        server.sendmail(sender_email, recipient_email, message.as_string())
        print(circo, recipient_email)
        time.sleep(1)
