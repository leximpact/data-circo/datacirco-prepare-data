# Datacirco-prepare-data

By [Benoît Courty](mailto:benoit.courty@assemblee-nationale.fr),
By Nehmat Hatoum,
By Christian Quest,

Copyright (C) 2022, 2023 Assemblée nationale

https://git.leximpact.dev/leximpact/barometre-application-lois/echeancier/

> Datacirco-prepare-data LexImpact is free software; you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
>
> Datacirco-prepare-data LexImpact is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program. If not, see <http://www.gnu.org/licenses/>.
